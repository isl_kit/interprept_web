// gulpfile.js
var gulp = require("gulp"),
    glob = require("glob"),
    _ = require("lodash"),
    path = require("path"),
    del = require("del"),

    ngConstant = require('gulp-ng-constant'),
    ngGraph = require('gulp-angular-architecture-graph'),
    extend = require('gulp-extend'),
    gutil = require("gulp-util"),
    rename = require("gulp-rename"),
    jshint = require("gulp-jshint"),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    header = require("gulp-header"),
    stylish = require("jshint-stylish"),
    ngAnnotate = require("gulp-ng-annotate"),
    template = require("gulp-template"),
    less = require("gulp-less"),
    minifyCSS = require('gulp-minify-css'),
    wrap = require("gulp-wrap"),
    ngHtml2Js = require("gulp-ng-html2js"),
    minifyHtml = require("gulp-minify-html"),

    karma = require("karma").server,

    runSequence = require("run-sequence"),
    streamqueue = require("streamqueue"),

    config = require("./build.config.js"),
    pkg = require("./package.json"),

    finalCSSFile = pkg.name + "-" + pkg.version + ".css",
    finalJSFile = pkg.name + "-" + pkg.version + ".js",
    templateJSFile = "templates.html.js",
    configFile = "config.module.js",

    banner, bannerInner, bannerHtml;

require("date-utils");

bannerInner =
    " * " + pkg.name + " - v" + pkg.version + " - " + getCurrentDate() + "\n" +
    " *\n" +
    " * Copyright (c) " + getCurrentYear() + " Institut für Anthropomatik und Robotik, Bereich Waibel, Karlsruhe Institute of Technology\n" +
    " * Author: " + pkg.author + "\n" +
    " * License: " + pkg.license + "\n";

banner = "/**\n" + bannerInner + " */\n";

bannerHtml = "<!--\n" + bannerInner + " -->\n";

function getPath(x) {
    if (_.isArray(x)) {
        return x.map(function (p) {
            return path.join(config.static_dir, p);
        });
    }
    return path.join(config.static_dir, x);
}

/* Clean */

gulp.task("clean", function () {
    del.sync([
        getPath(config.build_dir),
        getPath(config.compile_dir),
    ], {force: true});
});

/* Karma */

gulp.task("test", function (callback) {
    karma.start({
        configFile: path.join(__dirname, getPath(config.build_dir), "karma.conf.js"),
        singleRun: true
    }, callback);
});

/* Build Subtasks */

gulp.task("build_config", function() {
    return gulp.src(getPath([config.config_files.common, config.config_files.local]))
        .pipe(extend("res.json"))
        .pipe(ngConstant({
          name: "app.config",
          deps: []
        }))
        .pipe(rename(configFile))
        .pipe(gulp.dest(getPath(config.build_dir + "/conf")));
});

gulp.task("build_vendor_js", function() {
    return gulp.src(getPath(config.vendor_files.js), { base: config.static_dir })
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_vendor_css", function() {
    return gulp.src(getPath(config.vendor_files.css), { base: config.static_dir })
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_vendor_fonts", function() {
    return gulp.src(getPath(config.vendor_files.fonts))
        .pipe(gulp.dest(getPath(config.build_dir + "/assets/fonts")));
});

gulp.task("build_js", function() {
    return gulp.src(getPath(config.app_files.js), { base: config.static_dir })
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter("fail"))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("create_graph", function() {
    return gulp.src(getPath(config.app_files.js), { base: config.static_dir })
        .pipe(ngGraph({
            dest: path.join(getPath(config.build_dir), "diagram")
        }))
});

gulp.task("build_html2js", function() {
    return gulp.src(getPath(config.app_files.tpl))
        .pipe(minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(ngHtml2Js({
            moduleName: "app.templates"
        }))
        .pipe(concat(templateJSFile))
        .pipe(gulp.dest(path.join(getPath(config.build_dir), "tpl")));
});

gulp.task("build_css", function() {
    return gulp.src(getPath(config.app_files.less))
        .pipe(less())
        .on("error", function (error) {
            gutil.log(gutil.colors.red(error.message));
        })
        .pipe(rename(finalCSSFile))
        .pipe(gulp.dest(path.join(getPath(config.build_dir), "assets", "css")));
});

gulp.task("build_fonts", function() {
    return gulp.src(getPath(config.app_files.fonts))
        .pipe(gulp.dest(getPath(config.build_dir) + "/assets/fonts"));
});

gulp.task("build_images", function() {
    return gulp.src(getPath(config.app_files.images))
        .pipe(gulp.dest(getPath(config.build_dir) + "/assets/images"));
});

gulp.task("build_karma", function() {
    var karmaFiles = _.union(config.vendor_files.js, config.test_files.js.map(function (fileName) {
        return "../../../interprept/static/" + fileName;
    }));

    return gulp.src("karma.conf.js")
        .pipe(template({
            karmaFiles: karmaFiles
        }))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_test", function() {
    return gulp.src(getPath(config.app_files.jsunit), { base: config.static_dir })
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter("fail"))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_index", function() {
    var jsFiles = getBuildJSFiles().map(function (js_path) {
            return path.relative(getPath(config.build_dir), js_path);
        }),
        cssFiles = getBuildCSSFiles().map(function (css_path) {
            return path.relative(getPath(config.build_dir), css_path);
        });

    return gulp.src("interprept/static/app/index.html")
        .pipe(template({
            cssFiles: cssFiles,
            jsFiles: jsFiles,
            strict: false
        }))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

/* Compile Subtasks */

gulp.task("compile_css", function() {
    var files = _.union(getPath(config.vendor_files.css), [getPath(config.build_dir) + "/**/*.css"]);
    return gulp.src(files)
        .pipe(concat(finalCSSFile))
        .pipe(minifyCSS())
        .pipe(header(banner))
        .pipe(gulp.dest(getPath(config.compile_dir) + "/assets"));
});

gulp.task("compile_js", function(){
    return streamqueue({ objectMode: true },
        gulp.src(getPath(config.vendor_files.js)),
        gulp.src(getPath([config.config_files.common, config.config_files.production]))
        .pipe(extend("result.json"))
        .pipe(ngConstant({
          name: "app.config",
          deps: []
        }))
        .pipe(header(banner)),
        gulp.src(getPath(config.build_dir) + "/tpl/**/*.js"),
        gulp.src([getPath(config.build_dir) + "/app/**/*.module.js", getPath(config.build_dir) + "/app/**/*.js"])
        .pipe(wrap({ src: "module.template"}))
        .pipe(ngAnnotate()))
    .pipe(concat(finalJSFile))
    .pipe(uglify({
        preserveComments: "license"
    }))
    .pipe(gulp.dest(getPath(config.compile_dir) + "/assets"))
});

gulp.task("compile_fonts", function() {
    return gulp.src(getPath(config.build_dir) + "/assets/fonts/*")
        .pipe(gulp.dest(getPath(config.compile_dir) + "/fonts"));
});

gulp.task("compile_images", function() {
    return gulp.src(getPath(config.build_dir) + "/assets/images/*")
        .pipe(gulp.dest(getPath(config.compile_dir) + "/assets/images"));
});


gulp.task("compile_index", function() {
    var jsFiles = ["assets/" + finalJSFile],
        cssFiles = ["assets/" + finalCSSFile];

    return gulp.src("interprept/static/app/index.html")
        .pipe(template({
            cssFiles: cssFiles,
            jsFiles: jsFiles,
            strict: true
        }))
        .pipe(header(bannerHtml))
        .pipe(gulp.dest(getPath(config.compile_dir)));
});

/* Main Tasks */

gulp.task("build", function (callback) {
    runSequence(
        "clean",
        ["build_vendor_fonts", "build_vendor_js", "build_vendor_css", "build_config", "build_js", "build_html2js", "build_css", "build_images", "build_test", "build_karma"],
        "build_index",
        "test",
    callback);
});

gulp.task("compile", function (callback) {
    runSequence(
        "build",
        ["compile_js", "compile_css", "compile_fonts", "compile_images"],
        "compile_index",
    callback);
});

gulp.task("default", ["compile"], function () {});


/* Helper functions */

function getBuildJSFiles() {
    return _.union.apply(this, _.map(_.union(config.vendor_files.js, ["conf/" + configFile], ["tpl/" + templateJSFile], config.app_files.js), function (g) {
        return glob.sync(getPath(config.build_dir) + "/" + g);
    }));
}

function getBuildCSSFiles() {
    return _.union.apply(this, _.map(_.union(config.vendor_files.css, [path.join("assets", "css", finalCSSFile)]), function (g) {
        return glob.sync(getPath(config.build_dir) + "/" + g);
    }));
}

function getCurrentYear() {
    return Date.today().toFormat("YYYY");
}

function getCurrentDate() {
    return Date.today().toFormat("DD/MM/YYYY");
}

var url = require("url"),
    _ = require("underscore"),
    path = require("path"),
    fs = require("fs"),
    http = require("http"),
    https = require("https"),
    querystring = require("querystring"),
    express = require("express"),
    app = express(),
    server, io,
    protocol,
    auth_data = null,
    i, max_i, line,
    ca, chain, cert,
    socketio_url, socketio_https, socketio_port, url_parts,
    cert_file, key_file, chain_file;

require('dotenv').config({path: path.join(path.dirname(path.dirname(__filename)), ".env")});

function get_env_var(name) {
    var result = process.env[name];
    if (result === undefined) {
        throw "Environment variable " + name + " must be set!";
    }
    return result;
}

web_url = get_env_var("WEB_URL");
socketio_url = get_env_var("SOCKETIO_INT_URL");
console.log(socketio_url);
url_parts = socketio_url.split(":");
socketio_https = url_parts[0] === "https";
socketio_port = parseInt(url_parts[2], 10);

if (socketio_https) {
    cert_file = get_env_var("SSL_CERT_FILE");
    key_file = get_env_var("SSL_KEY_FILE");
    server = https.createServer({
        key: fs.readFileSync(key_file),
        cert: fs.readFileSync(cert_file)
    }, app);
    protocol = https;
} else {
    server = http.createServer(app);
    protocol = http;
}



io = require("socket.io")(server);

server.listen(socketio_port);

app.use(express.bodyParser());

function getRoom(appId, room) {
    return room.length > 0 ? appId + "!!" + room : appId;
}

app.post("/joined/", function (req, res) {
    var body = req.body,
        socket = io.sockets.connected[body.sessionid],
        room = getRoom(body.appid, body.room);

    if (socket) {
        socket.join(room);
    }
    res.send("everything is ok");
});

app.post("/left/", function (req, res) {
    var body = req.body,
        socket = io.sockets.connected[body.sessionid],
        room = getRoom(body.appid, body.room);

    if (socket) {
        socket.leave(room);
    }
    res.send("everything is ok");
});

app.post("/send/", function (req, res) {
    var body = req.body,
        socket = io.sockets.connected[body.sessionid],
        room = getRoom(body.appid, ""),
        message = body.message;

    if (socket && _.contains(socket.rooms, room)) {
        socket.emit("message", message);
    }
    res.send("everything is ok");
});

app.post("/send_to/", function (req, res) {
    var body = req.body,
        socket = io.sockets.connected[body.sessionid],
        room = getRoom(body.appid, body.room),
        message = body.message;

    if (socket && _.contains(socket.rooms, room)) {
        socket.emit("message_" + body.room, message);
    }
    res.send("everything is ok");
});

app.post("/emit/", function (req, res) {
    var body = req.body,
        room = getRoom(body.appid, ""),
        message = body.message;
    io.to(room).emit("message", message);
    res.send("everything is ok");
});

app.post("/emit_to/", function (req, res) {
    var body = req.body,
        room = getRoom(body.appid, body.room),
        message = body.message;

    io.to(room).emit("message_" + body.room, message);
    res.send("everything is ok");
});


function forward(type, data, sessionId, host, port, secure) {
    var options, req, protocol;

    data.sessionid = sessionId;
    data = JSON.stringify(data);
    protocol = secure ? https : http;


    options = {
        rejectUnauthorized: false,
        host: host,
        port: port,
        path: "/socket/" + type + "/",
        method: "POST",
        agent: false,
        headers: {
            "Content-Type": "application/json",
            "Content-Length": data.length
        }
    };

    //Send message to Django server

    try {
        req = protocol.request(options, function(res){
            res.setEncoding("utf8");
        });
        req.write(data);
        req.end();
    } catch (e) {

    }
}


io.sockets.on("connection", function (socket) {
    var sessionId = socket.id,
        parsedUrl = url.parse(web_url),
        secure = parsedUrl.protocol.indexOf("https") === 0;

    forward("connect", {}, sessionId, parsedUrl.hostname, parsedUrl.port, secure);
    socket.on("disconnect", function () {
        forward("disconnect", {}, sessionId, parsedUrl.hostname, parsedUrl.port, secure);
    });
});

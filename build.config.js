var path = require("path");

module.exports = {

    static_dir: "interprept/static",
    build_dir: "build",
    compile_dir: "bin",

    config_files: {
        common: "app/config/common.json",
        local: "app/config/local.json",
        production: "app/config/production.json"
    },

    app_files: {
        js: [ "app/**/*.module.js", "app/**/*.js" ],

        jsunit: [ "test/**/*.js" ],

        tpl: [ "app/**/*.tpl.html" ],
        html: [ "app/index.html" ],

        less: "assets/css/main.less",

        fonts: [],

        images: [ "assets/images/*" ]
    },

    test_files: {
        js: [
            "vendor/angular-mocks/angular-mocks.js"
        ]
    },

    vendor_files: {

        js: [
            "vendor/underscore/underscore-min.js",
            "vendor/jquery/jquery.js",
            "vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js",
            "vendor/blueimp-file-upload/js/jquery.fileupload.js",
            "vendor/blueimp-file-upload/js/jquery.iframe-transport.js",
            "vendor/angular/angular.js",
            "vendor/angular-animate/angular-animate.js",
            "vendor/angular-cookies/angular-cookies.js",
            "vendor/angular-sanitize/angular-sanitize.js",
            "vendor/angular-touch/angular-touch.js",
            "vendor/angular-ui-router/release/angular-ui-router.js",
            "vendor/angular-loading-bar/src/loading-bar.js"
        ],
        fonts: [
            "vendor/fontawesome/fonts/FontAwesome.otf",
            "vendor/fontawesome/fonts/fontawesome-webfont.eot",
            "vendor/fontawesome/fonts/fontawesome-webfont.svg",
            "vendor/fontawesome/fonts/fontawesome-webfont.ttf",
            "vendor/fontawesome/fonts/fontawesome-webfont.woff"
        ],
        css: [
            "vendor/angular-loading-bar/src/loading-bar.css"
        ]
    },
};

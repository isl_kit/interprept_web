Newsletter: WebDAV

Newsletter: WebDAV
------------------

The Interpreter Support Tool now supports file upload via a seperate WebDAV server. This is especially useful, if you don't have access to a web browser or if your device has only limited support for direct file upload to a website (i.e. iPad).

The WebDAV server can be accessed via the following URL:

    https://www.interpreter-support.eu/webdav
    
Your credentials for the WebDAV server are the same as for the Interpreter Support Tool website (email + password).

To enable your WebDAV account, the only thing you need to do, is perform a fresh login on our website. After that, your WebDAV directory will be created (which can take up to one minute) and you will be provided with additional information on how to make use of the WebDAV server.

You can access the WebDAV server from within your browser (no file upload) or use an WebDAV client to connect to it and upload files to your WebDAV file system. Once you have uploaded files, you can then easily transfer them to your Interpreter Support Tool file space from within the tool itself.

This will enable you to upload files from devices with limited access to a proper file system. For instance, there are several WebDAV client apps for the iPad and even other apps, like GoodReader, support the file transfer to WebDAV servers.

If you have questions about or comments on this feature, please let us know your thoughts via the feedback function, or simply answer directly to this email.

Best regards,
Bastian Krüger

from __future__ import absolute_import

import os
import json
from time import sleep
from thread import start_new_thread, allocate_lock
from hashlib import sha256
from celery import Celery
from django.conf import settings
from celery.task.control import revoke


celery = Celery('interprept')
celery.config_from_object('django.conf:settings')


class Runner():

    def __init__(self):
        self.tasks = {}
        self.revoked_ids = []
        self.lock = allocate_lock()
        start_new_thread(self.__loop__, ())

    def __loop__(self):
        while True:
            sleep(2)
            self.lock.acquire()
            ready_tasks = []
            started_tasks = []
            for tid, task in self.tasks.iteritems():
                if task["res"].ready():
                    ready_tasks.append(task)
                    if task["running"] is False:
                        started_tasks.append(task)
                elif task["running"] is False and task["res"].status == "STARTED":
                    task["running"] = True
                    started_tasks.append(task)

            #ready_tasks = [task for tid, task in self.tasks.iteritems() if task["res"].ready()]
            self.lock.release()
            for task in started_tasks:
                res = task["res"]
                meta = task["meta"]
                callback = task["started_callback"]
                if callback:
                    callback(res.id, meta)
            for task in ready_tasks:
                tid = task["tid"]
                res = task["res"]
                meta = task["meta"]
                callback = task["callback"]

                if tid in self.revoked_ids:
                    self.revoked_ids.remove(tid)
                    continue

                if callback:
                    if res.status == "SUCCESS":
                        callback(res.id, res.result, meta, None)
                    else:
                        callback(res.id, None, meta, res.result)
                del self.tasks[tid]


    def is_task(self, tid):
        return tid in self.tasks

    def start(self, name, args=[], meta={}, callback=None, queue="celery", scb=None):
        res = celery.send_task(name, args=args, queue=queue)
        self.lock.acquire()
        self.tasks[res.id] = {
            "tid": res.id,
            "res": res,
            "running": False,
            "meta": meta,
            "callback": callback,
            "started_callback": scb,
            "revoked": False
        }
        self.lock.release()
        return res.id

    def stop(self, tid):
        if tid in self.tasks:
            self.tasks[tid]["revoked"] = True
        elif tid not in self.revoked_ids:
            self.revoked_ids.append(tid)
        return revoke(tid)


runner = Runner()



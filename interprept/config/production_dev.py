#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Production Configurations
'''

from __future__ import absolute_import, unicode_literals
from .production import *

DEBUG = True

MEDIA_ROOT = str(BASE_DIR.path('media'))

STATICFILES_DIRS = (
    str(BASE_DIR.path('static/build')),
)

TEMPLATES[0]['DIRS'] = [str(BASE_DIR.path('static/build'))]
LOG_DIR = str(BASE_DIR.path('logs'))

QUEUE_DEFAULT = "celery"
QUEUE_USERDOCUMENTS = "celery"
QUEUE_UPDATE = "celery"

WEBDAV_MAP_FILE = str(BASE_DIR.path('webdav/webdav.map'))
WEBDAV_PASSWORD_FILE_PROD = str(BASE_DIR.path('webdav/webdav.passwords.prod'))
WEBDAV_PASSWORD_FILE_TEMP = str(BASE_DIR.path('webdav/webdav.passwords.tmp'))
WEBDAV_PASSWORD_FILE = str(BASE_DIR.path('webdav/webdav.passwords'))
WEBDAV_USER_HOME = str(BASE_DIR.path('webdav/files'))

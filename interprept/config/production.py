#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Production Configurations
'''

from __future__ import absolute_import, unicode_literals
from .common import *

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Raises ImproperlyConfigured exception if DJANGO_SECRET_KEY not in os.environ
SECRET_KEY = env("DJANGO_SECRET_KEY")

# This ensures that Django will be able to detect a secure connection
# properly on Heroku.
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# django-secure
# ------------------------------------------------------------------------------

if env.bool("SECURE", default=True):
    INSTALLED_APPS += ("djangosecure", )
    SECURITY_MIDDLEWARE = (
        'djangosecure.middleware.SecurityMiddleware',
    )

    # Make sure djangosecure.middleware.SecurityMiddleware is listed first
    MIDDLEWARE_CLASSES = SECURITY_MIDDLEWARE + MIDDLEWARE_CLASSES

    # set this to 60 seconds and then to 518400 when you can prove it works
    SECURE_HSTS_SECONDS = 60
    SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
        "DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS", default=True)
    SECURE_FRAME_DENY = env.bool("DJANGO_SECURE_FRAME_DENY", default=True)
    SECURE_CONTENT_TYPE_NOSNIFF = env.bool(
        "DJANGO_SECURE_CONTENT_TYPE_NOSNIFF", default=True)
    SECURE_BROWSER_XSS_FILTER = True
    SESSION_COOKIE_SECURE = False
    SESSION_COOKIE_HTTPONLY = True
    SECURE_SSL_REDIRECT = env.bool("DJANGO_SECURE_SSL_REDIRECT", default=True)


# SITE CONFIGURATION
# ------------------------------------------------------------------------------
# Hosts/domain names that are valid for this site
# See https://docs.djangoproject.com/en/1.6/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=[])

# STATIC FILES
# ------------------------------------------------------------------------------
MEDIA_ROOT = str(ROOT_DIR.path('media'))
# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(BASE_DIR.path('static/bin')),
)

# EMAIL
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
SERVER_EMAIL = env("DJANGO_SERVER_EMAIL")
EMAIL_HOST = env("DJANGO_EMAIL_HOST")
EMAIL_PORT = env.int("DJANGO_EMAIL_PORT")
EMAIL_HOST_USER = env("DJANGO_EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = env("DJANGO_EMAIL_HOST_PASSWORD")
EMAIL_USE_TLS = env.bool("DJANGO_EMAIL_USE_TLS")

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES[0]['DIRS'] = [str(ROOT_DIR.path('dist'))]
TEMPLATES[0]['OPTIONS']['loaders'] = [
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader', 'django.template.loaders.app_directories.Loader', ]),
]

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
DATABASES['default'] = env.db("DATABASE_URL")

# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}


# CUSTOM CONFIGURATION
# ------------------------------------------------------------------------------

# CELERY

BROKER_URL = env('BROKER_URL')
VERSION_FILE = env('VERSION_FILE')
CELERY_DISABLE_RATE_LIMITS = True
CELERYD_MAX_TASKS_PER_CHILD = 20
QUEUE_USERDOCUMENTS = "high"
QUEUE_UPDATE = "high"

# WEBDAV

WEBDAV_MAP_FILE = str(ROOT_DIR.path('webdav.map'))
WEBDAV_PASSWORD_FILE_PROD = str(ROOT_DIR.path('webdav/webdav.password.prod'))
WEBDAV_PASSWORD_FILE_TEMP = str(ROOT_DIR.path('webdav/webdav.password.tmp'))
WEBDAV_PASSWORD_FILE = str(ROOT_DIR.path('webdav/webdav.password'))
WEBDAV_USER_HOME = str(ROOT_DIR.path('webdav/files'))

# JSLOG
LOG_DIR = str(ROOT_DIR.path('logs'))

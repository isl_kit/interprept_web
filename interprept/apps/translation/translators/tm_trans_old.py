#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import re
import time
from os import walk as walk_dir
from os.path import join as join_path
from hashlib import sha256

from django.conf import settings
from django.db import transaction

from apps.languages.models import Language
from .translator import Translator
from ..models import Phrase, Translation, TMTranslation, TMDone


class Bench(object):

    def __init__(self):
        self.tasks = {}

    def start(self, task_name):
        if task_name not in self.tasks:
            self.tasks[task_name] = {
                "start": None,
                "total": 0
            }

        if self.tasks[task_name]["start"] is None:
            self.tasks[task_name]["start"] = time.time()

    def stop(self, task_name):
        if task_name not in self.tasks:
            return

        if self.tasks[task_name]["start"] is None:
            return

        self.tasks[task_name]["total"] += time.time() - self.tasks[task_name]["start"]
        self.tasks[task_name]["start"] = None

    def print_out(self):
        for task_name, task in self.tasks.iteritems():
            print task_name + ": " + str(task["total"])

bench = Bench()

class TMTranslator(Translator):

    TAG_COUNT = "Infinity"
    PHRASE_COUNT = "Infinity"

    TRANSLATOR_ID = 3

    ALLOW_REQUEST = False
    ALLOW_FETCH = True

    def __init__(self, *args, **kwargs):
        super(TMTranslator, self).__init__(*args, **kwargs)
        self.lang_cache = {}

    def get_source_infos(self):
        return {
            "id": self.TRANSLATOR_ID,
            "name": "tm",
            "label": "Translation Memory",
            "url": "http://google.com"
        }

    def is_valid_tm_file(self, file_name):
        return file_name.endswith("tr.single")

    def get_language_pair(self, file_name):
        return ["en", "de"]

    def get_fdr_no(self, file_name):
        m = re.search("(^|[^0-9])([0-9]{6,7})($|[^0-9])", file_name)
        if m is not None:
            return m.group(2)
        return None

    def get_tm_params(self, line):
        required_number_of_params = 7
        sp = line.split(" ||| ")
        param_length = len(sp)
        if param_length > required_number_of_params:
            return sp[:required_number_of_params]
        if param_length < required_number_of_params:
            for i in range(required_number_of_params - param_length):
                sp.append("")
        return sp

    def create_hash(self, file_path):
        sha256_hash = sha256()
        with open(file_path, "r") as f:
            for chunk in iter(lambda: f.read(128 * sha256_hash.block_size), b''):
                sha256_hash.update(chunk)
        return sha256_hash.hexdigest()

    def handle_tm_line(self, line, language_pair, fdr_no):
        source_tag = language_pair[0]
        target_tag = language_pair[1]
        try:
            source_lang = self.lang_cache[source_tag]
            target_lang = self.lang_cache[target_tag]
        except KeyError:
            return
        phrase, translated_phrase, o_context, orig_indices, t_context, trans_indices, alignment = self.get_tm_params(line)
        o_start, o_length = [int(x) for x in orig_indices.split(" ")]
        t_start, t_length = [int(x) for x in trans_indices.split(" ")]

        new_phrase = False
        new_translation = False

        try:
            ph = Phrase.objects.get(name=phrase.lower(), language=source_lang)
        except Phrase.DoesNotExist:
            new_phrase = True
            ph = Phrase(name=phrase.lower(), language=source_lang)
            ph.save()

        if new_phrase is False:
            try:
                translation = Translation.objects.get(language=target_lang, phrase=ph, lower_name=translated_phrase.lower())
            except Translation.DoesNotExist:
                translation = None
        else:
            translation = None

        if translation:
            translator_ids = json.loads(translation.translator_ids)
            if self.TRANSLATOR_ID not in translator_ids:
                translator_ids.append(self.TRANSLATOR_ID)
                translation.translator_ids = json.dumps(translator_ids)
                translation.save()
        else:
            new_translation = True
            translator_ids = json.dumps([self.TRANSLATOR_ID])
            translation = Translation(language=target_lang, phrase=ph, name=translated_phrase, translator_ids=translator_ids)
            translation.save()
  
        if new_translation is False:
            try:
                tm_translation = TMTranslation.objects.get(translation=translation, fdr_no=fdr_no, o_context__iexact=o_context, t_context__iexact=t_context)
            except TMTranslation.DoesNotExist:
                tm_translation = None
        else:
            tm_translation = None

        if tm_translation:
            tm_translation.o_index = o_start
            tm_translation.o_length = o_length
            tm_translation.t_index = t_start
            tm_translation.t_length = t_length
        else:
            tm_translation = TMTranslation(translation=translation, fdr_no=fdr_no, o_context=o_context, o_index=o_start, o_length=o_length, t_context=t_context, t_index=t_start, t_length=t_length)
        tm_translation.save()

    def print_status(self, i, message):
        print "(" + str(i) + ") " + message

    def update_lang_cache(self):
        langs = Language.objects.all()
        for lang in langs:
            self.lang_cache[lang.tag] = lang

    def is_hash_already_present(self, fhash):
        try: 
            tmdone = TMDone.objects.get(pk=fhash)
            return True
        except TMDone.DoesNotExist:
            return False

    def insert_hash(self, fhash):
        tmdone = TMDone(pk=fhash)
        tmdone.save()


    def fetch(self):
        self.lang_cache = {}


        path = settings.TM_DIR
 
        number_of_files = 0
        file_objs = []

        print "Analizing..."

        for root, dirs, files in walk_dir(path):
            files = [f for f in files if self.is_valid_tm_file(f)]
            for file_name in files:
                number_of_files += 1
                file_path = join_path(root, file_name)
                fhash = self.create_hash(file_path)
                if self.is_hash_already_present(fhash):
                    self.print_status(number_of_files, "Skipped (already processed)")
                    continue
                language_pair = self.get_language_pair(file_name)
                fdr_no = self.get_fdr_no(file_name)
                if fdr_no is None:
                    self.print_status(number_of_files, "Skipped (no FDR No found)")
                    continue
                self.print_status(number_of_files, "Qeued")
                file_objs.append({
                    "path": file_path,
                    "hash": fhash,
                    "langs": language_pair,
                    "fdr": fdr_no
                })

        print "Processing " + str(len(file_objs)) + "/" + str(number_of_files) + " Files..."

        for idx, file_obj in enumerate(file_objs):
            print str(idx) + "/" + str(len(file_objs))
            file_path = file_obj["path"]
            language_pair = file_obj["langs"]
            fdr_no = file_obj["fdr"]
            fhash = file_obj["hash"]
            self.update_lang_cache()
            with transaction.atomic():
                with open(file_path, "r") as f:
                    for i, line in enumerate(f):
                        self.handle_tm_line(line, language_pair, fdr_no)
                self.insert_hash(fhash)
                #bench.print_out()



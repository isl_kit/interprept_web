#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import partial

class CombTranslator:

    def __init__(self, translators):
        self.translators = translators
        self.async_counter = 0
        self.async_processes = {}

    def get_key(self, d, key):
        lkey = key.lower()
        for k in d:
            if k.lower() == lkey:
                return k
        return None

    def translate(self, source_tag, target_tags, phrases):

        translators = [t for t in self.translators if t.ALLOW_REQUEST]

        res = {}
        result = {
            "translator_ids": [x.TRANSLATOR_ID for x in translators],
            "res": res
        }
        for phrase in phrases:
            res[phrase.lower()] = {}
            for tag in target_tags:
                res[phrase.lower()][tag] = {}

        for translator in translators:
            part_res = translator.translate(source_tag, target_tags, phrases)
            for phrase, tag_res in part_res.iteritems():
                for tag, trans in tag_res.iteritems():
                    if trans is None:
                        res[phrase.lower()][tag]["T:I:D" + str(translator.TRANSLATOR_ID)] = None
                    else:
                        for translation in trans:
                            key = self.get_key(res[phrase.lower()][tag], translation)
                            if key is None:
                                res[phrase.lower()][tag][translation] = []
                                key = translation
                            if translator.TRANSLATOR_ID not in res[phrase.lower()][tag][key]:
                                res[phrase.lower()][tag][key].append(translator.TRANSLATOR_ID)


        return result

    def handle_async_result(self, aid, translator_id, part_res):
        proc = self.async_processes[aid]
        proc["ids"].remove(translator_id)
        result = proc["result"]
        for phrase, tag_res in part_res.iteritems():
            for tag, trans in tag_res.iteritems():
                if trans is None:
                    result["res"][phrase.lower()][tag]["T:I:D" + str(translator_id)] = None
                else:
                    for translation in trans:
                        key = self.get_key(result["res"][phrase.lower()][tag], translation)
                        if key is None:
                            result["res"][phrase.lower()][tag][translation] = []
                            key = translation
                        if translator_id not in result["res"][phrase.lower()][tag][key]:
                            result["res"][phrase.lower()][tag][key].append(translator_id)

        if len(proc["ids"]) == 0:
            proc["callback"](result)

    def translate_async(self, source_tag, target_tags, phrases, callback):

        translators = [t for t in self.translators if t.ALLOW_REQUEST]

        res = {}
        result = {
            "translator_ids": [x.TRANSLATOR_ID for x in translators],
            "res": res
        }

        aid = "a" + str(self.async_counter)
        self.async_counter += 1

        self.async_processes[aid] = {
            "result": result,
            "callback": callback,
            "ids": [x.TRANSLATOR_ID for x in translators]
        }

        for phrase in phrases:
            res[phrase.lower()] = {}
            for tag in target_tags:
                res[phrase.lower()][tag] = {}


        if len(phrases) == 0 or len(target_tags) == 0 or len(translators) == 0:
            callback(result)
        else:
            for translator in translators:
                part_res = translator.translate_async(source_tag, target_tags, phrases, partial(self.handle_async_result, aid, translator.TRANSLATOR_ID))

    def fetch(self):
        translators = [t for t in self.translators if t.ALLOW_FETCH]
        for translator in translators:
            translator.fetch()

    def submit(self, source_tag, target_tag, phrase, translation):
        result = []
        for translator in self.translators:
            success = translator.submit(source_tag, target_tag, phrase, translation)
            if success:
                result.append(translator.TRANSLATOR_ID)
        return result
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from time import sleep
from thread import start_new_thread


class TranslateQueue(object):

    def __init__(self, request_func, interval):
        self.counter = 0
        self.tasks = []
        self.running = False
        self.request_func = request_func
        self.interval = interval

    def __handle_task__(self, id, task, callback):
        tmp_res = self.request_func(task["source_tag"], task["tag_param"], task["phrase_param"])
        callback(id, task, tmp_res)

    def __loop__(self):
        if self.running:
            return

        self.running = True
        while len(self.tasks) > 0:
            sleep(self.interval)
            self.__handle_task__(self.tasks[0]["id"], self.tasks[0]["task"], self.tasks[0]["callback"])
            self.tasks.pop(0)
        self.running = False

    def push(self, task, callback):
        self.counter += 1
        self.tasks.append({
            "id": self.counter,
            "task": task,
            "callback": callback
        })
        if not self.running:
            start_new_thread(self.__loop__, ())
        return self.counter


class Translator(object):

    TAG_COUNT = 1
    PHRASE_COUNT = 1

    TRANSLATOR_ID = -1

    TIME_INTERVAL = 1

    ALLOW_REQUEST = True
    ALLOW_FETCH = False

    def __init__(self):
        self.queue = TranslateQueue(self.request, self.TIME_INTERVAL)
        self.async_processes = {}
        self.async_counter = 0

    def icontains(self, arr, text):
        return text.lower() in [t.lower() for t in arr]

    def get_source_infos(self):
        return {}

    def request(self, source_tag, target_tags, phrases):
        return []

    def split_arr(self, arr, length):
        if length == "Infinity":
            length = len(arr)
        res = []
        while len(arr) > 0:
            res.append(arr[0:length])
            arr = arr[length:]
        return res

    def get_split_phrases(self, phrases):
        return self.split_arr(phrases, self.PHRASE_COUNT)

    def get_split_tags(self, tags):
        return self.split_arr(tags, self.TAG_COUNT)


    def mix_results(self, result, tmp_res, tag_param, phrase_param):
        if self.TAG_COUNT == 1:
            if self.PHRASE_COUNT == 1:
                result[phrase_param.lower()][tag_param] = tmp_res
            else:
                for phrase, trans in tmp_res.iteritems():
                    result[phrase.lower()][tag_param] = trans
        elif self.PHRASE_COUNT == 1:
            for tag, trans in tmp_res.iteritems():
                result[phrase_param.lower()][tag] = trans
        else:
            for phrase, tag_obj in tmp_res.iteritems():
                for tag, trans in tag_obj.iteritems():
                    result[phrase.lower()][tag] = trans

    def translate(self, source_tag, target_tags, phrases):
        result = {}
        for phrase in phrases:
            result[phrase.lower()] = {}
            for tag in target_tags:
                result[phrase.lower()][tag] = []
        split_phrases = self.get_split_phrases(phrases)
        split_tags = self.get_split_tags(target_tags)

        if len(split_phrases) == 0 or len(split_tags) == 0:
            return result

        for phrases in split_phrases:
            for tags in split_tags:
                tag_param = tags[0] if self.TAG_COUNT == 1 else tags
                phrase_param = phrases[0] if self.PHRASE_COUNT == 1 else phrases
                tmp_res = self.request(source_tag, tag_param, phrase_param)
                self.mix_results(result, tmp_res, tag_param, phrase_param)
        return result

    def handle_async_result(self, id, task, res):
        proc = self.async_processes[task["aid"]]
        self.mix_results(proc["result"], res, task["tag_param"], task["phrase_param"])
        proc["ids"].remove(id)
        if len(proc["ids"]) == 0:
            del self.async_processes[task["aid"]]
            proc["callback"](proc["result"])

    def submit(self, source_tag, target_tag, phrase, translation):
        return False

    def translate_async(self, source_tag, target_tags, phrases, callback):
        ids = []
        result = {}
        for phrase in phrases:
            result[phrase.lower()] = {}
            for tag in target_tags:
                result[phrase.lower()][tag] = []

        split_phrases = self.get_split_phrases(phrases)
        split_tags = self.get_split_tags(target_tags)

        if len(split_phrases) == 0 or len(split_tags) == 0:
            callback(result)
            return

        aid = "a" + str(self.async_counter)
        self.async_counter += 1
        self.async_processes[aid] = {
            "result": result,
            "ids": ids,
            "callback": callback
        }

        for phrases in split_phrases:
            for tags in split_tags:
                tag_param = tags[0] if self.TAG_COUNT == 1 else tags
                phrase_param = phrases[0] if self.PHRASE_COUNT == 1 else phrases
                id = self.queue.push({
                    "aid": aid,
                    "source_tag": source_tag,
                    "tag_param": tag_param,
                    "phrase_param": phrase_param
                }, self.handle_async_result)
                ids.append(id)


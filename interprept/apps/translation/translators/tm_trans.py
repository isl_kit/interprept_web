#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import re
import time
import string
from os import walk as walk_dir
from os.path import join as join_path
from hashlib import sha256
from datetime import datetime, timedelta

from django.conf import settings
from django.db import transaction

from apps.languages.models import Language
from .translator import Translator
from ..models import BasePhrase, Phrase, Translation, TMTranslation, TMDone
from ..db_service import get_or_create_translation, get_or_create_translation_two_phrase, get_or_create_phrase, create_base_form

class Bench(object):

    def __init__(self):
        self.tasks = {}

    def start(self, task_name):
        if task_name not in self.tasks:
            self.tasks[task_name] = {
                "start": None,
                "total": 0
            }

        if self.tasks[task_name]["start"] is None:
            self.tasks[task_name]["start"] = time.time()

    def stop(self, task_name):
        if task_name not in self.tasks:
            return

        if self.tasks[task_name]["start"] is None:
            return

        self.tasks[task_name]["total"] += time.time() - self.tasks[task_name]["start"]
        self.tasks[task_name]["start"] = None

    def print_out(self):
        for task_name, task in self.tasks.iteritems():
            print task_name + ": " + str(task["total"])

bench = Bench()

class TMTranslator(Translator):

    TAG_COUNT = "Infinity"
    PHRASE_COUNT = "Infinity"

    TRANSLATOR_ID = 3

    ALLOW_REQUEST = False
    ALLOW_FETCH = True

    def __init__(self, *args, **kwargs):
        super(TMTranslator, self).__init__(*args, **kwargs)
        self.lang_cache = {}

    def get_source_infos(self):
        return {
            "id": self.TRANSLATOR_ID,
            "name": "tm",
            "label": "Translation Memory",
            "url": "http://google.com"
        }

    def is_valid_tm_file(self, file_name):
        return file_name.endswith("tr.single")

    def get_language_pair(self, file_name):
        return ["en", "de"]

    def get_fdr_no(self, file_name):
        m = re.search("(^|[^0-9])([0-9]{6,7})($|[^0-9])", file_name)
        if m is not None:
            return m.group(2)
        return None

    def get_tm_params(self, line):
        required_number_of_params = 7
        sp = line.split(" ||| ")
        param_length = len(sp)
        if param_length > required_number_of_params:
            return sp[:required_number_of_params]
        if param_length < required_number_of_params:
            for i in range(required_number_of_params - param_length):
                sp.append("")
        return sp

    def create_hash(self, file_path):
        sha256_hash = sha256()
        with open(file_path, "r") as f:
            for chunk in iter(lambda: f.read(128 * sha256_hash.block_size), b''):
                sha256_hash.update(chunk)
        return sha256_hash.hexdigest()

    def validate_phrase(self, text):
        for p in string.punctuation:
            if text.startswith(p) or text.endswith(p):
                return False
        return True

    def conv_num(self, text):
        m = re.match("^[0-9]+", text)
        if m is None:
            return -1
        return int(m.group(0))

    def handle_lines(self, lines, language_pair, fdr_no):
        source_tag = language_pair[0]
        target_tag = language_pair[1]

        bases = {
            source_tag: {},
            target_tag: {}
        }
        phrases = {
            source_tag: {},
            target_tag: {}
        }
        transes = {}
        tms = {}
        try:
            source_lang = self.lang_cache[source_tag]
            target_lang = self.lang_cache[target_tag]
        except KeyError:
            return


        for line in lines:

            phrase, translated_phrase, o_context, orig_indices, t_context, trans_indices, alignment = self.get_tm_params(line)

            val1 = self.validate_phrase(phrase)
            val2 = self.validate_phrase(translated_phrase)

            if val1 is False or val2 is False:
                continue

            try:
                o_start, o_length = [self.conv_num(x) for x in orig_indices.split(" ")]
                t_start, t_length = [self.conv_num(x) for x in trans_indices.split(" ")]
            except:
                continue

            if o_start < 0 or o_length < 0 or t_start < 0 or t_length < 0 or o_start > 10000 or o_length > 10000 or t_start > 10000 or t_length > 10000:
                continue

            if source_tag < target_tag:
                o_context, o_start, o_length, t_context, t_start, t_length = t_context, t_start, t_length, o_context, o_start, o_length

            base_form1 = create_base_form(phrase, source_tag)
            base_form2 = create_base_form(translated_phrase, target_tag)

            if base_form1 in bases[source_tag]:
                base1_obj = bases[source_tag][base_form1]
            else:
                base1 = None
                try:
                    base1 = BasePhrase.objects.get(name=base_form1, language=source_lang)
                except BasePhrase.DoesNotExist:
                    pass

                base1_obj = {
                    "id": base1.id if base1 is not None else None,
                    "name": base_form1,
                    "language_id": source_lang.id
                }
                bases[source_tag][base_form1] = base1_obj

            if phrase.lower() in phrases[source_tag]:
                phrase1_obj = phrases[source_tag][phrase.lower()]
            else:
                phrase1 = None
                if base1_obj["id"] is not None:
                    try:
                        phrase1 = Phrase.objects.get(base_id=base1_obj["id"], name=phrase.lower())
                    except Phrase.DoesNotExist:
                        pass

                phrase1_obj = {
                    "id": phrase1.id if phrase1 is not None else None,
                    "name": phrase.lower(),
                    "base": base1_obj
                }
                phrases[source_tag][phrase.lower()] = phrase1_obj

            if base_form2 in bases[target_tag]:
                base2_obj = bases[target_tag][base_form2]
            else:
                base2 = None
                try:
                    base2 = BasePhrase.objects.get(name=base_form2, language=target_lang)
                except BasePhrase.DoesNotExist:
                    pass

                base2_obj = {
                    "id": base2.id if base2 is not None else None,
                    "name": base_form2,
                    "language_id": target_lang.id
                }
                bases[target_tag][base_form2] = base2_obj

            if translated_phrase.lower() in phrases[target_tag]:
                phrase2_obj = phrases[target_tag][translated_phrase.lower()]
            else:
                phrase2 = None
                if base2_obj["id"] is not None:
                    try:
                        phrase2 = Phrase.objects.get(base_id=base2_obj["id"], name=translated_phrase.lower())
                    except Phrase.DoesNotExist:
                        pass

                phrase2_obj = {
                    "id": phrase2.id if phrase2 is not None else None,
                    "name": translated_phrase.lower(),
                    "base": base2_obj
                }
                phrases[target_tag][translated_phrase.lower()] = phrase2_obj

            if source_tag > target_tag:
                phrase1_obj, phrase2_obj = phrase2_obj, phrase1_obj

            tid = str(phrase1_obj["base"]["language_id"]) + "_" +  phrase1_obj["name"] + "$$$" + phrase2_obj["name"] + "_" + str(phrase2_obj["base"]["language_id"])

            if tid in transes:
                trans_obj = transes[tid]
            else:
                translation = None
                if phrase2_obj["id"] is not None and phrase1_obj["id"] is not None:
                    try:
                        translation = Translation.objects.get(phrase1_id=phrase1_obj["id"], phrase2_id=phrase2_obj["id"])
                        translator_ids = json.loads(translation.translator_ids)
                        if self.TRANSLATOR_ID not in translator_ids:
                            translator_ids.append(self.TRANSLATOR_ID)
                            translation.translator_ids = json.dumps(translator_ids)
                            translation.save()
                    except Translation.DoesNotExist:
                        pass

                trans_obj = {
                    "id": translation.id if translation is not None else None,
                    "phrase1": phrase1_obj,
                    "phrase2": phrase2_obj,
                    "translator_ids": [self.TRANSLATOR_ID]
                }
                transes[tid] = trans_obj

            tmid = tid + "_" + fdr_no + "_" + o_context.lower() + "_" + t_context.lower()
            if tmid not in tms:
                tm_translation = None
                if trans_obj["id"] is not None:
                    try:
                        tm_translation = TMTranslation.objects.get(translation_id=trans_obj["id"], fdr_no=fdr_no, o_context__iexact=o_context, t_context__iexact=t_context)
                    except TMTranslation.DoesNotExist:
                        pass

                if tm_translation is None:
                    tm_obj = {
                        "translation": trans_obj,
                        "fdr_no": fdr_no,
                        "o_context": o_context,
                        "o_index": o_start,
                        "o_length": o_length,
                        "t_context": t_context,
                        "t_index": t_start,
                        "t_length": t_length
                    }
                    tms[tmid] = tm_obj

        for tag, t_obj in bases.iteritems():
            for name, base_obj in t_obj.iteritems():
                if base_obj["id"] is not None:
                    continue
                base = BasePhrase(name=base_obj["name"])
                base.language_id = base_obj["language_id"]
                base.save()
                base_obj["id"] = base.id

        for tag, t_obj in phrases.iteritems():
            for name, phrase_obj in t_obj.iteritems():
                if phrase_obj["id"] is not None:
                    continue
                phrase = Phrase(name=phrase_obj["name"])
                phrase.base_id = phrase_obj["base"]["id"]
                phrase.save()
                phrase_obj["id"] = phrase.id

        for tid, trans_obj in transes.iteritems():
            if trans_obj["id"] is not None:
                continue
            translation = Translation(translator_ids=trans_obj["translator_ids"])
            translation.phrase1_id = trans_obj["phrase1"]["id"]
            translation.phrase2_id = trans_obj["phrase2"]["id"]
            translation.save()
            trans_obj["id"] = translation.id

        for tmid, tm_obj in tms.iteritems():
            tm = TMTranslation(fdr_no=tm_obj["fdr_no"], o_context=tm_obj["o_context"], o_index=tm_obj["o_index"], o_length=tm_obj["o_length"], t_context=tm_obj["t_context"], t_index=tm_obj["t_index"], t_length=tm_obj["t_length"])
            tm.translation_id = tm_obj["translation"]["id"]
            tm.save()



    def handle_tm_line(self, line, language_pair, fdr_no):
        source_tag = language_pair[0]
        target_tag = language_pair[1]
        try:
            source_lang = self.lang_cache[source_tag]
            target_lang = self.lang_cache[target_tag]
        except KeyError:
            return
        phrase, translated_phrase, o_context, orig_indices, t_context, trans_indices, alignment = self.get_tm_params(line)

        val1 = self.validate_phrase(phrase)
        val2 = self.validate_phrase(translated_phrase)

        if val1 is False or val2 is False:
            return

        try:
            o_start, o_length = [self.conv_num(x) for x in orig_indices.split(" ")]
            t_start, t_length = [self.conv_num(x) for x in trans_indices.split(" ")]
        except:
            return

        if o_start < 0 or o_length < 0 or t_start < 0 or t_length < 0 or o_start > 10000 or o_length > 10000 or t_start > 10000 or t_length > 10000:
            return

        if source_tag < target_tag:
            o_context, o_start, o_length, t_context, t_start, t_length = t_context, t_start, t_length, o_context, o_start, o_length


        '''
        base_form1 = create_base_form(phrase, source_tag)
        base_form2 = create_base_form(translated_phrase, target_tag)

        created_base1 = False


        try:
            base1 = BasePhrase.objects.get(name=base_form1, language=source_lang)
        except BasePhrase.DoesNotExist:
            base1 = BasePhrase(name=base_form1, language=source_lang)
            base1.save()
            created_base1 = True

        created_phrase1 = created_base1
        if not created_base1:
            try:
                phrase1 = Phrase.objects.get(base=base1, name=phrase.lower())
            except Phrase.DoesNotExist:
                created_phrase1 = True

        if created_phrase1:
            phrase1 = Phrase(base=base1, name=phrase)
            try:
                phrase1.save()
            except:
                raise

        created_base2 = False
        try:
            base2 = BasePhrase.objects.get(name=base_form2, language=target_lang)
        except BasePhrase.DoesNotExist:
            base2 = BasePhrase(name=base_form2, language=target_lang)
            base2.save()
            created_base2 = True

        created_phrase2 = created_base2
        if not created_base2:
            try:
                phrase2 = Phrase.objects.get(base=base2, name=translated_phrase.lower())
            except Phrase.DoesNotExist:
                created_phrase2 = True

        if created_phrase2:
            phrase2 = Phrase(base=base2, name=translated_phrase)
            phrase2.save()


        created_trans = created_phrase1 or created_phrase2
        if source_tag > target_tag:
            phrase1, phrase2 = phrase2, phrase1
        if not created_trans:
            try:
                translation = Translation.objects.get(phrase1=phrase1, phrase2=phrase2)
                translator_ids = json.loads(translation.translator_ids)
                if self.TRANSLATOR_ID not in translator_ids:
                    translator_ids.append(self.TRANSLATOR_ID)
                    translation.translator_ids = json.dumps(translator_ids)
                    translation.save()
            except Translation.DoesNotExist:
                created_trans = True

        if created_trans:
            translation = Translation(phrase1=phrase1, phrase2=phrase2, translator_ids=[self.TRANSLATOR_ID])
            translation.save()

        if not created_trans:
            try:
                tm_translation = TMTranslation.objects.get(translation=translation, fdr_no=fdr_no, o_context__iexact=o_context, t_context__iexact=t_context)
            except TMTranslation.DoesNotExist:
                tm_translation = None
        else:
            tm_translation = None
        '''
        (source_phrase, _, _) = get_or_create_phrase(phrase, source_lang)
        (target_phrase, _, _) = get_or_create_phrase(translated_phrase, target_lang)
        (translation, created) = get_or_create_translation_two_phrase(source_phrase, target_phrase, translator_ids=[self.TRANSLATOR_ID])
        #(translation, created) = get_or_create_translation(phrase, source_lang, translated_phrase, target_lang, translator_ids=[self.TRANSLATOR_ID])
        if not created:
            translator_ids = json.loads(translation.translator_ids)
            if self.TRANSLATOR_ID not in translator_ids:
                translator_ids.append(self.TRANSLATOR_ID)
                translation.translator_ids = json.dumps(translator_ids)
                translation.save()
            try:
                tm_translation = TMTranslation.objects.get(translation=translation, fdr_no=fdr_no, o_context__iexact=o_context, t_context__iexact=t_context)
            except TMTranslation.DoesNotExist:
                tm_translation = None
        else:
            tm_translation = None

        if tm_translation:
            tm_translation.o_index = o_start
            tm_translation.o_length = o_length
            tm_translation.t_index = t_start
            tm_translation.t_length = t_length
        else:
            tm_translation = TMTranslation(translation=translation, fdr_no=fdr_no, o_context=o_context, o_index=o_start, o_length=o_length, t_context=t_context, t_index=t_start, t_length=t_length)
        tm_translation.save()

    def print_status(self, i, message):
        print "(" + str(i) + ") " + message

    def update_lang_cache(self):
        langs = Language.objects.all()
        for lang in langs:
            self.lang_cache[lang.tag] = lang

    def is_hash_already_present(self, fhash):
        try:
            tmdone = TMDone.objects.get(pk=fhash)
            return True
        except TMDone.DoesNotExist:
            return False

    def insert_hash(self, fhash):
        tmdone = TMDone(pk=fhash)
        tmdone.save()

    def file_len(self, fname):
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        return i + 1

    def fetch(self):
        self.lang_cache = {}

        paths = settings.TM_DIR
        tags = [l.tag for l in Language.objects.all()]

        for tag in tags:
            if tag not in paths:
                continue
            path = paths[tag]
            number_of_files = 0
            file_objs = []
            total_lines = 0
            done_lines = 0
            language_pair = ["en", tag]
            print "Analizing..."
            for root, dirs, files in walk_dir(path):
                files = [f for f in files if self.is_valid_tm_file(f)]
                for file_name in files:
                    number_of_files += 1
                    file_path = join_path(root, file_name)
                    fhash = self.create_hash(file_path)
                    if self.is_hash_already_present(fhash):
                        #self.print_status(number_of_files, "Skipped (already processed)")
                        continue
                    fdr_no = self.get_fdr_no(file_name)
                    if fdr_no is None:
                        #self.print_status(number_of_files, "Skipped (no FDR No found)")
                        continue
                    #self.print_status(number_of_files, "Qeued")
                    total_lines += self.file_len(file_path)
                    file_objs.append({
                        "path": file_path,
                        "hash": fhash,
                        "fdr": fdr_no
                    })

            print "(" + tag + ") Processing " + str(len(file_objs)) + "/" + str(number_of_files) + " Files... (" + str(total_lines) + " lines)"
            #start_time = datetime.now()
            for idx, file_obj in enumerate(file_objs):
                print str(idx) + "/" + str(len(file_objs))
                #if idx != 5:
                #    continue
                file_path = file_obj["path"]
                fdr_no = file_obj["fdr"]
                fhash = file_obj["hash"]
                self.update_lang_cache()

                with transaction.atomic():
                    lines = []
                    with open(file_path, "r") as f:
                        for i, line in enumerate(f):
                            lines.append(line)
                    self.handle_lines(lines, language_pair, fdr_no)

                    self.insert_hash(fhash)
                    done_lines += i
                    #current_time = datetime.now()
                    #elapsed = (current_time - start_time).total_seconds()
                    #lines_per_sec = done_lines / elapsed
                    #estimated_seconds = int(round((total_lines - done_lines) / lines_per_sec))
                    #delta = timedelta(seconds=estimated_seconds)
                    print "L: " + str(done_lines) + "/" + str(total_lines)
                    #raise Exception("blabla")



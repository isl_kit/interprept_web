#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from pyquery import PyQuery as pq

from .translator import Translator

source = {
    "id": 2,
    "name": "iate",
    "label": "iate",
    "url": "http://iate.europa.eu"
}


def get_tr_results(tr):
    result = []
    while True:
        text = unicode(tr.find("td").not_("[rowspan]").find("div.termRecord").text().strip())
        result.append(text)
        tr = tr.next()
        if not tr or len(tr) == 0 or len(tr.find("td[rowspan]")) > 0:
            break
    return result


def check_source_tr(tr, phrase):
    while True:
        text = tr.find("td").not_("[rowspan]").find("div.termRecord").text().strip().lower().encode("utf-8")
        if text == phrase.lower():
            return True
        tr = tr.next()
        if not tr or len(tr) == 0 or len(tr.find("td[rowspan]")) > 0:
            return False


class IateTranslator(Translator):

    TAG_COUNT = 25
    PHRASE_COUNT = 1

    TRANSLATOR_ID = 2
    TIME_INTERVAL = 1

    def get_source_infos(self):
        return {
            "id": self.TRANSLATOR_ID,
            "name": "iate",
            "label": "iate",
            "url": "http://iate.europa.eu"
        }

    def request(self, source_tag, target_tags, phrase):
        result = {}

        for tag in target_tags:
            result[tag] = []

        requestData = {
            'method': 'search',
            'sourceLanguage': source_tag,
            'domain': '0',
            'matching': '',
            'typeOfSearch': 's',
            'fromSearchResults': 'yes',
            'saveStats':  'true',
            'targetLanguages': target_tags,
            'query': phrase,
            'valid': 'Search'
        }

        r = requests.post('http://iate.europa.eu/SearchByQuery.do', data=requestData)

        if r.status_code != 200:
            print "STATUS CODE != 200!!!"
            for tag, arr in result.iteritems():
                result[tag] = None
            return result

        d = pq(r.text)
        search_res = d.find('div').attr('id', 'searchResultBody')

        if search_res:
            tables = search_res.eq(1).find('table')
            for table in tables:
                tab = pq(table)
                tds = tab.find("td[rowspan]")
                check = False
                for td in tds:
                    td = pq(td)
                    if td.text().lower() == source_tag:
                        tr = td.parent()
                        check = check_source_tr(tr, phrase)
                        if not check:
                            break
                    elif check:
                        lang = td.text().lower()
                        if lang in result:
                            tr = td.parent()
                            tr_res = get_tr_results(tr)
                            for res in tr_res:
                                if not self.icontains(result[lang], res):
                                    result[lang].append(res)

        return result

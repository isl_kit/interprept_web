#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json

from django.conf import settings

from .translator import Translator

class GlosbeTranslator(Translator):

    TAG_COUNT = 25
    PHRASE_COUNT = 200

    TRANSLATOR_ID = 1
    TIME_INTERVAL = 1

    def get_source_infos(self):
        return {
            "id": self.TRANSLATOR_ID,
            "name": "glosbe",
            "label": "Glosbe",
            "url": "http://glosbe.com"
        }

    def request1(self, source_tag, target_tag, phrases):
        result = {}
        lower_phrases = []
        for phrase in phrases:
            lphrase = phrase.lower()
            lower_phrases.append(lphrase)
            result[lphrase] = []

        params = {
            "from": source_tag,
            "dest": target_tag,
            "phrase": ";;".join(lower_phrases),
            "format": "json",
            "pretty": "false"
        }
        print "START REQ"
        r = requests.get("http://glosbe.com/gapi/fast-translate?" + "&".join([key + "=" + val for key, val in params.iteritems()]))
        print "END REQ"
        if r.status_code != 200:
            print "STATUS CODE != 200!!!"
            for key, arr in result.iteritems():
                result[key] = None
            return result
        res = json.loads(r.text)
        if "translations" in res:
            for phrase, tags in res["translations"].iteritems():
                if phrase.lower() not in result:
                    continue
                for tag, trans in tags.iteritems():
                    if tag != target_tag:
                        continue
                    for trans_obj in trans:
                        result[phrase.lower()].append(trans_obj["P"])

        return result

    def request(self, source_tag, target_tags, phrases):
        result = {}
        lower_phrases = []
        for phrase in phrases:
            lphrase = phrase.lower()
            lower_phrases.append(lphrase)
            result[lphrase] = {}
            for tag in target_tags:
                result[lphrase][tag] = []

        params = {
            "from": source_tag,
            "dest": ";;".join(target_tags),
            "phrase": ";;".join(lower_phrases),
            "format": "json",
            "pretty": "false"
        }
        r = requests.get("http://glosbe.com/gapi/fast-translate?" + "&".join([key + "=" + val for key, val in params.iteritems()]))
        if r.status_code != 200:
            print "STATUS CODE != 200!!!"
            for phrase, tag_obj in result.iteritems():
                for tag, arr in tag_obj.iteritems():
                    tag_obj[tag] = None
            return result

        res = json.loads(r.text)
        if "translations" in res:
            for phrase, tags in res["translations"].iteritems():
                if phrase.lower() not in result:
                    continue
                for tag, trans in tags.iteritems():
                    if tag not in target_tags:
                        continue
                    for trans_obj in trans:
                        p_res = trans_obj["P"]
                        if not self.icontains(result[phrase.lower()][tag], p_res):
                            result[phrase.lower()][tag].append(p_res)

        return result

    def submit(self, source_tag, target_tag, phrase, translation):

        if not settings.GLOSBE_USER_ID or not settings.GLOSBE_KEY:
            print "| SUBMIT TO GLOSBE | " + phrase + " --(" + source_tag + "|" + target_tag + ")--> " + translation + " |"
            return False


        requestData = {
            'phrase1': phrase,
            'phrase2': translation,
            'lang1': source_tag,
            'lang2': target_tag,
            'authorId': settings.GLOSBE_USER_ID,
            'authorApiKey': settings.GLOSBE_KEY
        }

        r = requests.post('https://glosbe.com/gapi/addTranslation', data=requestData)

        if r.status_code != 200:
            print "STATUS CODE != 200!!!"
            return False

        res = json.loads(r.text)
        return "result" in res and res["result"] == "ok"

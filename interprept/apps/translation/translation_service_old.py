#!/usr/bin/env python
# -*- coding: utf-8 -*-


import json
import traceback
from sys import stdout
from functools import partial

from django.db import transaction

from apps.languages.models import Language
from .models import Phrase, Translation
from .translators.iate_trans import IateTranslator
from .translators.glosbe_trans import GlosbeTranslator
from .translators.comb_trans import CombTranslator
from .translators.tm_trans import TMTranslator

translators_all = [
    IateTranslator,
    GlosbeTranslator,
    TMTranslator
]

translators = [
    IateTranslator,
    GlosbeTranslator,
    TMTranslator
]

register_all = {}
register = {}

for t in translators:
    register[t.TRANSLATOR_ID] = t()

for t in translators_all:
    register_all[t.TRANSLATOR_ID] = register[t.TRANSLATOR_ID] if t.TRANSLATOR_ID in register else t()

def handle_result(source_tag, result):
    langs = Language.objects.all()
    lang_dict = {}
    for lang in langs:
        lang_dict[lang.tag] = lang

    translator_ids = result["translator_ids"]
    result = result["res"]
    has_failed_requests = False

    source_lang = lang_dict[source_tag]

    for phrase, tag_res in result.iteritems():
        with transaction.atomic():
            try:
                ph = Phrase.objects.get(name=phrase, language=source_lang)
            except Phrase.DoesNotExist:
                ph = Phrase(name=phrase, language=source_lang)
                ph.save()

            finished = json.loads(ph.finished)

            for tag, trans_res in tag_res.iteritems():
                if tag not in finished:
                    finished[tag] = []
                language = lang_dict[tag]
                for translator_id in translator_ids:
                    if translator_id not in finished[tag]:
                        finished[tag].append(translator_id)

                for translation, trans_ids in trans_res.iteritems():
                    if translation.startswith("T:I:D"):
                        has_failed_requests = True
                        translator_id = int(translation[5:])
                        if translator_id in finished[tag]:
                            finished[tag].remove(translator_id)
                    else:
                        try:
                            trans = Translation.objects.get(language=language, phrase=ph, lower_name=translation.lower())
                            ids = json.loads(trans.translator_ids)
                            save_trans = False
                            for translator_id in trans_ids:
                                if translator_id not in ids:
                                    ids.append(translator_id)
                                    save_trans = True
                            if save_trans:
                                trans.translator_ids = json.dumps(ids)
                                trans.save()
                        except Translation.DoesNotExist:
                            ot = Translation(language=language, phrase=ph, name=translation, translator_ids=json.dumps(trans_ids))
                            ot.save()

            ph.finished = json.dumps(finished)
            ph.save()
    print "FINISHED " + str(len(result)) + " TRANSLATIONS (" + ",".join([str(x) for x in translator_ids]) + ")"
    if has_failed_requests:
        print "FAILED REQUESTS"
    return result

def handle_async_result(source_tag, callback, result):
    callback(handle_result(source_tag, result))

def handle_submit_result(translation, translator_ids):
    if len(translator_ids) == 0:
        return

    phrase = translation.phrase
    source_tag = translation.phrase.language.tag
    target_tag = translation.language.tag
    finished = json.loads(phrase.finished)
    ids = json.loads(translation.translator_ids)

    save_phrase = False
    save_translation = False
    if target_tag not in finished:
        finished[target_tag] = []
    for translator_id in translator_ids:
        if translator_id not in finished[target_tag]:
            finished[target_tag].append(translator_id)
            save_phrase = True
        if translator_id not in ids:
            ids.append(translator_id)
            save_translation = True
    if save_phrase:
        phrase.finished = json.dumps(finished)
        phrase.save()
    if save_translation:
        translation.translator_ids = json.dumps(ids)
        translation.save()

def __translate__(source_tag, target_tags, phrases, force, callback, translator_ids, async):

    translators = []

    for translator_id, translator in register.iteritems():
        if translator.ALLOW_REQUEST and (translator_ids is None or translator_id in translator_ids):
            translators.append(translator)

    try:
        source_language = Language.objects.get(tag=source_tag)
    except Language.DoesNotExist:
        if async:
            callback({})
        else:
            return {}

    if not force:
        tag_count = {}
        new_phrases = []
        new_tags = []
        new_translators = []
        keep_translators = False
        keep_tags = False
        for phrase in phrases:
            try:
                ph = Phrase.objects.get(name=phrase.lower(), language=source_language)
            except Phrase.DoesNotExist:
                keep_tags = True
                keep_translators = True
                new_phrases.append(phrase)
                continue
            can_exclude_phrase = True
            finished = json.loads(ph.finished)
            for tag in target_tags:
                if tag not in finished:
                    can_exclude_phrase = False
                    keep_translators = True
                    if tag not in new_tags:
                        new_tags.append(tag)
                    continue
                for translator in translators:
                    if translator.TRANSLATOR_ID not in finished[tag]:
                        can_exclude_phrase = False
                        if translator not in new_translators:
                            new_translators.append(translator)
                        if tag not in new_tags:
                            new_tags.append(tag)
            if not can_exclude_phrase:
                new_phrases.append(phrase)
        if not keep_translators:
            translators = new_translators
        if not keep_tags:
            target_tags = new_tags
        print "GET " + str(len(new_phrases))+ "/" + str(len(phrases)) + " TRANSLATIONS (" + ",".join([str(x.TRANSLATOR_ID) for x in translators]) + ")"
        phrases = new_phrases
    else:
        print "GET " + str(len(phrases)) + " TRANSLATIONS (" + ",".join([str(x.TRANSLATOR_ID) for x in translators]) + ")"

    comb_translator = CombTranslator(translators)


    if async:
        comb_translator.translate_async(source_tag, target_tags, phrases, partial(handle_async_result, source_tag, callback))
    else:
        result = comb_translator.translate(source_tag, target_tags, phrases)
        return handle_result(source_tag, result)

def translate_async(source_tag, target_tags, phrases, force, callback, translator_ids=None):
    __translate__(source_tag, target_tags, phrases, force, callback, translator_ids, True)

def translate(source_tag, target_tags, phrases, force, translator_ids=None):
    return __translate__(source_tag, target_tags, phrases, force, None, translator_ids, False)

def submit(translation, translator_ids=None):
    translators = []

    for translator_id, translator in register.iteritems():
        if translator_ids is None or translator_id in translator_ids:
            translators.append(translator)

    phrase = translation.phrase.name
    source_tag = translation.phrase.language.tag
    target_tag = translation.language.tag
    trans_text = translation.name

    comb_translator = CombTranslator(translators)
    translator_ids = comb_translator.submit(source_tag, target_tag, phrase, trans_text)
    handle_submit_result(translation, translator_ids)
    return translator_ids



def fetch(translator_ids=None):
    translators = []

    for translator_id, translator in register.iteritems():
        if translator.ALLOW_FETCH and (translator_ids is None or translator_id in translator_ids):
            translators.append(translator)

    comb_translator = CombTranslator(translators)
    comb_translator.fetch()



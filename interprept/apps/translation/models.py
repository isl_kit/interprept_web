#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.db import models
from django.conf import settings

from apps.languages.models import Language


'''
class OnlineTranslationSource(models.Model):
    id = models.IntegerField(primary_key=True)
    url = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    label = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = "translation"
'''


'''
class Phrase(models.Model):
    name = models.TextField()
    language = models.ForeignKey(Language)
    #checked_sources = models.ManyToManyField(OnlineTranslationSource)
    finished = models.TextField(default="{}")
    def __unicode__(self):
        return self.name + " (" + self.language.tag + ")"

    class Meta:
        app_label = "translation"
        #unique_together = ("name", "language")
        #index_together = [
        #    ["name", "language"]
        #]

class Translation(models.Model):
    name = models.TextField()
    lower_name = models.TextField()
    language = models.ForeignKey(Language)
    phrase = models.ForeignKey(Phrase)
    rating = models.IntegerField(default=0)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)
    #sources = models.ManyToManyField(OnlineTranslationSource)
    translator_ids = models.TextField(default="[]")

    def save(self, *args, **kwargs):
        self.lower_name = self.name.lower()
        return super(Translation, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.phrase.name + " (" + self.phrase.language.tag + " => " + self.language.tag + ") " + self.name

    class Meta:
        app_label = "translation"
        #unique_together = ("lower_name", "language", "phrase")
        #index_together = [
        #    ["lower_name", "language", "phrase"]
        #]

'''


class TMDone(models.Model):
    hash = models.CharField(max_length=64, primary_key=True)

    def __unicode__(self):
        return self.hash

    class Meta:
        app_label = "translation"


class BasePhrase(models.Model):
    name = models.TextField()
    language = models.ForeignKey(Language)

    def __unicode__(self):
        return self.name + " (" + self.language.tag + ")"

    class Meta:
        app_label = "translation"
        unique_together = ("name", "language")
        index_together = [
            ["name", "language"]
        ]


class Phrase(models.Model):
    name = models.TextField()
    display_name = models.TextField()
    base = models.ForeignKey(BasePhrase)
    casing = models.TextField(default="{\"__max__\":0}")
    finished = models.TextField(default="{}")

    def set_casing(self, name):
        casing = json.loads(self.casing)
        if name not in casing:
            casing[name] = 1
        else:
            casing[name] += 1
        if casing[name] > casing["__max__"]:
            casing["__max__"] = casing[name]
            self.display_name = name
        self.casing = json.dumps(casing)

    def save(self, *args, **kwargs):
        if not self.id:
            name = self.name
            self.name = name.lower()
            self.set_casing(name)
        return super(Phrase, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name + " (" + self.base.language.tag + ")"

    class Meta:
        app_label = "translation"
        unique_together = ("name", "base")
        index_together = [
            ["name", "base"]
        ]

class Translation(models.Model):
    phrase1 = models.ForeignKey(Phrase, related_name="translation_as_1")
    phrase2 = models.ForeignKey(Phrase, related_name="translation_as_2")
    rating = models.IntegerField(default=0)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)
    translator_ids = models.TextField(default="[]")

    def __unicode__(self):
        return self.phrase1.name + " (" + self.phrase1.base.language.tag + " <=> " + self.phrase2.base.language.tag + ") " + self.phrase2.name

    class Meta:
        app_label = "translation"
        unique_together = ("phrase1", "phrase2")
        index_together = [
            ["phrase1", "phrase2"]
        ]


class TMTranslation(models.Model):
    #translation = models.ForeignKey(Translation)
    translation = models.ForeignKey(Translation)
    fdr_no = models.CharField(max_length=10)
    o_context = models.TextField()
    o_index = models.IntegerField()
    o_length = models.IntegerField()
    t_context = models.TextField()
    t_index = models.IntegerField()
    t_length = models.IntegerField()

    def __unicode__(self):
        return self.fdr_no  + " " + unicode(self.translation)

    class Meta:
        app_label = "translation"

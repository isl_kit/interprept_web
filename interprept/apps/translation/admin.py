#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from django.contrib import admin
from .models import Phrase, Translation, BasePhrase, TMTranslation

class PhraseAdmin(admin.ModelAdmin):
    fields = ('name', 'display_name', "casing", "finished")
    raw_id_fields = ("base",)

class TranslationAdmin(admin.ModelAdmin):
    fields = ('rating', 'users', "translator_ids")


class BasePhraseAdmin(admin.ModelAdmin):
    readonly_fields=('id',)
    fields = ('name', 'language')

class TMTranslationAdmin(admin.ModelAdmin):
    fields = ("o_context", "o_index", "o_length", "t_context", "t_index", "t_length")

admin.site.register(Phrase, PhraseAdmin)
admin.site.register(BasePhrase, BasePhraseAdmin)
#admin.site.register(OnlineTranslationSource)
admin.site.register(Translation, TranslationAdmin)
admin.site.register(TMTranslation, TMTranslationAdmin)
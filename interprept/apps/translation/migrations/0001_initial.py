# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('languages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BasePhrase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('language', models.ForeignKey(to='languages.Language')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Phrase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('display_name', models.TextField()),
                ('casing', models.TextField(default=b'{"__max__":0}')),
                ('finished', models.TextField(default=b'{}')),
                ('base', models.ForeignKey(to='translation.BasePhrase')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TMDone',
            fields=[
                ('hash', models.CharField(max_length=64, serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TMTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fdr_no', models.CharField(max_length=10)),
                ('o_context', models.TextField()),
                ('o_index', models.IntegerField()),
                ('o_length', models.IntegerField()),
                ('t_context', models.TextField()),
                ('t_index', models.IntegerField()),
                ('t_length', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Translation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField(default=0)),
                ('translator_ids', models.TextField(default=b'[]')),
                ('phrase1', models.ForeignKey(related_name='translation_as_1', to='translation.Phrase')),
                ('phrase2', models.ForeignKey(related_name='translation_as_2', to='translation.Phrase')),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='translation',
            unique_together=set([('phrase1', 'phrase2')]),
        ),
        migrations.AlterIndexTogether(
            name='translation',
            index_together=set([('phrase1', 'phrase2')]),
        ),
        migrations.AddField(
            model_name='tmtranslation',
            name='translation',
            field=models.ForeignKey(to='translation.Translation'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='phrase',
            unique_together=set([('name', 'base')]),
        ),
        migrations.AlterIndexTogether(
            name='phrase',
            index_together=set([('name', 'base')]),
        ),
        migrations.AlterUniqueTogether(
            name='basephrase',
            unique_together=set([('name', 'language')]),
        ),
        migrations.AlterIndexTogether(
            name='basephrase',
            index_together=set([('name', 'language')]),
        ),
    ]

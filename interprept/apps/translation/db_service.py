#!/usr/bin/env python
# -*- coding: utf-8 -*-

from nltk import stem
import re
import string
import json

from django.db.models import Prefetch

from apps.languages.models import Language
from .models import Phrase, BasePhrase, Translation

stemmer_hash = {}

def create_stemmer(tag):
    if tag == "en":
        return stem.SnowballStemmer("english")
    elif tag == "de":
        return stem.SnowballStemmer("german")
    elif tag == "fr":
        return stem.SnowballStemmer("french")
    elif tag == "nl":
        return stem.SnowballStemmer("dutch")
    elif tag == "da":
        return stem.SnowballStemmer("danish")
    elif tag == "fi":
        return stem.SnowballStemmer("finnish")
    elif tag == "hu":
        return stem.SnowballStemmer("hungarian")
    elif tag == "it":
        return stem.SnowballStemmer("italian")
    elif tag == "pt":
        return stem.SnowballStemmer("portuguese")
    elif tag == "ro":
        return stem.SnowballStemmer("romanian")
    elif tag == "es":
        return stem.SnowballStemmer("spanish")
    elif tag == "sv":
        return stem.SnowballStemmer("swedish")
    return None

def get_stemmer(tag):
    if tag in stemmer_hash:
        stemmer = stemmer_hash[tag]
    else:
        stemmer = create_stemmer(tag)
        stemmer_hash[tag] = stemmer
    return stemmer

def normalize(name):
    name = name.lower()
    name = re.sub("\s+", " ", name)
    return name.strip()

def remove_punctuation(name):
    for p in string.punctuation:
        name = name.replace(p, "")
    return name

def create_base_form(name, language_tag):
    rp_name = remove_punctuation(name)
    norm_name = normalize(rp_name)

    stemmer = get_stemmer(language_tag)
    if stemmer is not None:
        try:
            return " ".join([stemmer.stem(x) for x in norm_name.split(" ")])
        except:
            return norm_name
    return norm_name

def get_base(base_form, language):
    try:
        base_phrase = BasePhrase.objects.get(name=base_form, language=language)
    except BasePhrase.DoesNotExist:
        return None
    return base_phrase

def create_base(base_form, language):
    base_phrase = BasePhrase(name=base_form, language=language)
    base_phrase.save()
    return base_phrase

def get_or_create_base(base_form, language):
    created = False
    base_phrase = get_base(base_form, language)
    if base_phrase is None:
        created = True
        base_phrase = create_base(base_form, language)
    return (base_phrase, created)


def get_phrase(name, language):
    base_form = create_base_form(name, language.tag)

    try:
        phrase = Phrase.objects.get(name=name.lower(), base__name=base_form, base__language=language)
    except Phrase.DoesNotExist:
        return None

    return phrase

def __create_phrase(name, base):
    phrase = Phrase(name=name, base=base)
    phrase.save()
    return phrase

def create_phrase(name, language):
    base_form = create_base_form(name, language.tag)
    (base_phrase, _) = get_or_create_base(base_form, language)

    return __create_phrase(name, base_phrase)


def get_or_create_phrase(name, language, set_casing=False):
    base_form = create_base_form(name, language.tag)
    created = False
    created_base = False
    phrase = get_phrase(name, language)

    if phrase is None:
        created = True
        (base_phrase, created_base) = get_or_create_base(base_form, language)
        phrase = __create_phrase(name, base_phrase)
    elif set_casing:
        phrase.set_casing(name)
        phrase.save()

    return (phrase, created, created_base)

def sort_translation_names(name1, language1, name2, language2):
    if language1.tag < language2.tag:
        return ((name1, language1), (name2, language2))
    elif language2.tag < language1.tag:
        return ((name2, language2), (name1, language1))
    else:
        raise Exception("Cannot get translation with same language")

def sort_translation_phrases(phrase1, phrase2):
    if phrase1.base.language.tag < phrase2.base.language.tag:
        return (phrase1, phrase2)
    elif phrase2.base.language.tag < phrase1.base.language.tag:
        return (phrase2, phrase1)
    else:
        raise Exception("Cannot get translation with same language")

def get_phrase_number(phrase, language):
    if phrase.base.language.tag < language.tag:
        return 1
    elif phrase.base.language.tag > language.tag:
        return 2
    else:
        raise Exception("Cannot get translation with same language")

def partition_languages(root_language, languages):
    smaller = []
    larger = []

    for language in languages:
        if language.tag < root_language.tag:
            smaller.append(language)
        elif language.tag > root_language.tag:
            larger.append(language)
        else:
            raise Exception("Cannot partition with same languages")
    return (smaller, larger)

def __get_translation(name1, language1, name2, language2):
    try:
        translation = Translation.objects.get(phrase1__name=name1.lower(), phrase2__name=name2.lower(), phrase1__base__language=language1, phrase2__base__language=language2)
    except Translation.DoesNotExist:
        translation = None
    return translation

def __get_translation_one_phrase_1(phrase, name, language):
    try:
        translation = Translation.objects.get(phrase1=phrase, phrase2__name=name.lower(), phrase2__base__language=language)
    except Translation.DoesNotExist:
        translation = None
    return translation

def __get_translation_one_phrase_2(phrase, name, language):
    try:
        translation = Translation.objects.get(phrase2=phrase, phrase1__name=name.lower(), phrase1__base__language=language)
    except Translation.DoesNotExist:
        translation = None
    return translation

def __get_translation_two_phrase(phrase1, phrase2):
    try:
        translation = Translation.objects.get(phrase1=phrase1, phrase2=phrase2)
    except Translation.DoesNotExist:
        translation = None
    return translation


def __create_translation(name1, language1, name2, language2, translator_ids):
    (phrase1, _, _) = get_or_create_phrase(name1, language1)
    (phrase2, _, _) = get_or_create_phrase(name2, language2)
    return __create_translation_two_phrase(phrase1, phrase2, translator_ids)

def __create_translation_one_phrase_1(phrase1, name, language, translator_ids):
    (phrase2, _, _) = get_or_create_phrase(name, language)
    return __create_translation_two_phrase(phrase1, phrase2, translator_ids)

def __create_translation_one_phrase_2(phrase2, name, language, translator_ids):
    (phrase1, _, _) = get_or_create_phrase(name, language)
    return __create_translation_two_phrase(phrase1, phrase2, translator_ids)

def __create_translation_two_phrase(phrase1, phrase2, translator_ids):
    translation = Translation(phrase1=phrase1, phrase2=phrase2)
    if translator_ids is not None:
        translation.translator_ids = json.dumps(translator_ids)
    translation.save()
    return translation


def get_translation(name1, language1, name2, language2):
    ((name1, language1), (name2, language2)) = sort_translation_names(name1, language1, name2, language2)
    return __get_translation(name1, language1, name2, language2)

def get_translation_one_phrase(phrase, name, language):
    phrase_num = get_phrase_number(phrase, language)
    if phrase_num == 1:
        translation = __get_translation_one_phrase_1(phrase, name, language)
    else:
        translation = __get_translation_one_phrase_2(phrase, name, language)
    return translation

def get_translation_two_phrase(phrase1, phrase2):
    (phrase1, phrase2) = sort_translation_phrases(phrase1, phrase2)
    return __get_translation_two_phrase(phrase1, phrase2)


def get_or_create_translation(name1, language1, name2, language2, translator_ids=None):
    ((name1, language1), (name2, language2)) = sort_translation_names(name1, language1, name2, language2)
    translation = __get_translation(name1, language1, name2, language2)
    created = False
    if translation is None:
        created = True
        translation = __create_translation(name1, language1, name2, language2, translator_ids)
    return (translation, created)

def get_or_create_translation_one_phrase(phrase, name, language, translator_ids=None):
    phrase_num = get_phrase_number(phrase, language)
    if phrase_num == 1:
        translation = __get_translation_one_phrase_1(phrase, name, language)
    else:
        translation = __get_translation_one_phrase_2(phrase, name, language)

    created = False
    if translation is None:
        created = True
        if phrase_num == 1:
            translation = __create_translation_one_phrase_1(phrase, name, language, translator_ids)
        else:
            translation = __create_translation_one_phrase_2(phrase, name, language, translator_ids)
    return (translation, created)

def get_or_create_translation_two_phrase(phrase1, phrase2, translator_ids=None):
    (phrase1, phrase2) = sort_translation_phrases(phrase1, phrase2)
    translation = __get_translation_two_phrase(phrase1, phrase2)

    created = False
    if translation is None:
        created = True
        translation = __create_translation_two_phrase(phrase1, phrase2, translator_ids)

    return (translation, created)

def get_translations(names, source_language, target_languages):
    (smaller, larger) = partition_languages(source_language, target_languages)
    lower_names = [n.lower() for n in names]

    translations_forw = Translation.objects.filter(phrase1__name__in=lower_names, phrase1__base__language=source_language, phrase2__base__language__in=larger).select_related("phrase1", "phrase2", "phrase1__base__language", "phrase2__base__language").prefetch_related("users").values("id", "rating", "translator_ids", "phrase1__name", "phrase2__name", "phrase1__display_name", "phrase2__display_name", "phrase1__base__language__tag", "phrase2__base__language__tag", "users__id", "users__first_name", "users__last_name")
    translations_back = Translation.objects.filter(phrase2__name__in=lower_names, phrase2__base__language=source_language, phrase1__base__language__in=smaller).select_related("phrase1", "phrase2", "phrase1__base__language", "phrase2__base__language").prefetch_related("users").values("id", "rating", "translator_ids", "phrase1__name", "phrase2__name", "phrase1__display_name", "phrase2__display_name", "phrase1__base__language__tag", "phrase2__base__language__tag", "users__id", "users__first_name", "users__last_name")

    return (translations_forw, translations_back)


def get_translations2(names, source_language, target_languages):
    (smaller, larger) = partition_languages(source_language, target_languages)
    lower_names = [n.lower() for n in names]

    translations_forw = Translation.objects.filter(phrase1__name__in=lower_names, phrase1__base__language=source_language, phrase2__base__language__in=larger).select_related("phrase1", "phrase2", "phrase2__base__language").prefetch_related("users").values("id", "rating", "translator_ids", "phrase1__name", "phrase2__name", "phrase1__display_name", "phrase2__display_name", "phrase2__base__language__tag", "users__id", "users__first_name", "users__last_name")
    translations_back = Translation.objects.filter(phrase2__name__in=lower_names, phrase2__base__language=source_language, phrase1__base__language__in=smaller).select_related("phrase1", "phrase2", "phrase1__base__language").prefetch_related("users").values("id", "rating", "translator_ids", "phrase1__name", "phrase2__name", "phrase1__display_name", "phrase2__display_name", "phrase1__base__language__tag", "users__id", "users__first_name", "users__last_name")

    return (translations_forw, translations_back)

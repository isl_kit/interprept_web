#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
import json

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from apps.translation.models import TMTranslation, TMDone, Translation, Phrase
from apps.translation.translators.tm_trans import TMTranslator

class Command(BaseCommand):

    def handle(self, *args, **options):
        translation_ids = []
        phrase_ids = []
        with transaction.atomic():
            for tm_translation in TMTranslation.objects.all().select_related("translation"):
                tr = tm_translation.translation
                ids = json.loads(tr.translator_ids)
                ids.remove(TMTranslator.TRANSLATOR_ID)
                if len(ids) == 0:
                    translation_ids.append(tr.id)
                    phrase_ids.append(tr.phrase.id)
                else:
                    tr.translator_ids = json.dumps(ids)
                    tr.save()
                tm_translation.delete()

        with transaction.atomic():
            for translation in Translation.objects.filter(id__in=translation_ids):
                translation.delete()

        with transaction.atomic():
            for phrase in Phrase.objects.filter(id__in=phrase_ids):
                if phrase.translation_set.count() == 0:
                    phrase.delete()

        TMDone.objects.all().delete()



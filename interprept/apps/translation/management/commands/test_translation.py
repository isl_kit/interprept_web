#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from apps.translation.translation_service import translate_async, translate


class Command(BaseCommand):
    help = "lets you manually fetch translations from all sources"

    def add_arguments(self, parser):
        parser.add_argument("--source", dest="source_tag", default="", help="source language tag")
        parser.add_argument("--target", dest="target_tags", default="", help="target language tags (comma seperated)")
        parser.add_argument("--phrases", dest="phrases", default="", help="comma-seperated phrases to translate")

    def handle(self, *args, **options):
        source_tag = options["source_tag"]
        target_tags = options["target_tags"].split(",")
        phrases = options["phrases"].split(",")
        result = translate(source_tag, target_tags, phrases, False)
        print result

    def translate_done(self, result):
        print result

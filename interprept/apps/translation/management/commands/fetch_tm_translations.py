#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback

from django.core.management.base import BaseCommand, CommandError

from apps.translation.translators.tm_trans import TMTranslator

class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            translator = TMTranslator()
            translator.fetch()
        except:
            print traceback.format_exc()
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model

from apps.translation.models import Translation2

class Command(BaseCommand):

    def handle(self, *args, **options):
        User = get_user_model()
        t = Translation2.objects.all()[0]
        u = User.objects.all()[0]
        t.users.add(u)

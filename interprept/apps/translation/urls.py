#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^translate/$', views.TranslateView.as_view(), name="translate"),
    url(r'^submit/$', views.SubmitView.as_view(), name="submit"),
    url(r'^getContext/$', views.GetContextView.as_view(), name="getContext"),
    url(r'^addTranslation/$', views.AddTranslationView.as_view(), name="addTranslation"),
    url(r'^getOnlineSources/$', views.GetOnlineSourcesView.as_view(), name="getOnlineSources"),
    url(r'^bumpRating/$', views.BumpRatingView.as_view(), name="bumpRating"),
    url(r'^getLanguageDefinitions/$', views.GetLanguageDefinitionsView.as_view(), name="getLanguageDefinitions"),
)

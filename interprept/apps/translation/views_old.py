#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import thread

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.db.models import F

from apps.common.decorators.cache import never_ever_cache
from apps.common.decorators.auth import check_login
from apps.languages.models import Language
from .models import Translation, Phrase
from .translation_service import register_all, translate, submit
from apps.accounts.views import getUserData

def get_translation_values(translation):
    return {
        "id": translation.id,
        "name": translation.name,
        "target_tag": translation.language.tag,
        "rating": translation.rating,
        "translator_ids": json.loads(translation.translator_ids),
        "users": [getUserData(user, True) for user in translation.users.all()]
    }


def get_tmtranslation_values(tmtranslation):
    return {
        "id": tmtranslation.id,
        "fdr_no": tmtranslation.fdr_no,
        "o_context": tmtranslation.o_context,
        "o_index": tmtranslation.o_index,
        "o_length": tmtranslation.o_length,
        "t_context": tmtranslation.t_context,
        "t_index": tmtranslation.t_index,
        "t_length": tmtranslation.t_length
    }

class TranslateView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            texts = data["texts"]
            source_tag = data["source_tag"]
            target_tags = data["target_tags"]
            refresh = data.get("refresh", 0)

            if refresh == 1:
                translate(source_tag, target_tags, texts, True)
            elif refresh == 2:
                new_texts = []
                for text in texts:
                    if not Phrase.objects.filter(name=text.lower()).exists():
                        new_texts.append(text)
                translate(source_tag, target_tags, new_texts, True)


            result = {}
            lower_texts = []
            for text in texts:
                translations = {}
                lower_texts.append(text.lower())
                result[text.lower()] = {
                    "name": text,
                    "translations": translations
                }
                for tag in target_tags:
                    translations[tag] = []

            translations = Translation.objects.filter(phrase__name__in=lower_texts, phrase__language__tag=source_tag, language__tag__in=target_tags).select_related("phrase", "language").prefetch_related("users")

            for translation in translations:
                result[translation.phrase.name]["translations"][translation.language.tag].append(get_translation_values(translation))

            result = [res for ph, res in result.iteritems()]

            return HttpResponse(
                json.dumps(result),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(TranslateView, self).dispatch(*args, **kwargs)

class SubmitView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            translation_id = data["translation_id"]

            try:
                translation = Translation.objects.get(id=translation_id)
            except Translation.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "translation does not exist"}),
                    content_type="application/json",
                    status=400
                )

            translator_ids = submit(translation)
            result = {
                "sources": [translator.get_source_infos() for id, translator in register_all.iteritems() if id in translator_ids]
            }
            return HttpResponse(
                json.dumps(result),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(SubmitView, self).dispatch(*args, **kwargs)



class GetContextView(View):
    def get(self, request, *args, **kwargs):
        try:
            translation_id = request.GET["translation_id"]            

            try:
                translation = Translation.objects.get(pk=translation_id)
            except Translation.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "translation does not exist"}),
                    content_type="application/json",
                    status=400
                )

            result = {
                "tm_translations": [get_tmtranslation_values(t) for t in translation.tmtranslation_set.all()]
            }
            print result
            return HttpResponse(
                json.dumps(result),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetContextView, self).dispatch(*args, **kwargs)



class BumpRatingView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            translation_ids = data["translation_ids"]

            Translation.objects.filter(id__in=translation_ids).update(rating=F("rating") + 1)

            return HttpResponse(
                json.dumps({}),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(BumpRatingView, self).dispatch(*args, **kwargs)


class AddTranslationView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            ph = data["phrase"].strip()
            source_tag = data["source_tag"]
            target_tag = data["target_tag"]
            translation = data["translation"].strip()

            try:
                source_lang = Language.objects.get(tag=source_tag)
                target_lang = Language.objects.get(tag=target_tag)
            except Language.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "language does not exist"}),
                    content_type="application/json",
                    status=400
                )

            if len(ph) == 0 or len(translation) == 0:
                return HttpResponse(
                    json.dumps({"message": "arguments of length zero not allowed"}),
                    content_type="application/json",
                    status=400
                )

            try:
                phrase = Phrase.objects.get(name=ph.lower(), language=source_lang)
            except Phrase.DoesNotExist:
                phrase = Phrase(name=ph.lower(), language=source_lang)
                phrase.save()


            try:
                translation = Translation.objects.get(phrase=phrase, language=target_lang, lower_name=translation.lower())
                if not translation.users.filter(pk=request.user.id).exists():
                    translation.users.add(request.user)
            except Translation.DoesNotExist:
                translation = Translation(language=target_lang, name=translation, phrase=phrase)
                translation.save()
                translation.users.add(request.user)

            result = {
                "translation": get_translation_values(translation)
            }

            return HttpResponse(
                json.dumps(result),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(AddTranslationView, self).dispatch(*args, **kwargs)


class GetOnlineSourcesView(View):
    def get(self, request, *args, **kwargs):
        result = {
            "sources": [translator.get_source_infos() for id, translator in register_all.iteritems()]
        }
        return HttpResponse(
            json.dumps(result),
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetOnlineSourcesView, self).dispatch(*args, **kwargs)


class GetLanguageDefinitionsView(View):
    def get(self, request, *args, **kwargs):
        result = {
            "languages": language_definitions
        }
        return HttpResponse(
            json.dumps(result),
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetLanguageDefinitionsView, self).dispatch(*args, **kwargs)

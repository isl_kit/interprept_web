#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import thread
from itertools import chain
import traceback

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.db.models import F

from apps.common.decorators.cache import never_ever_cache
from apps.common.decorators.auth import check_login
from apps.languages.models import Language
from .models import Translation, Phrase
from .translation_service import register_all, translate, submit
from apps.accounts.views import getUserData
from .db_service import get_translations, get_phrase, get_or_create_translation


def get_phrase_value(phrase):
    return {
        "id": phrase.id,
        "name": phrase.name,
        "display_name": phrase.display_name,
        "tag": phrase.base.language.tag
    }

def get_translation_values_raw(translation, direction="forw"):
    if direction == "forw":
        name = translation.phrase2.display_name
        target_tag = translation.phrase2.base.language.tag
    else:
        name = translation.phrase1.display_name
        target_tag = translation.phrase1.base.language.tag

    users = []
    for user in translation.users.all():
        users.append({
            "id": user.id,
            "firstname": user.first_name,
            "lastname": user.last_name
        })
    res = {
        "id": translation.id,
        "name": name,
        "target_tag": target_tag,
        "rating": translation.rating,
        "translator_ids": json.loads(translation.translator_ids),
        "users": users
    }
    return res

def get_translation_values(translation, direction="forw"):
    if direction == "forw":
        name = translation["phrase2__display_name"]
        target_tag = translation["phrase2__base__language__tag"]
    else:
        name = translation["phrase1__display_name"]
        target_tag = translation["phrase1__base__language__tag"]
    user_id = translation["users__id"]
    users = []
    if user_id is not None:
        users.append({
            "id": user_id,
            "firstname": translation["users__first_name"],
            "lastname": translation["users__last_name"]
        })
    res = {
        "id": translation["id"],
        "name": name,
        "target_tag": target_tag,
        "rating": translation["rating"],
        "translator_ids": json.loads(translation["translator_ids"]),
        "users": users
    }
    return res

'''
def get_translation_values(translation):
    return {
        "id": translation.id,
        "phrase1": get_phrase_value(translation.phrase1),
        "phrase2": get_phrase_value(translation.phrase2),
        "rating": translation.rating,
        "translator_ids": json.loads(translation.translator_ids),
        "users": [getUserData(user, True) for user in translation.users.all()]
    }
'''

def get_tmtranslation_values(tmtranslation):
    return {
        "id": tmtranslation.id,
        "fdr_no": tmtranslation.fdr_no,
        "o_context": tmtranslation.o_context,
        "o_index": tmtranslation.o_index,
        "o_length": tmtranslation.o_length,
        "t_context": tmtranslation.t_context,
        "t_index": tmtranslation.t_index,
        "t_length": tmtranslation.t_length
    }

class TranslateView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            texts = data["texts"]
            source_tag = data["source_tag"]
            target_tags = data["target_tags"]
            refresh = data.get("refresh", 0)


            try:
                target_langs = []
                source_lang = Language.objects.get(tag=source_tag)
                for target_tag in target_tags:
                    target_langs.append(Language.objects.get(tag=target_tag))
            except Language.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "language does not exist"}),
                    content_type="application/json",
                    status=400
                )

            if refresh == 1:
                translate(source_tag, target_tags, texts, True)
            elif refresh == 2:
                new_texts = []
                for text in texts:
                    phrase = get_phrase(text, source_lang)
                    if phrase is None:
                        new_texts.append(text)
                translate(source_tag, target_tags, new_texts, True)

            (translations_forw, translations_back) = get_translations(texts, source_lang, target_langs)

            #translations = list(chain(translations_forw, translations_back))

            #res_forw = [get_translation_values(t, direction="forw") for t in translations_forw]
            #res_back = [get_translation_values(t, direction="back") for t in translations_back]
            result = {}
            for text in texts:
                translations = {}
                result[text.lower()] = {
                    "name": text,
                    "translations": translations
                }
                for tag in target_tags:
                    translations[tag] = []


            res_dict = {}
            for t in translations_forw:
                translation_id = t["id"]
                user_id = t["users__id"]
                phrase1_name = t["phrase1__name"]
                phrase2_tag = t["phrase2__base__language__tag"]
                if translation_id in res_dict:
                    if user_id is not None:
                        tres = res_dict[translation_id]
                        tres["users"].append({
                            "id": user_id,
                            "firstname": t["users__first_name"],
                            "lastname": t["users__last_name"]
                        })
                else:
                    tres = get_translation_values(t, direction="forw")
                    res_dict[user_id] = tres
                    result[phrase1_name]["translations"][phrase2_tag].append(tres)
            for t in translations_back:
                translation_id = t["id"]
                user_id = t["users__id"]
                phrase2_name = t["phrase2__name"]
                phrase1_tag = t["phrase1__base__language__tag"]
                if translation_id in res_dict:
                    if user_id is not None:
                        tres = res_dict[translation_id]
                        tres["users"].append({
                            "id": user_id,
                            "firstname": t["users__first_name"],
                            "lastname": t["users__last_name"]
                        })
                else:
                    tres = get_translation_values(t, direction="back")
                    res_dict[user_id] = tres
                    result[phrase2_name]["translations"][phrase1_tag].append(tres)

            return HttpResponse(
                json.dumps(result),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(TranslateView, self).dispatch(*args, **kwargs)

class SubmitView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            translation_id = data["translation_id"]

            try:
                translation = Translation.objects.get(id=translation_id)
            except Translation.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "translation does not exist"}),
                    content_type="application/json",
                    status=400
                )

            translator_ids = submit(translation)
            result = {
                "sources": [translator.get_source_infos() for id, translator in register_all.iteritems() if id in translator_ids]
            }
            return HttpResponse(
                json.dumps(result),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(SubmitView, self).dispatch(*args, **kwargs)



class GetContextView(View):
    def get(self, request, *args, **kwargs):
        try:
            translation_id = request.GET["translation_id"]

            try:
                translation = Translation.objects.get(pk=translation_id)
            except Translation.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "translation does not exist"}),
                    content_type="application/json",
                    status=400
                )

            result = {
                "tm_translations": [get_tmtranslation_values(t) for t in translation.tmtranslation_set.all()]
            }

            return HttpResponse(
                json.dumps(result),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetContextView, self).dispatch(*args, **kwargs)



class BumpRatingView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            translation_ids = data["translation_ids"]

            Translation.objects.filter(id__in=translation_ids).update(rating=F("rating") + 1)

            return HttpResponse(
                json.dumps({}),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(BumpRatingView, self).dispatch(*args, **kwargs)


class AddTranslationView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            ph = data["phrase"].strip()
            source_tag = data["source_tag"]
            target_tag = data["target_tag"]
            translation = data["translation"].strip()

            try:
                source_lang = Language.objects.get(tag=source_tag)
                target_lang = Language.objects.get(tag=target_tag)
            except Language.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "language does not exist"}),
                    content_type="application/json",
                    status=400
                )

            if len(ph) == 0 or len(translation) == 0:
                return HttpResponse(
                    json.dumps({"message": "arguments of length zero not allowed"}),
                    content_type="application/json",
                    status=400
                )

            (translation, created) = get_or_create_translation(ph, source_lang, translation, target_lang)

            if created or not translation.users.filter(pk=request.user.id).exists():
                translation.users.add(request.user)

            if source_tag < target_tag:
                direction = "forw"
            else:
                direction = "back"

            result = {
                "translation": get_translation_values_raw(translation, direction=direction)
            }

            return HttpResponse(
                json.dumps(result),
                content_type="application/json",
                status=200
            )

        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(AddTranslationView, self).dispatch(*args, **kwargs)


class GetOnlineSourcesView(View):
    def get(self, request, *args, **kwargs):
        result = {
            "sources": [translator.get_source_infos() for id, translator in register_all.iteritems()]
        }
        return HttpResponse(
            json.dumps(result),
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetOnlineSourcesView, self).dispatch(*args, **kwargs)


class GetLanguageDefinitionsView(View):
    def get(self, request, *args, **kwargs):
        result = {
            "languages": language_definitions
        }
        return HttpResponse(
            json.dumps(result),
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetLanguageDefinitionsView, self).dispatch(*args, **kwargs)

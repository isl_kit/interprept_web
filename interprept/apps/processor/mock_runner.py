

class Runner(object):

    def start(self, name, args=[], meta={}, callback=None, queue="celery", scb=None):
        if name == "interprept.pdf.tasks.convert_pdf_and_save":
            dest = args[1]
            with open(dest, "w+") as f:
                f.write("This is a test on Fishery where Fishery is very important.\n")
            result = {
                "converted": True,
                "tag": "en",
                "fdr_no": None,
                "pe_no": None,
                "a7_no": None,
                "published": None,
                "doc_type": None,
                "error": None
            }
        elif name == "interprept.terminology.tasks.create_terminology_list":
            result = {
                "sequences": [
                    {
                        "name": "Fishery",
                        "importance_score": 0.5,
                        "docs": [
                            {
                                "positions": [
                                    "18:25", "32:39"
                                ]
                            }
                        ]
                    },
                    {
                        "name": "test",
                        "importance_score": 0.4,
                        "docs": [
                            {
                                "positions": [
                                    "10:14"
                                ]
                            }
                        ]
                    },
                    {
                        "name": "important",
                        "importance_score": 0.35,
                        "docs": [
                            {
                                "positions": [
                                    "48:57"
                                ]
                            }
                        ]
                    }
                ],
                "error": None
            }
        elif name == "interprept.ne.tasks.create_ne_list":
            result = {
                "error": None,
                "nes": [
                    {
                        "name": "Fishery",
                        "type_id": 3,
                        "cursor_pos": "18:25"
                    },
                    {
                        "name": "Fishery",
                        "type_id": 3,
                        "cursor_pos": "32:39"
                    },
                    {
                        "name": "very important",
                        "type_id": 2,
                        "cursor_pos": "43:57"
                    }
                ]
            }
        callback(1, result, meta, None)

runner = Runner()

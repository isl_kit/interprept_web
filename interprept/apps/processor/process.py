#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .tasks.task import TaskRunner
from .tasks.text_task import TextTask
from .tasks.terminology_task import TerminologyTask
from .tasks.ne_task import NeTask
from .tasks.translation_task import TranslationTask

runner = TaskRunner()
runner.add(TextTask)
runner.add(TerminologyTask)
runner.add(NeTask)
runner.add(TranslationTask)

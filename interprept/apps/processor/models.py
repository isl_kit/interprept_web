#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

from apps.resources.models import Resource


class ResourceTaskState(models.Model):
    resource = models.ForeignKey(Resource)
    task_name = models.TextField()
    running = models.BooleanField(default=False)
    state = models.TextField(default="init")
    version = models.IntegerField(default=0)
    hash = models.TextField(default="")
    error_message = models.TextField(default="")
    dep = models.TextField(default="")

    def reset(self):
        self.running = False
        self.state = "init"
        self.version = 0
        self.hash = ""
        self.error_message = ""
        self.dep = ""
        self.save()

    def __unicode__(self):
        return self.task_name + " - " + str(self.resource.id)

    class Meta:
        app_label = "processor"
        unique_together = ("resource", "task_name")
        index_together = [
            ["resource", "task_name"]
        ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from hashlib import sha256
from os.path import join as join_path, exists as path_exists

from django.db import models, migrations
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings


def create_terminology_hash(resource):
    try:
        terminologylist = resource.terminologylist
    except ObjectDoesNotExist:
        return ""
    return sha256("|".join([term.name.lower() for term in terminologylist.term_set.all().order_by("name")]).encode("utf-8")).hexdigest()


def create_text_hash(resource):
    textfile_path = join_path(settings.MEDIA_ROOT, "files", "text", resource.hash)
    sha256_hash = sha256()
    with open(textfile_path, "r") as f:
        for chunk in iter(lambda: f.read(128 * sha256_hash.block_size), b''):
            sha256_hash.update(chunk)
    return sha256_hash.hexdigest()


def create_ne_hash(resource):
    try:
        nelist = resource.nelist
    except ObjectDoesNotExist:
        return ""
    return sha256("|".join([ne.name.lower() for ne in nelist.namedentity_set.all().order_by("name")]).encode("utf-8")).hexdigest()


def create_translation_hash(resource):
    return sha256("").hexdigest()


def __get_dep_hash(self, resource):
    c_hashes = ""
    names = [task_name for task_name, states in self.depends_on.iteritems()]
    names.sort()
    for task_name in names:
        task_state = self.__get_task_state(resource, task_name)
        c_hashes += task_state.hash
    return sha256(c_hashes).hexdigest()


def get_rts(ResourceTaskState, resource, name):
    try:
        rts = ResourceTaskState.objects.get(resource=resource, task_name=name)
    except ResourceTaskState.DoesNotExist:
        rts = ResourceTaskState(resource=resource, task_name=name)
        rts.save()
    return rts


def update_rts_state(rts, old_state):
    if old_state in [0, 1]:
        rts.state = "init"
    elif old_state == 2:
        rts.state = "ok"
    elif old_state == 3:
        rts.state = "error"
    else:
        rts.state = "deprecated"


def update_rts_error(rts, old_state, old_error):
    if old_state in [3, 4, 5]:
        rts.error_message = old_error


def zero_pad(text, num):
    if len(text) < num:
        return "".join(["0" for _ in range(num - len(text))]) + text
    return text


def port_version(v):
    return int("".join([zero_pad(p, 3) for p in v.split(".")]))


def port_task_states(apps, schema_editor):
    Resource = apps.get_model("resources", "Resource")
    ResourceTaskState = apps.get_model("processor", "ResourceTaskState")
    v_text = set()
    v_ne = set()
    v_te = set()

    for resource in Resource.objects.all():
        ne_rts = get_rts(ResourceTaskState, resource, "ne")
        te_rts = get_rts(ResourceTaskState, resource, "terminology")
        text_rts = get_rts(ResourceTaskState, resource, "text")
        trans_rts = get_rts(ResourceTaskState, resource, "translation")

        update_rts_state(text_rts, resource.text_state)
        update_rts_error(text_rts, resource.text_state, resource.text_error)

        if resource.text_state in [2, 4, 5]:
            text_rts.hash = create_text_hash(resource)
            text_rts.version = port_version(resource.conversion_version)
            v_text.add(text_rts.version)

        text_rts.save()

        if resource.text_state not in [2, 4, 5]:
            continue

        update_rts_state(ne_rts, resource.ne_state)
        update_rts_error(ne_rts, resource.ne_state, resource.ne_error)

        if resource.ne_state in [2, 4, 5]:
            ne_rts.hash = create_ne_hash(resource)
            ne_rts.version = port_version(resource.nelist.version)
            ne_rts.dep = sha256(text_rts.hash).hexdigest()
            v_ne.add(ne_rts.version)

        ne_rts.save()

        update_rts_state(te_rts, resource.terminology_state)
        update_rts_error(te_rts, resource.terminology_state, resource.terminology_error)

        if resource.terminology_state in [2, 4, 5]:
            te_rts.hash = create_terminology_hash(resource)
            te_rts.version = port_version(resource.terminologylist.version)
            te_rts.dep = sha256(text_rts.hash).hexdigest()
            v_te.add(te_rts.version)

        te_rts.save()

        if resource.terminology_state not in [2, 4, 5]:
            continue

        update_rts_state(trans_rts, resource.translate_state)
        update_rts_error(trans_rts, resource.translate_state, resource.translate_error)

        if resource.translate_state in [2, 4, 5]:
            trans_rts.hash = create_translation_hash(resource)
            trans_rts.version = 1
            trans_rts.dep = sha256(te_rts.hash).hexdigest()

        trans_rts.save()

    v_text = list(v_text)
    v_ne = list(v_ne)
    v_te = list(v_te)

    print "----VERSIONS----"
    print v_text
    print v_ne
    print v_te

    for resource in Resource.objects.all():
        ne_rts = get_rts(ResourceTaskState, resource, "ne")
        te_rts = get_rts(ResourceTaskState, resource, "terminology")
        text_rts = get_rts(ResourceTaskState, resource, "text")

        if ne_rts.state in ["ok", "deprecated"]:
            ne_rts.version = v_ne.index(ne_rts.version) + 1
            ne_rts.save()

        if te_rts.state in ["ok", "deprecated"]:
            te_rts.version = v_te.index(te_rts.version) + 1
            te_rts.save()

        if text_rts.state in ["ok", "deprecated"]:
            text_rts.version = v_text.index(text_rts.version) + 1
            text_rts.save()


class Migration(migrations.Migration):

    dependencies = [
        ('processor', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(port_task_states),
    ]

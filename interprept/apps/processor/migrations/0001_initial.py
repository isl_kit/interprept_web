# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResourceTaskState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('task_name', models.TextField()),
                ('running', models.BooleanField(default=False)),
                ('state', models.TextField(default=b'init')),
                ('version', models.IntegerField(default=0)),
                ('hash', models.TextField(default=b'')),
                ('error_message', models.TextField(default=b'')),
                ('dep', models.TextField(default=b'')),
                ('resource', models.ForeignKey(to='resources.Resource')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='resourcetaskstate',
            unique_together=set([('resource', 'task_name')]),
        ),
        migrations.AlterIndexTogether(
            name='resourcetaskstate',
            index_together=set([('resource', 'task_name')]),
        ),
    ]

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
import json
from hashlib import sha256

from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from apps.common.services.communication.libwrapper import get_terminology_version
from apps.translation.db_service import get_or_create_phrase
from apps.resources.models import TerminologyList, Term, Resource

from celeryrunner import runner
from .task import Task


class TerminologyTask(Task):

    name = "terminology"
    depends_on = {
        "text": ["ok", "deprecated"]
    }

    def get_version(self):
        return get_terminology_version()

    def create_hash(self, resource):
        try:
            terminologylist = resource.terminologylist
        except ObjectDoesNotExist:
            return ""
        return sha256("|".join([term.name.lower() for term in terminologylist.term_set.all().order_by("name")]).encode("utf-8")).hexdigest()

    def process(self, resource):
        runner.start("interprept.terminology.tasks.create_terminology_list", args=[resource.get_text_content(), resource.id, resource.language.tag], meta={"resource_id": resource.id}, callback=self.terminology_done)

    def terminology_done(self, tid, result, meta, error):
        resource_id = meta["resource_id"]
        try:
            resource = Resource.objects.get(pk=resource_id)
        except Resource.DoesNotExist:
            self.__error__(None, "Resource does not exist")
            return

        try:
            if error:
                self.__error__(resource, str(error))
            else:
                sequences = result["sequences"]
                error = result["error"]
                if sequences is None:
                    self.__error__(resource, error)
                    return

                try:
                    resource.terminologylist.delete()
                except ObjectDoesNotExist:
                    pass

                language = resource.language

                with transaction.atomic():
                    terminology_list = TerminologyList(resource=resource)
                    terminology_list.save()
                    for sequence in sequences:
                        name = sequence["name"]
                        importance = sequence["importance_score"]
                        occurances = json.dumps(sequence["docs"][0]["positions"])
                        (phrase, _, _) = get_or_create_phrase(name, language, set_casing=True)
                        term = Term(name=name, importance=importance, occurances=occurances, terminology_list=terminology_list, phrase=phrase)
                        term.save()

                self.__success__(resource)
        except:
            self.__error__(resource, traceback.format_exc(), True)

    def clean_up(self, resource):
        TerminologyList.objects.filter(resource=resource).delete()

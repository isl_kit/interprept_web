#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
from os.path import split as split_path, exists as path_exists
from os import makedirs, remove
from shutil import copyfile
import dateutil.parser
from time import sleep
from hashlib import sha256

from django.core.exceptions import ObjectDoesNotExist

from guess_language import guess_language

from apps.common.services.pdf.pdfparser import get_pdf_version
from apps.languages.models import Language
from apps.resources.models import Resource, convert_to_comp_path
from apps.speeches.models import ParlDocument

from celeryrunner import runner
from .task import Task


class TextTask(Task):

    name = "text"
    depends_on = {}

    def get_extra_data(self, resource):
        return {
            "language": resource.language.tag if resource.language else None
        }

    def get_version(self):
        return get_pdf_version()

    def create_hash(self, resource):
        textfile_path = resource.get_text_path()
        sha256_hash = sha256()
        with open(textfile_path, "r") as f:
            for chunk in iter(lambda: f.read(128 * sha256_hash.block_size), b''):
                sha256_hash.update(chunk)
        return sha256_hash.hexdigest()


    def process(self, resource):
        textfile_path = resource.get_text_path()
        origfile_path = resource.get_orig_path()
        textfile_basepath = split_path(textfile_path)[0]

        if not path_exists(textfile_basepath):
            makedirs(textfile_basepath)

        if resource.mimetype == "application/pdf":
            comp_textfile = convert_to_comp_path(textfile_path)
            comp_origfile = convert_to_comp_path(origfile_path)
            runner.start("interprept.pdf.tasks.convert_pdf_and_save", args=[comp_origfile, comp_textfile], meta={"resource_id": resource.id}, callback=self.convert_done)
        elif resource.mimetype.startswith("text/"):
            copyfile(origfile_path, textfile_path)
            self.convert_finish(resource, True)
        else:
            self.convert_finish(resource, False, error="Unsupported Filetype")

    def set_resource_language(self, resource, tag=None):
        if resource.language is not None:
            return
        text = resource.get_text_content()
        if not tag:
            tag = guess_language(text)
        try:
            language = Language.objects.get(tag=tag)
            resource.language = language
            resource.save()
        except Language.DoesNotExist:
            pass

    def convert_finish(self, resource, success, tag=None, error=""):
        if success:
            self.set_resource_language(resource, tag)
            self.__success__(resource)
        else:
            self.__error__(resource, error)

    def convert_done(self, tid, result, meta, error):
        resource_id = meta["resource_id"]
        try:
            resource = Resource.objects.get(pk=resource_id)
        except Resource.DoesNotExist:
            self.convert_finish(None, False, error="Resource does not exist")
            return

        try:
            if error:
                self.convert_finish(resource, False, error=str(error))
            else:
                converted = result["converted"]
                tag = result["tag"]
                fdr_no = result["fdr_no"]
                pe_no = result["pe_no"]
                a7_no = result["a7_no"]
                published = result["published"]
                doc_type = result["doc_type"]
                error = result["error"]

                textfile_path = resource.get_text_path()

                if converted:
                    counter = 0
                    while not path_exists(textfile_path):
                        counter += 1
                        if counter > 60:
                            self.convert_finish(resource, False, error="Converted but no file access")
                            return
                        sleep(1)

                    try:
                        parldocument = resource.parldocument
                        save_pd = False
                        if doc_type and parldocument.doc_type != doc_type:
                            parldocument.doc_type = doc_type
                            save_pd = True
                        if fdr_no and parldocument.fdr_no != fdr_no:
                            parldocument.fdr_no = fdr_no
                            save_pd = True
                        if pe_no and parldocument.pe_no != pe_no:
                            parldocument.pe_no = pe_no
                            save_pd = True
                        if a7_no and not parldocument.a7_no:
                            parldocument.a7_no = a7_no
                            save_pd = True
                        if published and not parldocument.published:
                            parldocument.published = dateutil.parser.parse(published)
                            save_pd = True
                        if save_pd:
                            parldocument.save()
                    except ObjectDoesNotExist:
                        if fdr_no is not None:
                            doc_type = doc_type if doc_type is not None else "RR"
                            pe_no = pe_no if pe_no is not None else ""
                            a7_no = a7_no if a7_no is not None else ""
                            published = dateutil.parser.parse(published) if published is not None else None
                            self.set_resource_language(resource, tag)
                            if resource.language is not None:
                                parl_doc = ParlDocument(title="", a7_no=a7_no, doc_type=doc_type, language=resource.language, pe_no=pe_no, resource=resource, fdr_no=fdr_no, published=published)
                                parl_doc.save()

                self.convert_finish(resource, converted, error=error, tag=tag)
        except:
            self.__error__(resource, traceback.format_exc(), True)

    def clean_up(self, resource):
        textfile_path = resource.get_text_path()
        if path_exists(textfile_path):
            remove(textfile_path)

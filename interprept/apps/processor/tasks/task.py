#!/usr/bin/env python
# -*- coding: utf-8 -*-

from hashlib import sha256

from django.utils.timezone import now as timezone_now

from apps.socketioapi.api import emit_to

from apps.resources.models import Resource

from ..models import ResourceTaskState


class TaskRunner(object):

    def __init__(self):
        self.tasks = {}
        self.running = {}

    def add(self, task_class):
        if task_class.__name__ not in self.tasks:
            task = task_class(self)
            self.tasks[task.name] = task

    def run_single(self, resource, task_name, callback):
        if task_name not in self.tasks:
            callback(task_name, resource, {
                "executed": False,
                "success": False,
                "error_message": "unknown task"
            })
            return
        self.__init_resource__(resource)
        task = self.tasks[task_name]
        task.__run__(resource, callback)

    def run(self, resource, callback):
        if resource.id in self.running:
            return

        self.__init_resource__(resource)
        self.running[resource.id] = {
            "started": [],
            "done": [],
            "callback": callback
        }
        self.__continue__("", resource, None)

    def __init_resource__(self, resource):
        for task_name in self.tasks:
            try:
                ResourceTaskState.objects.get(resource=resource, task_name=task_name)
            except ResourceTaskState.DoesNotExist:
                rts = ResourceTaskState(resource=resource, task_name=task_name)
                rts.save()

    def __continue__(self, task_name, resource, status):
        resource_id = resource.id
        if task_name:
            self.running[resource_id]["done"].append(task_name)
        not_running_tasks = [task for t_name, task in self.tasks.iteritems() if t_name not in self.running[resource_id]["started"]]
        queued_for_run = []
        for task in not_running_tasks:
            can_run = True
            for t_name, states in task.depends_on.iteritems():
                if t_name not in self.running[resource_id]["done"]:
                    can_run = False
            if can_run:
                queued_for_run.append(task)

        for task in queued_for_run:
            self.running[resource_id]["started"].append(task.name)
        for task in queued_for_run:
            task.__run__(resource, self.__continue__)

        if len(queued_for_run) == 0:
            if len(self.running[resource_id]["started"]) == len(self.running[resource_id]["done"]):
                callback = self.running[resource_id]["callback"]
                del self.running[resource_id]
                if callback:
                    callback(resource)

    def clean_up_after(self, task_name, resource):
        for other_task_name, task in self.tasks.iteritems():
            if other_task_name == task_name:
                continue
            if task_name in task.depends_on:
                task.__clean_up__(resource)
                self.clean_up_after(task.name, resource)


class Task(object):

    name = ""
    depends_on = {}

    def __init__(self, runner):
        self.running_tasks = {}
        self.runner = runner

    def get_version(self):
        return 0

    def get_extra_data(self, resource):
        return {}

    def process(self, resource):
        self.__success__(resource.id)

    def create_hash(self, resource):
        return ""

    def clean_up(self, resource):
        return

    def __clean_up__(self, resource):
        self.clean_up(resource)
        task_state = self.__get_task_state(resource, self.name)
        task_state.reset()
        self.__broadcast_task_data__(resource, task_state)

    def __broadcast_task_data__(self, resource, task_state):
        emit_to({
            "type": "task_data_change",
            "data": {
                "resource": {
                    "id": resource.id,
                    "task_name": self.name,
                    "task_data": {
                        "running": task_state.running,
                        "state": task_state.state,
                        "version": task_state.version,
                        "hash": task_state.hash,
                        "error_message": task_state.error_message
                    },
                    "extra": self.get_extra_data(resource),
                    "time": timezone_now()
                }
            }
        }, "resources")

    def __get_task_state(self, resource, name):
        try:
            rts = ResourceTaskState.objects.get(resource=resource, task_name=name)
        except ResourceTaskState.DoesNotExist:
            rts = ResourceTaskState(resource=resource, task_name=name)
            rts.save()
        return rts

    def __get_dep_hash(self, resource):
        c_hashes = ""
        names = [task_name for task_name, states in self.depends_on.iteritems()]
        names.sort()
        for task_name in names:
            task_state = self.__get_task_state(resource, task_name)
            c_hashes += task_state.hash
        return sha256(c_hashes).hexdigest()

    def __init_run__(self, resource):
        dep_hash = None
        if resource.id in self.running_tasks:
            return (False, dep_hash, None)

        for task_name, states in self.depends_on.iteritems():
            t_state = self.__get_task_state(resource, task_name)
            if t_state.state not in states:
                return (False, dep_hash, None)
        task_state = self.__get_task_state(resource, self.name)
        if task_state.state not in ["init", "error"]:
            version = self.get_version()
            if task_state.version > version:
                return (False, dep_hash, task_state)
            elif task_state.version == version:
                dep_hash = self.__get_dep_hash(resource)
                if dep_hash == task_state.dep:
                    return (False, dep_hash, task_state)
        return (True, dep_hash, task_state)

    def __run__(self, resource, callback):
        (ready_to_run, dep_hash, task_state) = self.__init_run__(resource)
        if not ready_to_run:
            if callback:
                callback(self.name, resource, {
                    "executed": False,
                    "success": False,
                    "error_message": "running not possible/needed"
                })
            return

        if not dep_hash:
            dep_hash = self.__get_dep_hash(resource)

        self.running_tasks[resource.id] = {
            "version": self.get_version(),
            "dep": dep_hash,
            "callback": callback
        }
        task_state.running = True
        task_state.save()
        self.__broadcast_task_data__(resource, task_state)
        self.process(resource)

    def __success__(self, resource):
        run_object = self.running_tasks[resource.id]
        task_hash = self.create_hash(resource)
        task_state = self.__get_task_state(resource, self.name)
        old_hash = task_state.hash
        task_state.state = "ok"
        task_state.version = run_object["version"]
        task_state.dep = run_object["dep"]
        task_state.hash = task_hash
        task_state.error_message = ""
        task_state.running = False
        task_state.save()
        callback = run_object["callback"]
        del self.running_tasks[resource.id]
        self.__broadcast_task_data__(resource, task_state)

        if old_hash != task_hash:
            self.runner.clean_up_after(self.name, resource)

        if callback:
            callback(self.name, resource, {
                "executed": True,
                "success": True,
                "error_message": ""
            })

    def __error__(self, resource, error_message, clean_up=False):
        if not isinstance(resource, Resource):
            resource_id = resource
            resource = None
        else:
            resource_id = resource.id

        if resource is not None:
            if clean_up:
                self.__clean_up__(resource)
                self.runner.clean_up_after(self.name, resource)

            task_state = self.__get_task_state(resource, self.name)
            old_state = task_state.state
            if old_state in ["ok", "deprecated"]:
                task_state.state = "deprecated"
            else:
                task_state.state = "error"
            task_state.error_message = error_message
            task_state.running = False
            task_state.save()
            self.__broadcast_task_data__(resource, task_state)

        run_object = self.running_tasks[resource_id]
        del self.running_tasks[resource_id]
        callback = run_object["callback"]
        if callback:
            callback(self.name, resource, {
                "executed": True,
                "success": False,
                "error_message": error_message
            })

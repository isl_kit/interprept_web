#!/usr/bin/env python
# -*- coding: utf-8 -*-

from hashlib import sha256
from functools import partial

from apps.resources.models import Term, Resource
from apps.languages.models import Language
from apps.translation.translation_service import translate_async

from .task import Task


class TranslationTask(Task):

    name = "translation"
    depends_on = {
        "terminology": ["ok", "deprecated"]
    }

    def get_version(self):
        return 1

    def create_hash(self, resource):
        return sha256("").hexdigest()

    def process(self, resource):
        phrases = [x.name for x in Term.objects.filter(terminology_list__resource=resource).order_by("importance").reverse()]
        source_tag = resource.language.tag
        target_tags = [x.tag for x in Language.objects.exclude(pk=resource.language.pk)]
        translate_async(source_tag, target_tags, phrases, False, partial(self.translate_done, resource.id))

    def translate_done(self, resource_id, result):
        try:
            resource = Resource.objects.get(pk=resource_id)
            self.__success__(resource)
        except Resource.DoesNotExist:
            self.__error__(None, "Resource does not exist")

    def clean_up(self, resource):
        return

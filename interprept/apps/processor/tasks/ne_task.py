#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
from hashlib import sha256

from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from apps.common.services.communication.libwrapper import get_ne_version
from apps.resources.models import NeList, NamedEntity, NeType, Resource
from celeryrunner import runner

from .task import Task


class NeTask(Task):

    name = "ne"
    depends_on = {
        "text": ["ok", "deprecated"]
    }

    def get_version(self):
        return get_ne_version()

    def create_hash(self, resource):
        try:
            nelist = resource.nelist
        except ObjectDoesNotExist:
            return ""
        return sha256("|".join([ne.name.lower() for ne in nelist.namedentity_set.all().order_by("name")]).encode("utf-8")).hexdigest()

    def process(self, resource):
        runner.start("interprept.ne.tasks.create_ne_list", args=[resource.get_text_content(), resource.language.tag], meta={"resource_id": resource.id}, callback=self.ne_done)

    def ne_done(self, tid, result, meta, error):
        resource_id = meta["resource_id"]
        try:
            resource = Resource.objects.get(pk=resource_id)
        except Resource.DoesNotExist:
            self.__error__(None, "Resource does not exist")
            return

        try:
            if error:
                self.__error__(resource, str(error))
            else:
                nes = result["nes"]
                error = result["error"]

                if nes is None:
                    self.__error__(resource, error)
                    return

                NeList.objects.filter(resource=resource).delete()
                with transaction.atomic():
                    ne_list = NeList(resource=resource)
                    ne_list.save()
                    for ne in nes:
                        start_index, end_index = ne["cursor_pos"].split(":")
                        name = ne["name"]
                        type_id = ne["type_id"]
                        try:
                            ne_type = NeType.objects.get(id=type_id)
                            named_entity = NamedEntity(start_index=int(start_index), end_index=int(end_index), name=name, ne_type=ne_type, ne_list=ne_list)
                            named_entity.save()
                        except NeType.DoesNotExist:
                            pass

                self.__success__(resource)
        except:
            self.__error__(resource, traceback.format_exc(), True)

    def clean_up(self, resource):
        NeList.objects.filter(resource=resource).delete()

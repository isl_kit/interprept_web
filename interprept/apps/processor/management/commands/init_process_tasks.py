#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from apps.resources.models import Resource
from apps.processor.process import runner


class Command(BaseCommand):
    help = "initializes process tasks"

    def add_arguments(self, parser):
        parser.add_argument("--langs", dest="languages", default="", help="languages to consider")

    def handle(self, *args, **options):
        if options["languages"]:
            langs = options["languages"].split(",")
            resources = Resource.objects.filter(language__tag__in=langs)
        else:
            resources = Resource.objects.all()
        resources = resources.prefetch_related("resourcetaskstate_set")
        for resource in resources:
            runner.__init_resource__(resource)

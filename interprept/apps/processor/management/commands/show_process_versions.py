#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from apps.resources.models import Resource


class Command(BaseCommand):
    help = "lists statistics of the processing of all resources"

    def add_arguments(self, parser):
        parser.add_argument("--langs", dest="languages", default="", help="languages to consider")

    def handle(self, *args, **options):
        res = {}
        if options["languages"]:
            langs = options["languages"].split(",")
            resources = Resource.objects.filter(language__tag__in=langs)
        else:
            resources = Resource.objects.all()
        resources = resources.prefetch_related("resourcetaskstate_set")
        for resource in resources:
            for task_state in resource.resourcetaskstate_set.all():
                if task_state.task_name not in res:
                    res[task_state.task_name] = {}
                t_res = res[task_state.task_name]
                if task_state.version not in t_res:
                    t_res[task_state.version] = 0
                t_res[task_state.version] += 1
        print res

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from apps.resources.models import Resource


class Command(BaseCommand):
    help = "update task states"

    def add_arguments(self, parser):
        parser.add_argument("--langs", dest="languages", default="", help="languages to consider")
        parser.add_argument("--tasks", dest="task_names", default="", help="names of the tasks to change")
        parser.add_argument("--from", dest="from", default="", help="from state")
        parser.add_argument("--to", dest="to", default="", help="to state")

    def handle(self, *args, **options):
        if options["languages"]:
            langs = options["languages"].split(",")
            resources = Resource.objects.filter(language__tag__in=langs)
        else:
            resources = Resource.objects.all()
        resources = resources.prefetch_related("resourcetaskstate_set")
        res = {}
        for resource in resources:
            if options["task_names"]:
                task_names = options["task_names"].split(",")
                resourcestates = resource.resourcetaskstate_set.filter(task_name__in=task_names, state=options["from"])
            else:
                resourcestates = resource.resourcetaskstate_set.filter(state=options["from"])
            resourcestates.update(state=options["to"])
            for task_state in resourcestates:
                if task_state.task_name not in res:
                    res[task_state.task_name] = 0
                res[task_state.task_name] += 1
        print res

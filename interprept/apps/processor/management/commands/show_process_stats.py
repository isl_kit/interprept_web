#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from apps.resources.models import Resource


class Command(BaseCommand):
    help = "lists statistics of the processing of all resources"

    def add_arguments(self, parser):
        parser.add_argument("--langs", dest="languages", default="", help="languages to consider")

    def handle(self, *args, **options):
        if options["languages"]:
            langs = options["languages"].split(",")
            resources = Resource.objects.filter(language__tag__in=langs)
        else:
            resources = Resource.objects.all()
        resources = resources.prefetch_related("resourcetaskstate_set")
        res = {}
        for resource in resources:
            for task_state in resource.resourcetaskstate_set.all():
                if task_state.task_name not in res:
                    res[task_state.task_name] = {}
                if task_state.state not in res[task_state.task_name]:
                    res[task_state.task_name][task_state.state] = 0
                res[task_state.task_name][task_state.state] += 1
        print res

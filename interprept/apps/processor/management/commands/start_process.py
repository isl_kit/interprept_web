#!/usr/bin/env python
# -*- coding: utf-8 -*-

from time import sleep

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count

from apps.resources.models import Resource
from apps.processor.process import runner


class Command(BaseCommand):
    help = "lists statistics of the processing of all resources"

    def add_arguments(self, parser):
        parser.add_argument("--langs", dest="languages", default="", help="languages to consider")
        parser.add_argument("--tasks", dest="task_names", default="", help="names of the tasks to change")
        parser.add_argument("--state", dest="state", default="", help="limit to tasks with state")
        parser.add_argument("--only-parldocs", dest="only_parldocs", default=False, action="store_true", help="limit to synced parliament documents")

    def handle(self, *args, **options):
        if options["task_names"]:
            task_names = options["task_names"].split(",")
        else:
            task_names = ["text", "ne", "terminology", "translation"]

        if options["languages"]:
            self.lan = options["languages"].split(",")
        else:
            self.lan = None

        self.current_state = options["state"] if options["state"] else None
        self.only_parldocs = options["only_parldocs"]
        self.length = 0
        self.counter = 0

        for task_name in task_names:
            self.run(task_name)
            while self.counter < self.length:
                sleep(1)

    def run(self, task_name):
        if self.only_parldocs:
            if self.current_state:
                resources = Resource.objects.annotate(num_doc=Count("parldocument")).filter(num_doc__gt=0, resourcetaskstate__task_name=task_name, resourcetaskstate__state=self.current_state)
            else:
                resources = Resource.objects.annotate(num_doc=Count("parldocument")).filter(num_doc__gt=0)
        else:
            if self.current_state:
                resources = Resource.objects.filter(resourcetaskstate__task_name=task_name, resourcetaskstate__state=self.current_state)
            else:
                resources = Resource.objects.all()
        if self.lan is not None:
            resources = resources.filter(language__tag__in=self.lan)
        #print "RUN TASK %s (%d, %d)" % (task_name, resources.count())
        self.length = resources.count()
        self.counter = 0
        for resource in resources:
            runner.run_single(resource, task_name, self.__done)

    def __done(self, task_name, resource, status):
        self.counter += 1
        msg = task_name.upper() + " DONE (" + str(self.counter) + "/" + str(self.length) + "): "
        if status["success"]:
            print msg + "SUCCESS"
        elif not status["executed"]:
            print msg + "SKIPPED"
            print "  --> " + status["error_message"]
        else:
            print msg + "ERROR"
            print "  --> " + status["error_message"]

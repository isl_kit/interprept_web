#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime

from django.core.management.base import BaseCommand, CommandError

from apps.parliament.db_sync import sync

class Command(BaseCommand):
    help = "crawls the parliament website and syncs content"

    def add_arguments(self, parser):
        parser.add_argument("--langs", dest="language_tags", default="", help="languages to consider")
        parser.add_argument("--start", dest="start", default="", help="start date YYYY-MM-DD")
        parser.add_argument("--end", dest="end", default="", help="end date YYYY-MM-DD")
        parser.add_argument("--inc", dest="inc", default="", help="only execute these tasks")
        parser.add_argument("--exc", dest="exc", default="", help="do not execute these tasks")

    def handle(self, *args, **options):
        tasks = ["org", "mep", "rep", "ses", "con", "com"]
        start = None
        end = None
        inc = None
        exc = None
        language_tags = None
        if options["language_tags"]:
            language_tags = options["language_tags"].split(",")
        if options["start"]:
            start = datetime.date(*[int(x) for x in options["start"].split("-")])
        if options["end"]:
            end = datetime.date(*[int(x) for x in options["end"].split("-")])
        if options["inc"]:
            if options["exc"]:
                raise Exception("'exc' and 'inc' cannot be used at the same time")
            inc = options["inc"].split(",")
            for i in inc:
                if i not in tasks:
                   raise Exception("unsupported task '" + i + "'")
        if options["exc"]:
            if options["inc"]:
                raise Exception("'exc' and 'inc' cannot be used at the same time")
            exc = options["exc"].split(",")
            for i in exc:
                if i not in tasks:
                   raise Exception("unsupported task '" + i + "'")

        if inc is not None:
            tasks = inc
        elif exc is not None:
            tasks = [x for x in tasks if x not in exc]

        sync(tasks, start, end, language_tags)

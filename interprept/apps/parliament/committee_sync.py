#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import transaction
from django.core.files.base import ContentFile
from django.core.exceptions import ObjectDoesNotExist

from .models import SyncHash
from .webcrawler import download_doc
from apps.speeches.models import CommitteeMeeting, ParlDocument, Organ
from apps.languages.models import Language
from .sync_log import log_create
from .sync_helper import set_uhash, get_uhash, sync_property
from apps.resources.db_handler import create_resource


def create_parldocument(comm_meeting, doc_type, link, language, organ):
    doc_bytes = download_doc(link)
    doc_file = ContentFile(bytearray(doc_bytes))

    language_group = None
    parldocs = ParlDocument.objects.filter(committeemeeting=comm_meeting)
    if parldocs.count() > 0:
        language_group = parldocs[0].resource.language_group
    resource_result = create_resource(doc_file, doc_file.size, language, language_group)
    resource = resource_result["resource"]
    created = resource_result["created"]

    parl_doc = None
    if not created:
        try:
            parl_doc = resource.parldocument
            parl_doc.doc_type = doc_type
            parl_doc.language = language
            parl_doc.save()
            if not parl_doc.organs.filter(id=organ.id).exists():
                parl_doc.organs.add(organ)
        except ObjectDoesNotExist:
            parl_doc = None

    if not parl_doc:
        parl_doc = ParlDocument(doc_type=doc_type, title="", a7_no="", language=language, pe_no="", fdr_no="", resource=resource)
        parl_doc.save()
        parl_doc.organs.add(organ)

    if not comm_meeting.parl_documents.filter(id=parl_doc.id).exists():
        comm_meeting.parl_documents.add(parl_doc)
    return parl_doc


def create_committeemeeting(title, date, organ, agenda_no, ref, doc_type):
    comm_meeting = CommitteeMeeting(title=title, date=date, organ=organ, agenda_no=agenda_no, ref=ref.lower(), doc_type=doc_type)
    comm_meeting.save()
    log_create(get_uhash(), comm_meeting)
    return comm_meeting


@transaction.atomic
def sync_doc(doc, date, organ):
    agenda_no = doc["agenda_no"]
    ref = doc["ref"]
    doc_type = doc["doc_type"]
    title = doc["title"]
    links = doc["links"]
    comm_meeting = None
    if len(ref) > 0:
        comm_meetings = CommitteeMeeting.objects.filter(organ=organ, date=date, ref=ref.lower())
        if comm_meetings.count() > 0:
            comm_meeting = comm_meetings[0]
    else:
        comm_meetings = CommitteeMeeting.objects.filter(organ=organ, date=date, ref="", parl_documents__title=title)
        if comm_meetings.count() > 0:
            comm_meeting = comm_meetings[0]

    if not comm_meeting:
        comm_meeting = create_committeemeeting(title, date, organ, agenda_no, ref, doc_type)
    else:
        save_cm = False
        save_cm = sync_property(comm_meeting, "agenda_no", agenda_no) or save_cm
        save_cm = sync_property(comm_meeting, "ref", ref) or save_cm
        save_cm = sync_property(comm_meeting, "title", title) or save_cm
        if save_cm:
            comm_meeting.save()

    for tag, link in links.iteritems():
        try:
            language = Language.objects.get(tag=tag.lower())
        except Language.DoesNotExist:
            continue

        parldocs = ParlDocument.objects.filter(committeemeeting=comm_meeting, language=language)
        if parldocs.count() == 0:
            create_parldocument(comm_meeting, doc_type, link, language, organ)



@transaction.atomic
def sync_committee(label, data):

    print "SYNC: " + label
    try:
        organ = Organ.objects.get(label__iexact=label)
    except Organ.DoesNotExist:
        print "!!!ERROR: Organ does not exist"
        return

    sync_hash = SyncHash(sync_type=data["sync_key"], uhash=data["uhash"])
    sync_hash.save()
    set_uhash(sync_hash)

    date = data["date"]
    docs = data["docs"]

    for idx, doc in enumerate(docs):
        print "\t" + str(idx) + "/" + str(len(docs))
        sync_doc(doc, date, organ)


def sync_committees(committees):
    for label, results in committees.iteritems():
        for res in results:
            sync_committee(label, res)

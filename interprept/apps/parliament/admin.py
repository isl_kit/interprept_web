#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import SyncHash, SyncLog

admin.site.register(SyncHash)
admin.site.register(SyncLog)

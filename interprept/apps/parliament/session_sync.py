#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import re

from django.db import transaction

from .models import SyncHash
from apps.speeches.models import Session, SessionLanguage, Procedure, ProcedureLanguage, ProcedureGroup, ProcedureGroupLanguage, Organ, Person, ParlDocument
from apps.languages.models import Language
from .sync_log import log_create, log_delete, log_modify
from .sync_helper import set_uhash, get_uhash, sync_property


def find_matching_session(sessions_test, session):
    start_time = session.start_time
    end_time = session.end_time
    session_title = session.title

    matched_by_time = [i for i in sessions_test if i["start_time"] == start_time and i["end_time"] == end_time]

    if len(matched_by_time) == 1:
        return matched_by_time[0]
    elif len(matched_by_time) > 1:
        matched_by_time_and_title = [i for i in matched_by_time if i["title"].lower() == session_title.lower()]
        if len(matched_by_time_and_title) == 1:
            return matched_by_time_and_title[0]
        else:
            return None

    matched_by_title = [i for i in sessions_test if i["title"].lower() == session_title.lower()]

    if len(matched_by_title) == 1:
        return matched_by_title[0]

    return None

def create_organ(label):
    organ = Organ(label=label,name="unknown organ")
    organ.save()
    log_create(get_uhash(), organ)
    return organ

def create_person(name):
    person = Person(pid=0,name=name,is_active=False,is_mep=False)
    person.save()
    log_create(get_uhash(), person)
    return person


def create_procedure_language(procedure, title, language):
    procedurelanguage = ProcedureLanguage(procedure=procedure, title=title, language=language)
    procedurelanguage.save()
    log_create(get_uhash(), procedurelanguage)
    return procedurelanguage

def sync_procedure(procedure, procedure_test):
    save_procedure = False
    title = procedure_test["title"]
    position = procedure_test["position"]
    a7_no = procedure_test["doc"]
    languages_test = procedure_test["trans"]
    organs_text = procedure_test["organs"]
    rapporteurs_text = procedure_test["persons"]

    save_procedure = sync_property(procedure,"title", title) or save_procedure
    save_procedure = sync_property(procedure,"position", position) or save_procedure

    for tag, trans_obj in languages_test.iteritems():
        foreign_title = trans_obj["title"]
        language = Language.objects.get(tag=tag)
        procedurelanguages = procedure.procedurelanguage_set.filter(language=language)
        if procedurelanguages.count() > 0:
            procedurelanguage = procedurelanguages[0]
            if sync_property(procedurelanguage, "title", foreign_title) == True:
                procedurelanguage.save()
        else:
            create_procedure_language(procedure, foreign_title, language)

    organs_test = []
    for organ_text in organs_text:
        organs = Organ.objects.filter(label=organ_text)
        if organs.count() > 0:
            organ = organs[:1].get()
        else:
            organ = create_organ(organ_text)
        organs_test.append(organ)

    persons_test = []
    for rapporteur_text in rapporteurs_text:
        persons = Person.objects.filter(lowercase_name=rapporteur_text.lower())
        if persons.count() > 0:
            person = persons[:1].get()
        else:
            person = create_person(rapporteur_text)
        persons_test.append(person)

    persons = procedure.authors.all()
    for person in persons:
        if person not in persons_test:
            save_procedure = True
            procedure.authors.remove(person)
        else:
            persons_test.remove(person)
    for person_test in persons_test:
        save_procedure = True
        procedure.authors.add(person_test)

    organs = procedure.organs.all()
    for organ in organs:
        if organ not in organs_test:
            save_procedure = True
            procedure.organs.remove(organ)
        else:
            organs_test.remove(organ)
    for organ_test in organs_test:
        save_procedure = True
        procedure.organs.add(organ_test)

    if a7_no is not None:
        parl_docs = ParlDocument.objects.filter(a7_no=a7_no)
        for parl_doc in parl_docs:
            if not procedure.parl_documents.filter(id=parl_doc.id).exists():
                save_procedure = True
                procedure.parl_documents.add(parl_doc)

    return save_procedure


def create_procedure(session, title, position):
    procedure = Procedure(session=session, title=title, position=position)
    procedure.save()
    log_create(get_uhash(), procedure)
    return procedure

def create_group(title, langs):
    group = ProcedureGroup(title=title)
    group.save()
    for tag, trans_obj in langs.iteritems():
        foreign_title = trans_obj["title"]
        create_procedure_group_language(group, foreign_title, Language.objects.get(tag=tag))
    log_create(get_uhash(), group)
    return group

def create_procedure_group_language(proceduregroup, title, language):
    proceduregrouplanguage = ProcedureGroupLanguage(proceduregroup=proceduregroup, title=title, language=language)
    proceduregrouplanguage.save()
    log_create(get_uhash(), proceduregrouplanguage)
    return proceduregrouplanguage

def create_session_language(session, title, language):
    sessionlanguage = SessionLanguage(session=session, title=title, language=language)
    sessionlanguage.save()
    log_create(get_uhash(), sessionlanguage)
    return sessionlanguage

def sync_groups(session, session_test):
    groups_test = session_test["groups"]

    for group_test in groups_test:
        title = group_test["title"]
        groups = ProcedureGroup.objects.filter(lowercase_title=title.lower())
        if groups.count() > 0:
            for group in groups:
                for tag, trans_obj in group_test["trans"].iteritems():
                    foreign_title = trans_obj["title"]
                    proceduregrouplanguages = group.proceduregrouplanguage_set.filter(language__tag=tag)
                    if proceduregrouplanguages.count() > 0:
                        proceduregrouplanguage = proceduregrouplanguages[0]
                        if sync_property(proceduregrouplanguage,"title", foreign_title) == True:
                            proceduregrouplanguage.save()
                    else:
                        create_procedure_group_language(group, foreign_title, Language.objects.get(tag=tag))
        else:
            create_group(title, group_test["trans"])

def sync_procedures(session, session_test):
    procedures_test = session_test["points"]
    groups_test = session_test["groups"]
    ids = []
    for procedure_test in procedures_test:
        title = procedure_test["title"]
        position = procedure_test["position"]
        procedure = None

        match_in_session_by_title = Procedure.objects.filter(session=session, lowercase_title=title.lower())
        if match_in_session_by_title.count() == 1:
            procedure = match_in_session_by_title[0]
        else:
            match_by_title_without_session = Procedure.objects.filter(session=None, lowercase_title=title.lower())
            if match_by_title_without_session.count() == 1:
                procedure = match_by_title_without_session[0]

        if procedure == None:
            procedure = create_procedure(session, title, position)

        ids.append(procedure.id)
        save_procedure = sync_property(procedure, "session", session)
        save_procedure = sync_procedure(procedure, procedure_test) or save_procedure

        group_test = [i for i in groups_test if position in i["positions"]]
        if len(group_test) > 0:
            group_title = group_test[0]["title"]
            group_langs = group_test[0]["trans"]
            group = procedure.group

            if group is None or group.lowercase_title != group_title.lower():
                groups = ProcedureGroup.objects.filter(lowercase_title=group_title.lower())

                found = False
                for group in groups:
                    if group.procedure_set.count() == 0 or group.procedure_set.filter()[:1].get().session == session:
                        found = True
                        break
                if found == False:
                    group = create_group(group_title, group_langs)

                save_procedure = sync_property(procedure, "group", group) or save_procedure

        else:
            save_procedure = sync_property(procedure, "group", None) or save_procedure

        if save_procedure == True:
            procedure.save()

    procedures_left_over = session.procedure_set.exclude(id__in=ids)

    for procedure in procedures_left_over:
        sync_property(procedure, "session", None)
        procedure.save()



def sync_session(session, session_test):
    save_session = False
    save_session = sync_property(session, "title", session_test["title"]) or save_session
    save_session = sync_property(session, "start_time", session_test["start_time"]) or save_session
    save_session = sync_property(session, "end_time", session_test["end_time"]) or save_session

    for tag, trans_obj in session_test["trans"].iteritems():
        foreign_title = trans_obj["title"]
        sessionlanguages = session.sessionlanguage_set.filter(language__tag=tag)
        if sessionlanguages.count() > 0:
            sessionlanguage = sessionlanguages[0]
            if sync_property(sessionlanguage,"title", foreign_title) == True:
                sessionlanguage.save()
        else:
            create_session_language(session, foreign_title, Language.objects.get(tag=tag))

    if save_session == True:
        session.save()

    sync_groups(session, session_test)
    sync_procedures(session, session_test)



def create_session(title, date, start_time, end_time):
    session = Session(title=title, date=date, start_time=start_time, end_time=end_time)
    session.save()
    log_create(get_uhash(), session)
    return session

def remove_session(session):
    log_delete(get_uhash(), session)
    session.delete()

@transaction.atomic
def sync_single_session(session_test_day):

    uhash = session_test_day["uhash"]
    date = session_test_day["date"]

    sync_hash = SyncHash(sync_type=date.strftime("%Y-%m-%d"), uhash=uhash)
    sync_hash.save()

    sessions_test = session_test_day["sessions"]

    set_uhash(sync_hash)

    sessions = Session.objects.filter(date=date)
    for session in sessions:
        session_test = find_matching_session(sessions_test, session)

        if session_test is not None:
            sync_session(session, session_test)
            sessions_test.remove(session_test)
        else:
            remove_session(session)

    for session_test in sessions_test:
        session = create_session(session_test["title"], date, session_test["start_time"], session_test["end_time"])
        sync_session(session, session_test)

def sync_sessions(sessions_test_day):

    for session_test_day in sessions_test_day:
        sync_single_session(session_test_day)

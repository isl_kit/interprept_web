# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SyncHash',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('performed', models.DateTimeField(editable=False)),
                ('sync_type', models.CharField(max_length=20)),
                ('uhash', models.CharField(max_length=64)),
            ],
            options={
                'get_latest_by': 'performed',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SyncLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('performed', models.DateTimeField(editable=False)),
                ('changes', models.TextField()),
                ('sync_hash', models.ForeignKey(to='parliament.SyncHash')),
            ],
            options={
                'get_latest_by': 'performed',
            },
            bases=(models.Model,),
        ),
    ]

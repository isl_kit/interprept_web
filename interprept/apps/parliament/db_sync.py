#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import datetime
import traceback

from django.utils import timezone

from apps.speeches.models import Organ, Person, ProcedureGroup, Procedure
from .webcrawler import get_sessions, get_meps, get_organs, get_reports, get_report_document
from .webcrawler_committee import get_committees
from .organ_sync import sync_organs
from .mep_sync import sync_meps
from .session_sync import sync_sessions
from .committee_sync import sync_committees
from .report_sync import sync_reports, convert_reports
from .models import SyncHash

'''
organs = {
    "update_required": True,
    "sync_hash": sync_hash,
    "result": {
        "ORG1": {
            "label": "ORG1",
            "name": "organ number one",
            "names": {
                "en": "organ number one",
                "de": "Verwaltungseinheit eins"
            }
        },
        "ORG3": {
            "label": "ORG3",
            "name": "organ no. two",
            "names": {
                "en": "organ no.  two",
                "es": "organisacion duos"
            }
        }
    }
}
meps = {
    "update_required": True,
    "sync_hash": sync_hash,
    "result": {
        1: {
            "pid": 1,
            "name": u"Jack White"
        },
        4: {
            "pid": 4,
            "name": u"Dave Grohl"
        },
        3: {
            "pid": 3,
            "name": u"Brock Isaac"
        }
    }
}

    sessions = [
        {
            "date": datetime.date(2014, 4, 13),
            "sync_hash": sync_hash,
            "sessions": [
                {
                    "start_time": datetime.time(9, 30),
                    "end_time": datetime.time(11, 30),
                    "title": u"Debates",
                    "trans": {
                        "en": {"title": u"Debates"},
                        "de": {"title": u"Debatten"}
                    },
                    "points": [
                        {
                            "position": 0,
                            "title": u"Mammals",
                            "trans": {
                                "en": {"title": u"Mammals"},
                                "de": {"title": u"Säugetiere"}
                            },
                            "doc": u"A7-0199/2013",
                            "organs": ["ITRE"],
                            "persons": [u"Jan Mulder", u"Alyn Smith"]
                        },
                        {
                            "position": 1,
                            "title": u"Birds",
                            "trans": {
                                "en": {"title": u"Birds"},
                                "de": {"title": u"Vögel"}
                            },
                            "doc": u"A7-0199/2013",
                            "organs": ["TRAN"],
                            "persons": [u"Monika Panayotova"]
                        },
                    ],
                    "groups": [

                    ]
                },
                {
                    "start_time": datetime.time(14, 30),
                    "end_time": datetime.time(18, 30),
                    "title": "Votes",
                    "trans": {
                        "en": {"title": u"Votes"},
                        "de": {"title": u"Abstimmungen"}
                    },
                    "points": [

                        {
                            "position": 0,
                            "title": u"Fish",
                            "trans": {
                                "en": {"title": u"Fish"},
                                "de": {"title": u"Fische"}
                            },
                            "doc": u"A7-0130/2014",
                            "organs": ["CULT"],
                            "persons": [u"Peter Liese"]
                        },

                        {
                            "position": 1,
                            "title": u"Reptiles",
                            "trans": {
                                "en": {"title": u"Reptiles"},
                                "de": {"title": u"Reptilien"}
                            },
                            "doc": u"A7-0109/2014",
                            "organs": ["TRAN"],
                            "persons": [u"Alyn Smith"]
                        }
                    ],
                    "groups": [
                        {
                            "title": u"Waterliking",
                            "trans": {
                                "en": {"title": u"Waterliking"},
                                "de": {"title": u"Wassermögend"}
                            },
                            "positions": [0, 1]
                        }
                    ]
                }
            ]
        }
    ]
'''
from django.core.serializers.json import DjangoJSONEncoder

def sync(tasks=["org", "mep", "rep", "ses", "com", "con"], start=None, end=None, lan=None):

    try:
        if "org" in tasks:
            organs = get_organs()
            if organs["update_required"] == True:
                sync_organs(organs["result"], organs["uhash"])

        if "mep" in tasks:
            meps = get_meps()
            if meps["update_required"] == True:
                sync_meps(meps["result"], meps["uhash"])

        if "rep" in tasks:
            reports = get_reports(start, end)
            sync_reports(reports)

        if "ses" in tasks:
            sessions = get_sessions(start, end)
            sync_sessions(sessions)

        if "com" in tasks:
            organ_labels = [x.label for x in Organ.objects.all()]
            coms = get_committees(organ_labels, start, end)
            sync_committees(coms)

        if "con" in tasks:
            convert_reports(lan)
    except:
        print "Unexpected error:", traceback.format_exc()

    #raise Exception("just to not put anything into db")
#    if sessions["update_required"] == True:
#        sync_sessions(sessions["result"], sessions["sync_hash"])

    #sync_persons(points)
    #sync_organs(points)


'''
def sync_persons(points):
    done = {}
    for point in points:
        if point["is_group"] == True:
            sync_persons(point["points"])
            continue
        report = point["report"]
        if report is None:
            continue
        authors = report["authors"]
        authors_done = []
        for author in authors:
            name = author["name"]
            if name not in done:
                done[name] = sync_person(name)
            authors_done.append(done[name])
        report["authors"] = authors_done

def sync_organs(points):
    done = {}
    for point in points:
        if point["is_group"] == True:
            sync_organs(point["points"])
            continue
        report = point["report"]
        if report is None:
            continue
        organ = report["organ"]
        name = organ["name"]
        if name not in done:
            done[name] = sync_organ(name)
        report["organ"] = done[name]

def sync_person(name):
    person, created = Person.objects.get_or_create(name=name)
    return person

def sync_organ(name):
    organ, created = Organ.objects.get_or_create(name=name)
    return organ
'''

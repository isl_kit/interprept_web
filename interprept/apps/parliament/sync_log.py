#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.core import serializers

from .models import SyncLog

'''
{
    "action": "create",
    "data": {

    }
}
'''

def log(sync_hash, log_data):
    sync_log = SyncLog(changes=log_data, sync_hash=sync_hash)
    sync_log.save()


def log_create(sync_hash, db_object):
    log_data = {
        "action": "create",
        "model": db_object.__class__.__name__,
        "data": serializers.serialize('json', [db_object])[1:-1]
    }
    log(sync_hash, log_data)

def log_delete(sync_hash, db_object):
    log_data = {
        "action": "delete",
        "model": db_object.__class__.__name__,
        "data": serializers.serialize('json', [db_object])[1:-1]
    }
    log(sync_hash, log_data)

def log_modify(sync_hash, db_object, name, old_value, new_value):
    log_data = {
        "action": "modify",
        "model": db_object.__class__.__name__,
        "pk": db_object.pk,
        "data": {}
    }
    log_data["data"][name] = {
        "old": unicode(old_value),
        "new": unicode(new_value)
    }
    log_data["data"] = json.dumps(log_data["data"])
    log(sync_hash, log_data)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import datetime
import requests
import hashlib

from .models import SyncHash


def get_dates(organ_label, start, end):
    if start is None:
        start = datetime.date.today() - datetime.timedelta(14)
    if end and start > end:
        start = end - datetime.timedelta(14)
    start_year = start.year
    end_year = end.year if end else 9999
    url = "http://www.emeeting.europarl.europa.eu/committees/proxy.do?http://www.ecomback.europarl.europa.eu/ecomback/ws/EMeetingRESTService/agendaArchive&language=EN&organ=" + organ_label
    r = requests.get(url)
    res = []
    try:
        json_r = json.loads(r.text)
    except ValueError:
        return res
    for year_item in json_r:
        if year_item["year"] < start_year or year_item["year"] > end_year:
            continue
        for m in year_item["months"]:
            for d in m["ojs"]:
                date = datetime.datetime.strptime(d["date"], "%d.%m.%Y").date()
                if date < start or (end and date < end):
                    continue
                res.append([d["ojReference"], date])
    return res


def get_committees(organs, start=None, end=None):
    result = {}
    for organ in organs:
        print "get organ: " + organ
        result[organ] = []
        dates = get_dates(organ, start, end)
        for ref, date in dates:
            url = "http://www.emeeting.europarl.europa.eu/committees/proxy.do?http://www.ecomback.europarl.europa.eu/ecomback/ws/EMeetingRESTService/oj&language=EN&reference={}&securedContext=false".format(ref)
            r = requests.get(url)
            try:
                json_r = json.loads(r.text)
            except ValueError:
                continue

            uhash = hashlib.sha256(unicode(r.text).encode("utf-8")).hexdigest()
            try:
                sync_hash = SyncHash.objects.filter(sync_type=ref).latest()
                if sync_hash.uhash == uhash:
                    continue
            except SyncHash.DoesNotExist:
                pass

            docs = []

            result[organ].append({
                "sync_key": ref,
                "date": date,
                "docs": docs,
                "uhash": uhash
            })

            for i, item in enumerate(json_r["items"]):
                print "\titem: {}/{}".format(i, len(json_r["items"]))
                if len(item["documentSets"]) == 0:
                    continue
                for k, document in enumerate(item["documentSets"][0]["documents"]):
                    if not document["geproCode"]:
                        continue
                    print "\t\tdoc: {}/{}".format(k, len(item["documentSets"][0]["documents"]))
                    links = {}
                    doc = {
                        "agenda_no": item["number"] if item["number"] else 0,
                        "ref": document["reference"] if document["reference"] else "",
                        "doc_type": document["geproCode"],
                        "title": document["title"],
                        "links": links
                    }
                    docs.append(doc)
                    if document["documentLinks"]:
                        for link in document["documentLinks"]:
                            if link["type"] != "pdf":
                                continue
                            for lang in link["languages"]:
                                if lang["url"] is not None:
                                    links[lang["code"]] = lang["url"]
    return result

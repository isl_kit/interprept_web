#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import transaction

from .models import SyncHash
from apps.speeches.models import Person
from .sync_log import log_create
from .sync_helper import set_uhash, get_uhash, sync_property


def create_person(pid, name):
    person = Person(pid=pid,name=name)
    person.save()
    log_create(get_uhash(), person)
    return person

@transaction.atomic
def sync_meps(meps, uhash):
    sync_hash = SyncHash(sync_type="P", uhash=uhash)
    sync_hash.save()
    set_uhash(sync_hash)
    persons = Person.objects.all()
    for person in persons:
        save_person = False
        pid = person.pid
        if pid in meps:
            mep = meps[pid]
            save_person = sync_property(person, "name", mep["name"]) or save_person
            save_person = sync_property(person, "is_mep", True) or save_person
            save_person = sync_property(person, "is_active", True) or save_person
            del meps[pid]
        elif pid > 0:
            save_person = sync_property(person, "is_active", False) or save_person
        else:
            matched_meps = [i for i in meps.values() if i["name"].lower() == person.lowercase_name]
            if len(matched_meps) > 0:
                mep = matched_meps[0]
                save_person = sync_property(person, "pid", mep["pid"]) or save_person
                save_person = sync_property(person, "is_mep", True) or save_person
                save_person = sync_property(person, "is_active", True) or save_person
                del meps[mep["pid"]]
            else:
                save_person = sync_property(person, "is_active", False) or save_person

        if save_person == True:
            person.save()

    for pid, mep in meps.iteritems():
        create_person(mep["pid"], mep["name"])
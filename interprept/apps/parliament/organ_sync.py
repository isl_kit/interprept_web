#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import transaction

from .models import SyncHash
from apps.speeches.models import Organ
from apps.languages.models import Language
from .sync_log import log_create, log_delete
from .sync_helper import set_uhash, get_uhash, sync_property

def remove_organ(organ):
    log_delete(get_uhash(), organ)
    #organ.organlanguage_set.all().delete()
    #organ.delete()

def create_organ(label, name):
    organ = Organ(label=label,name=name)
    organ.save()
    log_create(get_uhash(), organ)
    return organ

def remove_organlanguage(organlanguage):
    log_delete(get_uhash(), organlanguage)
    organlanguage.delete()

def create_organlanguage(organ, tag, name):
    language = Language.objects.get(tag=tag)
    organlanguage = organ.organlanguage_set.create(name=name, language=language)
    log_create(get_uhash(), organlanguage)

@transaction.atomic
def sync_organs(orgs, uhash):
    sync_hash = SyncHash(sync_type="O", uhash=uhash)
    sync_hash.save()
    set_uhash(sync_hash)
    organs = Organ.objects.all()
    organs_to_delete = []
    for organ in organs:
        save_organ = False
        label = organ.label
        if label in orgs:
            org = orgs[label]
            save_organ = sync_property(organ, "name", org["name"]) or save_organ
            organlanguages = organ.organlanguage_set.all()
            for organlanguage in organlanguages:
                save_organlanguage = False
                tag = organlanguage.language.tag
                if tag in org["names"]:
                    save_organlanguage = sync_property(organlanguage, "name", org["names"][tag]) or save_organlanguage
                    del org["names"][tag]
                else:
                    remove_organlanguage(organlanguage)
                if save_organlanguage:
                    organlanguage.save()
            for tag, name in org["names"].iteritems():
                create_organlanguage(organ, tag, name)
            del orgs[label]
        else:
            remove_organ(organ)
        if save_organ == True:
            organ.save()

    for label, org in orgs.iteritems():
        organ = create_organ(label, org["name"])
        for tag, name in org["names"].iteritems():
            create_organlanguage(organ, tag, name)

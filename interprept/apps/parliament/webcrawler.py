#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import datetime
import re
import hashlib
import itertools
import pytz
from pytz import timezone
from xml.etree import ElementTree

import requests
from pyquery import PyQuery as pq

from .models import SyncHash
from apps.languages.models import Language
from apps.speeches.helper import transform_report_key

time_regex = "([0-9]{2}):([0-9]{2}).-.([0-9]{2}):([0-9]{2})"
stime_regex = "([0-9]{2}):([0-9]{2})"
key_regex = "\(\s*([A-Z][0-9]-[0-9]{4}/[0-9]{4})\s*\)$"
key_regex_soft = "A[0-9]-[0-9]{4}/[0-9]{4}"
type_regex = "type_([A-Z]{2})"
body_regex = "\<body[^\>]*\>.*\</body\>"
#session_id_regex = "sessionid=[0-9A-F]{32}(\.node[0-9]+)?"

TZ = timezone("Europe/Brussels")

def unify(seq):
    seen = set()
    seen_add = seen.add
    return [ x for x in seq if x not in seen and not seen_add(x)]

class NotImplementedException(Exception):
    pass

def throw_error(point, message):
    raise NotImplementedException("ERROR: " + message + "\r\n" + point.text())


def extract_time(text, date):
    m = re.search(time_regex, text)
    if m:
        start_hour = int(m.group(1))
        start_minute = int(m.group(2))
        end_hour = int(m.group(3))
        end_minute = int(m.group(4))
        if start_hour > 23:
            start_hour = 0
        if end_hour > 23:
            end_hour = 0
        start_time = TZ.localize(datetime.datetime(date.year, date.month, date.day, start_hour, start_minute))
        end_time = TZ.localize(datetime.datetime(date.year, date.month, date.day, end_hour, end_minute))
    else:
        m = re.search(stime_regex, text)
        if m:
            start_hour = int(m.group(1))
            start_minute = int(m.group(2))
            if start_hour > 23:
                start_hour = 0
            start_time = TZ.localize(datetime.datetime(date.year, date.month, date.day, start_hour, start_minute))
            end_time = None
        else:
            start_time = None
            end_time = None
    return {
        "start": start_time,
        "end": end_time,
    }


def get_pq_body(text):
    re_res = re.search(body_regex, text, re.S)
    if re_res is not None:
        return pq(re_res.group(0), parser="html")
    else:
        return pq("")

def loop_part_sessions(callback_page, callback_finish, start, end):
    today = start if start is not None else datetime.date.today()
    date = datetime.date(today.year, today.month, 1)
    def incr_month(date):
        if date.month == 12:
            month = 1
            year = date.year + 1
        else:
            month = date.month + 1
            year = date.year
        return datetime.date(year, month, 1)

    counter = 0
    while counter < 12:
        date_str = date.strftime("%Y-%m")
        payload = {
            "language": "EN",
            "reference": date_str
        }
        r = requests.get("http://www.europarl.europa.eu/sides/indexPartSession.do", params=payload)

        d = get_pq_body(r.text)
        minitabs = d.find(".minitabs")
        if len(minitabs) > 0:
            break
        date = incr_month(date)
        counter += 1


    result = []

    if counter == 12:
        return callback_finish(result)

    while True:

        result.append(callback_page(d, date_str))

        href = minitabs("li:last > a").attr("href")
        if "/sides/indexPartSession.do" in href:
            date_str = re.search("[\?|&]reference=([^&]+)", href).group(1)
            if end is not None:
                date_str_split = date_str.split("-")
                test_date = datetime.date(int(date_str_split[0]), int(date_str_split[1]), 1)
                if test_date > end:
                    break
            r = requests.get("http://www.europarl.europa.eu" + href)
            d = get_pq_body(r.text)
            minitabs = d.find(".minitabs")
        else:
            break

    return callback_finish(result)

def get_report_document(key, tag):
    url_key = transform_report_key(key)
    payload = {
        "pubRef": "-//EP//NONSGML+REPORT+" + url_key + "+0+DOC+PDF+V0//" + tag.upper()
    }
    r = requests.get("http://www.europarl.europa.eu/sides/getDoc.do", params=payload)
    return r.content

def download_doc(link):
    r = requests.get(link)
    return r.content

def get_report_keys(start, end):

    #return ["A7-0301/2013"]

    print "get keys";
    def handle_page(d, ref):
        minitabs = d.find(".minitabs a[href^='/sides/getDoc.do']")
        dates = []
        matches = []
        for minitab in minitabs:
            date_text = pq(minitab).text()
            dates.append(datetime.datetime.strptime(date_text, "%d %B %Y").date())
        sittings = d.find("td[class^='td_sitting']")
        for idx, sitting in enumerate(sittings):
            if start is not None and start > dates[idx]:
                continue
            if end is not None and end < dates[idx]:
                continue
            matches = matches + re.findall(key_regex_soft, pq(sitting).text())

        return matches

    def handle_finish(result):
        return unify(list(itertools.chain.from_iterable(result)))

    return loop_part_sessions(handle_page, handle_finish, start, end)


def get_report_title_from_box_header(box_header):
    title_ta = unicode(box_header.find("td.title_TA").text())
    subt_lev1 = box_header.find("td.doc_subtitle_level1")
    subt_lev1_p = subt_lev1("p")
    if len(subt_lev1_p) > 0:
        title = subt_lev1_p.eq(0).text()
    else:
        title = subt_lev1.contents()[0]

    re_pre = re.match("^[^" + unichr(160) + "]+", title_ta)
    if re_pre is not None:
        pre_title = re_pre.group(0)
    else:
        pre_title = "UNKNOWN"

    return pre_title + " " + title

def get_pea_from_box_header(box_header):
    pea_doc = box_header.find("td.numPEA_doc")
    if len(pea_doc) > 0:
        return pea_doc.eq(0).text()
    else:
        return ""

def request_report_page(key, tag):
    url_key = transform_report_key(key)
    payload = {
        "type": "REPORT",
        "reference": url_key,
        "language": tag.upper()
    }
    r = requests.get("http://www.europarl.europa.eu/sides/getDoc.do", params=payload)
    return get_pq_body(r.text)

def get_reports(start, end):
    keys = get_report_keys(start, end)
    languages = Language.objects.all()
    results = []

    for key in keys:
        print "REPORT: " + key
        is_shallow = False
        d = request_report_page(key, "en")
        box_header_all = d.find("table.doc_box_header")
        if len(box_header_all) == 0:
            is_shallow = True
            uhash = hashlib.sha256(unicode(d.text()).encode("utf-8")).hexdigest()
        else:
            uhash = hashlib.sha256(unicode(box_header_all.text()).encode("utf-8")).hexdigest()

        try:
            sync_hash = SyncHash.objects.filter(sync_type=key).latest()
            if sync_hash.uhash == uhash:
                continue
        except SyncHash.DoesNotExist:
            pass

        if is_shallow == False:
            box_headers = {
                "en": box_header_all.filter(lambda i: pq(this).attr("cellpadding") == "5").eq(0)
            }

            for language in languages:
                if language.tag == "en":
                    continue
                d = request_report_page(key, language.tag)
                error_doc = d.find("table.error_doc")
                if len(error_doc) > 0:
                    continue
                box_header_all = d.find("table.doc_box_header")
                if len(box_header_all) == 0:
                    continue
                print language.tag
                box_headers[language.tag] = box_header_all.filter(lambda i: pq(this).attr("cellpadding") == "5").eq(0)

            trans = {}

            for tag, box_header in box_headers.iteritems():
                trans[tag] = {
                    "title": unicode(get_report_title_from_box_header(box_header)),
                    "pea": unicode(get_pea_from_box_header(box_header))
                }


            box_header = box_headers["en"]

            title = trans["en"]["title"]

            doc_title = box_header.find("td.doc_title").text().strip()
            date = datetime.datetime.strptime(doc_title, "%d %B %Y").date()

            subt_lev2 = box_header.find("table.doc_subtitle_level2")
            organs = []
            subt_lev2_td = subt_lev2.find("td")
            organs_text = ""
            for p in subt_lev2_td.eq(0)("p"):
                organs_text += pq(p).text().strip()

            organs_text_arr = organs_text.split("Committee")
            organs = [(u"Committee" + x).strip() for x in organs_text_arr if len(x) > 0]

            subt_lev2_td1 = subt_lev2_td.eq(1)
            subt_lev2_td1_p = subt_lev2_td1("p")
            if len(subt_lev2_td1_p) > 0:
                rapporteur_text = subt_lev2_td1_p.eq(0).text()
            else:
                rapporteur_text = subt_lev2_td1.text()

            if rapporteur_text and re.match("^Rapporteur[s]?:", rapporteur_text) is not None:
                rapporteurs = [unicode(x.strip()) for x in rapporteur_text.split(":")[1].strip().split(",")]
                rapporteurs = [x for x in rapporteurs if len(x) > 0]
            else:
                rapporteurs = []
        else:
            date = None
            title = ""
            organs = []
            rapporteurs = []
            trans = {}

        results.append({
            "uhash": uhash,
            "key": key,
            "date": date,
            "title": title,
            "organs": organs,
            "rapporteurs": rapporteurs,
            "trans": trans
        })

    return results


def read_point(point, pos):
    if point.hasClass("expandable") or point.hasClass("expandableFirst"):
        return read_group_point(point, pos)
    else:
        return {
            "groups": [],
            "points": [read_single_point(point, pos)]
        }

def read_group_point(point, pos):
    group_title = point.children(".title").text()
    sub_points = point(".point")
    points = []
    positions = []
    for idx, sub_point in enumerate(sub_points):
        positions.append(pos + idx)
        points.append(read_single_point(pq(sub_point), pos + idx))
    return {
        "groups": [{
            "title": unicode(group_title),
            "positions": positions
        }],
        "points": points
    }


def read_single_point(point, pos):
    point_title = point(".point_title").text()
    docs_num = point(".document_number")
    if len(docs_num) > 0:
        doc_num = docs_num.eq(0).text()
        if re.match(key_regex_soft, doc_num) is None:
            doc_num = None
    else:
        doc_num = None
    reporter = point(".reporter")
    organ_container = reporter(".organ")
    organs = []
    persons = []
    if len(reporter) > 0:
        rep_text = reporter.text()
        if len(organ_container) > 0:
            org_text = organ_container.text()
            organs = [unicode(x.strip()) for x in org_text.split(",")]
            rep_text = rep_text.replace(org_text, "").strip()
        persons = [unicode(x.strip()) for x in rep_text.split(",")]

    return {
        "title": point_title,
        "doc": doc_num,
        "organs": organs,
        "persons": persons,
        "position": pos
    }

def read_sessions(ref, language, compute):
    result = []
    payload = {
        "language": language.tag.upper(),
        "reference": ref
    }
    r = requests.get("http://www.europarl.europa.eu/sides/indexPartSession.do", params=payload)
    d = get_pq_body(r.text)
    sittings = d.find("td[class^='td_sitting']")
    if len(sittings) > 0:
        for comp_item in compute:
            date = comp_item["date"]
            uhash = comp_item["uhash"]
            sessions = []
            sitting = pq(sittings[comp_item["index"]])
            frame_blue = sitting(".frame_blue")
            for frame in frame_blue:
                frame = pq(frame)
                time_span = extract_time(frame.text(), date)
                title = frame(".frame_title").text()
                point = frame.next()
                points = []
                groups = []
                counter = 0
                while len(point) == 1 and not point.hasClass("frame_blue"):
                    point_result = read_point(point, counter)
                    points += point_result["points"]
                    groups += point_result["groups"]
                    counter += len(point_result["points"])
                    point = point.next()

                sessions.append({
                    "start_time": time_span["start"],
                    "end_time": time_span["end"],
                    "title": unicode(title),
                    "points": points,
                    "groups": groups
                })
            result.append({
                "date": date,
                "uhash": uhash,
                "sessions": sessions
            })
        return result
    return None


def extract_session_translations(lang_results):
    result = lang_results["en"]
    for day_idx, day in enumerate(result):
        for session_idx, session in enumerate(day["sessions"]):
            session["trans"] = {}
            for tag, res in lang_results.iteritems():
                session["trans"][tag] = {
                    "title": res[day_idx]["sessions"][session_idx]["title"]
                }

            for point_idx, point in enumerate(session["points"]):
                point["trans"] = {}
                for tag, res in lang_results.iteritems():
                    point["trans"][tag] = {
                        "title": res[day_idx]["sessions"][session_idx]["points"][point_idx]["title"]
                    }
            for group_idx, group in enumerate(session["groups"]):
                group["trans"] = {}
                for tag, res in lang_results.iteritems():
                    group["trans"][tag] = {
                        "title": res[day_idx]["sessions"][session_idx]["groups"][group_idx]["title"]
                    }
    return result

def get_sessions(start, end):

    def handle_page(d, ref):
        minitabs = d.find(".minitabs a[href^='/sides/getDoc.do']")
        dates = []
        compute = []
        for minitab in minitabs:
            date_text = pq(minitab).text()
            dates.append(datetime.datetime.strptime(date_text, "%d %B %Y").date())
        sittings = d.find("td[class^='td_sitting']")
        for idx, sitting in enumerate(sittings):
            if start is not None and start > dates[idx]:
                continue
            if end is not None and end < dates[idx]:
                continue
            date_string = dates[idx].strftime("%Y-%m-%d")
            sitting = pq(sitting)
            uhash = hashlib.sha256(unicode(sitting.text()).encode("utf-8")).hexdigest()

            try:
                sync_hash = SyncHash.objects.filter(sync_type=date_string).latest()
                if sync_hash.uhash == uhash:
                    continue
            except SyncHash.DoesNotExist:
                pass


            compute.append({
                "index": idx,
                "date": dates[idx],
                "uhash": uhash
            })

        return {
            "ref": ref,
            "compute": compute
        }


    def handle_finish(result):
        sessions = []
        languages = Language.objects.all()
        for res in result:
            lang_results = {}
            for language in languages:
                session_result = read_sessions(res["ref"], language, res["compute"])
                if session_result is not None:
                    lang_results[language.tag] = session_result
            sessions += extract_session_translations(lang_results)

        return sessions

    return loop_part_sessions(handle_page, handle_finish, start, end)


def get_organs():
    languages = Language.objects.all()
    mainzones = {}
    full_text = ""
    for language in languages:
        tag = language.tag
        url = "http://www.europarl.europa.eu/committees/" + tag + "/parliamentary-committees.html"
        r = requests.get(url)
        if r.headers["content-language"] != tag:
            continue
        d = pq(r.text)
        mainzones[tag] = d.find("#content_left")
        full_text += mainzones[tag].text()

    uhash = hashlib.sha256(full_text.encode("utf-8")).hexdigest()
    update_required = False
    try:
        sync_hash = SyncHash.objects.filter(sync_type="O").latest()
        if sync_hash.uhash != uhash:
            update_required = True
    except SyncHash.DoesNotExist:
        update_required = True

    result = {}
    if update_required is True:

        for tag, mainzone in mainzones.iteritems():
            commissions = mainzone.find(".boxcontent")
            for commission in commissions:
                commission = pq(commission)
                label = unicode(commission.find(".commission_label").text())
                name = unicode(commission.find("h5 > a").text())
                if label not in result:
                    result[label] = {
                        "label": label,
                        "name": "",
                        "names": {}
                    }
                result[label]["names"][tag] = name
                if tag == "en":
                    result[label]["name"] = name

    return {
        "update_required": update_required,
        "uhash": uhash,
        "result": result
    }

def get_meps():
    payload = {
        "query": "full",
        "filter": "all"
    }
    r = requests.get("http://www.europarl.europa.eu/meps/en/xml.html", params=payload)

    uhash = hashlib.sha256(r.content).hexdigest()

    update_required = False
    try:
        sync_hash = SyncHash.objects.filter(sync_type="P").latest()
        if sync_hash.uhash != uhash:
            update_required = True
    except SyncHash.DoesNotExist:
        update_required = True


    result = {}
    if update_required == True:

        root = ElementTree.fromstring(r.content)
        for mep_xml in root:
            full_name = mep_xml.find("fullName").text
            pid = int(mep_xml.find("id").text)
            result[pid] = {
                "name": unicode(full_name),
                "pid": pid
            }

    return {
        "update_required": update_required,
        "uhash": uhash,
        "result": result
    }

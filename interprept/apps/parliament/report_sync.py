#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from time import sleep
from datetime import datetime

from django.db import transaction
from django.core.files.base import ContentFile
from django.db.models import Count
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist


from .models import SyncHash
from apps.speeches.models import Organ, Person, ParlDocument
from apps.languages.models import Language
from apps.resources.models import Resource, ResourceLanguageGroup
from apps.resources.db_handler import create_resource

from apps.processor.process import runner

from .sync_log import log_create
from .sync_helper import set_uhash, get_uhash, sync_property
from .webcrawler import get_report_document


organ_regex = "^Committee on (the )?"


def create_parldocument(title, published, doc_type, a7_no, language, pe_no):
    doc_bytes = get_report_document(a7_no, language.tag)
    doc_file = ContentFile(bytearray(doc_bytes))

    language_group = None
    parldocs = ParlDocument.objects.filter(a7_no=a7_no)
    if parldocs.count() > 0:
        language_group = parldocs[0].resource.language_group
    resource_result = create_resource(doc_file, doc_file.size, language, language_group)
    resource = resource_result["resource"]
    created = resource_result["created"]
    parl_doc = None
    if not created:
        try:
            parl_doc = resource.parldocument
            parl_doc.a7_no = a7_no
            parl_doc.language = language
        except ObjectDoesNotExist:
            parl_doc = None
    if not parl_doc:
        parl_doc = ParlDocument(doc_type=doc_type, published=published, title=title, a7_no=a7_no, language=language, pe_no=pe_no, fdr_no="", resource=resource)
    parl_doc.save()
    return parl_doc


def create_organ(name):
    #print "create ORGAN: " + name
    organ = Organ(label="-UNKN-", name=name)
    organ.save()
    log_create(get_uhash(), organ)
    return organ


def create_person(name):
    #print "create PERSON: " + name
    person = Person(pid=0, name=name, is_active=False, is_mep=False)
    person.save()
    log_create(get_uhash(), person)
    return person


@transaction.atomic
def sync_report(report_test):

    key = report_test["key"]

    sync_hash = SyncHash(sync_type=key, uhash=report_test["uhash"])
    sync_hash.save()
    set_uhash(sync_hash)

    date = report_test["date"]
    title = report_test["title"]
    organs_text = report_test["organs"]
    rapporteurs_text = report_test["rapporteurs"]
    languages_test = report_test["trans"]
    print "SYNC: " + key
    organs_test = []
    for organ_text in organs_text:
        organ_name = re.sub(organ_regex, "", organ_text)
        organs = Organ.objects.filter(compare_name=re.sub("[^a-z]", "", organ_name.lower()))
        if organs.count() > 0:
            organ = organs[:1].get()
        else:
            organ = create_organ(organ_name)
        organs_test.append(organ)

    persons_test = []
    for rapporteur_text in rapporteurs_text:
        persons = Person.objects.filter(lowercase_name=rapporteur_text.lower())
        if persons.count() > 0:
            person = persons[:1].get()
        else:
            person = create_person(rapporteur_text)
        persons_test.append(person)

    for tag, trans_obj in languages_test.iteritems():
        language = Language.objects.get(tag=tag)
        foreign_title = trans_obj["title"]
        pe_no = trans_obj["pea"]
        save_pd = False
        parldocs = ParlDocument.objects.filter(a7_no=key, language=language)
        if parldocs.count() > 0:
            parl_doc = parldocs[0]
            save_pd = sync_property(parl_doc, "title", foreign_title) or save_pd
            save_pd = sync_property(parl_doc, "published", date) or save_pd
            save_pd = sync_property(parl_doc, "pe_no", pe_no) or save_pd
        else:
            parl_doc = create_parldocument(foreign_title, date, "RR", key, language, pe_no)

        persons = parl_doc.authors.all()
        for person in persons:
            if person not in persons_test:
                save_pd = True
                parl_doc.authors.remove(person)
            else:
                persons_test.remove(person)
        for person_test in persons_test:
            save_pd = True
            parl_doc.authors.add(person_test)

        organs = parl_doc.organs.all()
        for organ in organs:
            if organ not in organs_test:
                save_pd = True
                parl_doc.organs.remove(organ)
            else:
                organs_test.remove(organ)
        for organ_test in organs_test:
            save_pd = True
            parl_doc.organs.add(organ_test)

        if save_pd is True:
            parl_doc.save()


def sync_reports(reports):
    for idx, report in enumerate(reports):
        print str(idx) + "/" + str(len(reports))
        sync_report(report)


class Processor:

    def __init__(self, lan):

        self.lan = lan
        self.counter = 0
        self.length = 0

    def run(self, task_name):
        resources = Resource.objects.annotate(num_doc=Count("parldocument")).filter(num_doc__gt=0, resourcetaskstate__task_name=task_name, resourcetaskstate__state="init")
        if self.lan is not None:
            resources = resources.filter(language__tag__in=self.lan)
        #print "RUN TASK %s (%d, %d)" % (task_name, resources.count())
        self.length = resources.count()
        self.counter = 0
        for resource in resources:
            runner.run_single(resource, task_name, self.__done)

    def __done(self, task_name, resource, status):
        self.counter += 1
        msg = task_name.upper() + " DONE (" + str(self.counter) + "/" + str(self.length) + "): "
        if status["success"]:
            print msg + "SUCCESS"
        elif not status["executed"]:
            print msg + "SKIPPED"
            print "  --> " + status["error_message"]
        else:
            print msg + "ERROR"
            print "  --> " + status["error_message"]


def convert_reports(lan=None):

    proc = Processor(lan)
    proc.run("text")

    while proc.counter < proc.length:
        sleep(1)

    proc.run("ne")

    while proc.counter < proc.length:
        sleep(1)

    proc.run("terminology")

    while proc.counter < proc.length:
        sleep(1)

    proc.run("translation")

    while proc.counter < proc.length:
        sleep(1)

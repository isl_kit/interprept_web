#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone

class SyncHash(models.Model):
    performed = models.DateTimeField(editable=False)
    sync_type = models.CharField(max_length=20)
    uhash = models.CharField(max_length=64)

    def save(self, *args, **kwargs):
        if not self.id:
            self.performed = timezone.now()
        super(SyncHash, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.sync_type + ": " + unicode(timezone.localtime(self.performed).strftime("%A, %d.%m.%Y %H:%M:%S"))

    class Meta:
        app_label = "parliament"
        get_latest_by = "performed"

class SyncLog(models.Model):
    performed = models.DateTimeField(editable=False)
    sync_hash = models.ForeignKey(SyncHash)
    changes = models.TextField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.performed = timezone.now()
        super(SyncLog, self).save(*args, **kwargs)

    def __unicode__(self):
        return unicode(timezone.localtime(self.performed).strftime("%A, %d.%m.%Y %H:%M:%S"))

    class Meta:
        app_label = "parliament"
        get_latest_by = "performed"


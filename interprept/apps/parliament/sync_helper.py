#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .sync_log import log_modify

stored_uhash = None

def set_uhash(uhash):
    global stored_uhash
    stored_uhash = uhash

def get_uhash():
    global stored_uhash
    return stored_uhash


def sync_property(db_object, name, value):
    if getattr(db_object, name) != value:
        log_modify(get_uhash(), db_object, name,  getattr(db_object, name), value)
        setattr(db_object, name, value)
        return True
    return False
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import remove
from os.path import splitext, exists as path_exists,  join as join_path, normpath
import json
from datetime import datetime
import random
import string
#import thread

from django.conf import settings
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Q
from django.db import DatabaseError
from django.views.decorators.csrf import csrf_exempt
from django.middleware.csrf import get_token
from django.core.files.base import ContentFile

from .models import UserDocument, UserDirectory, DocumentType, create_document, create_directory
from apps.speeches.models import ParlDocument
from apps.common.decorators.auth import check_login
from apps.common.decorators.cache import never_ever_cache
from apps.accounts.views import getUserData
from apps.resources.views import get_resource_values

def get_user_document_values(doc):
    return {
        "id": doc.id,
        "type": doc.type,
        "name": doc.name,
        "created": doc.created,
        "modified": doc.modified,
        "user": getUserData(doc.user),
        "is_private": doc.is_private,
        "resource": get_resource_values(doc.resource) if doc.resource else None,
        "parldocument": doc.parldocument_id,
        "terminology_data": json.loads(doc.terminology_data) if doc.terminology_data else None,
        "draft": doc.draft
    }

def get_user_directory_values(directory, include_parent=False):
    parent = get_user_directory_values(directory.parent, True) if include_parent and directory.parent is not None else None

    child_docs = directory.userdocument_set.count()
    child_dirs = directory.userdirectory_set.count()

    result = {
        "id": directory.id,
        "name": directory.name,
        "created": directory.created,
        "modified": directory.modified,
        "is_private": directory.is_private,
        "path": directory.path,
        "user": getUserData(directory.user) if directory.user is not None else None,
        "doc_count": child_docs,
        "dir_count": child_dirs,
        "parent": parent
    }
    return result

class GetAllView(View):
    def get(self, request, *args, **kwargs):
        result = {
            "documents": list(UserDocument.objects.filter(user=request.user).values())
        }
        return HttpResponse(json.dumps(result), content_type="application/json", status=200)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetAllView, self).dispatch(*args, **kwargs)


def get_or_create_shared_dir():
    try:
        d = UserDirectory.objects.get(user=None)
    except UserDirectory.DoesNotExist:
        d = UserDirectory(name="SHARED", is_private=False)
        d.save()
    return d

class GetDirTreeView(View):
    def get(self, request, *args, **kwargs):

        if "dir_id" in request.GET:
            try:
                base = UserDirectory.objects.get(id=request.GET["dir_id"])
                if base.user != request.user and base.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="application/json", status=403)
            except UserDirectory.DoesNotExist:
                return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="application/json", status=404)
        else:
            base = request.user.home_dir


        result = None
        bases = []
        while base is not None:
            bases.insert(0, base)
            base = base.parent

        home = None
        for base in bases:
            if result is None:
                result = get_user_directory_values(base)
                home = result
            else:
                result = filter(lambda child: child["id"] == base.id, result["children"])[0]
            dirs = base.userdirectory_set.all()
            result["children"] = [get_user_directory_values(directory) for directory in dirs]

        if bases[0].user is None:
            shared = home
            home = get_user_directory_values(request.user.home_dir)
            home["children"] = []
        else:
            shared = get_user_directory_values(get_or_create_shared_dir())
            shared["children"] = []

        result = {
            "home": home,
            "shared": shared
        }
        return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)


    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetDirTreeView, self).dispatch(*args, **kwargs)

class GetDirContentView(View):
    def get(self, request, *args, **kwargs):

        if "dir_id" in request.GET:
            try:
                base = UserDirectory.objects.get(id=request.GET["dir_id"])
                if base.user != request.user and base.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="application/json", status=403)
            except UserDirectory.DoesNotExist:
                return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="application/json", status=404)
        else:
            base = request.user.home_dir

        docs = base.userdocument_set.all().select_related("resource", "resource__language", "user")
        dirs = base.userdirectory_set.all().select_related("user")

        result = {
            "dirs": [get_user_directory_values(directory) for directory in dirs],
            "docs": [get_user_document_values(doc) for doc in docs],
            "base": get_user_directory_values(base, True)
        }
        return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)


    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetDirContentView, self).dispatch(*args, **kwargs)

class GetDocsFromDirsView(View):
    def get(self, request, *args, **kwargs):
        try:
            dir_ids = json.loads(request.GET["dir_ids"])
            docs = UserDocument.objects.filter(Q(user=request.user) | Q(is_private=False), directory_id__in=dir_ids).select_related("resource", "resource__language").values("id", "name", "resource__id", "resource__language__tag")
            result = {
                "docs": [{
                    "name": doc["name"],
                    "id": doc["id"],
                    "resource": {
                        "id": doc["resource__id"],
                        "language": {
                            "tag": doc["resource__language__tag"]
                        }
                    }
                } for doc in docs],
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetDocsFromDirsView, self).dispatch(*args, **kwargs)


class GetDocsView(View):
    def get(self, request, *args, **kwargs):
        try:
            doc_ids = json.loads(request.GET["doc_ids"])
            docs = UserDocument.objects.filter(Q(user=request.user) | Q(is_private=False), id__in=doc_ids).select_related("resource", "resource__language", "user")
            docs_values = [get_user_document_values(doc) for doc in docs]
            docs_values.sort(key=lambda x: doc_ids.index(x["id"]))

            result = {
                "docs": docs_values
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetDocsView, self).dispatch(*args, **kwargs)


class DeleteDocsByIdArrayView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            doc_ids = data["doc_ids"]
            documents = UserDocument.objects.filter(Q(user=request.user) | Q(is_private=False), id__in=doc_ids)
            deleted_ids = []
            for document in documents:
                deleted_ids.append(document.id)
                document.delete()
            return HttpResponse(json.dumps({ "doc_ids": deleted_ids }), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=500)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(DeleteDocsByIdArrayView, self).dispatch(*args, **kwargs)


class DeleteDirsByIdArrayView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            dir_ids = data["dir_ids"]
            directories = UserDirectory.objects.filter(Q(user=request.user) | Q(is_private=False), id__in=dir_ids)
            deleted_ids = []
            for directory in directories:
                deleted_ids.append(directory.id)
                directory.delete()
            return HttpResponse(json.dumps({ "dir_ids": deleted_ids }), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=500)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(DeleteDirsByIdArrayView, self).dispatch(*args, **kwargs)

class CreateDirView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            dir_id = int(data["dir_id"])
            dir_name = data["dir_name"]

            try:
                base = UserDirectory.objects.get(id=dir_id)
                if base.user != request.user and base.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="application/json", status=404)
            except UserDirectory.DoesNotExist:
                return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="application/json", status=404)

            directory = create_directory(dir_name, request.user, base, is_private=base.is_private)
            result = get_user_directory_values(directory)
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(CreateDirView, self).dispatch(*args, **kwargs)

class RenameDocView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            doc_id = data["doc_id"]
            name = data["name"]

            try:
                document = UserDocument.objects.get(id=doc_id)
                if document.user != request.user and document.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this document"}), content_type="application/json", status=404)
            except UserDocument.DoesNotExist:
                return HttpResponse(json.dumps({"message": "document does not exist"}), content_type="application/json", status=404)

            old_name = document.name
            document.name = name
            document.save()

            result = {
                "old_name": old_name,
                "new_name": name,
                "doc_id": doc_id,
                "modified": document.modified
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(RenameDocView, self).dispatch(*args, **kwargs)

class RenameDirView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            dir_id = data["dir_id"]
            name = data["name"]

            try:
                directory = UserDirectory.objects.get(id=dir_id)
                if directory.user != request.user and directory.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="application/json", status=404)
            except UserDirectory.DoesNotExist:
                return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="application/json", status=404)

            old_name = directory.name
            directory.name = name
            directory.save()

            result = {
                "old_name": old_name,
                "new_name": name,
                "dir_id": dir_id,
                "modified": directory.modified
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(RenameDirView, self).dispatch(*args, **kwargs)


class AddTerminologyListView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            dir_id = data["dir_id"]
            terminology_data = data["terminology_data"]
            name = data["name"]
            try:
                base = UserDirectory.objects.get(id=dir_id)
                if base.user != request.user and base.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="application/json", status=404)
            except UserDirectory.DoesNotExist:
                return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="application/json", status=404)

            doc = UserDocument(type=DocumentType.TLIST, name=name, user=request.user, is_private=base.is_private, directory=base, resource=None, terminology_data=json.dumps(terminology_data))
            doc.save()

            result = {
                "docs": [get_user_document_values(doc)],
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(AddTerminologyListView, self).dispatch(*args, **kwargs)


class UpdateTerminologyListView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            doc_id = data["doc_id"]
            terminology_data = data["terminology_data"]
            if doc_id is not None:
                try:
                    document = UserDocument.objects.get(id=doc_id)
                except UserDocument.DoesNotExist:
                    return HttpResponse(json.dumps({"message": "document does not exist"}), content_type="application/json", status=404)

                if document.user != request.user and document.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this document"}), content_type="application/json", status=404)

                if document.type != DocumentType.TLIST:
                    return HttpResponse(json.dumps({"message": "document does not exist"}), content_type="application/json", status=404)
                document.terminology_data = json.dumps(terminology_data)
                document.save()
            else:
                base = request.user.draft_dir
                names = [d.name for d in base.userdocument_set.all()]
                while True:
                    name = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(8))
                    if name not in names:
                        break

                document = UserDocument(type=DocumentType.TLIST, name=name, user=request.user, is_private=True, directory=base, resource=None, terminology_data=json.dumps(terminology_data), draft=True)
                document.save()


            result = {
                "docs": [get_user_document_values(document)],
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(UpdateTerminologyListView, self).dispatch(*args, **kwargs)

class SaveDraftView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            terminology_data = data["terminology_data"]
            base = request.user.draft_dir

            names = [d["name"] for d in base.userdocument_set.all().values("name")]

            while True:
                name = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(8))
                if name not in names:
                    break

            doc = UserDocument(type=DocumentType.TLIST, name=name, user=request.user, is_private=True, directory=base, resource=None, terminology_data=json.dumps(terminology_data) if terminology_data else None, draft=True)
            doc.save()

            result = {
                "doc": get_user_document_values(doc),
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(SaveDraftView, self).dispatch(*args, **kwargs)


def generate_parl_doc_title(parl_doc):
    title = parl_doc.title
    if len(title) == 0:
        comms = parl_doc.committeemeeting_set.all()
        if comms.count() > 0:
            title = comms[0].title
    if len(title) == 0:
        procs = parl_doc.procedure_set.all()
        if procs.count() > 0:
            title = procs[0].title
    if len(title) == 0:
        title = "--unnamed--"
    return title[:40] + "_" + parl_doc.language.tag.upper() + ".pdf"


class AddParlDocsView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            dir_id = data["dir_id"]
            parldoc_ids = data["parldoc_ids"]
            try:
                base = UserDirectory.objects.get(id=dir_id)
                if base.user != request.user and base.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="application/json", status=404)
            except UserDirectory.DoesNotExist:
                return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="application/json", status=404)

            docs = []

            for parldoc_id in parldoc_ids:
                try:
                    parl_doc = ParlDocument.objects.get(id=parldoc_id)
                except ParlDocument.DoesNotExist:
                    pass

                doc = UserDocument(type=DocumentType.REPORT, name=generate_parl_doc_title(parl_doc), is_private=base.is_private, user=request.user, parldocument=parl_doc, directory=base, resource=parl_doc.resource)
                doc.save()
                docs.append(doc)

            result = {
                "docs": [get_user_document_values(doc) for doc in docs],
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(AddParlDocsView, self).dispatch(*args, **kwargs)


def copy_dir(dirA, dirB, user):
    documents = dirA.userdocument_set.all()
    directories = dirA.userdirectory_set.all()

    for document in documents:
        new_doc = UserDocument(type=document.type, name=document.name, is_private=dirB.is_private, user=user, directory=dirB, terminology_data=document.terminology_data)
        new_doc.parldocument_id = document.parldocument_id
        new_doc.resource_id = document.resource_id
        new_doc.save()

    for directory in directories:
        new_dir = UserDirectory(name=directory.name, user=user, is_private=dirB.is_private, parent=dirB)
        new_dir.save()
        copy_dir(directory, new_dir, user)

def is_same_or_descendant(dirA, dirB):
    while dirA is not None:
        if dirA == dirB:
            return True
        dirA = dirA.parent
    return False

class CopyView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            dir_ids = data["dir_ids"]
            doc_ids = data["doc_ids"]
            base_id = data["base_id"]
            remove_original = data["remove_original"]
            try:
                base = UserDirectory.objects.get(id=base_id)
                if base.user != request.user and base.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="application/json", status=404)
            except UserDirectory.DoesNotExist:
                return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="application/json", status=404)

            documents = UserDocument.objects.filter(Q(user=request.user) | Q(is_private=False), id__in=doc_ids)
            directories = UserDirectory.objects.filter(Q(user=request.user) | Q(is_private=False), id__in=dir_ids)

            new_docs = []
            rm_docIds = []
            new_dirs = []
            rm_dirIds = []

            for document in documents:
                if document.directory == base:
                    if remove_original != True:
                        name, ext = splitext(document.name)
                        new_doc = UserDocument(type=document.type, name=name + "_(copy)" + ext, is_private=base.is_private, user=request.user, directory=base, terminology_data=document.terminology_data)
                        new_doc.parldocument_id = document.parldocument_id
                        new_doc.resource_id = document.resource_id
                        new_doc.save()
                        new_docs.append(new_doc)
                else:
                    #conflicting_object = base.userdocument_set.filter(name=document.name)
                    #conflicting_object_exists = conflicting_object.exists()

                    new_doc = UserDocument(type=document.type, name=document.name, is_private=base.is_private, user=request.user, directory=base, terminology_data=document.terminology_data)
                    new_doc.parldocument_id = document.parldocument_id
                    new_doc.resource_id = document.resource_id
                    new_doc.save(rename=True)
                    new_docs.append(new_doc)

                    if remove_original == True:
                        rm_docIds.append(document.id)
                        document.delete()


            for directory in directories:
                if is_same_or_descendant(base, directory):
                    print "SAME DIR"
                    continue

                if directory.parent == base:
                    if remove_original != True:
                        new_dir = UserDirectory(name=directory.name + "_(copy)", user=request.user, is_private=base.is_private, parent=base)
                        new_dir.save()
                        new_dirs.append(new_dir)
                        copy_dir(directory, new_dir, request.user)
                else:
                    #conflicting_object = base.userdirectory_set.filter(name=directory.name)
                    #conflicting_object_exists = conflicting_object.exists()

                    new_dir = UserDirectory(name=directory.name, user=request.user, is_private=base.is_private, parent=base)
                    new_dir.save(rename=True)
                    new_dirs.append(new_dir)
                    copy_dir(directory, new_dir, request.user)

                    if remove_original == True:
                        rm_dirIds.append(directory.id)
                        directory.delete()


            result = {
                "docs": [get_user_document_values(doc) for doc in new_docs],
                "dirs": [get_user_directory_values(directory) for directory in new_dirs],
                "rm_doc_ids": rm_docIds,
                "rm_dir_ids": rm_dirIds
            }
            return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(CopyView, self).dispatch(*args, **kwargs)


class UploadView(View):
    def post(self, request, *args, **kwargs):
        try:
            token = request.POST["X-CSRFToken"]
            compare_token = get_token(request)
            if token != compare_token:
                return HttpResponse(json.dumps({"message": "missing or bad token"}), content_type="text/plain", status=403)
            try:
                dir_id = int(request.POST["dir_id"])
                try:
                    directory = UserDirectory.objects.get(id=dir_id)
                    if directory.user != request.user and directory.is_private is True:
                        return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="text/plain", status=404)
                except UserDirectory.DoesNotExist:
                    return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="text/plain", status=404)

                ufile = request.FILES["files[]"]
                file_name = str(ufile)
                size = ufile.size
                try:
                    document = create_document(ufile, file_name, request.user, size, directory, is_private=directory.is_private)
                    result = get_user_document_values(document)
                    print "FINISH REQUEST"
                    return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="text/plain", status=200)

                except DatabaseError as e:
                    print e
                    return HttpResponse(json.dumps({"message": "db error"}), content_type="text/plain", status=500)

            except KeyError as e:
                return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="text/plain", status=400)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing or bad token"}), content_type="text/plain", status=403)

    @method_decorator(never_ever_cache)
    @method_decorator(csrf_exempt)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(UploadView, self).dispatch(*args, **kwargs)


class DeleteFromWebdavView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            file_names = data["file_names"]
            path = join_path(settings.WEBDAV_USER_HOME, request.user.webdav_alias)

            for file_name in file_names:
                file_path = normpath(join_path(path, file_name))
                if path_exists(file_path) and file_path.startswith(path):
                    remove(file_path)

            return HttpResponse(json.dumps({}), content_type="application/json", status=200)

        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(DeleteFromWebdavView, self).dispatch(*args, **kwargs)


class TransferFromWebdavView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            file_names = data["file_names"]
            base_id = data["base_id"]
            try:
                base = UserDirectory.objects.get(id=base_id)
                if base.user != request.user and base.is_private is True:
                    return HttpResponse(json.dumps({"message": "you don't have access to this directory"}), content_type="application/json", status=404)
            except UserDirectory.DoesNotExist:
                return HttpResponse(json.dumps({"message": "directory does not exist"}), content_type="application/json", status=404)

            path = join_path(settings.WEBDAV_USER_HOME, request.user.webdav_alias)

            documents = []
            for file_name in file_names:
                file_path = normpath(join_path(path, file_name))
                if path_exists(file_path) and file_path.startswith(path):
                    with open(file_path, "rb") as f:
                        ufile = ContentFile(bytearray(f.read()))
                    size = ufile.size
                    document = create_document(ufile, file_name, request.user, size, base, is_private=base.is_private)
                    documents.append(get_user_document_values(document))
                    remove(file_path)

            return HttpResponse(json.dumps({"docs": documents}, cls=DjangoJSONEncoder), content_type="application/json", status=200)

        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(TransferFromWebdavView, self).dispatch(*args, **kwargs)

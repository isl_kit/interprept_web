# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('speeches', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('resources', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserDirectory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('is_private', models.BooleanField(default=True)),
                ('path', models.TextField(editable=False, blank=True)),
                ('parent', models.ForeignKey(to='documents.UserDirectory', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserDocument',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.PositiveSmallIntegerField(default=0, choices=[(0, b'UNKNOWN'), (1, b'Uploaded by User'), (2, b'Added from Reports'), (3, b'Terminology List')])),
                ('name', models.CharField(max_length=200)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('is_private', models.BooleanField(default=True)),
                ('terminology_data', models.TextField(null=True, blank=True)),
                ('draft', models.BooleanField(default=False)),
                ('directory', models.ForeignKey(to='documents.UserDirectory')),
                ('parldocument', models.ForeignKey(to='speeches.ParlDocument', null=True)),
                ('procedures', models.ManyToManyField(to='speeches.Procedure')),
                ('resource', models.ForeignKey(to='resources.Resource', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from django.contrib import admin
from .models import UserDocument, UserDirectory

def delete_model(modeladmin, request, queryset):
    for obj in queryset:
        obj.delete()

class UserDocumentAdmin(admin.ModelAdmin):
    fields = ["name", "user", "modified", "is_private", "terminology_data"]
    actions = [delete_model]

class UserDirectoryAdmin(admin.ModelAdmin):
    fields = ["name", "user", "modified", "parent", "is_private"]
    actions = [delete_model]

admin.site.register(UserDocument, UserDocumentAdmin)
admin.site.register(UserDirectory, UserDirectoryAdmin)
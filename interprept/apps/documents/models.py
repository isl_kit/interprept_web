#!/usr/bin/env python
# -*- coding: utf-8 -*-

import thread
from os.path import join as join_path, splitext, basename, exists as path_exists, split as path_split
from os import makedirs
from functools import partial

from django.db import models
from django.db.transaction import atomic
from django.conf import settings
from django.utils.timezone import now as timezone_now
from django.core.exceptions import ObjectDoesNotExist

from apps.resources.models import Resource, get_user_doc_path, get_user_dir_path, replace_in_path
from apps.resources.db_handler import create_resource
from apps.speeches.models import Procedure, ParlDocument
from apps.processor.process import runner

@atomic
def create_directory(name, user, directory, is_private=True):
    directory = UserDirectory(name=name, user=user, parent=directory, is_private=is_private)
    directory.save()
    return directory


class UserDirectory(models.Model):
    name = models.CharField(max_length=200)
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    parent = models.ForeignKey("self", null=True)
    is_private = models.BooleanField(default=True)
    path = models.TextField(blank=True, editable=False)

    def save(self, update=True, rename=False, *args, **kwargs):
        if not self.id:
            self.created = timezone_now()
        if rename or not self.id:
            orig_name = self.name
            counter = 1
            if self.parent:
                while self.parent.userdirectory_set.filter(name=self.name).count() > 0:
                    self.name = orig_name + "_" + str(counter)
                    counter += 1
        if update:
            self.modified = timezone_now()
        if self.parent:
            self.path = join_path(self.parent.path, self.name)
        else:
            self.path = self.name
        return super(UserDirectory, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        children = self.userdirectory_set.all()
        for userdirectory in children:
            userdirectory.delete()
        return super(UserDirectory, self).delete(*args, **kwargs)

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = "documents"


@atomic
def create_document(ufile, name, user, size, directory, is_private=True):
    resource_result = create_resource(ufile, size)
    resource = resource_result["resource"]
    document = UserDocument(type=DocumentType.DOC, name=name, user=user, is_private=is_private, directory=directory, resource=resource)
    if resource_result["created"]:
        document.save()
        runner.run(resource, partial(done_handler, document.id))
    else:
        try:
            document.parl_document = resource.parldocument
        except ObjectDoesNotExist:
            pass
        document.save()
    return document


def done_handler(document_id, resource):
    if resource is not None:
        try:
            document = UserDocument.objects.get(id=document_id)
            try:
                document.parl_document = resource.parldocument
                document.save()
            except ObjectDoesNotExist:
                pass
        except UserDocument.DoesNotExist:
            pass


class DocumentType:
    UNKNOWN = 0
    DOC = 1
    REPORT = 2
    TLIST = 3


class UserDocument(models.Model):
    DOCTYPE_CHOICES = (
        (DocumentType.UNKNOWN, "UNKNOWN"),
        (DocumentType.DOC, "Uploaded by User"),
        (DocumentType.REPORT, "Added from Reports"),
        (DocumentType.TLIST, "Terminology List")
    )
    type = models.PositiveSmallIntegerField(choices=DOCTYPE_CHOICES, default=DocumentType.UNKNOWN)
    name = models.CharField(max_length=200)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()
    directory = models.ForeignKey(UserDirectory)
    is_private = models.BooleanField(default=True)

    procedures = models.ManyToManyField(Procedure)
    resource = models.ForeignKey(Resource, null=True)
    parldocument = models.ForeignKey(ParlDocument, null=True)
    terminology_data = models.TextField(null=True, blank=True)
    draft = models.BooleanField(default=False)

    def save(self, update=True, rename=False, *args, **kwargs):
        if not self.id:
            self.created = timezone_now()
        if rename or not self.id:
            orig_name = self.name
            counter = 1
            while self.directory.userdocument_set.filter(name=self.name).count() > 0:
                name, ext = splitext(orig_name)
                self.name = name + "_" + str(counter) + ext
                counter += 1
        if update:
            self.modified = timezone_now()
        return super(UserDocument, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        resource = self.resource
        remove_result = super(UserDocument, self).delete(*args, **kwargs)
        if resource and resource.is_unbound():
            resource.delete()
        return remove_result

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = "documents"

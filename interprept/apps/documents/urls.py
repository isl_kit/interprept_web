#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^getAll/$', views.GetAllView.as_view(), name="getAll"),
    url(r'^getDirContent/$', views.GetDirContentView.as_view(), name="getDirContent"),
    url(r'^getDirTree/$', views.GetDirTreeView.as_view(), name="getDirTree"),
    url(r'^getDocs/$', views.GetDocsView.as_view(), name="getDocs"),
    url(r'^getDocsFromDirs/$', views.GetDocsFromDirsView.as_view(), name="getDocsFromDirs"),
    url(r'^createDir/$', views.CreateDirView.as_view(), name="createDir"),
    url(r'^copy/$', views.CopyView.as_view(), name="copy"),
    url(r'^addParlDocs/$', views.AddParlDocsView.as_view(), name="addParlDocs"),
    url(r'^addTerminologyList/$', views.AddTerminologyListView.as_view(), name="addTerminologyList"),
    url(r'^updateTerminologyList/$', views.UpdateTerminologyListView.as_view(), name="updateTerminologyList"),
    url(r'^renameDoc/$', views.RenameDocView.as_view(), name="renameDoc"),
    url(r'^renameDir/$', views.RenameDirView.as_view(), name="renameDir"),
    url(r'^deleteDocsByIdArray/$', views.DeleteDocsByIdArrayView.as_view(), name="deleteDocsByIdArray"),
    url(r'^deleteDirsByIdArray/$', views.DeleteDirsByIdArrayView.as_view(), name="deleteDirsByIdArray"),
    url(r'^upload/$', views.UploadView.as_view(), name="upload"),
    url(r'^deleteFromWebdav/$', views.DeleteFromWebdavView.as_view(), name="deleteFromWebdav"),
    url(r'^transferFromWebdav/$', views.TransferFromWebdavView.as_view(), name="transferFromWebdav"),
    url(r'^saveDraft/$', views.SaveDraftView.as_view(), name="saveDraft")
)

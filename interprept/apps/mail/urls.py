#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',
    url(r'^feedback/$', views.FeedbackView.as_view(), name="feedback")
)
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.conf import settings

from apps.common.decorators.auth import check_login
from django.core.mail import send_mail
from apps.common.decorators.cache import never_ever_cache

class FeedbackView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            title = data["title"]
            message = data["message"]
            send_copy = data["send_copy"]

            send_mail("FEEDBACK: " + title, "FROM: " + request.user.email + "\r\n\r\nMESSAGE:\r\n" + message, settings.EMAIL_HOST_USER, [settings.EMAIL_HOST_USER], fail_silently=False)

            if send_copy == True:
                request.user.email_user("Your Feedback: " + title, "We received the following feedback from you:\r\n\r\n" + message, settings.EMAIL_HOST_USER)

            return HttpResponse(
                json.dumps({}),
                content_type="application/json",
                status=200
            )
        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(FeedbackView, self).dispatch(*args, **kwargs)
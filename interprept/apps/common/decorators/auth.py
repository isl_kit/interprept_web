#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import json

from django.http import HttpResponse

def check_login(func):
    def inner(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponse(json.dumps({"message": "no user found"}), content_type="application/json", status=401)
        else:
            return func(request, *args, **kwargs)
    return inner
#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from django.shortcuts import redirect

def redirect_on_get(func):
    def inner(request, *args, **kwargs):
        if (request.method == 'POST'):
            return func(request, *args, **kwargs)
        else:
            return redirect("/")
    return inner    
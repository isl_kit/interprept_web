#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.conf import settings


def get_pdf_version():
    with open(settings.VERSION_FILE, "r") as f:
        data = json.load(f)
    return data["PDF"]

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.conf import settings


def get_ne_version():
    with open(settings.VERSION_FILE, "r") as f:
        data = json.load(f)
    return data["NE"]


def get_terminology_version():
    with open(settings.VERSION_FILE, "r") as f:
        data = json.load(f)
    return data["TE"]

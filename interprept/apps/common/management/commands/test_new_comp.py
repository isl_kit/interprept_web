# -*- coding: utf-8 -*-

from time import sleep
import traceback

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from apps.processor.process import runner
from apps.resources.models import Resource
from apps.processor.models import ResourceTaskState


class Command(BaseCommand):

    def __init__(self, *args, **kwargs):
        self.running = True
        super(Command, self).__init__(*args, **kwargs)

    def cb(self, resource):
        print resource.task_data
        self.running = False


    def handle(self, *args, **options):
        try:
            resource1 = Resource.objects.filter(language__tag="en").order_by("pk")[0]
            resource2 = Resource.objects.filter(language__tag="en").order_by("pk")[1]
            for name in ["text", "ne", "terminology", "translation"]:
                try:
                    rts = ResourceTaskState.objects.get(resource=resource1, task_name=name)
                    rts.delete()
                except ResourceTaskState.DoesNotExist:
                    pass
                try:
                    rts = ResourceTaskState.objects.get(resource=resource2, task_name=name)
                    rts.delete()
                except ResourceTaskState.DoesNotExist:
                    pass

            for name in ["text", "ne", "terminology", "translation"]:
                rts = ResourceTaskState(resource=resource1, task_name=name)
                if name == "text":
                    rts.state = "ok"
                rts.save()
                rts = ResourceTaskState(resource=resource2, task_name=name)
                if name == "text":
                    rts.state = "ok"
                rts.save()

            resources = Resource.objects.filter(resourcetaskstate__task_name="text", resourcetaskstate__state="ok")
            print resources.count()

            for name in ["text", "ne", "terminology", "translation"]:
                rts = ResourceTaskState.objects.get(resource=resource1, task_name=name)
                rts.delete()
                rts = ResourceTaskState.objects.get(resource=resource2, task_name=name)
                rts.delete()

            return
            resource = Resource.objects.filter(language__tag="en").order_by("pk")[0]
            resource.task_data = "{\"text\": {\"hash\": \"\", \"dep\": {}, \"error_message\": \"\", \"state\": \"error\", \"running\": false, \"version\": 0}, \"translation\": {\"hash\": \"\", \"dep\": {\"terminology\": \"\"}, \"error_message\": \"\", \"running\": false, \"state\": \"init\", \"version\": 0}, \"ne\": {\"hash\": \"\", \"dep\": {\"text\": \"\"}, \"error_message\": \"\", \"running\": false, \"state\": \"init\", \"version\": 0}, \"terminology\": {\"hash\": \"\", \"dep\": {\"text\": \"\"}, \"error_message\": \"\", \"state\": \"init\", \"running\": false, \"version\": 0}}"
            #resource.task_data = ""
            resource.save()
            #runner.run_single(resource, "translation", self.cb)
            runner.run(resource, self.cb)
            while self.running:
                sleep(1)
            print "FINISH"
        except:
            print traceback.format_exc()

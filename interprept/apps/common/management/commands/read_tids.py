# -*- coding: utf-8 -*-

from time import sleep
import json
import gc

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from apps.translation.Translation

def queryset_iterator(queryset, chunksize=1000):
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()

class Command(BaseCommand):

    def handle(self, *args, **options):

        with open("/tmp/tids.out", "r") as f:
            tids = json.load(f)

        counter = 0
        leng = Translation.objects.count()

        xc = 0
        oc = 0
        oo = 0
        for t in Translation.objects.all().select_related("phrase1", "phrase2", "phrase1__base__language", "phrase2__base__language")[:100000]:
            counter += 1
            print str(counter) + "/" + str(leng)

            tag1 = t.phrase1.base.language.tag
            tag2 = t.phrase2.base.language.tag
            name1 = t.phrase1.name.lower()
            name2 = t.phrase2.name.lower()

            try:
                ids = tids[tag1][tag2][name1][name2]
                if len(ids) == 0:
                    oo += 1
                else:
                    oc += 1
            except:
                ids = []
                xc += 1

        print str(oc) + "/" + str(oo) + "/" + str(xc)

        return


        with transaction.atomic():
            for t in queryset_iterator(Translation.objects.all().select_related("phrase1", "phrase2", "phrase1__base__language", "phrase2__base__language")):
                counter += 1
                print str(counter) + "/" + str(leng)

                tag1 = t.phrase1.base.language.tag
                tag2 = t.phrase2.base.language.tag
                name1 = t.phrase1.name.lower()
                name2 = t.phrase2.name.lower()

                try:
                    ids = tids[tag1][tag2][name1][name2]
                except:
                    ids = []

                t.translation_ids = json.dumps(ids)
                t.save()
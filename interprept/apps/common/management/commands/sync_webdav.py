# -*- coding: utf-8 -*-

import pwd
import grp
from passlib.apache import HtpasswdFile
from os.path import exists as path_exists, join as join_path
from os import makedirs, chown, chmod

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth import get_user_model


def get_apache_gid():
    groups = ["httpd", "apache", "www-data", "root"]
    gid = None
    for group in groups:
        try:
            gid = grp.getgrnam(group).gr_gid
            break
        except KeyError:
            pass
    return gid


def get_apache_uid():
    users = ["httpd", "apache", "www-data", "root"]
    uid = None
    for user in users:
        try:
            uid = pwd.getpwnam(user).pw_uid
            break
        except KeyError:
            pass
    return uid


class Command(BaseCommand):

    def handle(self, *args, **options):

        uid = get_apache_uid()
        gid = get_apache_gid()
        uid_root = pwd.getpwnam("root").pw_uid

        # clean webdav.password and store users in array
        # if doesn't exist, create empty file and add permissions
        pwd_file_exists = path_exists(settings.WEBDAV_PASSWORD_FILE)
        ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE, new=not path_exists(settings.WEBDAV_PASSWORD_FILE))
        users = [[u, ht.get_hash(u)] for u in ht.users()]
        for user in users:
            ht.delete(user[0])
        ht.save()

        if not pwd_file_exists:
            chmod(settings.WEBDAV_PASSWORD_FILE, 0660)
            chown(settings.WEBDAV_PASSWORD_FILE, uid_root, gid)

        # clean webdav.password.prod
        # delete all users that were listed in webdav.password
        # if doesn't exist, create empty file and add permissions
        prod_file_exists = path_exists(settings.WEBDAV_PASSWORD_FILE_PROD)
        ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_PROD, new=not prod_file_exists)
        for user in users:
            ht.delete(user[0])
        ht.save()
        if not prod_file_exists:
            chmod(settings.WEBDAV_PASSWORD_FILE_PROD, 0640)
            chown(settings.WEBDAV_PASSWORD_FILE_PROD, uid_root, gid)

        # add all users that were listed in webdav.password
        with open(settings.WEBDAV_PASSWORD_FILE_PROD, "a") as f:
            for user in users:
                f.write(user[0] + ":" + user[1] + "\n")

        wlines = []
        max_num = 0
        emails = []
        email_num_hash = {}

        # create map file if it does not exist
        if not path_exists(settings.WEBDAV_MAP_FILE):
            open(settings.WEBDAV_MAP_FILE, 'a').close()
            chmod(settings.WEBDAV_MAP_FILE, 0640)
            chown(settings.WEBDAV_MAP_FILE, uid_root, gid)

        # read all emails and determine max_num from map file
        with open(settings.WEBDAV_MAP_FILE, "r") as f:
            wlines = f.read().splitlines()
            for line in wlines:
                tline = line.strip()
                if len(tline) > 0:
                    line_arr = tline.split(" ")
                    num = int(line_arr[1][:-1])
                    email = line_arr[0]
                    emails.append(email)
                    if num > max_num:
                        max_num = num

        # rewrite map file and add all emails with new, larger numbers
        # create hash with NEW emails and their numbers
        with open(settings.WEBDAV_MAP_FILE, "w") as f:
            for line in wlines:
                f.write(line + "\n")
            for user in users:
                if user[0] not in emails:
                    max_num += 1
                    uname = str(max_num) + "_"
                    email_num_hash[user[0]] = uname
                    f.write(user[0] + " " + uname + "\n")

        User = get_user_model()

        # create upload folder for users
        # update user models
        for user in users:
            email = user[0]
            if email not in email_num_hash:
                continue
            path = join_path(settings.WEBDAV_USER_HOME, email_num_hash[email])
            if not path_exists(path):
                makedirs(path, 0755)
                chown(path, uid, gid)

            try:
                dbuser = User.objects.get(email=email)
                dbuser.is_webdav_initialized = True
                dbuser.webdav_alias = email_num_hash[email]
                dbuser.save()
            except User.DoesNotExist:
                pass

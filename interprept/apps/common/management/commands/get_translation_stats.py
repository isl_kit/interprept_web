# -*- coding: utf-8 -*-

import gc
import json
from django.core.management.base import BaseCommand, CommandError

from apps.languages.models import Language
from apps.resources.models import Resource
from apps.translation.models import Phrase, Translation

def queryset_iterator(queryset, chunksize=1000):
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()

class Command(BaseCommand):

    def handle(self, *args, **options):
        terms = []
        resources = Resource.objects.filter(language__tag="en")
        for resource in resources:
            try:
                terminology_list = resource.terminologylist
            except:
                continue
            for term in terminology_list.term_set.all():
                name = term.name.lower()
                terms.append(name)
        terms = list(set(terms))

        res = {
            1: 0,
            2: 0,
            3: 0        
        }
        language = Language.objects.get(tag="en")

        for idx, term in enumerate(terms):
            print str(idx) + "/" + str(len(terms))
            try:
                phrase = Phrase.objects.get(language=language, name=term)
            except:
                continue
            for translation in phrase.translation_set.all():
                ids = json.loads(translation.translator_ids)
                for id in ids:
                    res[id] += 1

        print res

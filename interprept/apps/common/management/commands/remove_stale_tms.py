# -*- coding: utf-8 -*-

from time import sleep

from django.core.management.base import BaseCommand, CommandError

from apps.translation.models import TMTranslation

class Command(BaseCommand):

    def handle(self, *args, **options):
        t1 = TMTranslation.objects.count()
        t2 = TMTranslation.objects.filter(translation=None).count()
        print str(t1) + "/" + str(t2)
        TMTranslation.objects.filter(translation=None).delete()


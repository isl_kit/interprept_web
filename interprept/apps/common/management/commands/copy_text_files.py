# -*- coding: utf-8 -*-
import os
from shutil import copyfile

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, ProcessState


class Command(BaseCommand):

    def handle(self, *args, **options):
        resources = Resource.objects.filter(language__tag="en", text_state=ProcessState.SUCCESS)[:50]

        path = os.path.dirname(os.path.abspath(__file__))
        for index, arg in enumerate(args):
            if index == 0:
                path = os.path.abspath(arg)

        for resource in resources:
            text_path = resource.get_text_path()
            dest_file = os.path.join(path, str(resource.id) + ".txt")
            copyfile(text_path, dest_file)

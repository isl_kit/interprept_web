# -*- coding: utf-8 -*-

import json
import gc
from django.core.management.base import BaseCommand, CommandError
from apps.translation.models import Translation

def queryset_iterator(queryset, chunksize=1000):
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()

class Command(BaseCommand):

    def handle(self, *args, **options):

        leng = Translation.objects.count()
        counter = 0
        for translation in queryset_iterator(Translation.objects.all()):
            counter += 1
            print str(counter) + "/" + str(leng)
            ids = json.loads(translation.translator_ids)
            new_ids = list(set(ids))
            if len(new_ids) != len(ids):
                translation.translator_ids = json.dumps(new_ids)
                translation.save()


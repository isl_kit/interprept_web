# -*- coding: utf-8 -*-
import os
from shutil import copyfile

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, QueueState


class Command(BaseCommand):

    def handle(self, *args, **options):
        resources = Resource.objects.all()

        for resource in resources:
            resource.queue_state = QueueState.UNQUEUED
            resource.save()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from time import sleep
import json
import gc
from os.path import split as split_path, exists as path_exists

from django.core.management.base import BaseCommand, CommandError

from apps.translation.translation_service import translate_async
from apps.translation.models import Phrase
from apps.languages.models import Language
from apps.resources.models import Term

def queryset_iterator(queryset, chunksize=1000):
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()

class Runner(object):

    def __init__(self):
        self.counter = 0
        
        lang = Language.objects.get(tag="en")
        self.tags = [x.tag for x in Language.objects.exclude(tag="en")]

        if path_exists("/tmp/phrase_ids.json"):
            with open("/tmp/phrase_ids.json", "r") as f:
                self.ids = json.load(f)
        else:
            self.ids = []
            terms = []
            with open("/tmp/terms.json", "r") as f:
                terms = json.load(f)
            counter = 0
            for term in terms:
                counter += 1
                print str(counter) + "/" + str(len(terms))
                try:
                    phrase = Phrase.objects.get(name=term, base__language=lang)
                except:
                    continue
                finished = json.loads(phrase.finished)
                save = True
                for tag, ids in finished.iteritems():
                    if 1 in ids:
                        save = False
                        break
                if save:
                    self.ids.append(phrase.id)
            with open("/tmp/phrase_ids.json", "wb+") as f:
                json.dump(self.ids, f)

        self.ph_count = len(self.ids)
        print "LENGTH: " + str(self.ph_count)

    def run(self, res):

        if res is not None:
            self.counter += 200

        if self.counter >= self.ph_count:
            return
        print str(self.counter) + "/" + str(self.ph_count)
        sub_ids = self.ids[self.counter:self.counter + 200]
        phrases = [x.name for x in Phrase.objects.filter(id__in=sub_ids)]
        
        translate_async("en", self.tags, phrases, True, self.run, translator_ids=[1])

class Command(BaseCommand):

    def handle(self, *args, **options):

        runner = Runner()

        runner.run(None)

        while runner.counter < runner.ph_count:
            sleep(1)

# -*- coding: utf-8 -*-

from time import sleep
import gc
import json

from django.core.management.base import BaseCommand, CommandError

from apps.translation.models import Translation


def queryset_iterator(queryset, chunksize=1000):
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()

class Command(BaseCommand):

    def handle(self, *args, **options):
        res = {}
        counter = 0
        leng = Translation.objects.count()
 
        for t in queryset_iterator(Translation.objects.all().select_related("language", "phrase", "phrase__language")):
            counter += 1
            print str(counter) + "/" + str(leng)
            tag1 = t.language.tag
            tag2 = t.phrase.language.tag
            name1 = t.lower_name.lower()
            name2 = t.phrase.name.lower()
            tids = json.loads(t.translator_ids)
            if tag1 == tag2:
                continue
            if tag2 < tag1:
                tag1, tag2, name1, name2 = tag2, tag1, name2, name1
            
            if tag1 not in res:
                res[tag1] = {}
            
            if tag2 not in res[tag1]:
                res[tag1][tag2] = {}

            if name1 not in res[tag1][tag2]:
                res[tag1][tag2][name1] = {}

            res[tag1][tag2][name1][name2] = tids

            
        with open("/tmp/tids.out", "wb+") as f:
            json.dump(res, f)

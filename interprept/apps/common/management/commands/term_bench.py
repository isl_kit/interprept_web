# -*- coding: utf-8 -*-

from __future__ import absolute_import

from time import sleep
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError

from celery import Celery
from django.conf import settings

celery = Celery('interprept')
celery.config_from_object('django.conf:settings')


class Command(BaseCommand):

    def handle(self, *args, **options):

        results = []
        counter = 0
        limit = 100

        with open("/tmp/test_text.txt", "r") as f:
            text = f.read()

        start = datetime.now()

        for i in range(limit):
            res = celery.send_task("isc.terminology.tasks.create_terminology_list", args=[text, i, "en"], queue="celery")
            results.append(res)

        while counter < limit:
            sleep(2)
            res_ready = [res for res in results if res.ready()]
            for res in res_ready:
                counter += 1
                delta = (datetime.now() - start).total_seconds()
                files_per_min = counter * 60.0 / delta
                print "READY " + str(counter) + " / " + str(round(files_per_min, 3)) + " Files/min"
                results.remove(res)

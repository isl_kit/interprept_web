#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model


class Command(BaseCommand):

    def handle(self, *args, **options):
        User = get_user_model()

        users = User.objects.all()
        for user in users:
            print str(user.id) + ": " + user.first_name + " " + user.last_name
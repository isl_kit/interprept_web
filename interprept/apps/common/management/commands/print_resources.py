#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join as join_path

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, convert_start
from apps.speeches.models import Report, ReportLanguage

class Command(BaseCommand):

    def handle(self, *args, **options):
        ids = None
        for arg in args:
            if arg.startswith("ids="):
                if ids is not None:
                    raise Exception("multiple use of 'ids' argument")
                ids = arg[4:].split(",")
            else:
                raise Exception("unknown argument")

        if ids is None:
            return

        res = {}

        for id in ids:
            res[id] = {}
            try:
                resource = Resource.objects.get(id=id)
                res[id]["hash"] = resource.hash
                if resource.reportlanguage_set.count() > 0:
                    rl = resource.reportlanguage_set.all()[:1].get()
                    res[id]["msg"] = rl.report.key
                elif resource.userdocument_set.count() > 0:
                    ud = resource.userdocument_set.all()[:1].get()
                    res[id]["msg"] = join_path(ud.directory.path, ud.name)
                else:
                    res[id]["msg"] = "Unbound Resource"
            except Resource.DoesNotExist:
                res[id]["msg"] = "DoesNotExist"

        for key,val in res.iteritems():
            print str(key) + " (" + val["hash"] + ") => " + val["msg"]

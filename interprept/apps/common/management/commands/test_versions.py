#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

import semantic_version
from apps.common.services.pdf.pdfparser import get_pdf_version
from apps.common.services.communication.libwrapper import get_terminology_version, get_ne_version, create_ne_list_from_text

class Command(BaseCommand):

    def handle(self, *args, **options):
        v = get_pdf_version()
        v2 = get_terminology_version()
        v3 = get_ne_version()
        print v
        print v2
        print v3
        res = create_ne_list_from_text("Hello, Nr. 24. Mr. Smith is today not at home!")
        print res

        v1 = semantic_version.Version("0.0.1")
        v2 = semantic_version.Version("0.0.2")
        res = v1 < v2
        print res

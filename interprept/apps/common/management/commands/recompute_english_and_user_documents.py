# -*- coding: utf-8 -*-

import json
from time import sleep
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.models import Count
import semantic_version

from apps.speeches.models import ReportLanguage
from apps.resources.models import Resource, convert_start, terminology_start, ne_start
from apps.common.services.pdf.pdfparser import get_pdf_version
from apps.common.services.communication.libwrapper import get_ne_version, get_terminology_version

class Command(BaseCommand):

    def handle(self, *args, **options):

        counter = 0
        done = []
        touched = []
        not_processing = []

        finished_total = []
        abort_total = []

        resources_all = Resource.objects.annotate(num_doc=Count("userdocument"), num_rep=Count("reportlanguage")).filter(Q(language__tag="en") | Q(num_doc__gt=0), is_converted=True)

        all_ids = [x.id for x in resources_all]
        len_docs = resources_all.count()
        resources = resources_all[:7]

        for resource in resources:
            counter += 1
            print str(counter) + "/" + str(len_docs)
            touched.append({
                "id": resource.id,
                "d": datetime.now()
            })
            terminology_start(resource, True)
            ne_start(resource, True)

        while True:
            sleep(10)
            touched_ids = [x["id"] for x in touched]
            touched_but_not_done = [x for x in touched_ids if x not in done]
            finished_resources = Resource.objects.filter(id__in=touched_but_not_done, has_ne_list=True, has_terminology_list=True)
            finished_ids = [x.id for x in finished_resources]
            finished_total = finished_total + finished_ids

            touched_but_not_finished = [x for x in touched_but_not_done if x not in finished_ids]
            not_processing_resources = Resource.objects.filter(id__in=touched_but_not_finished, is_busy_on_terminology=False, is_busy_on_ne=False)
            not_processing_ids = [x.id for x in not_processing_resources]

            for resource_id in not_processing_ids:
                if resource_id in not_processing:
                    finished_ids.append(resource_id)
                    abort_total.append(resource_id)
                    not_processing.remove(resource_id)
                    print "removed " + str(resource_id) + " after twice not active"
                else:
                    not_processing.append(resource_id)

            remains = [x for x in touched if x["id"] not in finished_ids and x["id"] not in done]
            d_test = datetime.now()
            for obj in remains:
                if (d_test - obj["d"]).total_seconds() > 1800:
                    finished_ids.append(obj["id"])
                    abort_total.append(obj["id"])
                    print "removed " + str(obj["id"]) + " after 10min not active"
            for resource_id in finished_ids:
                done.append(resource_id)

            if len(finished_ids) > 0:
                resources = Resource.objects.filter(id__in=all_ids).exclude(id__in=touched_ids)[:len(finished_ids)]
                for resource in resources:
                    counter += 1
                    print str(counter) + "/" + str(len_docs)
                    touched.append({
                        "id": resource.id,
                        "d": datetime.now()
                    })
                    terminology_start(resource, True)
                    ne_start(resource, True)

            print str(len(finished_total)) + " : " + str(len(abort_total))

            if len(finished_total) + len(abort_total) >= len(all_ids):
                break


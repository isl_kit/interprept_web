#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from time import sleep
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, convert_start


class Command(BaseCommand):

    def handle(self, *args, **options):

        id_file = None
        for arg in args:
            if arg.startswith("id_file="):
                if id_file is not None:
                    raise Exception("multiple use of 'id_file' argument")
                id_file = arg[8:]
            else:
                raise Exception("unknown argument")

        def unify(seq):
            seen = set()
            seen_add = seen.add
            return [x for x in seq if x not in seen and not seen_add(x)]

        with open(id_file, "r") as f:
            try:
                arr = json.loads(f.read())
            except ValueError:
                arr = []

        counter = 0
        done = []
        touched = []
        not_processing = []
        resources_all = Resource.objects.exclude(id__in=arr)
        all_ids = [x.id for x in resources_all]
        len_docs = resources_all.count()
        resources = resources_all[:7]

        for resource in resources:
            counter += 1
            print str(counter) + "/" + str(len_docs)
            touched.append({
                "id": resource.id,
                "d": datetime.now()
            })
            convert_start(resource, True)

        while True:
            sleep(10)
            touched_ids = [x["id"] for x in touched]
            touched_but_not_done = [x for x in touched_ids if x not in done]
            finished_resources = Resource.objects.filter(id__in=touched_but_not_done, has_ne_list=True, has_terminology_list=True, is_converted=True)
            finished_ids = [x.id for x in finished_resources]

            with open(id_file, "r+") as f:
                try:
                    arr = json.loads(f.read())
                except ValueError:
                    arr = []
                arr = unify(arr + finished_ids)
                f.seek(0)
                json.dump(arr, f)

            touched_but_not_finished = [x for x in touched_but_not_done if x not in finished_ids]
            not_processing_resources = Resource.objects.filter(id__in=touched_but_not_finished, is_busy_on_conversion=False, is_busy_on_terminology=False, is_busy_on_ne=False)
            not_processing_ids = [x.id for x in not_processing_resources]

            for resource_id in not_processing_ids:
                if resource_id in not_processing:
                    finished_ids.append(resource_id)
                    not_processing.remove(resource_id)
                else:
                    not_processing.append(resource_id)

            remains = [x for x in touched if x["id"] not in finished_ids and x["id"] not in done]
            d_test = datetime.now()
            for obj in remains:
                if (d_test - obj["d"]).total_seconds() > 600:
                    finished_ids.append(obj["id"])


            for resource_id in finished_ids:
                done.append(resource_id)

            if len(finished_ids) > 0:
                resources = Resource.objects.filter(id__in=all_ids).exclude(id__in=touched_ids)[:len(finished_ids)]
                for resource in resources:
                    counter += 1
                    print str(counter) + "/" + str(len_docs)
                    touched.append({
                        "id": resource.id,
                        "d": datetime.now()
                    })
                    convert_start(resource, True)

            if counter >= len_docs:
                break
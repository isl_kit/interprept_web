#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.core.management.base import BaseCommand, CommandError
from apps.resources.models import Resource

class Command(BaseCommand):

    def handle(self, *args, **options):
        terms = []
        resources = Resource.objects.filter(language__tag="en")
        counter = 0
        leng = resources.count()
        for resource in resources:
            counter += 1
            print str(counter) + "/" + str(leng)
            try:
                terminology_list = resource.terminologylist
            except:
                continue
            for term in terminology_list.term_set.all():
                name = term.name.lower()
                terms.append(name)
        print len(terms)
        print "make unique"
        terms = list(set(terms))
        print "sort"
        terms.sort()
        print len(terms)
        with open("/tmp/terms.json", "wb+") as f:
            json.dump(terms, f)

# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **options):
        print settings.REST_DOMAIN

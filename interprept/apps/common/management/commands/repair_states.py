#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from time import sleep
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.models import Count
from django.db import transaction
import semantic_version

from apps.resources.models import Resource, ProcessState

class Command(BaseCommand):

    def handle(self, *args, **options):
        resources = Resource.objects.all()
        with transaction.atomic():
            for resource in resources:
                if resource.text_state == ProcessState.IN_PROCESS:
                    resource.text_state = ProcessState.NOT_INITIALIZED
                elif resource.text_state == ProcessState.UPDATING:
                    resource.text_state = ProcessState.DEPRECATED
                if resource.terminology_state == ProcessState.IN_PROCESS:
                    resource.terminology_state = ProcessState.NOT_INITIALIZED
                elif resource.terminology_state == ProcessState.UPDATING:
                    resource.terminology_state = ProcessState.DEPRECATED
                if resource.translate_state == ProcessState.IN_PROCESS:
                    resource.translate_state = ProcessState.NOT_INITIALIZED
                elif resource.translate_state == ProcessState.UPDATING:
                    resource.translate_state = ProcessState.DEPRECATED
                if resource.ne_state == ProcessState.IN_PROCESS:
                    resource.ne_state = ProcessState.NOT_INITIALIZED
                elif resource.ne_state == ProcessState.UPDATING:
                    resource.ne_state = ProcessState.DEPRECATED
                resource.save()

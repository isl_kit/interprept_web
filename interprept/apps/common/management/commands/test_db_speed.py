# -*- coding: utf-8 -*-

import json

from datetime import datetime

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, Term
from apps.languages.models import Language
from apps.translation.db_service import get_translations


class Bench(object):

    def __init__(self):
        self.store = {}

    def start(self, name):
        if name not in self.store:
            self.store[name] = {
                "start": None,
                "total": 0
            }

        if self.store[name]["start"] is None:
            self.store[name]["start"] = datetime.now()

    def stop(self, name):
        if name not in self.store or self.store[name]["start"] is None:
            return
        self.store[name]["total"] += (datetime.now() - self.store[name]["start"]).total_seconds()
        self.store[name]["start"] = None

    def show(self):
        for name, obj in self.store.iteritems():
            print name + " => " + str(obj["total"])

b = Bench()


class Command(BaseCommand):

    def combine_terms(self, terms):
        comb_terms = {}
        for resource_id, ts in terms.iteritems():
            for term in ts:
                lower_name = term["name"].lower()
                base = term["phrase__base__name"]
                if lower_name in comb_terms:
                    comb_term = comb_terms[lower_name]
                    if term["importance"] > comb_term["importance"]:
                        comb_term["importance"] = term["importance"]
                    if resource_id in comb_term["resources"]:
                        resource = comb_term["resources"][resource_id]
                        resource["occurances"] = list(set(resource["occurances"]) | set(json.loads(term["occurances"])))
                    else:
                        comb_term["resources"][resource_id] = {
                            "id": resource_id,
                            "occurances": json.loads(term["occurances"])
                        }
                else:
                    comb_terms[lower_name] = {
                        "base": base,
                        "id": term["id"],
                        "name": term["name"],
                        "importance": term["importance"],
                        "resources": {
                            resource_id: {
                                "id": resource_id,
                                "occurances": json.loads(term["occurances"])
                            }
                        }
                    }
        result = [term for name, term in comb_terms.iteritems()]
        result.sort(key=lambda x: x["importance"], reverse=True)
        return result

    def handle(self, *args, **options):

        for _i in range(10):

            resources = Resource.objects.filter(language__tag="en").order_by("?")[:10]

            source_lang = Language.objects.get(tag="en")
            target_langs = [
                Language.objects.get(tag="de"),
                Language.objects.get(tag="es"),
                Language.objects.get(tag="fr")
            ]

            b.start("terms1")

            terms = {}
            for resource in resources:
                terms[resource.id] = list(Term.objects.filter(terminology_list__resource=resource).select_related("phrase__base").values("id", "importance", "name", "occurances", "phrase__base__name"))
            comb_terms = self.combine_terms(terms)[:200]
            texts = [term["name"] for term in comb_terms]

            b.stop("terms1")
            b.start("trans1")

            (translations_forw, translations_back) = get_translations(texts, source_lang, target_langs)
            for t in translations_forw:
                pass
            for t in translations_back:
                pass

            b.stop("trans1")
        b.show()



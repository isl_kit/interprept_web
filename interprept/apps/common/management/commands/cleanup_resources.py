#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join as join_path, basename, relpath, split as split_path, splitext, exists as path_exists, isfile
from os import makedirs, remove, listdir
import json
from time import sleep

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.models import Count
from django.db import transaction
import semantic_version
from django.conf import settings

from apps.resources.models import Resource, ProcessState
from apps.common.services.pdf.pdfparser import get_pdf_version
from apps.common.services.communication.libwrapper import get_ne_version, get_terminology_version


class Command(BaseCommand):

    def handle(self, *args, **options):

        path = join_path(settings.MEDIA_ROOT, "files", "orig")
        files = []
        for fn in listdir(path):
            fn = join_path(path, fn)
            if isfile(fn):
                files.append(fn)

        for resource in Resource.objects.all():
            if resource.file.path in files:
                files.remove(resource.file.path)

        for f in files:
            print f
            remove(f)


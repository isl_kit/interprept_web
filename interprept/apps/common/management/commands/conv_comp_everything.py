# -*- coding: utf-8 -*-

import json
from time import sleep
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.models import Count
from django.core.exceptions import ObjectDoesNotExist
import semantic_version

from apps.resources.models import Resource
from apps.resources.process import ne_start, terminology_start, convert_start, translate_start
from apps.common.services.pdf.pdfparser import get_pdf_version
from apps.common.services.communication.libwrapper import get_ne_version, get_terminology_version


class Processor:

    def __init__(self):
        self.resources = Resource.objects.all()
        self.length = self.resources.count()
        self.text_counter = 0
        self.ne_counter = 0
        self.te_counter = 0
        self.trans_counter = 0
        self.state = 0
        self.times = {
            "text": None,
            "ne": None,
            "te": None,
            "trans": None
        }
        self.sizes = {
            "text": 0,
            "ne": 0,
            "te": 0
        }

    def go_text(self):
        self.resources = Resource.objects.filter(language__tag="en")
        self.length = self.resources.count()
        self.text_counter = 0
        self.times["text"] = datetime.now()
        self.sizes["text"] = 0
        for resource in self.resources:
            convert_start(resource, self.__text_success, self.__text_error)

    def go_ne(self):
        self.resources = Resource.objects.filter(language__tag="en")
        self.length = self.resources.count()
        self.ne_counter = 0
        self.times["ne"] = datetime.now()
        self.sizes["ne"] = 0
        for resource in self.resources:
            ne_start(resource, success_handler=self.__ne_success, error_handler=self.__ne_error)

    def go_trans(self):
        self.resources = Resource.objects.filter(language__tag="en")
        self.length = self.resources.count()
        self.trans_counter = 0
        self.times["trans"] = datetime.now()
        self.sizes["trans"] = 0
        for resource in self.resources:
            translate_start(resource, success_handler=self.__trans_success, error_handler=self.__trans_error)

    def go_te(self):
        self.resources = Resource.objects.filter(language__tag="en")
        self.length = self.resources.count()
        self.te_counter = 0
        self.times["te"] = datetime.now()
        self.sizes["te"] = 0
        for resource in self.resources:
            terminology_start(resource, success_handler=self.__te_success, error_handler=self.__te_error)

    def __trans_success(self, resource):
        self.trans_counter += 1
        print "TRANS DONE (" + str(self.trans_counter) + "/" + str(self.length) + "): SUCCESS"

    def __trans_error(self, resource, error):
        self.trans_counter += 1
        print resource.get_text_path()
        print "TRANS DONE (" + str(self.trans_counter) + "/" + str(self.length) + "): ERROR --> " + error

    def __text_success(self, resource, version, fdr_no, pe_no, tag, doc_type, a7_no, published):
        self.text_counter += 1
        print "TEXT DONE (" + str(self.text_counter) + "/" + str(self.length) + "): SUCCESS"

    def __text_error(self, resource, error):
        self.text_counter += 1
        print resource.get_text_path()
        print "TEXT DONE (" + str(self.text_counter) + "/" + str(self.length) + "): ERROR --> " + error

    def __ne_success(self, resource, version):
        self.ne_counter += 1
        print "NE DONE (" + str(self.ne_counter) + "/" + str(self.length) + "): SUCCESS"

    def __ne_error(self, resource, error):
        self.ne_counter += 1
        print "NE DONE (" + str(self.ne_counter) + "/" + str(self.length) + "): ERROR --> " + error

    def __te_success(self, resource, version):
        self.te_counter += 1
        print "TE DONE (" + str(self.te_counter) + "/" + str(self.length) + "): SUCCESS"

    def __te_error(self, resource, error):
        self.te_counter += 1
        print "TE DONE (" + str(self.te_counter) + "/" + str(self.length) + "): ERROR --> " + error


class Command(BaseCommand):

    def handle(self, *args, **options):

        proc = Processor()
        proc.go_text()

        while proc.text_counter < proc.length:
            sleep(1)

        proc.go_ne()

        while proc.ne_counter < proc.length:
            sleep(1)

        proc.go_te()

        while proc.te_counter < proc.length:
            sleep(1)

        proc.go_trans()

        while proc.trans_counter < proc.length:
            sleep(1)

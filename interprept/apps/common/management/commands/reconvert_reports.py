#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, ResourceType


class Command(BaseCommand):

    def handle(self, *args, **options):
        resources = Resource.objects.filter(type=ResourceType.REPORT, is_converted=False)
        len_docs = resources.count()
        print "CONVERT RESOURCES:"
        for idx, resource in enumerate(resources):
            print str(idx + 1) + "/" + str(len_docs)
            resource.convert()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import paramiko
import json
import datetime
import shutil
from os.path import split as split_path, exists as path_exists, join as join_path, isfile
from os import mkdir, listdir
from hashlib import sha256
from time import sleep
from magic import from_file as magic_from_file

from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand, CommandError

from celeryrunner import runner
from apps.parliament.db_sync import sync
from apps.speeches.models import Organ
from apps.parliament.webcrawler import download_doc, get_report_keys, get_report_document
from apps.parliament.webcrawler_committee import get_committees

def create_hash(ufile):
    sha256_hash = sha256()
    for chunk in iter(lambda: ufile.read(128 * sha256_hash.block_size), b''):
        sha256_hash.update(chunk)
    return sha256_hash.hexdigest()

def get_mime_type(file_path):
    return magic_from_file(file_path, mime=True)


class Runner:

    def __init__(self, pdf_path, txt_path):
        self.pdf_path = pdf_path
        self.txt_path = txt_path
        self.length = 0
        self.counter = 0
        self.iter = 0
        self.size = 100
        self.hashs = []
        self.time = {}


    def run(self, hashs):
        self.hashs = hashs
        self.length = len(hashs)
        self.counter = 0
        self.iter = 0
        self.start()

    def log(self, text):
        with open("/tmp/doc_log.txt", "a") as f:
            f.write(text + "\n")
        print text

    def start(self):
        done_num = self.iter * self.size
        temp_hashs = self.hashs[done_num:done_num + self.size]
        for idx, hash in enumerate(temp_hashs):
            queue = "high" if idx % 2 == 0 else "celery"
            comp_origfile = join_path(self.pdf_path, hash)
            comp_textfile = join_path(self.txt_path, hash)
            pid = runner.start("isc.pdf.tasks.convert_pdf_wo_analizing", args=[comp_origfile, comp_textfile], meta={"hash":hash}, callback=self.done, queue=queue, scb=self.started)
            #print "send task " + str(pid) + " (" + comp_origfile + ")"
        self.iter += 1

    def started(self, tid, meta):
        h = meta["hash"]
        self.time[h] = datetime.datetime.now()
        self.log("STARTED: " + h + " at: " + self.time[h].strftime("%H:%M:%S"))

    def done(self, tid, result, meta, error):
        h = meta["hash"]
        if h in self.time:
            diff = (datetime.datetime.now() - self.time[h]).total_seconds()
        else:
            diff = 0
        self.log("ENDED: " + h + " total: " + str(diff))

        self.counter += 1
        print "CONVERT: (" + str(self.counter) + "/" + str(self.length) + ")"
        if self.counter == min(self.iter * self.size, len(self.hashs)):
            if self.counter < len(self.hashs):
                self.start()



class Command(BaseCommand):

    def handle(self, *args, **options):
        print args
        path = None
        for arg in args:
            if arg.startswith("path="):
                if path is not None:
                    raise Exception("multiple use of 'path' argument")
                path = arg[5:]
            else:
                raise Exception("unknown argument")

        '''
        if not path_exists(path):
            raise Exception("path does not exist")
        '''

        pdf_path = join_path(path, "pdf")
        txt_path = join_path(path, "txt")
        log_file = join_path(path, "done.log")

        '''

        if not path_exists(log_file):
            with open(log_file, "wb+") as f:
                json.dump({"rep": [], "com": []}, f)


        with open(log_file, "r") as f:
            done = json.loads(f.read())

        if not path_exists(pdf_path):
            mkdir(pdf_path)
        if not path_exists(txt_path):
            mkdir(txt_path)
        '''



        '''
        keys = get_report_keys(datetime.date(2011, 1, 1), datetime.date(2013, 12, 31))
        
        


        print "DOWNLOAD REPORTS"
        keys = [key for key in keys if key not in done["rep"]]
        for idx, key in enumerate(keys):
            print str(idx) + "/" + str(len(keys))
            doc_bytes = get_report_document(key, "en")
            doc_bytes_arr = bytearray(doc_bytes)
            doc_file = ContentFile(doc_bytes_arr)
            hash = create_hash(doc_file)
            if not path_exists(join_path(pdf_path, hash)):
                hashs.append(hash)
                with open(join_path(pdf_path, hash), "wb+") as f:
                    f.write(doc_bytes_arr)
            done["rep"].append(key)
            with open(log_file, "w") as f:
                json.dump(done, f)

        organ_labels = [x.label for x in Organ.objects.all()]
        coms = get_committees(organ_labels, datetime.date(2010, 1, 1), datetime.date(2013, 12, 31))

        print "DOWNLOAD COMMITTEE"
        total = 0
        links_total = []
        print str(len(coms))
        for label, results in coms.iteritems():
            print label
            for res in results:
                docs = res["docs"]
                for doc in docs:
                    links = doc["links"]
                    if "en" in links:
                        link = links["en"]
                        print link
                        if link not in done["com"]:
                            links_total.append(link)

        print str(len(links_total))
        for idx, link in enumerate(links_total):
            print str(idx) + "/" + str(len(links_total))
            doc_bytes = download_doc(link)
            doc_bytes_arr = bytearray(doc_bytes)
            doc_file = ContentFile(doc_bytes_arr)
            hash = create_hash(doc_file)
            if not path_exists(join_path(pdf_path, hash)):
                with open(join_path(pdf_path, hash), "wb+") as f:
                    f.write(doc_bytes_arr)
            done["com"].append(link)
            with open(log_file, "w") as f:
                json.dump(done, f)

        '''

        
        '''
        hashs = {}
        with open("/tmp/doc_log.txt", "r") as f:
            lines = f.read().splitlines()
        for line in lines:
            parts = line.split(" ")
            h = parts[1]
            t = parts[3]
            if parts[0] == "STARTED:":
                hashs[h] = t
            elif parts[0] == "ENDED:":
                if h in hashs:
                    del hashs[h]


        print hashs
        return
        '''

        hashs = []


        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            client.connect("141.3.25.95", username='root', password='#qasd-03')
        except:
            return

        try:
            stdin, stdout, stderr = client.exec_command("ls -la " + pdf_path + " | awk '{print $9}'")
            for line in stdout:
                if len(line) == 65:
                    hashs.append(line[:64])
            stdin, stdout, stderr = client.exec_command("ls -la " + txt_path + " | awk '{print $9}'")
            for line in stdout:
                if len(line) == 65:
                    if line[:64] in hashs:
                        hashs.remove(line[:64])

        except:
            try:
                client.close()
            except:
                pass
            return
        client.close()

        '''

        for fn in listdir(pdf_path):
            if isfile(join_path(pdf_path, fn)):
                if get_mime_type(join_path(pdf_path, fn)) == "application/pdf":
                    hashs.append(fn)

        for fn in listdir(txt_path):
            if isfile(join_path(txt_path, fn)):
                if fn in hashs:
                    hashs.remove(fn)

        '''

        print "list length " + str(len(hashs))

        hashs = [x for x in reversed(hashs)]
        r = Runner(pdf_path, txt_path)
        r.run(hashs)

        while r.length > r.counter:
            sleep(1)

# -*- coding: utf-8 -*-

from passlib.apache import HtpasswdFile
from os.path import exists

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth import get_user_model


def get_map_key(email):
    if not exists(settings.WEBDAV_MAP_FILE):
        return None
    with open(settings.WEBDAV_MAP_FILE, "r") as f:
        wlines = f.read().splitlines()
        for line in wlines:
            tline = line.strip()
            if len(tline) > 0:
                line_arr = tline.split(" ")
                num = line_arr[1]
                email_comp = line_arr[0]
                if email_comp == email:
                    return num
    return None


def remove_from_map(email):
    if not exists(settings.WEBDAV_MAP_FILE):
        return
    data = []
    with open(settings.WEBDAV_MAP_FILE, "r") as f:
        wlines = f.read().splitlines()
        for line in wlines:
            tline = line.strip()
            if len(tline) > 0 and tline.split(" ")[0] != email:
                data.append(tline)
    with open(settings.WEBDAV_MAP_FILE, "w") as f:
        for line in data:
            f.write(line + "\n")


def add_to_map(email):
    if not exists(settings.WEBDAV_MAP_FILE):
        open(settings.WEBDAV_MAP_FILE, "w+").close()
    max_num = 0
    with open(settings.WEBDAV_MAP_FILE, "r") as f:
        wlines = f.read().splitlines()
        for line in wlines:
            tline = line.strip()
            if len(tline) > 0:
                line_arr = tline.split(" ")
                num = int(line_arr[1][:-1])
                email_comp = line_arr[0]
                if email_comp == email:
                    return "{}_".format(num)
                if num > max_num:
                    max_num = num
    with open(settings.WEBDAV_MAP_FILE, "a") as f:
        line = "{} {}_".format(email, max_num)
        f.write(line + "\n")
    return "{}_".format(max_num)


def is_in_file(email, file_path):
    if not exists(file_path):
        return False
    ht = HtpasswdFile(file_path, new=False)
    return email in ht.users()


def remove_from_file(email, file_path):
    if not exists(file_path):
        return
    ht = HtpasswdFile(file_path, new=False)
    ht.delete(email)
    ht.save()


class Command(BaseCommand):

    def handle(self, *args, **options):
        User = get_user_model()

        for user in User.objects.all():
            email = user.email
            is_webdav_initialized = user.is_webdav_initialized
            webdav_alias = user.webdav_alias
            is_in_temp = is_in_file(email, settings.WEBDAV_PASSWORD_FILE_TEMP)
            is_in_main = is_in_file(email, settings.WEBDAV_PASSWORD_FILE)
            is_in_prod = is_in_file(email, settings.WEBDAV_PASSWORD_FILE_PROD)
            map_key = get_map_key(email)

            if is_webdav_initialized:
                if not is_in_prod:
                    user.is_webdav_initialized = False
                    user.save()
                    remove_from_map(email)
                elif map_key is None:
                    user.webdav_alias = add_to_map(email)
                    user.save()
                remove_from_file(email, settings.WEBDAV_PASSWORD_FILE)
                remove_from_file(email, settings.WEBDAV_PASSWORD_FILE_TEMP)
            else:
                if is_in_prod:
                    user.is_webdav_initialized = True
                    user.webdav_alias = add_to_map(email)
                    user.save()
                else:
                    remove_from_map(email)
                remove_from_file(email, settings.WEBDAV_PASSWORD_FILE)
                remove_from_file(email, settings.WEBDAV_PASSWORD_FILE_TEMP)

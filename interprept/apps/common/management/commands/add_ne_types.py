#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import NeType

class Command(BaseCommand):

    def handle(self, *args, **options):
        raw_types = [
            { "id": 1, "name": "loc", "label": "location", "color": "#7EB127" },
            { "id": 2, "name": "org", "label": "organization", "color": "#306925" },
            { "id": 3, "name": "pers", "label": "person", "color": "#f8d214" },


            { "id": 8, "name": "number", "label": "number", "color": "#1f588d"},
            { "id": 5, "name": "amount", "label": "amount", "color": "#357fdf"},
            { "id": 7, "name": "perc", "label": "percent", "color": "#95bde8"},

            { "id": 4, "name": "date", "label": "date", "color": "#ff2eb4" },
            { "id": 6, "name": "time", "label": "time", "color": "#F20056"},


            { "id": 9, "name": "abbr", "label": "abbreviation", "color": "#e67839"},

            { "id": 10, "name": "article", "label": "article reference", "color": "#dadada"},
            { "id": 11, "name": "rule", "label": "rule reference", "color": "#adadad"},
            { "id": 12, "name": "resolution", "label": "resolution reference", "color": "#838383"},
            { "id": 13, "name": "directive", "label": "directive reference", "color": "#5e5e5e"}
        ]

        for raw_type in raw_types:
            try:
                ne_type = NeType.objects.get(id=raw_type["id"])
                ne_type.name = raw_type["name"]
                ne_type.label = raw_type["label"]
                ne_type.color = raw_type["color"]
            except NeType.DoesNotExist:
                ne_type = NeType(id=raw_type["id"], name=raw_type["name"], label=raw_type["label"], color=raw_type["color"])
            ne_type.save()
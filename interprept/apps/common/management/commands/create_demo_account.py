# -*- coding: utf-8 -*-
import os
from shutil import copyfile

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, ProcessState

from django.contrib.auth import get_user_model

class Command(BaseCommand):

    def handle(self, *args, **options):
        User = get_user_model()
        user = User.objects.create_user("demo@example.com", "!-Qist-!")
        user.first_name = "Demo"
        user.last_name = "User"
        user.is_active = True
        user.is_subscriber = True
        user.has_logged_in = True
        user.has_enabled_logging = False
        user.save()

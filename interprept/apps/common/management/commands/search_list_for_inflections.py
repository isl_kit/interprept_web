#!/usr/bin/env python
# -*- coding: utf-8 -*-
from nltk import stem
import re
import string
import traceback
import gc
from datetime import datetime
from difflib import SequenceMatcher

from os.path import exists as path_exists

from django.core.management.base import BaseCommand, CommandError
from apps.translation.models import Phrase
from apps.resources.models import Resource

stemmer = stem.SnowballStemmer("english")

def queryset_iterator(queryset, chunksize=1000):
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()

class Bench(object):

    def __init__(self):
        self.store = {}

    def start(self, name):
        if name not in self.store:
            self.store[name] = {
                "start": None,
                "total": 0
            }

        if self.store[name]["start"] is None:
            self.store[name]["start"] = datetime.now()


    def stop(self, name):
        if name not in self.store or self.store[name]["start"] is None:
            return
        self.store[name]["total"] += (datetime.now() - self.store[name]["start"]).total_seconds()
        self.store[name]["start"] = None

    def show(self):
        for name, obj in self.store.iteritems():
            print name + " => " + str(obj["total"])

b = Bench()

def similar3(line1, line2, l_hash):
    h1 = [stemmer.stem(x.decode("utf-8")) for x in re.sub("\s+", " ", line1).split(" ")]
    h2 = [stemmer.stem(x.decode("utf-8")) for x in re.sub("\s+", " ", line2).split(" ")]
    if len(h1) != len(h2):
        return 0
    for i in range(len(h1)):
        if h1[i] != h2[i]:
            return 0
    return 1

def similar2(line1, line2, l_hash):
    return SequenceMatcher(None, line1, line2).ratio()

def similar(line1, line2, l_hash):
    av_len = (len(line1) + len(line2)) / 2.0
    score = 0
    h1 = l_hash[line1]
    h2 = l_hash[line2]

    for sub in h1:
        l = -1
        match = True
        for ch in sub:
            #b.start("find")
            l2 = line2[l+1:].find(ch)
            #b.stop("find")
            if l2 == -1:
                match = False
                break
            l = l2 + l + 1
        if match:
            if len(sub) > score:
                score = len(sub)
            break
    for sub in h2:
        l = -1
        match = True
        for ch in sub:
            #b.start("find")
            l2 = line1[l+1:].find(ch)
            #b.stop("find")
            if l2 == -1:
                match = False
                break
            l = l2 + l + 1
        if match:
            if len(sub) > score:
                score = len(sub)
            break

    return score * 1.0 / av_len

def r(text):
    for p in string.punctuation + u"\u2019\u2018":
        text = text.replace(p, "")
    return text

class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            '''
            print args
            tasks = ["org", "mep", "rep", "ses", "con", "com"]
            path = None
            limit = None
            for arg in args:
                if arg.startswith("path="):
                    if path is not None:
                        raise Exception("multiple use of 'path' argument")
                    path = arg[5:]
                elif arg.startswith("limit="):
                    if limit is not None:
                        raise Exception("multiple use of 'limit' argument")
                    limit = float(arg[6:])
                else:
                    raise Exception("unknown argument")

            if not path_exists(path):
                raise Exception("path does not exist")

            if limit < 0 or limit > 1:
                raise Exception("limit must be between 0 and 1")

            '''
            limit = 0.5
            names = [
                "tlist_Krüger_20141218_102917283099.csv",
                "tlist_Krüger_20141218_102903772017.csv",
                "tlist_Krüger_20141218_102851832732.csv",
                "tlist_Krüger_20141218_100633123530.csv",
                "tlist_Krüger_20141218_153215356097.csv",
                "tlist_Krüger_20141218_153603846715.csv",
                "tlist_Krüger_20141218_154548809761.csv",
                "tlist_Krüger_20141218_154758496380.csv",
                "tlist_Krüger_20141218_155142637327.csv",
                "tlist_Krüger_20141218_155319142193.csv"
            ]

            lines = []
            for name in names:
                with open("/home/bkrueger/Downloads/" + name, "r") as f:
                    lines = lines + f.read().splitlines()[1:]

            '''
            lines = []
            resources = Resource.objects.filter(language__tag="en")
            counter = 0
            leng = resources.count()
            for resource in resources:
                counter += 1
                print str(counter) + "/" + str(leng)
                try:
                    terminology_list = resource.terminologylist
                except:
                    continue
                for term in terminology_list.term_set.all():
                    name = term.name.lower()
                    lines.append(name)
            
            print "make unique"
            lines = list(set(lines))[:5000]

            print len(lines)
            '''

            '''
            print "read"
            with open(path, "r") as f:
                lines = f.read().splitlines()
            '''

            print "lower"
            lines = [l.lower() for l in lines]

            '''
            print "hash"
            l_hash = {}
            for line in lines:
                if line in l_hash:
                    continue
                l_hash[line] = []
                ll = len(line)
                for i in range(ll):
                    for k in range(ll - i + 1):
                        l_hash[line].append(line[i:k+i])
                l_hash[line] = list(set(l_hash[line]))
                l_hash[line].sort(key=len, reverse=True)
            '''
            #print l_hash["error rate"]
            #return

            lines = [re.sub("\s+", " ", l) for l in lines]
            lines = list(set(lines))
            stems = {}
            for line in lines:
                if line in stems:
                    continue
                print unicode(line, "utf-8")
                stems[line] = " ".join([stemmer.stem(r(x)) for x in unicode(line, "utf-8").split(" ")])

            print "go"
            b.start("total")
            save = 0
            skip = []
            couples = {}
            ll = len(lines)
            for i, line1 in enumerate(lines):
                print str(i) + "/" + str(ll)
                if i in skip:
                    continue
                for k in range(ll - i - 1):
                    i2 = k + i + 1
                    if i2 in skip:
                        continue
                    line2 = lines[i2]
                    b.start("similar")
                    if stems[line1] == stems[line2]:
                        score = 1
                    else:
                        score = 0
                    #score = similar(line1, line2, l_hash)
                    b.stop("similar")
                    if score > limit:
                        save += 1
                        skip.append(i2)
                        if len(line1) < len(line2):
                            sl = line1
                            ls = line2
                        elif len(line2) < len(line1):
                            sl = line2
                            ls = line1
                        elif line1 in couples:
                            sl = line1
                            ls = line2
                        elif line2 in couples:
                            sl = line2
                            ls = line1
                        else:
                            sl = line1
                            ls = line2

                        if sl not in couples:
                            couples[sl] = []
                        couples[sl].append([ls, score])
            b.stop("total")
            #b.show()
            #return

            proc = save * 1.0 / len(lines) * 100
            print "Saved: " + str(proc) + "%"
            with open("/tmp/res.csv", "wb+") as f:
                res = u""
                for name, matches in couples.iteritems():
                    #similar2(pair[0], pair[1])
                    print "\n"
                    print name
                    print stems[name]
                    print type(stems[name])
                    print type(name)
                    res += stems[name] + u";" + unicode(name, "utf-8") + u"\n"
                    for match in matches:
                        res += u";" + unicode(match[0], "utf-8") + u"\n"
                        print "--> " + match[0] + " (" + str(match[1]) + ")"
                f.write(res.encode("utf-8"))
        except:
            print traceback.format_exc()

 

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, NeList, TerminologyList
from apps.speeches.models import CommitteeMeeting, Procedure

class Command(BaseCommand):
    help = "prints stats"

    def handle(self, *args, **options):

        all_cm = []
        for cm in CommitteeMeeting.objects.all():
            if cm.date.year == 2015:
                pm = cm.parl_documents.filter(language__tag="en")
                if pm.count() > 0:
                    all_cm.append(pm[0].resource.hash)

        all_cm = list(set(all_cm))
        diff = len(all_cm) * 12 / 9 - len(all_cm)
        all_none = 0
        for pr in Procedure.objects.all():
            if pr.session:
                if pr.session.date.year == 2015:
                    pm = pr.parl_documents.filter(language__tag="en")
                    if pm.count() > 0:
                        all_cm.append(pm[0].resource.hash)
            else:
                all_none += 1
        all_cm = list(set(all_cm))
        print "NUMBER DOCS: %d" % (len(all_cm) / 9 * 12,)
        print "NUMBER DOCS (WO): %d" % (all_none,)
        print "DIFF: %d" % (diff,)
        return
        resources = Resource.objects.filter(language__tag="en")
        resources_de = Resource.objects.filter(language__tag="de")
        resources_all = Resource.objects.all()
        print "NUMBER OF RESOURCES: %d" % (resources.count(),)
        print "NUMBER OF RESOURCES: %d" % (resources_de.count(),)
        print "NUMBER OF RESOURCES: %d" % (resources_all.count(),)
        return
        nelists = NeList.objects.filter(resource__language__tag="en")
        print "NUMBER OF NELISTS: %d" % (nelists.count(),)
        all_ne = 0
        for nelist in nelists:
            all_ne += nelist.namedentity_set.count()
        print "NUMBER OF ITEMS PER NE LIST: %d" % (all_ne / nelists.count(),)

        terms = []
        telists = TerminologyList.objects.filter(resource__language__tag="en")
        print "NUMBER OF TELISTS: %d" % (telists.count(),)
        all_te = 0
        for telist in telists:
            all_te += telist.term_set.count()
            for term in telist.term_set.all():
                terms.append(term.name.lower())
        terms = list(set(terms))
        print "NUMBER OF ITEMS PER TE LIST: %d" % (all_te / telists.count(),)
        print "NUMBER OF UNIQUE ITEMS PER TE LIST: %d" % (len(terms) / telists.count(),)

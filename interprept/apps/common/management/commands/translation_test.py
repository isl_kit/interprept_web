# -*- coding: utf-8 -*-

from time import sleep

from django.core.management.base import BaseCommand, CommandError

from apps.translation.translators.iate_trans import IateTranslator
from apps.translation.translators.glosbe_trans import GlosbeTranslator
from apps.translation.translation_service import translate
class Command(BaseCommand):

    def handle(self, *args, **options):


        print translate("en", ["de"], ["pesticide resistance"], True)


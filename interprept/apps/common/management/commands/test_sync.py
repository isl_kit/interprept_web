#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from apps.parliament.webcrawler import get_organs


class Command(BaseCommand):

    def handle(self, *args, **options):
        print get_organs()

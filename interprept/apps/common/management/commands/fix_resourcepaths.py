#!/usr/bin/env python                                                                                   
# -*- coding: utf-8 -*-                                                                                                                                           

import json
import codecs
from os.path import join as join_path, relpath, split as split_path, splitext, exists as path_exists, getsize

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth import get_user_model

from apps.documents.models import UserDocument
from apps.speeches.models import ReportLanguage, Procedure
from apps.resources.models import Resource, ResourceType


class Command(BaseCommand):

    def handle(self, *args, **options):

        reportlanguages = ReportLanguage.objects.all()
                                                 
        for reportlanguage in reportlanguages:                                                                                                                    
            save_key = reportlanguage.report.get_save_key()                                             
            print save_key + " (" + reportlanguage.language.tag + ")"                                   
            rootdir = join_path( "reports", save_key, "files", reportlanguage.language.tag)
            orig_file = join_path(rootdir, save_key + ".pdf")                                          
            reportlanguage.resource.file.name = orig_file                                             
            reportlanguage.resource.save()    

        userdocuments = UserDocument.objects.all()
        for userdocument in userdocuments:                                                              
            print userdocument.name                                                                     
            dir_path = userdocument.directory.path if userdocument.directory is not None else ""
            orig_file = join_path("user", str(userdocument.user.id), "files", "orig", dir_path, userdocument.name)                                                
            userdocument.resource.file.name = orig_file                                                 
            userdocument.resource.save()                                                                

        resources = Resource.objects.all()
        len_docs = resources.count()
        print "CONVERT RESOURCES:"
        for resource in resources:
            if "/speeches/" in resource.file.path:
                print resource.file.path
                resource.delete()



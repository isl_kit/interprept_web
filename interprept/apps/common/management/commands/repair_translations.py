#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gc
import json

from django.core.management.base import BaseCommand, CommandError

from apps.translation.models import Phrase
from apps.languages.models import Language

def queryset_iterator(queryset, chunksize=1000):
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()


class Command(BaseCommand):

    def handle(self, *args, **options):
        counter = 0
        lang = Language.objects.get(tag="en")
        terms = []
        with open("/tmp/terms.json", "r") as f:
            terms = json.load(f)

        for term in terms:
            counter += 1
            print str(counter) + "/" + str(len(terms))
            try:
                phrase = Phrase.objects.get(name=term, language=lang, finished__contains="1")
            except:
                continue
            save = False
            for translation in phrase.translation_set.all():
                if "1" in translation.translator_ids:
                    save = True
                    break
            if not save:
                finished = json.loads(phrase.finished)
                for tag, translator_ids in finished.iteritems():
                    if 1 in translator_ids:
                        translator_ids.remove(1)
                phrase.finished = json.dumps(finished)
                phrase.save()

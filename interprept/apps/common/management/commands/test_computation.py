# -*- coding: utf-8 -*-

from __future__ import absolute_import

import os
import json
from time import sleep
from datetime import datetime
from uuid import uuid4

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.models import Count
import semantic_version

from apps.speeches.models import ReportLanguage
from apps.resources.models import Resource
from apps.resources.process import ne_start, terminology_start
from apps.common.services.pdf.pdfparser import get_pdf_version
from apps.common.services.communication.libwrapper import get_ne_version, get_terminology_version

from celery import Celery
from celery.task.control import revoke
from thread import start_new_thread
from django.conf import settings

celery = Celery('interprept')
celery.config_from_object('django.conf:settings')

class Runner():

    def __init__(self):
        print "START CELERY"
        self.tasks = []
        start_new_thread(self.__loop__, ())

    def __loop__(self):
        while True:
            sleep(2)
            ready_tasks = [task for task in self.tasks if task["res"].ready()]
            for task in ready_tasks:
                res = task["res"]
                meta = task["meta"]
                callback = task["callback"]
                queue = task["queue"]
                if callback:
                    if res.status == "SUCCESS":
                        callback(res.id, res.result, meta, None, queue)
                    else:
                        print "ERROR: " + str(res.result)
                        callback(res.id, None, meta, res.result, queue)
                self.tasks.remove(task)

    def start(self, name, args=[], meta={}, callback=None, queue="celery"):
        res = celery.send_task(name, args=args, queue=queue)
        self.tasks.append({
            "res": res,
            "meta": meta,
            "callback": callback,
            "queue": queue
        })
        return res.id

    def stop(self, tid):
        return revoke(tid, terminate=True)



class Processor:

    def __init__(self, path):
        self.callbacks = {}
        self.path = path
        self.length = 0
        self.runner = Runner()
        for f in os.listdir(self.path):
            if f.endswith(".txt"):
                self.length += 1
        self.ne_counter = 0
        self.te_counter = 0

    def go(self):
        index = 0
        for idx, f in enumerate(os.listdir(self.path)):
            if f.endswith(".txt"):

                if index > 0:
                    break

                index += 1

                queue = "celery"
                if index == 40:
                    queue = "high"

                with open(os.path.join(self.path, f), "r") as open_f:
                    text = open_f.read()


                process_id = uuid4()
                self.callbacks[process_id] = {
                    "success": self.__te_success,
                    "error": self.__te_error
                }
                tid = self.runner.start("isc.terminology.tasks.create_terminology_list", args=[text, index], meta={"resource_id": index, "process_id": process_id}, callback=self.__te_done, queue=queue)

                sleep(10)
                self.runner.stop(tid)

                '''
                process_id = uuid4()
                self.callbacks[process_id] = {
                    "success": self.__ne_success,
                    "error": self.__ne_error
                }
                self.runner.start("isc.ne.tasks.create_ne_list", args=[text], meta={"resource_id": index, "process_id": process_id}, callback=self.__ne_done, queue=queue)
                '''

    def __te_done(self, tid, result, meta, error, queue):
        print "TE DONE ON " + queue
        resource_id = meta["resource_id"]
        process_id = meta["process_id"]
        callback = self.callbacks[process_id]
        try:
            if error:
                callback["error"](None, str(error))
            else:
                sequences = result["sequences"]
                version = result["version"]
                error = result["error"]
                if sequences is None:
                    callback["error"](None, error)
                    return

                callback["success"](None, version)
        except:
            callback["error"](None, traceback.format_exc())
            raise

    def __ne_done(self, tid, result, meta, error, queue):
        print "TE DONE ON " + queue
        resource_id = meta["resource_id"]
        process_id = meta["process_id"]
        callback = self.callbacks[process_id]
        try:
            if error:
                callback["error"](None, str(error))
            else:
                nes = result["nes"]
                version = result["version"]
                error = result["error"]
                if nes is None:
                    callback["error"](None, error)
                    return

                callback["success"](None, version)
        except:
            callback["error"](None, traceback.format_exc())
            raise

    def __ne_success(self, resource, version):
        self.ne_counter += 1
        print "NE DONE (" + str(self.ne_counter) + "/" + str(self.length) +  "): SUCCESS"

    def __ne_error(self, resource, error):
        self.ne_counter += 1
        print "NE DONE (" + str(self.ne_counter) + "/" + str(self.length) +  "): ERROR => " + error

    def __te_success(self, resource, version):
        self.te_counter += 1
        print "TE DONE (" + str(self.te_counter) + "/" + str(self.length) +  "): SUCCESS"

    def __te_error(self, resource, error):
        self.te_counter += 1
        print "TE DONE (" + str(self.te_counter) + "/" + str(self.length) +  "): ERROR => " + error

class Command(BaseCommand):

    def handle(self, *args, **options):
        path = os.path.dirname(os.path.abspath(__file__))
        for index, arg in enumerate(args):
            if index == 0:
                path = os.path.abspath(arg)


        proc = Processor(path)
        proc.go()

        #while True:
        #    pass

        while proc.ne_counter < proc.length or proc.te_counter < proc.length:
            sleep(1)

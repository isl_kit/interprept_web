#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from apps.languages.models import Language
from django.contrib.auth import get_user_model

class Command(BaseCommand):

    def handle(self, *args, **options):
        User = get_user_model()

        languages = Language.objects.all()
        for language in languages:
            language.delete()
        users = User.objects.all()
        for user in users:
            user.delete()

        language_definitions = [
            { "tag": "bg", "name": "Bulgarian" },
            { "tag": "cs", "name": "Czech" },
            { "tag": "da", "name": "Danish" },
            { "tag": "de", "name": "German" },
            { "tag": "el", "name": "Greek" },
            { "tag": "en", "name": "English" },
            { "tag": "es", "name": "Spanish" },
            { "tag": "et", "name": "Estonian" },
            { "tag": "fi", "name": "Finnish" },
            { "tag": "fr", "name": "French" },
            { "tag": "ga", "name": "Irish" },
            { "tag": "hr", "name": "Croatian" },
            { "tag": "hu", "name": "Hungarian" },
            { "tag": "it", "name": "Italian" },
            { "tag": "la", "name": "Latin" },
            { "tag": "lt", "name": "Lithuanian" },
            { "tag": "lv", "name": "Latvian" },
            { "tag": "mt", "name": "Maltese" },
            { "tag": "nl", "name": "Dutch" },
            { "tag": "pl", "name": "Polish" },
            { "tag": "pt", "name": "Portuguese" },
            { "tag": "ro", "name": "Romanian" },
            { "tag": "sk", "name": "Slovak" },
            { "tag": "sl", "name": "Slovenian" },
            { "tag": "sv", "name": "Swedish" }
        ]

        for language_definition in language_definitions:
            language = Language(name=language_definition["name"], tag=language_definition["tag"])
            language.save()

        users = {}

        user = User.objects.create_user("bastian.krueger@kit.edu", "12345")
        user.first_name = "Bastian"
        user.last_name = "Krüger"
        user.is_staff = True
        user.is_superuser = True
        user.save()
        users[user.email] = user

        user = User.objects.create_user("info@interpreter-support.eu", "root")
        user.first_name = "Super"
        user.last_name = "User"
        user.is_staff = True
        user.is_superuser = True
        user.save()
        users[user.email] = user

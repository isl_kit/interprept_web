#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import codecs
from os.path import join as join_path, relpath, split as split_path, splitext, exists as path_exists, getsize

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.transaction import atomic

from apps.documents.models import UserDocument
from apps.speeches.models import ReportLanguage, Procedure
from apps.resources.models import Resource, ResourceType

class Command(BaseCommand):

    @atomic
    def handle(self, *args, **options):
        User = get_user_model()

        reportlanguages = ReportLanguage.objects.filter(resource=None)
        print "migrate reportlanguages"
        for reportlanguage in reportlanguages:
            save_key = reportlanguage.report.get_save_key()
            print save_key + " (" + reportlanguage.language.tag + ")"
            rootdir = join_path(settings.MEDIA_ROOT, "reports", save_key, "files", reportlanguage.language.tag)
            txtdir = join_path(rootdir, "text")
            orig_file = join_path(rootdir, save_key + ".pdf")
            text_file = join_path(txtdir, save_key + ".txt")
            print text_file
            if not path_exists(orig_file):
                raise Exception("File \"" + orig_file + "\" does not exist!")
            converted = path_exists(text_file)
            resource = Resource(size=getsize(orig_file), is_converted=converted, type=ResourceType.REPORT, file=orig_file)
            resource.save()
            if not converted:
                print "convert"
                resource.convert()
            reportlanguage.resource = resource
            reportlanguage.save()

        print "migrate userdocuments"
        userdocuments = UserDocument.objects.filter(resource=None)
        for userdocument in userdocuments:
            print userdocument.name
            dir_path = userdocument.directory.path if userdocument.directory is not None else ""
            orig_file = join_path(settings.MEDIA_ROOT, "user", str(userdocument.user.id), "files", "orig", dir_path, userdocument.name)
            file_name_wo_ext = splitext(userdocument.name)[0]
            text_file = join_path(settings.MEDIA_ROOT, "user", str(userdocument.user.id), "files", "text", dir_path, file_name_wo_ext + ".txt")

            if not path_exists(orig_file):
                raise Exception("File \"" + orig_file + "\" does not exist!")
            converted = path_exists(text_file)
            resource = Resource(size=getsize(orig_file), is_converted=converted, type=ResourceType.USER, file=orig_file)
            resource.save()
            if not converted:
                resource.convert()
            userdocument.resource = resource
            userdocument.save()

        print "migrate user files"
        users = User.objects.all()
        procedures = Procedure.objects.all()
        files_content = []
        for user in users:
            print user.first_name + " " + user.last_name
            for procedure in procedures:
                term_conf_file = join_path(settings.MEDIA_ROOT, "user", str(user.id), "terminology", str(procedure.id), "latest", "latest.conf")
                if path_exists(term_conf_file):
                    result = json.load(codecs.open(term_conf_file, "r", "utf-8"))
                    ids = result["file_ids"]
                    new_ids = []
                    for doc_id in ids:
                        try:
                            userdocument = UserDocument.objects.get(pk=doc_id)
                            new_ids.append(userdocument.resource.id)
                        except UserDocument.DoesNotExist:
                            pass
                    if result["consider_report"] and procedure.report is not None and not procedure.report.is_shallow:
                        new_ids.append(procedure.report.reportlanguage_set.get(language__tag="en").resource.id)


                    sequences = result["sequences"]
                    for sequence in sequences:
                        docs = sequence["docs"]
                        new_docs = []
                        for doc in docs:
                            doc_id = int(doc["id"])
                            if doc_id > -1:
                                try:
                                    userdocument = UserDocument.objects.get(pk=doc_id)
                                    doc["id"] = userdocument.resource.id
                                except UserDocument.DoesNotExist:
                                    continue
                            else:
                                doc["id"] = procedure.report.reportlanguage_set.get(language__tag="en").resource.id
                            new_docs.append(doc)
                        sequence["docs"] = new_docs
                    result["file_ids"] = new_ids

                    files_content.append({
                        "path": term_conf_file,
                        "content": result
                    })


                ne_conf_file = join_path(settings.MEDIA_ROOT, "user", str(user.id), "ne", str(procedure.id), "latest", "latest.conf")
                if path_exists(ne_conf_file):
                    result = json.load(codecs.open(ne_conf_file, "r", "utf-8"))
                    doc_id = result["file_id"]
                    if doc_id != "report":
                        try:
                            userdocument = UserDocument.objects.get(pk=doc_id)
                            new_id = userdocument.resource.id
                        except UserDocument.DoesNotExist:
                            new_id = 0
                    else:
                        new_id = procedure.report.reportlanguage_set.get(language__tag="en").resource.id
                    result["file_id"] = new_id

                    files_content.append({
                        "path": ne_conf_file,
                        "content": result
                    })

        print "write files"
        for fcontent in files_content:
            with codecs.open(fcontent["path"], "wb+", "utf-8") as destination:
                json.dump(fcontent["content"], destination)
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from apps.documents.models import UserDocument
from apps.speeches.models import ReportLanguage

class Command(BaseCommand):

    def handle(self, *args, **options):
        all_docs = UserDocument.objects.all()
        len_docs = all_docs.count()
        print "CONVERT USER DOCS:"
        for idx, doc in enumerate(all_docs):
            print str(idx + 1) + "/" + str(len_docs)
            doc.convert()

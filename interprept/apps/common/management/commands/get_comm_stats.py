#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join as join_path

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import ResourceLanguageGroup
from apps.speeches.models import CommitteeDocument

class Command(BaseCommand):

    def handle(self, *args, **options):
        zero_res = 0
        zero_com = 0
        multi_com = 0
        groups = ResourceLanguageGroup.objects.all()
        for g in groups:
            if g.resource_set.count() == 0:
                zero_res += 1
            if g.committeedocument_set.count() == 0:
                zero_com += 1
            if g.committeedocument_set.count() > 1:
                for c in g.committeedocument_set.all():
                    print c.title
                    print c.organ
                    print c.date
                multi_com += 1
        print groups.count()
        print zero_res
        print zero_com
        print multi_com



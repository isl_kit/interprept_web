#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from time import sleep
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.models import Count
import semantic_version

from apps.resources.models import Resource, ProcessState
from apps.resources.process import convert_start


class Processor:

    def __init__(self):
        valid_states = [ProcessState.FAILED, ProcessState.NOT_INITIALIZED]
        # self.resources = Resource.objects.filter(text_state__in=valid_states)[:20]

        self.resources = Resource.objects.annotate(num_doc=Count("userdocument"), num_rep=Count("reportlanguage")).filter(Q(language__tag="en") | Q(num_doc__gt=0))
        self.length = self.resources.count()
        self.counter = 0

    def go(self):
        for resource in self.resources:
            convert_start(resource, self.__success, self.__error)

    def __success(self, resource, version, fdr_no, pe_no, tag, doc_type):
        self.counter += 1
        print "DONE (" + str(self.counter) + "/" + str(self.length) + "): SUCCESS"

    def __error(self, resource, error):
        self.counter += 1
        print "DONE (" + str(self.counter) + "/" + str(self.length) + "): ERROR => " + error


class Command(BaseCommand):

    def handle(self, *args, **options):
        proc = Processor()
        proc.go()
        while proc.counter < proc.length:
            sleep(1)

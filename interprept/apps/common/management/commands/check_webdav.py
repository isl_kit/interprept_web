# -*- coding: utf-8 -*-

from passlib.apache import HtpasswdFile
from os.path import exists

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth import get_user_model


def get_map_key(email):
    with open(settings.WEBDAV_MAP_FILE, "r") as f:
        wlines = f.read().splitlines()
        for line in wlines:
            tline = line.strip()
            if len(tline) > 0:
                line_arr = tline.split(" ")
                num = line_arr[1]
                email_comp = line_arr[0]
                if email_comp == email:
                    return num
    return None


def is_in_file(email, file_path):
    if not exists(file_path):
        return False
    ht = HtpasswdFile(file_path, new=False)
    return email in ht.users()


class Command(BaseCommand):

    def handle(self, *args, **options):
        User = get_user_model()

        for user in User.objects.all():
            email = user.email
            is_webdav_initialized = user.is_webdav_initialized
            webdav_alias = user.webdav_alias
            is_in_temp = is_in_file(email, settings.WEBDAV_PASSWORD_FILE_TEMP)
            is_in_main = is_in_file(email, settings.WEBDAV_PASSWORD_FILE)
            is_in_prod = is_in_file(email, settings.WEBDAV_PASSWORD_FILE_PROD)
            map_key = get_map_key(email)
            print "{}||{}||{}||{}||{}||{}||{}".format(email, is_webdav_initialized, webdav_alias, is_in_temp, is_in_main, is_in_prod, map_key)

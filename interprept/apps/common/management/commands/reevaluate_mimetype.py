#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, get_mime_type

class Command(BaseCommand):

    def handle(self, *args, **options):
        resources = Resource.objects.all()

        for resource in resources:
            resource.mimetype = get_mime_type(resource.file.path)
            resource.save()

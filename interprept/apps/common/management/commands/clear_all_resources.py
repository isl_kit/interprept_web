#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join as join_path, basename, relpath, split as split_path, splitext, exists as path_exists
from os import makedirs, remove
import json
from time import sleep

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.models import Count
from django.db import transaction
import semantic_version

from apps.speeches.models import ReportLanguage
from apps.resources.models import Resource, ProcessState
from apps.common.services.pdf.pdfparser import get_pdf_version
from apps.common.services.communication.libwrapper import get_ne_version, get_terminology_version


class Command(BaseCommand):

    def handle(self, *args, **options):

        resources_all = Resource.objects.all()

        with transaction.atomic():
            for res in resources_all:
                res.text_state = ProcessState.NOT_INITIALIZED
                res.terminology_state = ProcessState.NOT_INITIALIZED
                res.ne_state = ProcessState.NOT_INITIALIZED
                res.translate_state = ProcessState.NOT_INITIALIZED
                res.conversion_version = "0.0.0"
                res.text_error = ""
                res.terminology_error = ""
                res.translate_error = ""
                res.ne_error = ""
                res.queued = False
                textfile = res.get_text_path()
                if path_exists(textfile):
                    remove(textfile)
                try:
                    res.nelist.delete()
                except:
                    pass
                try:
                    res.terminologylist.delete()
                except:
                    pass
                res.save()

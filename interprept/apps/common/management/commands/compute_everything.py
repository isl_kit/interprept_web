# -*- coding: utf-8 -*-

import json
from time import sleep
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.models import Count
import semantic_version

from apps.speeches.models import ReportLanguage
from apps.resources.models import Resource, ProcessState
from apps.resources.process import ne_start, terminology_start
from apps.common.services.pdf.pdfparser import get_pdf_version
from apps.common.services.communication.libwrapper import get_ne_version, get_terminology_version

class Processor:

    def __init__(self):
        self.valid_states = [ProcessState.FAILED, ProcessState.NOT_INITIALIZED]
        self.ne_resources = Resource.objects.filter(ne_state__in=self.valid_states, text_state=ProcessState.SUCCESS)
        self.te_resources = Resource.objects.filter(terminology_state__in=self.valid_states, text_state=ProcessState.SUCCESS)
        self.ne_length = self.ne_resources.count()
        self.te_length = self.te_resources.count()
        self.ne_counter = 0
        self.te_counter = 0
        self.state = 0

    def go(self):
        for resource in self.ne_resources:
            ne_start(resource, self.__ne_success, self.__ne_error)
        for resource in self.te_resources:
            terminology_start(resource, self.__te_success, self.__te_error)
                

    def __ne_success(self, resource, version):
        self.ne_counter += 1
        print "NE DONE (" + str(self.ne_counter) + "/" + str(self.ne_length) +  "): SUCCESS"

    def __ne_error(self, resource, error):
        self.ne_counter += 1
        print "NE DONE (" + str(self.ne_counter) + "/" + str(self.ne_length) +  "): ERROR => " + error

    def __te_success(self, resource, version):
        self.te_counter += 1
        print "TE DONE (" + str(self.te_counter) + "/" + str(self.te_length) +  "): SUCCESS"

    def __te_error(self, resource, error):
        self.te_counter += 1
        print "TE DONE (" + str(self.te_counter) + "/" + str(self.te_length) +  "): ERROR => " + error

class Command(BaseCommand):

    def handle(self, *args, **options):

        proc = Processor()
        proc.go()

        while proc.ne_counter < proc.ne_length or proc.te_counter < proc.te_length:
            sleep(1)
#!/usr/bin/env python                                                                                  
# -*- coding: utf-8 -*-                                                                                

from time import sleep

from django.core.management.base import BaseCommand, CommandError

from apps.resources.models import Resource, convert_start
from apps.speeches.models import Report, ReportLanguage

class Command(BaseCommand):

    def handle(self, *args, **options):
        keys = ["A7-0077/2012", "A7-0140/2014", "A7-0150/2014", "A7-0392/2013"]
        report_langs = ReportLanguage.objects.filter(report__key__in=keys, language__tag="en")
        counter = 0
        length = report_langs.count()
        res_ids = []
        for rl in report_langs:
            counter += 1
            print str(counter) + "/" + str(length)
            res_ids.append(rl.resource.id)
            convert_start(rl.resource, True)

        while True:
            sleep(10)
            resources = Resource.objects.filter(id__in=res_ids, is_converted=True, has_ne_list=True, has_terminology_list=True)
#            print "finished"                                                                          
#            print [x.id for x in resources]                                                           

            cr = Resource.objects.filter(id__in=res_ids, is_busy_on_conversion=True)
#            print "busy conv"                                                                         
#            print [x.id for x in cr]                                                                  

            nr = Resource.objects.filter(id__in=res_ids, is_busy_on_ne=True)
#            print "busy ne"                                                                           
#            print [x.id for x in nr]                                                                  

            tr = Resource.objects.filter(id__in=res_ids, is_busy_on_terminology=True)
#            print "busy term"                                                                         
#            print [x.id for x in tr]      
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
from os.path import join as join_path

from django.core.management.base import BaseCommand, CommandError

from apps.parliament.db_sync import sync
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **options):
        mail_file = args[0]
        path = join_path(settings.MAIL_DIR, mail_file)

        with open(path, "r") as f:
            lines = f.readlines()
            mail_subject = lines[0].strip()
            mail_content = "".join(lines[2:])

        User = get_user_model()

        users = User.objects.filter(is_subscriber=True)
        for user in users:
            print "send to: " + user.email
            send_mail(mail_subject, mail_content, settings.EMAIL_HOST_USER, [user.email], fail_silently=False)

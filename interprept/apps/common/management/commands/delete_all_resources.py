#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from apps.documents.models import UserDocument
from apps.speeches.models import Report, Session
from apps.resources.models import Resource

class Command(BaseCommand):

    def handle(self, *args, **options):

        sessions = Session.objects.all()
        for session in sessions:
            session.delete()

        reports = Report.objects.all()
        for report in reports:
            report.delete()

        docs = UserDocument.objects.all()
        for doc in docs:
            doc.delete()

        resources = Resource.objects.all()
        print "RL: " + str(resources.count())
        for resource in resources:
            resource.delete()
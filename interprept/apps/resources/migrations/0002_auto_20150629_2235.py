# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='process',
            name='resource',
        ),
        migrations.DeleteModel(
            name='Process',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='conversion_version',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='ne_error',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='ne_state',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='queued',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='rank',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='task_data',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='terminology_error',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='terminology_state',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='text_error',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='text_state',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='translate_error',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='translate_state',
        ),
    ]

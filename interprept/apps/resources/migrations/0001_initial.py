# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.resources.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('translation', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('languages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlackList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num_terms', models.IntegerField(default=0)),
                ('terms', models.TextField(default=b'[]')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NamedEntity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('start_index', models.PositiveIntegerField()),
                ('end_index', models.PositiveIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NeList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('version', models.CharField(max_length=10)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NeType',
            fields=[
                ('id', models.PositiveSmallIntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('label', models.CharField(max_length=50)),
                ('color', models.CharField(max_length=7)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Process',
            fields=[
                ('id', models.CharField(max_length=36, serialize=False, primary_key=True)),
                ('process_type', models.PositiveSmallIntegerField(default=0, choices=[(0, b'Convert to Text'), (1, b'Terminology List'), (2, b'Named Entities')])),
                ('pid', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Resource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hash', models.CharField(unique=True, max_length=64)),
                ('size', models.IntegerField()),
                ('conversion_version', models.CharField(max_length=10)),
                ('file', models.FileField(max_length=500, upload_to=apps.resources.models.get_path)),
                ('mimetype', models.CharField(max_length=255)),
                ('text_state', models.PositiveSmallIntegerField(default=0, choices=[(0, b'Not Initialized'), (1, b'In Process'), (2, b'Success'), (3, b'Failed'), (4, b'Updating'), (5, b'Deprecated')])),
                ('ne_state', models.PositiveSmallIntegerField(default=0, choices=[(0, b'Not Initialized'), (1, b'In Process'), (2, b'Success'), (3, b'Failed'), (4, b'Updating'), (5, b'Deprecated')])),
                ('terminology_state', models.PositiveSmallIntegerField(default=0, choices=[(0, b'Not Initialized'), (1, b'In Process'), (2, b'Success'), (3, b'Failed'), (4, b'Updating'), (5, b'Deprecated')])),
                ('translate_state', models.PositiveSmallIntegerField(default=0, choices=[(0, b'Not Initialized'), (1, b'In Process'), (2, b'Success'), (3, b'Failed'), (4, b'Updating'), (5, b'Deprecated')])),
                ('text_error', models.TextField(default=b'', blank=True)),
                ('ne_error', models.TextField(default=b'', blank=True)),
                ('terminology_error', models.TextField(default=b'', blank=True)),
                ('translate_error', models.TextField(default=b'', blank=True)),
                ('task_data', models.TextField(default=b'', blank=True)),
                ('rank', models.IntegerField(default=0)),
                ('queued', models.BooleanField(default=False)),
                ('language', models.ForeignKey(to='languages.Language', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ResourceLanguageGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Term',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('importance', models.FloatField()),
                ('occurances', models.TextField()),
                ('phrase', models.ForeignKey(to='translation.Phrase')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TerminologyList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('version', models.CharField(max_length=10)),
                ('resource', models.OneToOneField(to='resources.Resource')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WhiteList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num_terms', models.IntegerField(default=0)),
                ('terms', models.TextField(default=b'[]')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='term',
            name='terminology_list',
            field=models.ForeignKey(to='resources.TerminologyList'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='resource',
            name='language_group',
            field=models.ForeignKey(to='resources.ResourceLanguageGroup', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='process',
            name='resource',
            field=models.ForeignKey(to='resources.Resource'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='nelist',
            name='resource',
            field=models.OneToOneField(to='resources.Resource'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='namedentity',
            name='ne_list',
            field=models.ForeignKey(to='resources.NeList'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='namedentity',
            name='ne_type',
            field=models.ForeignKey(to='resources.NeType'),
            preserve_default=True,
        ),
    ]

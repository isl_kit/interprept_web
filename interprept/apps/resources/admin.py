#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from django.contrib import admin
from .models import Resource, ResourceLanguageGroup, NeType, NeList, NamedEntity, TerminologyList, Term

def delete_model(modeladmin, request, queryset):
    for obj in queryset:
        obj.delete()

class ResourceAdmin(admin.ModelAdmin):
    fields = ["file", "hash", "size", "mimetype"]
    actions = [delete_model]


admin.site.register(ResourceLanguageGroup)
admin.site.register(Resource, ResourceAdmin)
admin.site.register(NeType)
admin.site.register(NeList)
admin.site.register(NamedEntity)


admin.site.register(TerminologyList)
admin.site.register(Term)
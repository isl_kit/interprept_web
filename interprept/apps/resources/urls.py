#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^getResource/$', views.GetResourceView.as_view(), name="getResource"),
    url(r'^getWebdavContent/$', views.GetWebdavContentView.as_view(), name="getWebdavContent"),
    url(r'^getNe/$', views.GetNeView.as_view(), name="getNe"),
    url(r'^getNeDefinitions/$', views.GetNeDefinitionsView.as_view(), name="getNeDefinitions"),
    url(r'^getTerms/$', views.GetTermsView.as_view(), name="getTerms"),
    url(r'^getText/$', views.GetTextView.as_view(), name="getText"),
    url(r'^saveBlackAndWhiteTerms/$', views.SaveBlackAndWhiteTermsView.as_view(), name="saveBlackAndWhiteTerms"),
)

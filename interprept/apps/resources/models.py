#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join as join_path, basename, relpath, split as split_path, exists as path_exists
from os import remove
from codecs import open as codecs_open

from django.db import models
from django.conf import settings
from magic import from_file as magic_from_file
from django.utils.timezone import now as timezone_now
from django.core.exceptions import ObjectDoesNotExist

from apps.languages.models import Language
from apps.translation.models import Phrase


def get_user_dir_path(userdirectory):
    return join_path("user", str(userdirectory.user.id), "files", "orig", userdirectory.path)


def get_user_doc_path(userdocument):
    dir_path = userdocument.directory.path if userdocument.directory is not None else ""
    return join_path("user", str(userdocument.user.id), "files", "orig", dir_path, userdocument.name)


def get_report_path(reportlanguage):
    save_key = reportlanguage.report.get_save_key()
    return join_path("reports", save_key, "files", reportlanguage.language.tag, save_key + ".pdf")


def get_path(instance, filename):
    return join_path("files", "orig", instance.hash)


def convert_to_comp_path(path):
    return join_path(settings.ISC_MEDIA_ROOT, relpath(path, settings.MEDIA_ROOT))


def replace_in_path(path, name, repl):
    dirs = []
    while 1:
        path, folder = split_path(path)
        if folder != "":
            dirs.append(folder)
        else:
            if path != "":
                dirs.append(path)
            break
    dirs.reverse()
    try:
        index = dirs.index(name)
        dirs[index] = repl
    except ValueError:
        pass
    return join_path(*dirs)


def get_mime_type(file_path):
    return magic_from_file(file_path, mime=True)


class ResourceLanguageGroup(models.Model):

    def __unicode__(self):
        resources = self.resource_set.all()
        return " | ".join([x.language.tag for x in resources])

    class Meta:
        app_label = "resources"


class Resource(models.Model):

    hash = models.CharField(max_length=64, unique=True)
    size = models.IntegerField()
    file = models.FileField(upload_to=get_path, max_length=500)
    mimetype = models.CharField(max_length=255)
    language = models.ForeignKey(Language, null=True)
    language_group = models.ForeignKey(ResourceLanguageGroup, null=True)

    def is_unbound(self):
        if self.userdocument_set.count() > 0:
            return False
        try:
            parldoc = self.parldocument
            if parldoc.committeemeeting_set.count() > 0 or parldoc.procedure_set.count() > 0:
                return False
            else:
                return True
        except ObjectDoesNotExist:
            return True

    def get_file_name(self):
        return basename(self.get_orig_path())

    def get_orig_path(self):
        return self.file.path

    def get_text_path(self):
        return join_path(settings.MEDIA_ROOT, "files", "text", self.hash)

    def has_state(self, task_name, states):
        rts = self.resourcetaskstate_set.filter(task_name=task_name)
        return (rts.count() > 0 and rts[0].state in states) or (rts.count() == 0 and "init" in states)

    def get_text_content(self):
        text_file_content = ""
        if self.has_state("text", ["ok", "deprecated"]):
            textfile_path = self.get_text_path()
            with codecs_open(textfile_path, "r", "utf-8", errors="ignore") as text_file:
                text_file_content = text_file.read()
        return text_file_content

    def save(self, ufile=None, name=None, language=None, language_group=None, *args, **kwargs):
        if ufile is not None and name is not None:
            path = join_path(settings.MEDIA_ROOT, get_path(self, ""))
            if path_exists(path):
                remove(path)
            self.file.save(name, ufile, save=False)
            self.mimetype = get_mime_type(self.file.path)
            self.language = language
            if language_group is None:
                language_group = ResourceLanguageGroup()
                language_group.save()
            self.language_group = language_group
        super(Resource, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        origfile_path = self.get_orig_path()
        textfile_path = self.get_text_path()
        delete_result = super(Resource, self).delete(*args, **kwargs)
        if path_exists(textfile_path):
            remove(textfile_path)
        if path_exists(origfile_path):
            remove(origfile_path)
        return delete_result

    def __unicode__(self):
        return self.get_orig_path()

    class Meta:
        app_label = "resources"


class NeType(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    label = models.CharField(max_length=50)
    color = models.CharField(max_length=7)

    def __unicode__(self):
        return self.label + " (" + self.color + ")"

    class Meta:
        app_label = "resources"


class NeList(models.Model):
    created = models.DateTimeField(editable=False)
    version = models.CharField(max_length=10)
    resource = models.OneToOneField(Resource)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone_now()
        return super(NeList, self).save(*args, **kwargs)

    def __unicode__(self):
        date_string = self.created.strftime("%d.%m.%Y %H:%M:%S")
        return "V" + self.version + " (" + date_string + ")"

    class Meta:
        app_label = "resources"


class NamedEntity(models.Model):
    name = models.TextField()
    ne_type = models.ForeignKey(NeType)
    start_index = models.PositiveIntegerField()
    end_index = models.PositiveIntegerField()
    ne_list = models.ForeignKey(NeList)

    def __unicode__(self):
        return self.name + " (" + str(self.start_index) + ":" + str(self.end_index) + ")"

    class Meta:
        app_label = "resources"


class TerminologyList(models.Model):
    created = models.DateTimeField(editable=False)
    version = models.CharField(max_length=10)
    resource = models.OneToOneField(Resource)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone_now()
        return super(TerminologyList, self).save(*args, **kwargs)

    def __unicode__(self):
        date_string = self.created.strftime("%d.%m.%Y %H:%M:%S")
        return "V" + self.version + " (" + date_string + ")"

    class Meta:
        app_label = "resources"


class Term(models.Model):
    name = models.TextField()
    importance = models.FloatField()
    occurances = models.TextField()
    terminology_list = models.ForeignKey(TerminologyList)
    phrase = models.ForeignKey(Phrase)

    def __unicode__(self):
        return self.name + " (" + str(self.importance) + ")"

    class Meta:
        app_label = "resources"


class WhiteList(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    num_terms = models.IntegerField(default=0)
    terms = models.TextField(default="[]")

    def __unicode__(self):
        return self.user.name + " (" + str(self.num_terms) + ")"

    class Meta:
        app_label = "resources"


class BlackList(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    num_terms = models.IntegerField(default=0)
    terms = models.TextField(default="[]")

    def __unicode__(self):
        return self.user.name + " (" + str(self.num_terms) + ")"

    class Meta:
        app_label = "resources"

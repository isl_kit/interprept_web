#!/usr/bin/env python
# -*- coding: utf-8 -*-

from hashlib import sha256

from models import Resource
from apps.processor.process import runner


def create_resource(ufile, size, language=None, language_group=None):

    hash = create_hash(ufile)
    try:
        resource = Resource.objects.get(hash=hash)
        created = False
    except Resource.DoesNotExist:
        resource = Resource(hash=hash, size=size)
        resource.save(ufile=ufile, name=hash, language=language, language_group=language_group)
        runner.__init_resource__(resource)
        created = True
    return {
        "resource": resource,
        "created": created
    }


def create_hash(ufile):
    sha256_hash = sha256()
    for chunk in iter(lambda: ufile.read(128 * sha256_hash.block_size), b''):
        sha256_hash.update(chunk)
    return sha256_hash.hexdigest()

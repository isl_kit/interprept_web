#!/usr/bin/env python
# -*- coding: utf-8 -*-

from thread import start_new_thread
import json
import time
from os.path import join as join_path, exists as path_exists
from os import listdir, stat

from passlib.apache import HtpasswdFile

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.core.serializers.json import DjangoJSONEncoder
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.conf import settings

from apps.common.decorators.cache import never_ever_cache
from apps.common.decorators.auth import check_login
from .models import Resource, NeType, NamedEntity, NeList, TerminologyList, Term, WhiteList, BlackList
from apps.accounts.views import getUserData


def get_resource_values(resource):
    lang = None
    if resource.language is not None:
        lang = {
            "id": resource.language.id,
            "name": resource.language.name,
            "tag": resource.language.tag
        }

    task_states = {}
    for rts in resource.resourcetaskstate_set.all():
        task_states[rts.task_name] = {
            "running": rts.running,
            "state": rts.state,
            "version": rts.version,
            "hash": rts.hash,
            "error_message": rts.error_message
        }

    return {
        "id": resource.id,
        #"text_state": resource.text_state,
        #"terminology_state": resource.terminology_state,
        #"ne_state": resource.ne_state,
        #"translate_state": resource.translate_state,
        #"text_error": resource.text_error,
        #"terminology_error": resource.terminology_error,
        #"ne_error": resource.ne_error,
        #"translate_error": resource.translate_error,
        "name": resource.get_file_name(),
        "size": resource.size,
        "mimetype": resource.mimetype,
        "hash": resource.hash,
        "language": lang,
        "task_states": task_states
    }


def get_resource_language_group_values(language_group):
    result = []
    for resource in language_group.resource_set.all():
        lang = resource.language
        if lang is not None:
            result.append({
                "tag": lang.tag,
                "id": resource.id
            })
    return result


def does_user_have_access(resource, user):
    try:
        parldoc = resource.parldocument
        return True
    except ObjectDoesNotExist:
        pass

    if resource.userdocument_set.filter(Q(user=user) | Q(is_private=False)).count() > 0:
        return True
    return False


class GetResourceView(View):
    def get(self, request, *args, **kwargs):
        try:
            resource_id = request.GET["resource_id"]
            try:
                resource = Resource.objects.get(pk=resource_id)
                if not does_user_have_access(resource, request.user):
                    return HttpResponse(json.dumps({"message": "no access"}), content_type="application/json", status=403)
            except Resource.DoesNotExist:
                return HttpResponse(json.dumps({"message": "resource does not exist"}), content_type="application/json", status=404)
            return HttpResponse(json.dumps({"resource": get_resource_values(resource)}, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetResourceView, self).dispatch(*args, **kwargs)


class GetTextView(View):
    def get(self, request, *args, **kwargs):
        try:
            resource_id = request.GET["resource_id"]
            try:
                resource = Resource.objects.get(pk=resource_id)
                if not does_user_have_access(resource, request.user):
                    return HttpResponse(json.dumps({"message": "no access"}), content_type="application/json", status=403)
            except Resource.DoesNotExist:
                return HttpResponse(json.dumps({"message": "resource does not exist"}), content_type="application/json", status=404)
            return HttpResponse(json.dumps({"text": resource.get_text_content()}), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetTextView, self).dispatch(*args, **kwargs)


#################
#
# WEBDAV
#
#################

class GetWebdavContentView(View):
    def get(self, request, *args, **kwargs):

        if not request.user.is_webdav_initialized:
            ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE, new=not path_exists(settings.WEBDAV_PASSWORD_FILE))
            return HttpResponse(json.dumps({"initialized": request.user.email in ht.users(), "files": []}), content_type="application/json", status=200)

        path = join_path(settings.WEBDAV_USER_HOME, request.user.webdav_alias)

        files = []
        for infile in listdir(path):
            st = stat(join_path(path, infile))
            files.append({
                "name": infile,
                "size": st.st_size,
                "modified": st.st_mtime
            })

        return HttpResponse(json.dumps({"initialized": True, "files": files}), content_type="application/json", status=200)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetWebdavContentView, self).dispatch(*args, **kwargs)




#################
#
# NAMED ENTITIES
#
#################


def get_ne_type_values(ne_type):
    return {
        "id": ne_type.id,
        "name": ne_type.name,
        "label": ne_type.label,
        "color": ne_type.color
    }


def get_named_entity_values(named_entity):
    return {
        "id": named_entity.id,
        "name": named_entity.name,
        "ne_type": get_ne_type_values(named_entity.ne_type),
        "start_index": named_entity.start_index,
        "end_index": named_entity.end_index
    }


class GetNeView(View):

    def get(self, request, *args, **kwargs):
        try:
            resource_id = request.GET["resource_id"]
            try:
                resource = Resource.objects.get(pk=resource_id)
                if not does_user_have_access(resource, request.user):
                    return HttpResponse(json.dumps({"message": "no access"}), content_type="application/json", status=403)
            except Resource.DoesNotExist:
                return HttpResponse(json.dumps({"message": "resource does not exist"}), content_type="application/json", status=404)

            nes = NamedEntity.objects.filter(ne_list__resource=resource).select_related("ne_type")
            nes = [get_named_entity_values(x) for x in nes]

            return HttpResponse(json.dumps({"nes": nes}), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetNeView, self).dispatch(*args, **kwargs)


class GetNeDefinitionsView(View):

    def get(self, request, *args, **kwargs):
        nes_def = [get_ne_type_values(x) for x in NeType.objects.all()]
        return HttpResponse(
            json.dumps({"nes_def": nes_def}),
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetNeDefinitionsView, self).dispatch(*args, **kwargs)


#################
#
# TERMINOLOGY
#
#################


def get_term_values(term):
    return {
        "id": term.id,
        "name": term.name,
        "importance": term.importance,
        "occurances": json.loads(term.occurances)
    }


def combine_terms(terms):
    comb_terms = {}
    for resource_id, ts in terms.iteritems():
        for term in ts:
            lower_name = term["name"].lower()
            base = term["phrase__base__name"]
            if lower_name in comb_terms:
                comb_term = comb_terms[lower_name]
                if term["importance"] > comb_term["importance"]:
                    comb_term["importance"] = term["importance"]
                if resource_id in comb_term["resources"]:
                    resource = comb_term["resources"][resource_id]
                    resource["occurances"] = list(set(resource["occurances"]) | set(json.loads(term["occurances"])))
                else:
                    comb_term["resources"][resource_id] = {
                        "id": resource_id,
                        "occurances": json.loads(term["occurances"])
                    }
            else:
                comb_terms[lower_name] = {
                    "base": base,
                    "id": term["id"],
                    "name": term["name"],
                    "importance": term["importance"],
                    "resources": {
                        resource_id: {
                            "id": resource_id,
                            "occurances": json.loads(term["occurances"])
                        }
                    }
                }
    result = [term for name, term in comb_terms.iteritems()]
    result.sort(key=lambda x: x["importance"], reverse=True)
    if len(result) > 0:
        max_importance = result[0]["importance"]
        for term in result:
            term["importance"] = term["importance"] / max_importance
    return result


class GetTermsView(View):

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            resource_ids = data["resource_ids"]

            terms = {}
            for resource_id in resource_ids:
                try:
                    resource = Resource.objects.get(pk=resource_id)
                    if not does_user_have_access(resource, request.user):
                        return HttpResponse(json.dumps({"message": "no access"}), content_type="application/json", status=403)
                except Resource.DoesNotExist:
                    continue

                terms[resource_id] = list(Term.objects.filter(terminology_list__resource=resource).select_related("phrase__base").values("id", "importance", "name", "occurances", "phrase__base__name"))

            comb_terms = combine_terms(terms)[:1000]

            return HttpResponse(json.dumps({"terms": comb_terms}), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetTermsView, self).dispatch(*args, **kwargs)


class SaveBlackAndWhiteTermsView(View):

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            black_terms = data["black"]
            white_terms = data["white"]

            try:
                white_list = WhiteList.objects.get(user=request.user)
            except WhiteList.DoesNotExist:
                white_list = WhiteList(user=request.user)
                white_list.save()

            try:
                black_list = BlackList.objects.get(user=request.user)
            except BlackList.DoesNotExist:
                black_list = BlackList(user=request.user)
                black_list.save()

            save_black_list = False
            save_white_list = False

            white_term_arr = json.loads(white_list.terms)
            for term in white_terms:
                if term not in white_term_arr:
                    save_white_list = True
                    white_term_arr.append(term)

            black_term_arr = json.loads(black_list.terms)
            for term in black_terms:
                if term not in black_term_arr:
                    save_black_list = True
                    black_term_arr.append(term)

            if save_white_list:
                white_list.terms = json.dumps(white_term_arr)
                white_list.num_terms = len(white_term_arr)
                white_list.save()

            if save_black_list:
                black_list.terms = json.dumps(black_term_arr)
                black_list.num_terms = len(black_term_arr)
                black_list.save()

            return HttpResponse(json.dumps({}), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(SaveBlackAndWhiteTermsView, self).dispatch(*args, **kwargs)

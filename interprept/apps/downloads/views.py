#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import csv
import codecs
import cStringIO
import os
from datetime import datetime

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.conf import settings
from django.core.files import File

from apps.common.decorators.auth import check_login
from apps.common.decorators.cache import never_ever_cache


class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)



class DownloadAsTableView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            content = data["content"]
            file_name = "tlist_" + datetime.now().strftime("%Y%m%d_%H%M%S%f") + ".csv"
            download_path = settings.MEDIA_ROOT + "/downloads"
            file_path = download_path + "/" + file_name
            if not os.path.exists(download_path):
                os.mkdir(download_path)
            with open(file_path, "wb+") as destination:
                writer = UnicodeWriter(destination, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                for row in content:
                    writer.writerow(row)

            return HttpResponse(
                json.dumps({"file_name": file_name}),
                content_type="application/json",
                status=200
            )
        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(DownloadAsTableView, self).dispatch(*args, **kwargs)

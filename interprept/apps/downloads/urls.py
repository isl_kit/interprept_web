#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^downloadAsTable/$', views.DownloadAsTableView.as_view(), name="downloadAsTable"), 
)
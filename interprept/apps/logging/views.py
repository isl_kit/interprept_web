#!/usr/bin/env python
# -*- coding: utf-8 -*-


import json
import traceback
from os import makedirs
from os.path import join as join_path, basename, relpath, split as split_path, exists as path_exists

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.conf import settings
from django.contrib.auth import get_user_model

from apps.common.decorators.auth import check_login
from apps.common.decorators.cache import never_ever_cache


class ChangeLoggingSettingView(View):

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            enabled = data["enabled"]
            request.user.has_enabled_logging = not not enabled
            request.user.save()

            return HttpResponse(
                json.dumps({}),
                content_type="application/json",
                status=200
            )
        except KeyError:
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )

    @method_decorator(check_login)
    @method_decorator(never_ever_cache)
    def dispatch(self, *args, **kwargs):
        return super(ChangeLoggingSettingView, self).dispatch(*args, **kwargs)


class WriteView(View):
    def get_message(self, log_type, log_data, time):
        if log_type == "init":
            return u"|".join([u"### INIT ###", time, log_data["browserName"], str(log_data["browserVersion"]), log_data["os"], str(log_data["osType"]), str(log_data["osBits"]), log_data["language"]])
        elif log_type == "click":
            return u"|".join([u"CLICK", time, str(log_data["x"]), str(log_data["y"]), str(log_data["w"]), str(log_data["h"]), log_data["element"]["name"], u",".join([name + u":" + str(value) for name, value in log_data["element"]["attributes"].iteritems()])])
        elif log_type == "stateChange":
            return u"|".join([u"SWITCH", time, log_data["state"], u",".join([name + u":" + str(value) for name, value in log_data["params"].iteritems()])])
        elif log_type == "end":
            return u"|".join([u"### END ###", time])
        elif log_type == "logout":
            return u"|".join([u"### LOGOUT ###", time])

    def post(self, request, *args, **kwargs):
        User = get_user_model()
        try:
            data = json.loads(request.body)
            messages = data["messages"]
            logout_messages = [x for x in messages if x["type"] == "logout"]
            if request.user.is_authenticated():
                user = request.user

            elif len(logout_messages) > 0:
                try:
                    user = User.objects.get(email=logout_messages[0]["data"]["email"])
                except User.DoesNotExist:
                    return HttpResponse(
                        json.dumps({"success": False}),
                        content_type="application/json",
                        status=200
                    )
            else:
                return HttpResponse(
                    json.dumps({"success": False}),
                    content_type="application/json",
                    status=200
                )

            if user.has_enabled_logging is not True:
                return HttpResponse(
                    json.dumps({"success": False}),
                    content_type="application/json",
                    status=200
                )

            email = user.email
            browser = data["browser"]
            uuid = data["uuid"]
            log_dir = join_path(settings.LOG_DIR, email, browser)
            if not path_exists(log_dir):
                makedirs(log_dir)
            file_path = join_path(log_dir, uuid)
            with open(file_path, "a") as f:
                for message in messages:
                    time = message["time"]
                    log_type = message["type"]
                    log_data = message["data"]
                    f.write(self.get_message(log_type, log_data, time) + "\n")

            return HttpResponse(
                json.dumps({"success": True}),
                content_type="application/json",
                status=200
            )
        except KeyError:
            print traceback.format_exc()
            return HttpResponse(
                json.dumps({"message": "incomplete data"}),
                content_type="application/json",
                status=404
            )


    @method_decorator(never_ever_cache)
    def dispatch(self, *args, **kwargs):
        return super(WriteView, self).dispatch(*args, **kwargs)

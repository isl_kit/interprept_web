#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^changeLoggingSetting/$', views.ChangeLoggingSettingView.as_view(), name="changeLoggingSetting"),
    url(r'^write/$', views.WriteView.as_view(), name="write"),
)
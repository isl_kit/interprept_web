#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import re

from django.db import models
from django.utils.timezone import now as timezone_now

from apps.languages.models import Language
from apps.resources.models import Resource


class Organ(models.Model):
    label = models.CharField(max_length=10)
    name = models.CharField(max_length=200)
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()
    compare_name = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone_now()
        self.modified = timezone_now()
        self.compare_name = re.sub("[^a-z]", "", self.name.lower())
        return super(Organ, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name + " (" + self.label + ")"

    class Meta:
        app_label = "speeches"


class OrganLanguage(models.Model):
    organ = models.ForeignKey(Organ)
    language = models.ForeignKey(Language)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.organ.label + " " + self.language.tag

    class Meta:
        app_label = "speeches"


class Person(models.Model):
    pid = models.PositiveIntegerField()
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()
    name = models.CharField(max_length=200)
    lowercase_name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    is_mep = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone_now()
        self.modified = timezone_now()
        self.lowercase_name = self.name.lower()
        return super(Person, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = "speeches"


class ParlDocument(models.Model):
    published = models.DateField(null=True)
    organs = models.ManyToManyField(Organ)
    authors = models.ManyToManyField(Person)
    a7_no = models.CharField(max_length=20, blank=True)
    pe_no = models.CharField(max_length=20, blank=True)
    fdr_no = models.CharField(max_length=10, blank=True)
    doc_type = models.CharField(max_length=10)
    title = models.TextField(blank=True)
    language = models.ForeignKey(Language)
    resource = models.OneToOneField(Resource)

    def delete(self, *args, **kwargs):
        resource = self.resource
        remove_result = super(ParlDocument, self).delete(*args, **kwargs)
        if resource.is_unbound():
            resource.delete()
        return remove_result

    def __unicode__(self):
        return self.language.tag.upper() + ": " + self.title + " (" + "|".join([x.label for x in self.organs.all()]) + ")"

    class Meta:
        app_label = "speeches"


class CommitteeMeeting(models.Model):
    title = models.TextField(blank=True)
    date = models.DateField()
    agenda_no = models.IntegerField()
    ref = models.TextField(blank=True)
    organ = models.ForeignKey(Organ)
    parl_documents = models.ManyToManyField(ParlDocument)
    doc_type = models.CharField(max_length=10)

    def __unicode__(self):
        return str(self.agenda_no) + ": (" + self.ref + ")"

    class Meta:
        app_label = "speeches"


def transform_save_key(save_key):
    key_list = re.split("-", save_key)
    return key_list[0] + "-" + key_list[2] + "/" + key_list[1]


class Session(models.Model):
    date = models.DateField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(null=True)
    title = models.CharField(max_length=100)

    def __unicode__(self):
        date_string = datetime.datetime.combine(self.date, datetime.datetime.min.time()).strftime("%A, %d.%m.%Y")
        start_string = self.start_time.strftime("%H:%M:%S")
        end_string = self.end_time.strftime("%H:%M:%S") if self.end_time else "none"
        return date_string + " " + start_string + " - " + end_string

    class Meta:
        app_label = "speeches"


class SessionLanguage(models.Model):
    session = models.ForeignKey(Session)
    language = models.ForeignKey(Language)
    title = models.CharField(max_length=100)

    def __unicode__(self):
        return self.session.title + " " + self.language.tag

    class Meta:
        app_label = "speeches"


class ProcedureGroup(models.Model):
    title = models.TextField()
    lowercase_title = models.TextField()

    def save(self, *args, **kwargs):
        self.lowercase_title = self.title.lower()
        return super(ProcedureGroup, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title

    class Meta:
        app_label = "speeches"


class ProcedureGroupLanguage(models.Model):
    proceduregroup = models.ForeignKey(ProcedureGroup)
    language = models.ForeignKey(Language)
    title = models.TextField()

    def __unicode__(self):
        return self.proceduregroup.title + " " + self.language.tag

    class Meta:
        app_label = "speeches"


class Procedure(models.Model):
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()
    title = models.TextField()
    lowercase_title = models.TextField()
    position = models.PositiveIntegerField()
    parl_documents = models.ManyToManyField(ParlDocument)
    # report = models.ForeignKey(Report, null=True)
    group = models.ForeignKey(ProcedureGroup, null=True)
    session = models.ForeignKey(Session, null=True)
    authors = models.ManyToManyField(Person)
    organs = models.ManyToManyField(Organ)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone_now()
        self.modified = timezone_now()
        self.lowercase_title = self.title.lower()
        return super(Procedure, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title

    class Meta:
        app_label = "speeches"


class ProcedureLanguage(models.Model):
    procedure = models.ForeignKey(Procedure)
    language = models.ForeignKey(Language)
    title = models.TextField()

    def __unicode__(self):
        return self.procedure.title + " " + self.language.tag

    class Meta:
        app_label = "speeches"

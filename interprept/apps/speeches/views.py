#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import datetime
from dateutil import parser

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.core.serializers.json import DjangoJSONEncoder

from apps.common.decorators.cache import never_ever_cache
from apps.common.decorators.auth import check_login
from .models import Session, Organ, CommitteeMeeting
from apps.resources.views import get_resource_values


def unify(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if x not in seen and not seen_add(x)]


def get_committee_meeting_values(comm_meeting):
    return {
        "id": comm_meeting.id,
        "organ": get_organ_values(comm_meeting.organ),
        "title": comm_meeting.title,
        "date": comm_meeting.date,
        "agenda_no": comm_meeting.agenda_no,
        "ref": comm_meeting.ref,
        "doc_type": comm_meeting.doc_type,
        "parl_documents": [get_parl_document_values(x) for x in comm_meeting.parl_documents.filter(language__tag__in=["en"])]
    }


def get_parl_document_values(parl_doc):
    return {
        "id": parl_doc.id,
        "title": parl_doc.title,
        "a7_no": parl_doc.a7_no,
        "organs": [get_organ_values(organ) for organ in parl_doc.organs.all()],
        "authors": [get_person_values(author) for author in parl_doc.authors.all()],
        "doc_type": parl_doc.doc_type,
        "published": parl_doc.published,
        "pe_no": parl_doc.pe_no,
        "fdr_no": parl_doc.fdr_no,
        "resource": get_resource_values(parl_doc.resource),
        "language": {
            "id": parl_doc.language.id,
            "name": parl_doc.language.name,
            "tag": parl_doc.language.tag
        }
    }


def get_group_values(group):
    return {
        "id": group.id,
        "title": group.title
    }


def get_organ_values(organ):
    return {
        "id": organ.id,
        "name": organ.name,
        "label": organ.label
    }


def get_person_values(person):
    return {
        "id": person.id,
        "name": person.name
    }


def get_procedure_values(procedure):
    return {
        "id": procedure.id,
        "created": procedure.created,
        "modified": procedure.modified,
        "title": procedure.title,
        "position": procedure.position,
        "parl_documents": [get_parl_document_values(x) for x in procedure.parl_documents.filter(language__tag__in=["en"])],
        #"report": get_report_values(procedure.report) if procedure.report is not None else None,
        "group": get_group_values(procedure.group) if procedure.group is not None else None,
        "authors": [get_person_values(x) for x in procedure.authors.all()],
        "organs": [get_organ_values(x) for x in procedure.organs.all()]
    }


def get_session_values(session, shallow=False):
    proc = [] if shallow else [get_procedure_values(x) for x in session.procedure_set.all()]
    return {
        "id": session.id,
        "date": session.date,
        "start_time": session.start_time,
        "end_time": session.end_time,
        "title": session.title,
        "procedures": proc
    }


class GetOrgansView(View):
    def get(self, request, *args, **kwargs):
        result = {
            "organs": [get_organ_values(x) for x in Organ.objects.all()]
        }

        return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder), content_type="application/json", status=200)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetOrgansView, self).dispatch(*args, **kwargs)


class GetCommitteeDaysView(View):
    def get(self, request, *args, **kwargs):
        try:
            year = request.GET["year"]
            organ_id = request.GET["organ_id"]

            try:
                organ = Organ.objects.get(id=organ_id)
            except Organ.DoesNotExist:
                return HttpResponse(json.dumps({"message": "organ does not exist"}), content_type="application/json", status=400)

            if len(year) != 4:
                year = datetime.date.today().year
            else:
                try:
                    year = int(year)
                except ValueError:
                    year = datetime.date.today().year

            dates = unify([x["date"] for x in CommitteeMeeting.objects.filter(organ=organ, date__year=year).order_by("date").values("date")])

            data = {
                "dates": dates,
                "year": year
            }
            return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=404)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetCommitteeDaysView, self).dispatch(*args, **kwargs)


class GetCommitteesOfDayView(View):
    def get(self, request, *args, **kwargs):
        try:
            date_str = request.GET["date"]
            organ_id = request.GET["organ_id"]

            try:
                organ = Organ.objects.get(id=organ_id)
            except Organ.DoesNotExist:
                return HttpResponse(json.dumps({"message": "organ does not exist"}), content_type="application/json", status=400)

            try:
                date = parser.parse(date_str).date()
            except ValueError:
                date = datetime.date.today()

            comm_meetings = CommitteeMeeting.objects.filter(organ=organ, date=date).order_by("agenda_no").select_related("organ").prefetch_related("parl_documents__language")

            data = {
                "committee_meetings": [get_committee_meeting_values(x) for x in comm_meetings]
            }
            return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=404)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetCommitteesOfDayView, self).dispatch(*args, **kwargs)


class GetDaysView(View):
    def get(self, request, *args, **kwargs):
        try:
            year = request.GET["year"]
            if len(year) != 4:
                year = datetime.date.today().year
            else:
                try:
                    year = int(year)
                except ValueError:
                    year = datetime.date.today().year

            dates = unify([x["date"] for x in Session.objects.filter(date__year=year).order_by("date").values("date")])

            data = {
                "dates": dates,
                "year": year
            }
            return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=404)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetDaysView, self).dispatch(*args, **kwargs)


class GetSessionsOfDayView(View):
    def get(self, request, *args, **kwargs):
        try:
            date_str = request.GET["date"]
            try:
                date = parser.parse(date_str).date()
            except ValueError:
                date = datetime.date.today()

            sessions = Session.objects.filter(date=date).order_by("start_time")

            data = {
                "sessions": [get_session_values(x) for x in sessions]
            }
            return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing parameter"}), content_type="application/json", status=404)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetSessionsOfDayView, self).dispatch(*args, **kwargs)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Organ, OrganLanguage, Person, ProcedureGroup, ProcedureGroupLanguage, Procedure, ProcedureLanguage, Session, SessionLanguage, CommitteeMeeting, ParlDocument

class CommitteeMeetingAdmin(admin.ModelAdmin):
    raw_id_fields = ("parl_documents", )

admin.site.register(Organ)
admin.site.register(OrganLanguage)
admin.site.register(Person)
admin.site.register(Procedure)
admin.site.register(ProcedureLanguage)
admin.site.register(ProcedureGroup)
admin.site.register(ProcedureGroupLanguage)
admin.site.register(Session)
admin.site.register(SessionLanguage)
admin.site.register(CommitteeMeeting, CommitteeMeetingAdmin)
admin.site.register(ParlDocument)

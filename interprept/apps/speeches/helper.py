#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

def transform_report_key(key):
    key_list = re.split("-|/", key)
    return key_list[0] + "-" + key_list[2] + "-" + key_list[1]
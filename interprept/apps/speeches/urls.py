#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^getOrgans/$', views.GetOrgansView.as_view(), name="getOrgans"),
    url(r'^getCommitteeDays/$', views.GetCommitteeDaysView.as_view(), name="getCommitteeDays"),
    url(r'^getCommitteesOfDay/$', views.GetCommitteesOfDayView.as_view(), name="getCommitteesOfDay"),
    url(r'^getDays/$', views.GetDaysView.as_view(), name="getDays"),
    url(r'^getSessionsOfDay/$', views.GetSessionsOfDayView.as_view(), name="getSessionsOfDay"),
)
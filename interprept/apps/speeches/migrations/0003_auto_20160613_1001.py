# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('speeches', '0002_auto_20150629_2317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='committeemeeting',
            name='ref',
            field=models.TextField(blank=True),
        ),
    ]

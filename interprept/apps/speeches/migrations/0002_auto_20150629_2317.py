# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('speeches', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='topic',
            name='speech',
        ),
        migrations.DeleteModel(
            name='Speech',
        ),
        migrations.DeleteModel(
            name='Topic',
        ),
    ]

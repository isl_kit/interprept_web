# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0001_initial'),
        ('languages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommitteeMeeting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField(blank=True)),
                ('date', models.DateField()),
                ('agenda_no', models.IntegerField()),
                ('ref', models.CharField(max_length=100, blank=True)),
                ('doc_type', models.CharField(max_length=10)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Organ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=10)),
                ('name', models.CharField(max_length=200)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('compare_name', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrganLanguage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('language', models.ForeignKey(to='languages.Language')),
                ('organ', models.ForeignKey(to='speeches.Organ')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ParlDocument',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('published', models.DateField(null=True)),
                ('a7_no', models.CharField(max_length=20, blank=True)),
                ('pe_no', models.CharField(max_length=20, blank=True)),
                ('fdr_no', models.CharField(max_length=10, blank=True)),
                ('doc_type', models.CharField(max_length=10)),
                ('title', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pid', models.PositiveIntegerField()),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('name', models.CharField(max_length=200)),
                ('lowercase_name', models.CharField(max_length=200)),
                ('is_active', models.BooleanField(default=True)),
                ('is_mep', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Procedure',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('title', models.TextField()),
                ('lowercase_title', models.TextField()),
                ('position', models.PositiveIntegerField()),
                ('authors', models.ManyToManyField(to='speeches.Person')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProcedureGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('lowercase_title', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProcedureGroupLanguage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('language', models.ForeignKey(to='languages.Language')),
                ('proceduregroup', models.ForeignKey(to='speeches.ProcedureGroup')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProcedureLanguage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('language', models.ForeignKey(to='languages.Language')),
                ('procedure', models.ForeignKey(to='speeches.Procedure')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField(null=True)),
                ('title', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SessionLanguage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('language', models.ForeignKey(to='languages.Language')),
                ('session', models.ForeignKey(to='speeches.Session')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Speech',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=1000)),
                ('time_window_start', models.PositiveIntegerField()),
                ('time_window_end', models.PositiveIntegerField()),
                ('key', models.CharField(max_length=20)),
                ('persons', models.CharField(max_length=1000)),
                ('speech', models.ForeignKey(to='speeches.Speech')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='procedure',
            name='group',
            field=models.ForeignKey(to='speeches.ProcedureGroup', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='procedure',
            name='organs',
            field=models.ManyToManyField(to='speeches.Organ'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='procedure',
            name='parl_documents',
            field=models.ManyToManyField(to='speeches.ParlDocument'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='procedure',
            name='session',
            field=models.ForeignKey(to='speeches.Session', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parldocument',
            name='authors',
            field=models.ManyToManyField(to='speeches.Person'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parldocument',
            name='language',
            field=models.ForeignKey(to='languages.Language'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parldocument',
            name='organs',
            field=models.ManyToManyField(to='speeches.Organ'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parldocument',
            name='resource',
            field=models.OneToOneField(to='resources.Resource'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='committeemeeting',
            name='organ',
            field=models.ForeignKey(to='speeches.Organ'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='committeemeeting',
            name='parl_documents',
            field=models.ManyToManyField(to='speeches.ParlDocument'),
            preserve_default=True,
        ),
    ]

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.conf import settings
from django.contrib.auth import get_user_model

from apps.socketioapi.api import emit_to

from apps.common.decorators.auth import check_login
from apps.common.decorators.cache import never_ever_cache

from apps.speeches.models import ParlDocument
from apps.documents.models import UserDocument, UserDirectory
from apps.documents.views import get_or_create_shared_dir


class GetDownloadView(View):
    def get(self, request, file_name=None, *args, **kwargs):

        download_path = settings.MEDIA_ROOT + "/downloads"
        file_path = download_path + "/" + file_name
        if not os.path.exists(file_path):
            return HttpResponse(
                json.dumps({"message": "file not found"}),
                content_type="application/json",
                status=404
            )

        with open(file_path, "r") as f:
            response = HttpResponse(f.read(), content_type="application/octet-stream")
            response["Content-Disposition"] = "inline; filename=" + file_name
            return response

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetDownloadView, self).dispatch(*args, **kwargs)


class GetDownloadInlineView(View):
    def get(self, request, file_name=None, *args, **kwargs):

        download_path = settings.MEDIA_ROOT + "/downloads"
        file_path = download_path + "/" + file_name
        if not os.path.exists(file_path):
            return HttpResponse(
                json.dumps({"message": "file not found"}),
                content_type="application/json",
                status=404
            )

        with open(file_path, "r") as f:
            response = HttpResponse(f.read(), content_type="text/plain")
            response["Content-Disposition"] = "inline; filename=" + file_name
            return response

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetDownloadInlineView, self).dispatch(*args, **kwargs)


def generate_parl_doc_title(parl_doc, postfix="pdf"):
    title = parl_doc.title
    if len(title) == 0:
        comms = parl_doc.committeemeeting_set.all()
        if comms.count() > 0:
            title = comms[0].title
    if len(title) == 0:
        procs = parl_doc.procedure_set.all()
        if procs.count() > 0:
            title = procs[0].title
    if len(title) == 0:
        title = "--unnamed--"
    return title[:40].replace(" ", "-").replace(",", "").replace(".", "") + "_" + parl_doc.language.tag.upper() + "." + postfix


class GetParlDocumentView(View):
    def get(self, request, id=None, file_format=None, *args, **kwargs):

        try:
            parl_doc = ParlDocument.objects.get(id=id)
        except ParlDocument.DoesNotExist:
            return HttpResponse(
                json.dumps({"message": "parliament document not found"}),
                content_type="application/json",
                status=404
            )

        resource = parl_doc.resource

        if file_format == "TXT":
            if resource.has_state("text", ["ok", "deprecated"]):
                return HttpResponse(
                    json.dumps({"message": "no text content available"}),
                    content_type="application/json",
                    status=404
                )
            file_path = resource.get_text_path()
            with open(file_path, "r") as f:
                response = HttpResponse(f.read(), content_type="text/plain")
                response["Content-Disposition"] = "inline; filename=" + generate_parl_doc_title(parl_doc, "txt")
                return response
        else:
            file_path = resource.get_orig_path()
            with open(file_path, "r") as f:
                response = HttpResponse(f.read(), content_type="application/pdf")
                response["Content-Disposition"] = "inline; filename=" + generate_parl_doc_title(parl_doc, "pdf")
                return response

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetParlDocumentView, self).dispatch(*args, **kwargs)


class GetUserDocumentView(View):
    def get(self, request, path=None, user_id=None, file_format=None, *args, **kwargs):
        User = get_user_model()

        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            return HttpResponse(
                json.dumps({"message": "user not found"}),
                content_type="application/json",
                status=404
            )

        home_dir = user.home_dir
        split_path = path.split("/")
        for dir_name in split_path[0:len(split_path) - 1]:
            try:
                new_dir = home_dir.userdirectory_set.get(name=dir_name)
                home_dir = new_dir
            except UserDirectory.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "path does not exist"}),
                    content_type="application/json",
                    status=404
                )
        file_name = split_path[len(split_path) - 1]
        try:
            userdocument = home_dir.userdocument_set.get(name=file_name)
        except UserDocument.DoesNotExist:
            return HttpResponse(
                json.dumps({"message": "path does not exist"}),
                content_type="application/json",
                status=404
            )

        if user != request.user and userdocument.is_private is True:
            return HttpResponse(
                json.dumps({"message": "not authorized"}),
                content_type="application/json",
                status=403
            )

        emit_to({
            "test": 1
        }, "docs")

        resource = userdocument.resource
        if file_format == "TXT":
            if resource.is_converted is False:
                return HttpResponse(
                    json.dumps({"message": "no text content available"}),
                    content_type="application/json",
                    status=404
                )
            file_path = resource.get_text_path()
            with open(file_path, "r") as f:
                response = HttpResponse(f.read(), content_type="text/plain")
                response["Content-Disposition"] = "inline; filename=" + file_name + ".txt"
                return response
        else:
            file_path = resource.get_orig_path()
            with open(file_path, "r") as f:
                response = HttpResponse(f.read(), content_type="application/pdf")
                response["Content-Disposition"] = "inline; filename=" + file_name + ".pdf"
                return response

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetUserDocumentView, self).dispatch(*args, **kwargs)


class GetSharedDocumentView(View):
    def get(self, request, path=None, file_format=None, *args, **kwargs):

        shared_dir = get_or_create_shared_dir()

        split_path = path.split("/")

        for dir_name in split_path[0:len(split_path) - 1]:
            try:
                new_dir = shared_dir.userdirectory_set.get(name=dir_name)
                shared_dir = new_dir
            except UserDirectory.DoesNotExist:
                return HttpResponse(
                    json.dumps({"message": "path does not exist"}),
                    content_type="application/json",
                    status=404
                )

        file_name = split_path[len(split_path) - 1]

        try:
            userdocument = shared_dir.userdocument_set.get(name=file_name)
        except UserDocument.DoesNotExist:
            return HttpResponse(
                json.dumps({"message": "path does not exist"}),
                content_type="application/json",
                status=404
            )

        resource = userdocument.resource
        if file_format == "TXT":
            if resource.is_converted is False:
                return HttpResponse(
                    json.dumps({"message": "no text content available"}),
                    content_type="application/json",
                    status=404
                )
            file_path = resource.get_text_path()
            with open(file_path, "r") as f:
                response = HttpResponse(f.read(), content_type="text/plain")
                response["Content-Disposition"] = "inline; filename=" + file_name + ".txt"
                return response
        else:
            file_path = resource.get_orig_path()
            with open(file_path, "r") as f:
                response = HttpResponse(f.read(), content_type="application/pdf")
                response["Content-Disposition"] = "inline; filename=" + file_name + ".pdf"
                return response

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetSharedDocumentView, self).dispatch(*args, **kwargs)

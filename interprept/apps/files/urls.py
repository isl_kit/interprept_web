#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^parldocs/(?P<id>\d+)/(?P<file_format>(PDF)|(TXT))$', views.GetParlDocumentView.as_view(), name="getParlDocument"),
    url(r'^user/(?P<user_id>\d+)/(?P<file_format>(PDF)|(TXT))/(?P<path>.+)$', views.GetUserDocumentView.as_view(), name="getUserDocument"),
    url(r'^shared/(?P<file_format>(PDF)|(TXT))/(?P<path>.+)$', views.GetSharedDocumentView.as_view(), name="getSharedDocument"),
    url(r'^download/(?P<file_name>.+)$', views.GetDownloadView.as_view(), name="getDownload"),
    url(r'^downloadinline/(?P<file_name>.+)$', views.GetDownloadInlineView.as_view(), name="getDownloadInline"),
)
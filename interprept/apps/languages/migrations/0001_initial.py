# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(max_length=10, verbose_name='language tag')),
                ('name', models.CharField(max_length=50, verbose_name='name')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

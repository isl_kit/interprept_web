#!/usr/bin/env python
# -*- coding: utf-8 -*-


import json

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View

from .models import Language
from apps.common.decorators.auth import check_login
from apps.common.decorators.cache import never_ever_cache

class GetAllView(View):
    def get(self, request, *args, **kwargs):
        result = {
            "languages": list(Language.objects.all().values())
        }
        return HttpResponse(json.dumps(result), content_type="application/json", status=200)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetAllView, self).dispatch(*args, **kwargs)

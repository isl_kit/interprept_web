#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

class Language(models.Model):
    tag = models.CharField(_("language tag"), max_length=10)
    name = models.CharField(_("name"), max_length=50)
    def __unicode__(self):
        return self.name
    class Meta:
        app_label = "languages"

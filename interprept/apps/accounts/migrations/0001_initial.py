# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EmailUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(unique=True, max_length=254, verbose_name='email address')),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('activation_key', models.CharField(max_length=32, verbose_name='activation key')),
                ('is_subscriber', models.BooleanField(default=False, help_text="Designates whether the user has subscribed to the site's newsletter.", verbose_name='subscriber')),
                ('customization', models.TextField(default=b'{}', help_text='Holds a JSON encoded Object with (mostly) design related configurations.', verbose_name='customization')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('is_webdav_initialized', models.BooleanField(default=False, help_text="Designates whether the user's webdav account is active.", verbose_name='is webdav initialized')),
                ('webdav_alias', models.CharField(max_length=10, verbose_name='webdav alias', blank=True)),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('has_logged_in', models.BooleanField(default=False, help_text='Designates whether the user has already logged in once.', verbose_name='login status')),
                ('has_enabled_logging', models.NullBooleanField(default=None, help_text='Designates whether the user has enabled statistical logging.', verbose_name='has enabled logging')),
                ('password_was_resetted', models.BooleanField(default=False, help_text='Designates whether the users password has been resetted.', verbose_name='login status')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

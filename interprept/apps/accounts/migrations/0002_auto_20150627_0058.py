# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0001_initial'),
        ('accounts', '0001_initial'),
        ('languages', '0001_initial'),
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailuser',
            name='draft_dir',
            field=models.OneToOneField(related_name='draft_user', null=True, to='documents.UserDirectory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='emailuser',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='emailuser',
            name='home_dir',
            field=models.OneToOneField(related_name='home_user', null=True, to='documents.UserDirectory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='emailuser',
            name='languages',
            field=models.ManyToManyField(to='languages.Language'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='emailuser',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
            preserve_default=True,
        ),
    ]

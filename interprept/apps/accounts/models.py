#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import random
from passlib.apache import HtpasswdFile
from os.path import exists as path_exists

from django.db import models
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.conf import settings

from apps.languages.models import Language
from apps.documents.models import create_directory, UserDirectory


def id_generator(size=32, chars=string.ascii_lowercase + string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

class EmailUserManager(BaseUserManager):

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, activation_key=id_generator(), **extra_fields)
        user.set_password(password)

        user.save(using=self._db)

        print "CREATE HOME"
        create_directory("HOME", user, None)

        ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_TEMP, new=not path_exists(settings.WEBDAV_PASSWORD_FILE_TEMP))
        ht.set_password(email, password)
        ht.save()

        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)



class EmailUser(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    email = models.EmailField(_("email address"), max_length=254, unique=True)
    first_name = models.CharField(_("first name"), max_length=30, blank=True)
    last_name = models.CharField(_("last name"), max_length=30, blank=True)
    activation_key = models.CharField(_("activation key"), max_length=32)
    is_subscriber = models.BooleanField(_("subscriber"), default=False,
        help_text=_("Designates whether the user has subscribed to the site's newsletter."))
    customization = models.TextField(_("customization"), default="{}",
        help_text=_("Holds a JSON encoded Object with (mostly) design related configurations."))
    is_staff = models.BooleanField(_("staff status"), default=False,
        help_text=_("Designates whether the user can log into this admin "
                    "site."))
    is_active = models.BooleanField(_("active"), default=True,
        help_text=_("Designates whether this user should be treated as "
                    "active. Unselect this instead of deleting accounts."))

    is_webdav_initialized = models.BooleanField(_("is webdav initialized"), default=False,
        help_text=_("Designates whether the user's webdav account is active."))
    webdav_alias = models.CharField(_("webdav alias"), max_length=10, blank=True)

    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    has_logged_in = models.BooleanField(_("login status"), default=False,
        help_text=_("Designates whether the user has already logged in once."))

    has_enabled_logging = models.NullBooleanField(_("has enabled logging"), default=None,
        help_text=_("Designates whether the user has enabled statistical logging."))

    password_was_resetted = models.BooleanField(_("login status"), default=False,
        help_text=_("Designates whether the users password has been resetted."))

    languages = models.ManyToManyField(Language)

    home_dir = models.OneToOneField(UserDirectory, null=True, related_name="home_user")
    draft_dir = models.OneToOneField(UserDirectory, null=True, related_name="draft_user")

    objects = EmailUserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.email)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def save(self, *args, **kwargs):
        create_dirs = False
        if not self.id:
            create_dirs = True
        save_result = super(EmailUser, self).save(*args, **kwargs)
        if create_dirs:
            print "CREATE HOME"
            self.home_dir = create_directory("HOME", self, None)
            self.draft_dir = create_directory("DRAFT", self, None)
            self.save()
        return save_result


    class Meta:
        app_label = "accounts"


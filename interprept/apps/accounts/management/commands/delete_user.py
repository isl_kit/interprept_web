#!/usr/bin/env python
# -*- coding: utf-8 -*-

import shutil
import getpass
from os.path import exists, join
from passlib.apache import HtpasswdFile

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth import get_user_model


class Command(BaseCommand):
    help = "deletes a user"

    def add_arguments(self, parser):
        parser.add_argument("--email", dest="email", default="", help="email address")

    def remove_webdav(self, user):
        try:
            ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_TEMP, new=not exists(settings.WEBDAV_PASSWORD_FILE_TEMP))
            ht.delete(user.email)
            ht.save()
        except:
            pass

        try:
            ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE, new=not exists(settings.WEBDAV_PASSWORD_FILE))
            ht.delete(user.email)
            ht.save()
        except:
            pass

        try:
            ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_PROD, new=not exists(settings.WEBDAV_PASSWORD_FILE_PROD))
            ht.delete(user.email)
            ht.save()
        except:
            pass

        if exists(settings.WEBDAV_MAP_FILE):
            with open(settings.WEBDAV_MAP_FILE, "r+") as f:
                wlines = f.read().splitlines()
                f.seek(0)
                for line in wlines:
                    tline = line.strip()
                    if len(tline) > 0:
                        line_arr = tline.split(" ")
                        if line_arr[0] != user.email:
                            f.write(tline + "\n")
                f.truncate()

        path = join(settings.WEBDAV_USER_HOME, user.webdav_alias)
        if exists(path):
            shutil.rmtree(path)

    def handle(self, *args, **options):
        User = get_user_model()
        try:
            user = User.objects.get(email=options["email"])
        except User.DoesNotExist:
            return
        self.remove_webdav(user)
        user.delete()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pwd
import grp
import getpass
from os import makedirs, chown, chmod
from os.path import exists, join
from passlib.apache import HtpasswdFile

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth import get_user_model


def ask_password():
    while True:
        p1 = getpass.getpass("Type password: ")
        p2 = getpass.getpass("Retype password: ")
        if p1 == p2:
            break
        else:
            print "Passwords do not match. Try again."
    return p1


class Command(BaseCommand):
    help = "creates a new user"

    def add_arguments(self, parser):
        parser.add_argument("--email", dest="email", default="", help="email adress")
        parser.add_argument("--first-name", dest="first_name", default="", help="first name")
        parser.add_argument("--last-name", dest="last_name", default="", help="last name")
        parser.add_argument("--password", dest="password", default="", help="password")
        parser.add_argument("--subscriber", dest="subscriber", default=False, action="store_true", help="does the user subscribe to the newsletter")
        parser.add_argument("--superuser", dest="superuser", default=False, action="store_true", help="is the user superuser")

    def get_apache_gid(self):
        groups = ["httpd", "apache", "www-data", "root"]
        gid = None
        for group in groups:
            try:
                gid = grp.getgrnam(group).gr_gid
                break
            except KeyError:
                pass
        return gid

    def get_apache_uid(self):
        users = ["httpd", "apache", "www-data", "root"]
        uid = None
        for user in users:
            try:
                uid = pwd.getpwnam(user).pw_uid
                break
            except KeyError:
                pass
        return uid

    def setup_webdav(self, user, password):
        uid = self.get_apache_uid()
        gid = self.get_apache_gid()
        uid_root = pwd.getpwnam("root").pw_uid

        if not uid or not gid:
            return

        try:
            ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_TEMP, new=not exists(settings.WEBDAV_PASSWORD_FILE_TEMP))
            ht.delete(user.email)
            ht.save()
        except:
            pass

        try:
            ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE, new=not exists(settings.WEBDAV_PASSWORD_FILE))
            ht.delete(user.email)
            ht.save()
        except:
            pass

        prod_file_exists = exists(settings.WEBDAV_PASSWORD_FILE_PROD)
        ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_PROD, new=not exists(settings.WEBDAV_PASSWORD_FILE_PROD))
        ht.set_password(user.email, password)
        ht.save()
        if not prod_file_exists:
            chmod(settings.WEBDAV_PASSWORD_FILE_PROD, 0640)
            chown(settings.WEBDAV_PASSWORD_FILE_PROD, uid_root, gid)

        wlines = []
        max_num = 0
        emails = {}
        if not exists(settings.WEBDAV_MAP_FILE):
            open(settings.WEBDAV_MAP_FILE, 'a').close()
            chmod(settings.WEBDAV_MAP_FILE, 0640)
            chown(settings.WEBDAV_MAP_FILE, uid_root, gid)

        with open(settings.WEBDAV_MAP_FILE, "r") as f:
            wlines = f.read().splitlines()
            for line in wlines:
                tline = line.strip()
                if len(tline) > 0:
                    line_arr = tline.split(" ")
                    num = int(line_arr[1][:-1])
                    emails[line_arr[0]] = num
                    if num > max_num:
                        max_num = num

        if user.email not in emails:
            with open(settings.WEBDAV_MAP_FILE, "a") as f:
                max_num += 1
                uname = str(max_num) + "_"
                f.write(user.email + " " + uname + "\n")
        else:
            uname = str(emails[user.email]) + "_"

        user.is_webdav_initialized = True
        user.webdav_alias = uname
        user.save()
        path = join(settings.WEBDAV_USER_HOME, uname)
        if not exists(path):
            makedirs(path, 0755)
            chown(path, uid, gid)

    def handle(self, *args, **options):
        User = get_user_model()
        email = User.objects.normalize_email(options["email"])
        try:
            user = User.objects.get(email=email)
            raise Exception("User with email address %s already exists!" % (email,))
        except User.DoesNotExist:
            pass
        password = options["password"]
        if not password:
            password = ask_password()
        if options["superuser"]:
            user = User.objects.create_superuser(email, password)
        else:
            user = User.objects.create_user(email, password)
        user.first_name = options["first_name"]
        user.last_name = options["last_name"]
        user.is_active = True
        user.is_subscriber = options["subscriber"]
        user.save()

        self.setup_webdav(user, password)

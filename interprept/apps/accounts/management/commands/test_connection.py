#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests

from django.core.management.base import BaseCommand, CommandError

from apps.translation.translators.glosbe_trans import GlosbeTranslator
from apps.translation.translators.iate_trans import IateTranslator
from apps.parliament.webcrawler import get_meps


class Command(BaseCommand):
    help = "tests the internet connection"

    def handle(self, *args, **options):

        print "---- Translations ----"
        gtr = GlosbeTranslator()
        glosbe_result = gtr.request("en", ["de"], ["dog"])
        glosbe_success = "Hund" in glosbe_result["dog"]["de"]
        print "GLOSBE: %s" % ("SUCCESS" if glosbe_success else "FAIL",)
        itr = IateTranslator()
        iate_result = itr.request("en", ["de"], "dog")
        iate_success = "Hund" in iate_result["de"]
        print "IATE: %s" % ("SUCCESS" if iate_success else "FAIL",)

        print "---- Parliament ----"
        r = requests.get("http://www.europarl.europa.eu")
        print "EUROPARL: %s" % ("SUCCESS" if r.status_code == 200 else "FAIL",)

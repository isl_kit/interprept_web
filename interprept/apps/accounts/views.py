#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import string
import random
import traceback
from os.path import exists as path_exists

from passlib.apache import HtpasswdFile

from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.auth import get_user_model
from django.contrib.auth.models import check_password
from django.shortcuts import redirect
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.conf import settings

from apps.common.decorators.auth import check_login
from apps.common.decorators.http import redirect_on_get
from apps.common.decorators.cache import never_ever_cache
from apps.languages.models import Language
from .customization_service import set_customization


def getUserData(user, simple=False):
    if simple:
        return {
            "id": user.id,
            "firstname": user.first_name,
            "lastname": user.last_name
        }
    else:
        return {
            "id": user.id,
            "email": user.email,
            "firstname": user.first_name,
            "lastname": user.last_name,
            "customization": json.loads(user.customization)
        }


def pw_generator(size=10, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


class RegisterView(View):
    def post(self, request, *args, **kwargs):
        User = get_user_model()
        if not settings.SELF_REGISTRATION:
            return HttpResponse(json.dumps({"message": "not allowed"}), content_type="application/json", status=404)
        try:
            data = json.loads(request.body)
            email = data["email"].lower()
            first_name = data["first_name"]
            last_name = data["last_name"]
            password1 = data["password1"]
            password2 = data["password2"]
            subscribe = data["subscribe"]
            if len(email) == 0 or len(first_name) == 0 or len(last_name) == 0 or len(password1) == 0:
                return HttpResponse(json.dumps({"message": "Please complete the form", "verr": 0}), content_type="application/json", status=404)
            if User.objects.filter(email=email).count():
                return HttpResponse(json.dumps({"message": "An account for this email does already exist", "verr": 1}), content_type="application/json", status=404)
            if password1 != password2:
                return HttpResponse(json.dumps({"message": "Passwords don't match", "verr": 2}), content_type="application/json", status=404)
            try:
                validate_email(email)
            except ValidationError:
                return HttpResponse(json.dumps({"message": "The email address you have entered is not valid", "verr": 3}), content_type="application/json", status=404)

            user = User.objects.create_user(email, password1)
            user.first_name = first_name
            user.last_name = last_name
            user.is_active = False
            user.is_subscriber = subscribe
            user.save()
            send_mail(
                "Your registration at interpreter-support.eu",
                "Hello " + user.first_name + " " + user.last_name + "!\r\n\r\nThank you for your registration at interpreter-support.eu. To activate your account, please visit the following Link:\r\nhttp://" + request.get_host() + "/auth/verify/?vk=" + user.activation_key,
                settings.EMAIL_HOST_USER, [user.email], fail_silently=False)
            return HttpResponse(
                json.dumps(getUserData(user)),
                content_type="application/json",
                status=200
            )
        except KeyError:
            return HttpResponse(json.dumps({"message": "incomplete userinfo"}), content_type="application/json", status=401)

    @method_decorator(never_ever_cache)
    @method_decorator(redirect_on_get)
    def dispatch(self, *args, **kwargs):
        return super(RegisterView, self).dispatch(*args, **kwargs)


class VerifyView(View):
    def get(self, request, *args, **kwargs):
        User = get_user_model()
        if not settings.SELF_REGISTRATION:
            return redirect("/")
        try:
            activation_key = request.GET["vk"]
            try:
                print activation_key
                user = User.objects.get(activation_key=activation_key)
                if user.is_active is False:
                    user.is_active = True
                    user.save()

                    ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_TEMP, new=not path_exists(settings.WEBDAV_PASSWORD_FILE_TEMP))
                    phash = ht.get_hash(user.email)
                    ht.delete(user.email)
                    ht.save()

                    if phash is not None:
                        with open(settings.WEBDAV_PASSWORD_FILE, "a") as f:
                            f.write(user.email + ":" + phash + "\n")

                    return redirect("/#/login?verify=1")
                else:
                    return HttpResponse(
                        json.dumps({"message": "user is already active"}),
                        content_type="application/json",
                        status=404
                    )
            except User.DoesNotExist:
                return HttpResponse(json.dumps({"message": "no user found"}), content_type="application/json", status=404)
            except MultipleObjectsReturned:
                return HttpResponse(json.dumps({"message": "multiple users found"}), content_type="application/json", status=404)
        except KeyError:
            return HttpResponse(json.dumps({"message": "missing data"}), content_type="application/json", status=404)

    @method_decorator(never_ever_cache)
    def dispatch(self, *args, **kwargs):
        return super(VerifyView, self).dispatch(*args, **kwargs)


class ChangeView(View):

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            password_old = data["password_old"]
            password_new1 = data["password_new1"]
            password_new2 = data["password_new2"]

            if len(password_old) == 0 or len(password_new1) == 0:
                return HttpResponse(json.dumps({"message": "Please complete the form", "verr": 0}), content_type="application/json", status=404)
            if not check_password(password_old, request.user.password):
                return HttpResponse(json.dumps({"message": "Invalid password", "verr": 1}), content_type="application/json", status=404)
            if password_new1 != password_new2:
                return HttpResponse(json.dumps({"message": "Passwords don't match", "verr": 2}), content_type="application/json", status=404)

            request.user.set_password(password_new1)
            request.user.password_was_resetted = False
            request.user.save()

            ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_TEMP, new=not path_exists(settings.WEBDAV_PASSWORD_FILE_TEMP))
            if request.user.email in ht.users():
                ht.set_password(request.user.email, password_new1)
                ht.save()
            ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE, new=not path_exists(settings.WEBDAV_PASSWORD_FILE))
            if request.user.email in ht.users():
                ht.set_password(request.user.email, password_new1)
                ht.save()

            return HttpResponse(json.dumps({}), content_type="application/json", status=200)

        except KeyError:
            return HttpResponse(json.dumps({"message": "incomplete data"}), content_type="application/json", status=500)

    @method_decorator(never_ever_cache)
    @method_decorator(redirect_on_get)
    def dispatch(self, *args, **kwargs):
        return super(ChangeView, self).dispatch(*args, **kwargs)


class ResetView(View):
    def post(self, request, *args, **kwargs):
        User = get_user_model()
        try:
            data = json.loads(request.body)
            email = data["email"].lower()
            try:
                user = User.objects.get(email=email)
                password = pw_generator()
                user.set_password(password)
                user.password_was_resetted = True
                user.save()

                ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE_TEMP, new=not path_exists(settings.WEBDAV_PASSWORD_FILE_TEMP))
                if user.email in ht.users():
                    ht.set_password(user.email, password)
                    ht.save()

                ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE, new=not path_exists(settings.WEBDAV_PASSWORD_FILE))
                if user.email in ht.users():
                    ht.set_password(user.email, password)
                    ht.save()

                send_mail(
                    "Your password has been reset",
                    "Hello " + user.first_name + " " + user.last_name + "!\r\n\r\nYour password at interpreter-support.eu has been reset. Your new password is:\r\n\r\n\t" + password + "\r\n\r\nTo log in you can use the following Link:\r\nhttp://" + request.get_host() + "/#/login",
                    settings.EMAIL_HOST_USER, [user.email], fail_silently=False)

                return HttpResponse(json.dumps({}), content_type="application/json", status=200)
            except User.DoesNotExist:
                return HttpResponse(json.dumps({"failed": True}), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "incomplete data"}), content_type="application/json", status=500)

    @method_decorator(never_ever_cache)
    @method_decorator(redirect_on_get)
    def dispatch(self, *args, **kwargs):
        return super(ResetView, self).dispatch(*args, **kwargs)


class LoginView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            email = data["email"]
            password = data["password"]
            user = authenticate(username=email, password=password)
            result = {
                "user": None,
                "authenticated": False
            }
            if user is not None:
                if user.is_active:
                    user_data = getUserData(user)
                    user_data["has_logged_in"] = user.has_logged_in
                    user_data["password_was_resetted"] = user.password_was_resetted
                    user_data["logging_decision_needed"] = user.has_enabled_logging is None
                    user_data["new_to_webdav"] = False

                    save_user = False
                    if not user.has_logged_in:
                        user.has_logged_in = True
                        save_user = True
                    if user.password_was_resetted:
                        user.password_was_resetted = False
                        save_user = True
                    if save_user:
                        user.save()

                    if not user.is_webdav_initialized:
                        ht = HtpasswdFile(settings.WEBDAV_PASSWORD_FILE, new=not path_exists(settings.WEBDAV_PASSWORD_FILE))
                        if user.email not in ht.users():
                            user_data["new_to_webdav"] = True
                            ht.set_password(user.email, password)
                            ht.save()

                    login(request, user)
                    result["user"] = user_data
                    result["authenticated"] = True
            return HttpResponse(json.dumps(result), content_type="application/json", status=200)
        except KeyError:
            return HttpResponse(json.dumps({"message": "incomplete credentials"}), content_type="application/json", status=500)

    @method_decorator(never_ever_cache)
    @method_decorator(redirect_on_get)
    def dispatch(self, *args, **kwargs):
        return super(LoginView, self).dispatch(*args, **kwargs)


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponse(
            json.dumps({}),
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    def dispatch(self, *args, **kwargs):
        return super(LogoutView, self).dispatch(*args, **kwargs)


class LoginStateView(View):

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponse(
                json.dumps({"authenticated": True, "user": getUserData(request.user)}),
                content_type="application/json",
                status=200
            )
        else:
            return HttpResponse(json.dumps({"authenticated": False}), content_type="application/json", status=200)

    @method_decorator(never_ever_cache)
    def dispatch(self, *args, **kwargs):
        return super(LoginStateView, self).dispatch(*args, **kwargs)


class UserView(View):
    def get(self, request, *args, **kwargs):
        return HttpResponse(
            json.dumps(getUserData(request.user)),
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(UserView, self).dispatch(*args, **kwargs)


class GetCustomizationView(View):
    def get(self, request, *args, **kwargs):
        user = request.user
        return HttpResponse(
            user.customization,
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetCustomizationView, self).dispatch(*args, **kwargs)


class SetCustomizationView(View):
    def post(self, request, *args, **kwargs):
        try:
            user = request.user
            data = json.loads(request.body)
            customization = data["customization"]
            try:
                result = set_customization(user, customization)
                if result:
                    return HttpResponse(
                        user.customization,
                        content_type="application/json",
                        status=200
                    )
                else:
                    return HttpResponse(
                        json.dumps({"message": "not allowed setting"}),
                        content_type="application/json",
                        status=400
                    )
            except:
                print traceback.format_exc()
                return HttpResponse(
                    json.dumps({"message": "error in customization service"}),
                    content_type="application/json",
                    status=500
                )
        except KeyError:
            return HttpResponse(json.dumps({"message": "incomplete data"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(SetCustomizationView, self).dispatch(*args, **kwargs)


class GetSettingsView(View):
    def get(self, request, *args, **kwargs):
        user = request.user
        settings = {
            "is_subscriber": user.is_subscriber,
            "has_enabled_logging": user.has_enabled_logging
        }
        return HttpResponse(
            json.dumps(settings),
            content_type="application/json",
            status=200
        )

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(GetSettingsView, self).dispatch(*args, **kwargs)


class SetSettingsView(View):
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            is_subscriber = data["is_subscriber"]
            has_enabled_logging = data["has_enabled_logging"]
            request.user.is_subscriber = is_subscriber
            request.user.has_enabled_logging = has_enabled_logging
            request.user.save()
            return HttpResponse(
                json.dumps({}),
                content_type="application/json",
                status=200
            )
        except KeyError:
            return HttpResponse(json.dumps({"message": "incomplete data"}), content_type="application/json", status=400)

    @method_decorator(never_ever_cache)
    @method_decorator(check_login)
    def dispatch(self, *args, **kwargs):
        return super(SetSettingsView, self).dispatch(*args, **kwargs)

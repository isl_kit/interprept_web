#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.db.transaction import atomic
from apps.resources.models import NeType
from apps.languages.models import Language

def verify_selected_langs(val, currentVal):
    if type(val) is not dict:
        return False

    tags = [x.tag for x in Language.objects.all()]

    for source, targets in val.iteritems():
        seen = []
        if source not in tags:
            return False
        if type(targets) is not list:
            return False
        for target in targets:
            if target not in tags or target == source or target in seen:
                return False
            seen.append(target)

    if not currentVal:
        currentVal = {}

    for source, targets in val.iteritems():
        currentVal[source] = targets

    return currentVal

def verify_selected_types(val, currentVal):
    if type(val) is not dict:
        return False

    ids = [x.id for x in NeType.objects.all()]

    for ne_id, selected in val.iteritems():
        if selected is not True and selected is not False:
            return False
        try:
            ne_id = int(ne_id)
        except ValueError:
            return False
        if ne_id not in ids:
            return False
    return val

CUSTOMIZATIONS = {
    "file_browser_view_mode": [0, 1],
    "terminology.list_length": [20, 50, 100, 200, 500, 1000, 0],
    "terminology.specificity": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    "ne.selected_types": verify_selected_types,
    "terminology.selected_langs": verify_selected_langs
}

@atomic
def set_customization(user, customization):
    global CUSTOMIZATIONS
    result = False
    user_cust = json.loads(user.customization)
    for key, value in customization.iteritems():
        print "KEY: " + key
        print value
        if key in CUSTOMIZATIONS:
            print CUSTOMIZATIONS[key]
            if type(CUSTOMIZATIONS[key]) is list:
                if value in CUSTOMIZATIONS[key]:
                    user_cust[key] = value
                    result = True
            elif hasattr(CUSTOMIZATIONS[key], '__call__'):
                res = CUSTOMIZATIONS[key](value, user_cust.get(key, None))
                if res:
                    user_cust[key] = res
                    result = True
            else:
                raise Exception("unsupported validation type")
    user.customization = json.dumps(user_cust)
    user.save()
    return result

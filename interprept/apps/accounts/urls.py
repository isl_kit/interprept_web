#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',
    url(r'^change/$', views.ChangeView.as_view(), name="change"),
    url(r'^reset/$', views.ResetView.as_view(), name="reset"),
    url(r'^register/$', views.RegisterView.as_view(), name="register"),
    url(r'^login/$', views.LoginView.as_view(), name="login"),
    url(r'^verify/$', views.VerifyView.as_view(), name="verify"),
    url(r'^logout/$', views.LogoutView.as_view(), name="logout"),
    url(r'^loginState/$', views.LoginStateView.as_view(), name="login_state"),
    url(r'^user/$', views.UserView.as_view(), name="user"),
    url(r'^getSettings/$', views.GetSettingsView.as_view(), name="getSettings"),
    url(r'^setSettings/$', views.SetSettingsView.as_view(), name="setSettings"),
    url(r'^setCustomization/$', views.SetCustomizationView.as_view(), name="setCustomization"),
    url(r'^getCustomization/$', views.GetCustomizationView.as_view(), name="getCustomization"),
)
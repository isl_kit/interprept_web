#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from django import template
from django.conf import settings


register = template.Library()


@register.simple_tag
def js_io_vars():
    obj = json.dumps({
        "url": settings.SOCKETIO_EXT_URL,
        "path_prefix": settings.SOCKETIO_PATH_PREFIX
    })
    return "<script>window.SOCKETIO_CONF=JSON.parse('" + obj + "');</script><script src='" + settings.SOCKETIO_EXT_URL + "/socket.io/socket.io.js'></script>"

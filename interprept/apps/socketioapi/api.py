#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json

from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder

handler = {
    "disconnect": [],
    "connect": [],
}


def generate_socket_url(url):
    return settings.SOCKETIO_INT_URL + url


def post_to_socket(url, data):
    headers = {"content-type": "application/json"}
    data["appid"] = settings.SOCKET_APP_ID
    requests.post(generate_socket_url(url), data=json.dumps(data, cls=DjangoJSONEncoder), headers=headers, verify=False)


def send_join(room, sessionid):
    post_to_socket("/joined/", {"room": room, "sessionid": sessionid})


def send_leave(room, sessionid):
    post_to_socket("/left/", {"room": room, "sessionid": sessionid})


def send(message, sessionid):
    post_to_socket("/send/", {"message": message, "sessionid": sessionid})


def send_to(message, room, sessionid):
    post_to_socket("/send_to/", {"message": message, "sessionid": sessionid, "room": room})


def emit(message):
    post_to_socket("/emit/", {"message": message})


def emit_to(message, room):
    post_to_socket("/emit_to/", {"message": message, "room": room})


def on(name, func):
    if name not in handler:
        handler[name] = []
    handler[name].append(func)


def fire(name, sessionid, data):
    if name in handler:
        for func in handler[name]:
            func(sessionid, data)


def on_join(room=""):
    event = "join:" + room

    def method_wrapper(func):
        if event not in handler:
            handler[event] = []
        handler[event].append(func)
        return func
    return method_wrapper


def on_leave(room=""):
    event = "leave:" + room

    def method_wrapper(func):
        if event not in handler:
            handler[event] = []
        handler[event].append(func)
        return func
    return method_wrapper


def on_connect(func):
    handler["connect"].append(func)
    return func


def on_disconnect(func):
    handler["disconnect"].append(func)
    return func

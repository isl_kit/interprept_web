#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^join/$', views.JoinView.as_view(), name="join"), 
    url(r'^leave/$', views.LeaveView.as_view(), name="leave"), 
    url(r'^connect/$', views.ConnectView.as_view(), name="connect"), 
    url(r'^disconnect/$', views.DisconnectView.as_view(), name="disconnect")
)
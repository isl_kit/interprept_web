#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import re

from django.http import HttpResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View

from api import fire, send_join, send_leave
from conf import io_auth


def authenticate_join(request, room):
    authenticated = True
    for room_re, auth_func in io_auth.auth_data.iteritems():
        if re.match(room_re, room):
            authenticated = authenticated and auth_func(request, room)
    return authenticated


class JoinView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        try:
            sessionid = data["sessionid"]
            room = data["room"]
            authenticated = authenticate_join(request, room)
            if authenticated:
                send_join(room, sessionid)
                fire("join:" + room, sessionid, {})
                fire("join:", sessionid, {"room": room})

            return HttpResponse(json.dumps({"authenticated": authenticated}), content_type="application/json", status=200)
        except Exception, e:
            print traceback.format_exc()
            return HttpResponseServerError(str(e))

    def dispatch(self, *args, **kwargs):
        return super(JoinView, self).dispatch(*args, **kwargs)


class LeaveView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        try:
            sessionid = data["sessionid"]
            room = data["room"]
            send_leave(room, sessionid)
            fire("leave:" + room, sessionid, {})
            fire("leave:", sessionid, {"room": room})
            return HttpResponse(json.dumps({}), content_type="application/json", status=200)
        except Exception, e:
            return HttpResponseServerError(str(e))

    def dispatch(self, *args, **kwargs):
        return super(LeaveView, self).dispatch(*args, **kwargs)


class ConnectView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)

        try:
            sessionid = data["sessionid"]
            send_join("", sessionid)
            fire("connect", sessionid, {})
            return HttpResponse("Everything worked :)")
        except Exception, e:
            return HttpResponseServerError(str(e))

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ConnectView, self).dispatch(*args, **kwargs)


class DisconnectView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        try:
            sessionid = data["sessionid"]
            fire("disconnect", sessionid, {})
            return HttpResponse("Everything worked :)")
        except Exception, e:
            return HttpResponseServerError(str(e))

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(DisconnectView, self).dispatch(*args, **kwargs)

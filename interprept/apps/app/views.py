#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.views.decorators.csrf import ensure_csrf_cookie
from django.shortcuts import render
from django.shortcuts import redirect
from django.conf import settings
from django.contrib.auth import logout

@ensure_csrf_cookie
def index(request):
    return render(request, 'index.html', {
        "self_registration": settings.SELF_REGISTRATION
    })

def angular_redirect(request, path=""):
    print "PATH: " + path
    return redirect("/#/" + path)


def maintenance(request, path=""):
    logout(request)
    return render(request, 'maintenance.html', {})

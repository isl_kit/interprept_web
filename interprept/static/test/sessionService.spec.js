describe('Session Service', function() {

    beforeEach(module('app.auth'));

    it("should be able to set and get value", inject(function (SessionService) {
        SessionService.set("testxystring1234567890", true);

        expect(SessionService.get("testxystring1234567890")).toBeTruthy();
    }));

    it("should be able to unset value", inject(function (SessionService) {
        SessionService.unset("testxystring1234567890");

        expect(SessionService.get("testxystring1234567890")).toBeFalsy();
    }));
});
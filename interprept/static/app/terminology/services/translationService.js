angular
    .module("app.terminology")
    .factory("TranslationService", TranslationService);

/* @ngInject */
function TranslationService($http, OnlineTranslationSource, Translation, TMTranslation) {

    var service = {
        translate: translate,
        submit: submit,
        getContext: getContext,
        bumpRating: bumpRating,
        addTranslation: addTranslation,
        getOnlineSources: getOnlineSources,
        getLanguageDefinitions: getLanguageDefinitions
    };

    return service;

    //////////

    function translate(texts, sourceTag, targetTags, refresh) {
        return $http.post("/translation/translate/", {
            target_tags: targetTags,
            source_tag: sourceTag,
            texts: texts,
            refresh: refresh
        }).then(function (response) {
            return response.data;
        });
    }

    function submit(translationId) {
        return $http.post("/translation/submit/", {
            translation_id: translationId
        }).then(function (response) {
            return response.data.sources.map(OnlineTranslationSource.map);
        });
    }

    function getContext(translationId) {
        return $http.get("/translation/getContext/?translation_id=" + translationId).then(function (response) {
            return response.data.tm_translations.map(TMTranslation.map);
        });
    }

    function bumpRating(translationIds) {
        return $http.post("/translation/bumpRating/", {
            translation_ids: translationIds
        });
    }

    function addTranslation(sourceTag, targetTag, phrase, translation) {
        return $http.post("/translation/addTranslation/", {
            target_tag: targetTag,
            source_tag: sourceTag,
            phrase: phrase,
            translation: translation
        }).then(function (response) {
            return Translation.map(response.data.translation);
        });
    }

    function getOnlineSources() {
        return $http.get("/translation/getOnlineSources/").then(function (response) {
            return response.data.sources.map(OnlineTranslationSource.map);
        });
    }

    function getLanguageDefinitions() {
        return $http.get("/translation/getLanguageDefinitions/").then(function (response) {
            return response.data.languages.map(function (item) {
                return {
                    languageId: item.id,
                    label: item.label
                };
            });
        });
    }
}

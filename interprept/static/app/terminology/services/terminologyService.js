angular
    .module("app.terminology")
    .factory("TerminologyService", TerminologyService);

/* @ngInject */
function TerminologyService($http, Term) {

    var service = {
        createDownloadLink: createDownloadLink,
        saveBlackAndWhiteTerms: saveBlackAndWhiteTerms,
        getTerms: getTerms
    };

    return service;

    //////////

    function createDownloadLink(csvData) {
        return $http.post("/downloads/downloadAsTable/", { content: csvData }).then(function (response) {
            return response.data.file_name;
        });
    }

    function saveBlackAndWhiteTerms(changeLog) {
        return $http.post("/resources/saveBlackAndWhiteTerms/", { black: changeLog.deleted, white: changeLog.added });
    }

    function getTerms(resourceIds) {
        var data = {
            resource_ids: resourceIds
        };
        return $http.post("/resources/getTerms/", data).then(function (response) {
            return response.data.terms.map(Term.map);
        });
    }
}

angular
    .module("app.terminology")
    .constant("terminologySettings", {
        numTermsOptions: [
            { number: 20, text: "20" },
            { number: 50, text: "50" },
            { number: 100, text: "100" },
            { number: 200, text: "200" },
            { number: 500, text: "500" },
            { number: 0, text: "no limit (slow)" }
        ]
    })
    .constant("TranslationType", {
        ONLINE: 1,
        USER: 2,
        MEMORY: 3
    });

angular
    .module("app.terminology")
    .config(terminologyRoutes);

/* @ngInject */
function terminologyRoutes($stateProvider) {

    $stateProvider.state("terminology", {
        url: "/terminology?file?draft?base",
        abstract: true,
        reloadOnSearch: false,
        views: {
            "main-content": {
                controller: "TerminologySplitController",
                controllerAs: "vm",
                templateUrl: "terminology/templates/terminology-split.tpl.html"
            },
            "sidebar": {
                controller: "TerminologySidebarController",
                controllerAs: "vm",
                templateUrl: "terminology/templates/terminology-sidebar.tpl.html"
            }
        },
        resolve: {
            onlineSources: /* @ngInject */ function (TranslationService) {
                return TranslationService.getOnlineSources();
            },
            termList: /* @ngInject */ function ($location, DocumentsDataService, TermList, $stateParams) {
                var draftId = $stateParams.draft || null,
                    docId = $stateParams.file || null,
                    ids = [],
                    id = draftId || docId;
                if (docId && _.has(TermList.draftDocMap, docId)) {
                    draftId = TermList.draftDocMap[docId];
                    id = draftId;
                    //$location.search('draft', draftId).replace();
                }
                ids.push(id);
                if (docId && docId !== id) {
                    ids.push(docId);
                }
                return DocumentsDataService.getDocs(ids).then(function (docs) {
                    var doc = docs[0],
                        name = docId ? docs[docs.length - 1].name : "";
                    console.log("DOCS", docs);
                    return TermList.deserialize(docs[0].terminologyData, docId, draftId, name);
                });
            },
            languages: /* @ngInject */ function (LanguageService) {
                return LanguageService.getAll();
            }
        }
    });

    $stateProvider.state("terminology.list", {
        url: "",
        reloadOnSearch: false,
        views: {
            "terminology-list": {
                controller: "TerminologyListController",
                controllerAs: "vm",
                templateUrl: "terminology/templates/terminology-list.tpl.html"
            },
            "terminology-context": {
                controller: "TerminologyContextController",
                controllerAs: "vm",
                templateUrl: "terminology/templates/terminology-context.tpl.html"
            }
        },
        data: {
            pageTitle: "Terminology List"
        }
    });
}

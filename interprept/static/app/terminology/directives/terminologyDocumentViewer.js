angular
    .module("app.terminology")
    .directive("terminologyDocumentViewer", terminologyDocumentViewer);

/* @ngInject */
function terminologyDocumentViewer($timeout, UDocumentTE) {

    var directive = {
        restrict: "C",
        scope: {
            onTermClick: "&",
            termList: "=",
            selectedCount: "=",
            selectedFocused: "=",
            selectedUid: "=",
            textSelection: "="
        },
        link: linkFunction
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        var termList = scope.termList,
            fullText = "",
            userDocument = null,
            entities = [],
            udoc = null;

        activate();

        //////////

        function activate() {
            scope.$watch("selectedFocused", selectedFocusedWatcher);
            document.addEventListener("mouseup", checkSelect, false);
            document.addEventListener("touchend", checkSelect, false);
            element.click(clickHandler);
            termList.on("selectDoc", selectDocHandler);
            termList.on("change", changeHandler);
            termList.on("select", selectHandler);
            element.on("$destroy", deactivate);
        }

        function deactivate() {
            document.removeEventListener("mouseup", checkSelect);
            document.removeEventListener("touchend", checkSelect);
        }

        function selectedFocusedWatcher(newVal, oldVal) {
            if (newVal === oldVal || !userDocument) {
                return;
            }
            var uterms = [], index = 0;
            _.each(udoc.pages, function (upage) {
                _.each(upage.paragraphs, function (paragraph) {
                    _.each(paragraph.terms, function (uterm) {
                        if (uterm.selected) {
                            uterms.push(uterm);
                        }
                    });
                });
            });
            if (newVal < 0) {
                newVal = 0;
            } else if (newVal > uterms.length - 1) {
                newVal = uterms.length - 1;
            }
            if (uterms.length === 0) {
                scope.selectedUid = 0;
            } else {
                scope.selectedUid = uterms[newVal].uid;
            }
        }

        function checkSelect() {
            $timeout(function () {
                scope.$apply(function () {
                    scope.textSelection = getSelectionText();
                    console.log(scope.textSelection);
                });
            });
        }

        function getSelectionText() {
            var selection = null,
                parent,
                selNode = null;
            if (window.getSelection) {
                selection = window.getSelection();
                selNode = selection.anchorNode;
                if (selNode) {
                    selNode = $(selNode);
                    parent = selNode.parents(".terminology-document-viewer");
                    if (parent.length > 0) {
                        return selection.toString();
                    }
                }
            }
            return "";
        }

        function clickHandler(ev) {
            var target = $(ev.target);
            if (target.hasClass("entity")) {
                var termId = target.data("termid"),
                    term = termList.getById(termId);
                    console.log(term, termId);
                if (scope.onTermClick) {
                    scope.$apply(function () {
                        scope.onTermClick({term:term});
                    });
                }
            }
        }

        function selectDocHandler(doc) {
            userDocument = doc;
            if (doc) {
                doc.resource.getFullText().then(function (text) {
                    fullText = text;
                    createEntities();
                    redrawFromScratch();
                });
            } else {
                clear();
            }
        }

        function changeHandler(evt) {
            if (!userDocument) {
                return;
            }
            if (evt.type === "move") {
                return;
            }
            else if (evt.type === "remove") {
                handleRemove(evt.items);
            }
            else if (evt.type === "add") {
                handleAdd(evt.item);
            }
            else if (evt.type === "group") {
                handleGroup(evt.root, evt.terms);
            } else if (evt.type === "ungroup") {
                handleUngroup(evt.root, evt.terms);
            } else {
                createEntities();
                redrawFromScratch();
            }
        }

        function selectHandler(evt) {
            if (!userDocument) {
                return;
            }
            updateSelect();
        }

        function handleGroup(rootTerm, terms) {
            var ids = terms.map(function (term) {
                return term.id;
            });
            udoc.setIds(ids, rootTerm.id);
            updateSelect([rootTerm.id]);
        }

        function handleUngroup(rootTerm, terms) {
            var ids = terms.map(function (term) {
                return term.id;
            });
            udoc.unsetIds(rootTerm.id);
            updateSelect(_.union(ids, rootTerm.id));
        }

        function handleRemove(terms) {
            var ids = terms.map(function (term) {
                return term.id;
            });
            udoc.removeEntities(ids);
            udoc.update();
            countSelect();
        }

        function handleAdd(term) {
            var entities = termToEntities([term]);
            udoc.addEntities(entities);
            udoc.update();
            countSelect();
        }

        function calcOccurances(term) {
            var text = fullText.toLowerCase(),
                name = term.name.toLowerCase(),
                length = name.length,
                sections = [],
                startIndex = 0,
                index, occurance;

            occurance = {
                resourceId: userDocument.resource.id,
                markedText: null,
                sections: sections,
                totalMatches: 0
            };
            while ((index = text.indexOf(name, startIndex)) > -1) {
                startIndex = index + length;
                sections.push({
                    index: index,
                    length: length,
                    preText: "",
                    actualText: "",
                    postText: ""
                });
                occurance.totalMatches += 1;
            }
            term.occurances.push(occurance);
            return occurance;
        }

        function termToEntities(terms) {
            var res = [];
            _.each(terms, function (term) {
                var allTerms = _.union([term], term.related);
                _.each(allTerms, function (subTerm) {
                    if (!subTerm.occurances) {
                        subTerm.occurances = [];
                    }
                    var occurance = _.find(subTerm.occurances, function (occurance) {
                        return occurance.resourceId === userDocument.resource.id;
                    });
                    if (subTerm.edited && !occurance) {
                        occurance = calcOccurances(term);
                    }
                    if (occurance) {
                        _.each(occurance.sections, function (section) {
                            res.push({
                                id: term.id,
                                name: subTerm.name,
                                selected: term.selected,
                                start: section.index,
                                importance: term.combinedImportance,
                                end: section.index + section.length
                            });
                        });
                    }
                });
            });
            res.sort(function (a, b) {
                return a.start - b.start || b.importance - a.importance;
            });
            return res;
        }

        function createEntities() {
            entities = termToEntities(termList.items);
        }

        function updateSelect(ids) {
            var terms;

            if (ids) {
                terms = [];
                _.each(ids, function (id) {
                    var term = termList.getById(id);
                    if (term) {
                        terms.push(term);
                    }
                });
            } else {
                terms = termList.items;
            }

            _.each(terms, function (term) {
                var docEntities = udoc.uterms[term.id];
                _.each(docEntities, function (docEntity) {
                    docEntity.select(term.selected);
                });
            });
            countSelect();
        }

        function countSelect() {
            var count = 0;
            _.each(udoc.uterms, function (uterms) {
                _.each(uterms, function (uterm) {
                    if (uterm.selected) {
                        count += 1;
                    }
                });
            });
            scope.selectedCount = count;
        }

        function redrawFromScratch() {
            udoc = new UDocumentTE(fullText, "pdf", entities);
            element.empty();
            element.append(udoc.htmlObj);
            udoc.flushHtml(10);
            countSelect();
        }

        function clear() {
            udoc = null;
            entities = [];
            fullText = "";
            element.empty();
            scope.selectedCount = 0;
        }
    }
}

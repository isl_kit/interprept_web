angular
    .module("app.terminology")
    .factory("TermList", TermListModel);

/* @ngInject */
function TermListModel($q, SortableList, ChangeLog, Term, TranslationService, Translation, TerminologyService, ModalService, CustomizationService, DocumentsDataService, FileOperationsService, LanguageService, $location) {

    function TermList(language, rawTerms, terms, docs, docId, name, draftId, minOcc, maxLength, locked, keepOrder, keepRelated) {
        var that = this;
        if (terms === null) {
            terms = createFilteredList(rawTerms, minOcc, maxLength);
        }
        SortableList.call(this, terms, {
            options: {
                importance: sortByImportance,
                name: sortByName,
                occurance: sortByOccurance
            },
            def: {
                name: "importance",
                dir: "descending"
            }
        }, keepOrder);
        this.name = name;
        this.minOcc = minOcc;
        this.maxLength = maxLength;
        this.rawTerms = rawTerms;
        this.docs = docs;
        this.selectedDoc = null;
        this.changeLog = new ChangeLog();
        this.language = language;
        this.targetLanguages = [];
        this.targetTags = [];
        this.draftId = draftId;
        this.docId = docId;
        this.locked = locked;
        this.saved = true;
        if (!keepRelated) {
            this.resetRelated();
        }
        this.handler.selectDoc = [];
        this.on("change", function () {
            that.locked = true;
        });
    }
    TermList.draftDocMap = {};
    TermList.serializeInit = serializeInit;
    TermList.deserialize = deserialize;


    TermList.prototype = Object.create(SortableList.prototype);
    TermList.prototype.constructor = TermList;
    TermList.prototype.selectDoc = selectDoc;
    TermList.prototype.update = update;
    TermList.prototype.saveAs = saveAs;
    TermList.prototype.save = save;
    TermList.prototype.autoSave = autoSave;
    TermList.prototype.destroy = destroy;
    TermList.prototype.setTargetLanguages = setTargetLanguages;
    TermList.prototype.addTargetLanguage = addTargetLanguage;
    TermList.prototype.removeTargetLanguage = removeTargetLanguage;
    TermList.prototype.moveTargetLanguage = moveTargetLanguage;
    TermList.prototype.updateTargetTags = updateTargetTags;
    TermList.prototype.add = add;
    TermList.prototype.remove = remove;
    TermList.prototype.rename = rename;
    TermList.prototype.groupTerms = groupTerms;
    TermList.prototype.destroyTermGroup = destroyTermGroup;
    TermList.prototype.download = download;
    TermList.prototype.getFlattenTerms = getFlattenTerms;
    TermList.prototype.getTranslations = getTranslations;
    TermList.prototype.initTranslations = initTranslations;
    TermList.prototype.updateTranslations = updateTranslations;
    TermList.prototype.applyTranslations = applyTranslations;
    TermList.prototype.getTranslationIds = getTranslationIds;
    TermList.prototype.resetRelated = resetRelated;

    return TermList;

    //////////

    function serializeDoc(userDocument) {
        return {
            id: userDocument.id,
            name: userDocument.name,
            rId: userDocument.resource.id
        };
    }

    function serializeTranslation(translation) {
        return {
            id: translation.id,
            name: translation.name
        };
    }

    function serializeTerm(term, targetTags) {
        var translations = {};

        _.each(term.combinedTranslations, function (transList, tag) {
            if (!_.contains(targetTags, tag)) {
                return;
            }
            translations[tag] = transList.filter(function (translation) {
                return translation.visible;
            }).map(serializeTranslation);
        });
        return {
            name: term.name,
            id: term.id,
            edited: term.edited,
            related: term.related.map(function (relTerm) {
                return serializeTerm(relTerm, targetTags);
            }),
            translations: translations
        };
    }

    function serialize(list) {
        var targetTags = list.targetLanguages.map(function (language) {
            return language.tag;
        });
        var res = {
            docs: list.docs.map(serializeDoc),
            minOcc: list.minOcc,
            locked: list.locked,
            maxLength: list.maxLength,
            source: list.language.tag,
            langs: targetTags,
            terms: list.items.map(function (term) {
                return serializeTerm(term, targetTags);
            })
        };
        return res;
    }

    function serializeInit(documents, language) {
        var selectedTags = CustomizationService.get("terminology.selected_langs", {}),
            maxLength = CustomizationService.get("terminology.list_length", 200),
            minOcc = CustomizationService.get("terminology.specificity", 2),
            tags = [];

        if (_.has(selectedTags, language.tag)) {
            tags = selectedTags[language.tag];
        }

        return {
            docs: documents.map(serializeDoc),
            minOcc: minOcc,
            locked: false,
            maxLength: maxLength,
            source: language.tag,
            langs: tags,
            terms: null
        };
    }

    function deserializeTerm(term, idHash, nameHash) {
        var matchedTerm = null;
        if (_.has(idHash, term.id)) {
            matchedTerm = idHash[term.id];
        } else if (_.has(nameHash, term.name.toLowerCase())) {
            matchedTerm = nameHash[term.name.toLowerCase()];
        } else {
            matchedTerm = new Term({
                id: term.id,
                name: term.name,
                edited: true,
                importance: 1,
                occurances: null,
                totalMatches: 0
            });
        }

        matchedTerm.related = [];
        _.each(term.related, function (term) {
            matchedTerm.related.push(deserializeTerm(term, idHash, nameHash));
        });
        matchedTerm.updateCombinedImportance();
        return matchedTerm;
    }

    function deserialize(listObj, docId, draftId, name) {

        var docIds = listObj.docs.map(function (doc) {
            return doc.id;
        }),
        resourceIds = _.unique(listObj.docs.map(function (doc) {
            return doc.rId;
        }));

        return $q.all({
            docs: DocumentsDataService.getDocs(docIds),
            languages: LanguageService.getAll(),
            terms: TerminologyService.getTerms(resourceIds)
        }).then(function (result) {
            var docs = [], termIdHash = {}, termNameHash = {}, terms = [],
                sourceLanguage, targetLanguages, termList;

            sourceLanguage = _.find(result.languages, function (language) {
                return language.tag === listObj.source;
            });

            targetLanguages = listObj.langs.map(function (tag) {
                return _.find(result.languages, function (language) {
                    return language.tag === tag;
                });
            });

            if (listObj.terms === null) {
                termList = new TermList(sourceLanguage, autoGroup(result.terms), null, result.docs, docId, name, draftId, listObj.minOcc, listObj.maxLength, listObj.locked, false, true);
                termList.setTargetLanguages(targetLanguages, true).then(function () {

                });

            } else {
                _.each(result.terms, function (term) {
                    termIdHash[term.id] = term;
                    termNameHash[term.name.toLowerCase()] = term;
                });

                _.each(listObj.terms, function (term) {
                    terms.push(deserializeTerm(term, termIdHash, termNameHash));
                });
                termList = new TermList(sourceLanguage, result.terms, terms, result.docs, docId, name, draftId, listObj.minOcc, listObj.maxLength, listObj.locked, true, true);
                termList.setTargetLanguages(targetLanguages, true).then(function () {
                    _.each(terms, function (term, i) {
                        var savedTerm = listObj.terms[i];
                        term.updateCombinedTranslations();
                        _.each(term.combinedTranslations, function (transList) {
                            transList.each(function (translation) {
                                translation.visible = false;
                            });
                        });
                        _.each(savedTerm.translations, function (translations, tag) {
                            var index = 0;
                            _.each(translations, function (translation) {
                                var t = term.combinedTranslations[tag].getById(translation.id),
                                    tIndex;
                                if (t) {
                                    tIndex = term.combinedTranslations[tag].indexOf(t);
                                    term.combinedTranslations[tag].toggleVisible(t, true);
                                    term.combinedTranslations[tag].moveItems([t], index - tIndex);
                                    index += 1;
                                }
                            });
                        });
                    });
                });
            }
            return termList;
        });
    }

    function createFilteredList(rawTerms, minOcc, maxLength) {
        var that = this;
        var terms = rawTerms.filter(function (term) {
            return term.totalMatches >= minOcc;
        });
        if (maxLength > 0) {
            terms = terms.slice(0, maxLength);
        }
        return autoGroup(terms);
    }

    function autoGroup(terms) {
        var groups = {}, result = [];
        _.each(terms, function (term) {
            var base = term.base;
            if (!_.has(groups, base)) {
                groups[base] = {root:term, terms:[], minL:term.name.length};
            } else {
                if (term.name.length < groups[base].minL) {
                    groups[base].terms.push(groups[base].root);
                    groups[base].root = term;
                    groups[base].minL = term.name.length;
                } else {
                    groups[base].terms.push(term);
                }
            }
        });
        _.each(groups, function (termObj) {
            _.each(termObj.terms, function (term) {
                termObj.root.add(term);
            });
            result.push(termObj.root);
        });
        return result;
    }

    function selectDoc(doc) {
        if (doc === this.selectedDoc) {
            this.selectedDoc = null;
        } else {
            this.selectedDoc = doc;
        }
        this.fire("selectDoc", this.selectedDoc);
    }

    function update() {
        this.setItems(createFilteredList(this.rawTerms, this.minOcc, this.maxLength));
        this.updateTranslations();
        this.locked = false;
    }

    function saveAs(name, dirId) {
        var terminologyData = serialize(this), that = this;
        FileOperationsService.addTerminologyList(dirId, name, terminologyData).then(function (doc) {
            that.docId = doc.id;
            that.name = doc.name;
            $location.search('file', doc.id).replace();
        });
        if (this.draftId) {
            FileOperationsService.removeDocs([this.draftId]);
            $location.search('draft', null).replace();
            this.draftId = null;
        }
        this.changeLog.save();
        this.saved = true;
    }

    function save() {
        if (!this.docId) {
            return;
        }
        var terminologyData = serialize(this);

        FileOperationsService.updateTerminologyList(this.docId, terminologyData);
        if (this.draftId) {
            FileOperationsService.removeDocs([this.draftId]);
            this.draftId = null;
            $location.search('draft', null).replace();
        }
        this.saved = true;
    }

    function autoSave() {
        var terminologyData = serialize(this), that = this;

        FileOperationsService.updateTerminologyList(this.draftId, terminologyData).then(function (doc) {
            if (that.draftId !== doc.id) {
                TermList.draftDocMap[that.docId] = doc.id;
            }
        });
    }

    function destroy() {
        this.destroyed = true;
    }

    function setTargetLanguages(languages, update) {
        this.targetLanguages = languages.slice(0);
        this.updateTargetTags();
        if (update) {
            return this.updateTranslations();
        }
    }

    function addTargetLanguage(language, update) {
        if (_.contains(this.targetTags, language.tag)) {
            return;
        }
        this.targetLanguages.push(language);
        this.updateTargetTags();
        if (update) {
            this.updateTranslations();
        }
        this.saved = false;
    }

    function removeTargetLanguage(language) {
        if (!_.contains(this.targetTags, language.tag)) {
            return;
        }
        this.targetLanguages = _.without(this.targetLanguages, language);
        this.updateTargetTags();
        this.saved = false;
    }

    function moveTargetLanguage(language, offset) {
        var index, newIndex;
        if (!_.contains(this.targetTags, language.tag)) {
            return;
        }
        index = _.indexOf(this.targetLanguages, language);
        newIndex = index + offset;
        if (newIndex < 0) {
            newIndex = 0;
        } else if (newIndex >= this.targetLanguages.length) {
            newIndex = this.targetLanguages.length - 1;
        }
        if (newIndex === index) {
            return;
        }
        this.targetLanguages.splice(newIndex, 0, this.targetLanguages.splice(index, 1)[0]);
        this.updateTargetTags();
        this.saved = false;
    }

    function updateTargetTags() {
        this.targetTags = this.targetLanguages.map(function (language) {
            return language.tag;
        });
    }

    function add(term, index) {
        SortableList.prototype.add.call(this, term, index);
        this.changeLog.add(term.name, term.id);
        this.updateTranslations(true);
        this.saved = false;
    }

    function remove(terms) {
        var that = this;
        SortableList.prototype.remove.call(this, terms);
        _.each(terms, function (term) {
            that.changeLog.remove(term.name, term.id);
        });
        this.saved = false;
    }

    function rename(term, newName, destroyGroup) {
        var index, newTerm;
        if (!this.has(term)) {
            return;
        }
        index = this.indexOf(term);
        this.remove([term]);
        newTerm = new Term({
            id: this.getUniqueId(),
            name: newName,
            edited: true,
            importance: 1,
            occurances: null,
            totalMatches: 0
        });
        if (!destroyGroup && term.related.length > 0) {
            newTerm.related = term.related;
        }
        this.add(newTerm, index);
        return newTerm;
    }

    function groupTerms(rootTerm, terms, selectRoot) {
        var that = this, flattenTerms = [];
        if (!that.has(rootTerm) || _.contains(terms, rootTerm)) {
            return;
        }
        _.each(terms, function (term) {
            if (that.has(term)) {
                flattenTerms.push(term);
            }
            if (term.related.length > 0) {
                _.each(term.related, function (relTerm) {
                    flattenTerms.push(relTerm);
                });
                term.related = [];
                term.selected = false;
                term.updateCombinedTranslations();
            }
        });
        if (selectRoot) {
            rootTerm.selected = true;
        }
        this.eventless(function () {
            _.each(flattenTerms, function (term) {
                that.remove([term]);
                rootTerm.add(term);
            });
        });
        this.fire("change", {
            "type": "group",
            "root": rootTerm,
            "terms": terms
        });
    }

    function destroyTermGroup(rootTerm) {
        var that = this, terms = rootTerm.related;
        if (!that.has(rootTerm)) {
            return;
        }
        this.eventless(function () {
            _.each(terms, function (term) {
                rootTerm.remove(term);
                term.selected = true;
                that.add(term, that.indexOf(rootTerm) + 1);
            });
        });
        this.fire("change", {
            "type": "ungroup",
            "root": rootTerm,
            "terms": terms
        });
    }

    function download() {
        var that = this,
            tags = that.targetTags;
        //TranslationService.bumpRating(that.getTranslationIds());
        that.changeLog.save();
        var content = [];
        that.each(function (term) {
            var result = [term.name], i, max_i = 1, selectedTrans = {};
            function pushTrans (index) {
                _.each(selectedTrans, function (translations, languageTag) {
                    result.push(translations[index] ? translations[index].name : "-");
                });
            }
            if (tags.length > 0) {
                _.each(tags, function (tag) {
                    selectedTrans[tag] = term.combinedTranslations[tag].filter(function (translation) {
                        return translation.visible;
                    });
                    if (selectedTrans[tag].length > max_i) {
                        max_i = selectedTrans[tag].length;
                    }
                });
                for (i = 0; i < max_i; i++) {
                    pushTrans(i);
                    content.push(result);
                    result = [""];
                }
            } else {
                content.push(result);
            }
        });
        var header = [that.language.name];
        _.each(that.targetLanguages, function (language) {
            header.push(language.name);
        });
        content.unshift(header);
        TerminologyService.createDownloadLink(content).then(function (fileName) {
            ModalService.open("normal@terminologyDownload", { fileName: fileName });
        });
    }

    function getFlattenTerms(terms) {
        var flattenTerms = [];
        terms = terms || this.items;
        _.each(terms, function (term) {
            flattenTerms.push(term);
            _.each(term.related, function (term) {
                flattenTerms.push(term);
            });
        });
        return flattenTerms;
    }

    function getTranslations(term) {
        var names, terms, that = this;
        terms = that.getFlattenTerms([term]);
        names = terms.map(function (term) {
            return term.name;
        });
        TranslationService.translate(names, that.language.tag, that.targetTags, 1).then(function (result) {
            that.applyTranslations(result, terms);
        });
    }

    function initTranslations(tags) {
        var names, terms, that = this;
        tags = tags || that.targetTags;
        terms = that.getFlattenTerms();
        names = terms.map(function (term) {
            term.translations = {};
            return term.name;
        });
        TranslationService.translate(names, that.language.tag, tags, 0).then(function (result) {
            that.applyTranslations(result, terms);
        });
    }

    function updateTranslations(reload) {
        var that = this,
            tags = [],
            names = [],
            terms = [],
            flattenTerms = that.getFlattenTerms();
        _.each(flattenTerms, function (term) {
            var i, max_i, tag, nameAdded = false;
            for (i = 0, max_i = that.targetTags.length; i < max_i; i++) {
                tag = that.targetTags[i];
                if (!_.has(term.translations, tag)) {
                    if (!nameAdded) {
                        names.push(term.name);
                        terms.push(term);
                        nameAdded = true;
                    }
                    if (!_.contains(tags, tag)) {
                        tags.push(tag);
                    }
                }
            }
        });
        if (names.length > 0) {
            return TranslationService.translate(names, that.language.tag, tags, reload ? 2 : 0).then(function (result) {
                that.applyTranslations(result, terms);
            });
        } else {
            return $q.when(undefined);
        }
    }

    function applyTranslations(result, terms) {
        var flattenTerms, resultHash;
        flattenTerms = terms || this.getFlattenTerms(this.items);
        resultHash = {};
        _.each(result, function (res) {
            resultHash[res.name] = res;
        });
        _.each(flattenTerms, function (term) {
            var result = resultHash[term.name];
            if (result) {
                _.each(result.translations, function (translations, tag) {
                    var currTranslations;
                    if (!_.has(term.translations, tag)) {
                        term.translations[tag] = [];
                    }
                    currTranslations = term.translations[tag];
                    _.each(translations, function (translation) {
                        translation = Translation.map(translation);
                        translation.visible = true;
                        currItem = _.find(currTranslations, function (item) {
                            return item.id === translation.id;
                        });

                        if (!currItem) {
                            currTranslations.push(translation);
                        } else {
                            currItem.rating = translation.rating;
                            currItem.sources = translation.sources;
                        }
                    });
                });
                term.updateCombinedTranslations();
            }
        });
    }

    function getTranslationIds() {
        var translationIds = [], that = this;
        _.each(that.targetTags, function (tag) {
            that.each(function (term) {
                if (term.translations[tag]) {
                    selectedTranslations = term.translations[tag].filter(function (translation) {
                        return translation.visible;
                    });
                    _.each(selectedTranslations, function (translation) {
                        translationIds.push(translation.id);
                    });
                }
            });
        });
        return translationIds;
    }

    function resetRelated() {
        this.each(function (term) {
            term.related = [];
            term.updateCombinedTranslations();
        });
    }

    function sortByImportance(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;
        return function (a, b) {
            var lowerA, lowerB;
            if (a.edited && !b.edited) {
                return dirCorrector;
            } else if (!a.edited && b.edited) {
                return dirCorrector * -1;
            } else if ((a.edited && b.edited) || a.combinedImportance === b.combinedImportance) {
                lowerA = a.name.toLowerCase();
                lowerB = b.name.toLowerCase();
                if (lowerA < lowerB) {
                    return -1 * dirCorrector;
                } else if (lowerB < lowerA) {
                    return 1 * dirCorrector;
                }
                return 0;
            }
            return (a.combinedImportance - b.combinedImportance) * dirCorrector;
        };
    }

    function sortByName(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;
        return function (a, b) {
            var lowerA, lowerB;
            lowerA = a.name.toLowerCase();
            lowerB = b.name.toLowerCase();
            if (lowerA < lowerB) {
                return -1 * dirCorrector;
            } else if (lowerB < lowerA) {
                return 1 * dirCorrector;
            }
            return 0;
        };
    }

    function sortByOccurance(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;
        return function (a, b) {
            var diff = dirCorrector * (a.occurances[0].sections[0].index - b.occurances[0].sections[0].index);
            if (diff === 0) {
                return (b.combinedImportance - a.combinedImportance) * dirCorrector;
            }
            return diff;
        };
    }
}

angular
    .module("app.terminology")
    .factory("Translation", TranslationModel);

/* @ngInject */
function TranslationModel(User, OnlineTranslationSource) {

    function Translation(data) {
        var that = this;
        that.id = data.id;
        that.sourceTag = data.sourceTag;
        that.targetTag = data.targetTag;
        that.name = data.name;
        that.rating = data.rating;
        that.sources = data.sources.filter(function (source) {
            return source.name !== "tm";
        });
        that.user = null;
        that.users = data.users;
        that.tm = that.sources.length < data.sources.length;
    }
    Translation.validateName = validateName;
    Translation.preProcessName = preProcessName;
    Translation.map = map;

    return Translation;

    //////////

    function validateName(name) {
        name = Translation.preProcessName(name);

        if (name.length > 0) {
            return name;
        } else {
            return false;
        }
    }

    function preProcessName(name) {
        return name.trim();
    }

    function map(translationDb) {
        return new Translation({
            id: translationDb.id,
            name: translationDb.name,
            sourceTag: translationDb.source_tag,
            targetTag: translationDb.target_tag,
            rating: translationDb.rating,
            sources: translationDb.translator_ids.map(OnlineTranslationSource.get),
            //sources: [1,2].map(OnlineTranslationSource.get),
            users: translationDb.users.map(User.map)
        });
    }
}

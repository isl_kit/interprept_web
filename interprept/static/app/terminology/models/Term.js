angular
    .module("app.terminology")
    .factory("Term", TermModel);

/* @ngInject */
function TermModel(TranslationType, TranslationList) {

    function Term(data) {
        this.id = data.id;
        this.base = data.base;
        this.name = data.name || "";
        this.edited = data.edited || false;
        this.importance = data.importance ? data.importance : 0;
        this.combinedImportance = this.importance;
        this.occurances = data.edited ? null : (data.occurances || []);
        this.totalMatches = data.totalMatches || 0;
        this.translations = {};
        this.combinedTranslations = {};
        this.related = [];
        this.selected = false;
    }
    Term.validateName = validateName;
    Term.preProcessName = preProcessName;
    Term.map = map;

    Term.prototype.addTranslation = addTranslation;
    Term.prototype.add = add;
    Term.prototype.remove = remove;
    Term.prototype.updateCombinedImportance = updateCombinedImportance;
    Term.prototype.updateCombinedTranslations = updateCombinedTranslations;
    Term.prototype.findOccurances = findOccurances;

    return Term;

    //////////

    function validateName(name) {
        name = Term.preProcessName(name);

        if (name.length > 0) {
            return name;
        } else {
            return false;
        }
    }

    function preProcessName(name) {
        return name.trim();
    }

    function map(termDb) {
        var total = 0;

        _.each(termDb.resources, function (resource) {
            total += resource.occurances.length;
        });

        return new Term({
            id: termDb.id,
            base: termDb.base,
            name: termDb.name,
            importance: Number(termDb.importance),
            totalMatches: total,
            occurances: _.map(termDb.resources, function (resource) {
                var occurance = {
                    resourceId: Number(resource.id),
                    markedText: null,
                    sections: resource.occurances.map(function (position) {
                        var indices = position.split(":").map(function (index) {
                            return parseInt(index, 10);
                        });
                        return {
                            index: indices[0],
                            length: indices[1] - indices[0],
                            preText: "",
                            actualText: "",
                            postText: ""
                        };
                    }),
                    totalMatches: resource.occurances.length
                };
                occurance.sections.sort(function (a, b) {
                    return a.index - b.index;
                });
                return occurance;
            })
        });
    }

    function addTranslation(tag, translation) {
        this.translations[tag].unshift(translation);
        this.combinedTranslations[tag].add(translation, 0);
    }

    function add(term) {
        this.related.push(term);
        this.updateCombinedTranslations();
        this.updateCombinedImportance();
    }

    function remove(term) {
        this.related = _.without(this.related, term);
        this.updateCombinedTranslations();
        this.updateCombinedImportance();
    }

    function updateCombinedImportance() {
        var importance = this.importance;

        _.each(this.related, function (term) {
            if (term.importance > importance) {
                importance = term.importance;
            }
        });
        this.combinedImportance = importance;
    }

    function updateCombinedTranslations() {
        var result = {}, that = this;

        that.combinedTranslations = {};
        _.each(that.translations, function (item, key) {
            result[key] = item;
        });
        _.each(that.related, function (sequence) {
            _.each(sequence.translations, function (item, key) {
                result[key] = _.union(result[key], item);
            });
        });
        _.each(result, function (item, key) {
            item.sort(translationSortingFunction);
            if (!that.combinedTranslations[key]) {
                that.combinedTranslations[key] = new TranslationList(item, key);
            } else {
                that.combinedTranslations[key].setItems(item);
            }
        });
    }

    function findOccurances(resources) {
        var that = this,
            total = 0,
            occurances = [];

        _.each(resources, function (resource) {
            var regex = new RegExp(that.name, "gi"), result, indices = [], occurance;
            while ( (result = regex.exec(resource.fullText)) ) {
                indices.push(result.index);
            }
            total += indices.length;
            occurance = {
                resourceId: resource.id,
                markedText: null,
                sections: indices.map(function(index) {
                    return {
                        index: index,
                        length: that.name.length,
                        preText: "",
                        actualText: "",
                        postText: ""
                    };
                }),
                totalMatches: indices.length
            };
            occurances.push(occurance);
        });
        that.occurances = occurances;
        that.totalMatches = total;
    }

    function translationSortingFunction(a, b) {
        var ratingDiff = b.rating - a.rating,
            typeDiff = b.type - a.type;
        if (ratingDiff !== 0) {
            return ratingDiff;
        }
        if (typeDiff !== 0) {
            return typeDiff;
        }
        if (a.type === TranslationType.ONLINE) {
            return b.sources.length - a.sources.length;
        }
        return 0;
    }
}

angular
    .module("app.terminology")
    .factory("UParagraphTE", UParagraphTEModel);

/* @ngInject */
function UParagraphTEModel(UTermTE) {

    function UParagraph(text, entities, start, udoc) {
        this.entities = entities;
        this.terms = [];
        this.text = text;
        this.udoc = udoc;
        this.start = start;
        this.htmlObj = $("<p></p>");
        this.createHtml();
        this.needsRedraw = false;
    }
    UParagraph.prototype.addEntities = addEntities;
    UParagraph.prototype.removeEntities = removeEntities;
    UParagraph.prototype.createHtml = createHtml;

    return UParagraph;

    //////////

    function addEntities(entities) {
        this.entities = _.union(this.entities, entities);
        this.entities.sort(function (a, b) {
            return a.start - b.start || b.importance - a.importance;
        });
        this.needsRedraw = true;
    }

    function removeEntities(ids) {
        var remEntities = this.terms.filter(function (uterm) {
            return _.contains(ids, uterm.entity.id);
        }).map(function (uterm) {
            return uterm.entity;
        });
        if (remEntities.length === 0) {
            return;
        }
        console.log("REM", remEntities, this.entities.length);
        this.entities = _.difference(this.entities, remEntities);
        console.log("NEW", this.entities.length);
        this.needsRedraw = true;
    }

    function createHtml() {
        var lastIndex = 0,
            that = this;

         that.htmlObj.empty();
         that.term = [];
        _.each(this.entities, function (entity) {
            var start = entity.start - that.start,
                end = entity.end - that.start,
                cutout,
                diff = 0,
                term,
                preString = that.text.substring(lastIndex, start);

            if (start < lastIndex) {
                return;
            }

            lastIndex = end;
            if (preString.length > 0) {
                that.htmlObj.append(document.createTextNode(preString));
            }
            cutout = that.text.substring(start, end);
            if (cutout.toLowerCase().indexOf(entity.name.toLowerCase()) > -1) {
                diff = cutout.length - entity.name.length;
                lastIndex -= diff;
            }
            term = new UTermTE(that.text.substring(start, end - diff), entity.selected, that.udoc, that, entity);
            that.terms.push(term);
            that.htmlObj.append(term.htmlObj);
        });
        if (lastIndex < that.text.length) {
            that.htmlObj.append(document.createTextNode(that.text.substring(lastIndex)));
        }
        that.needsRedraw = false;
    }
}

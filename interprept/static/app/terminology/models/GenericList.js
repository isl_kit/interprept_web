angular
    .module("app.terminology")
    .factory("GenericList", GenericListModel);

/* @ngInject */
function GenericListModel() {

    function GenericList(items) {
        this.items = items ? items.slice(0) : [];
        this.idToPos = {};
        this.handler = {
            "change": [],
            "select": []
        };
        this.savePositions();
        this.numberOfSelectedItems = 0;
        this.checkAllState = 0;
        this.length = this.items.length;
        this.resetSelection();
        this.silent = false;
    }

    GenericList.prototype.eventless = eventless;
    GenericList.prototype.setItems = setItems;
    GenericList.prototype.getItem = getItem;
    GenericList.prototype.getById = getById;
    GenericList.prototype.filter = filter;
    GenericList.prototype.each = each;
    GenericList.prototype.on = on;
    GenericList.prototype.fire = fire;
    GenericList.prototype.savePositions = savePositions;
    GenericList.prototype.indexOf = indexOf;
    GenericList.prototype.has = has;
    GenericList.prototype.add = add;
    GenericList.prototype.moveItems = moveItems;
    GenericList.prototype.remove = remove;
    GenericList.prototype.resetSelection = resetSelection;
    GenericList.prototype.updateAllCheck = updateAllCheck;
    GenericList.prototype.toggle = toggle;
    GenericList.prototype.select = select;
    GenericList.prototype.toggleAll = toggleAll;
    GenericList.prototype.getUniqueId = getUniqueId;

    return GenericList;

    //////////

    function eventless(callback) {
        this.silent = true;
        callback();
        this.silent = false;
    }

    function setItems(items) {
        this.items = items;
        this.length = items.length;
        this.savePositions();
        this.updateAllCheck();
        this.fire("change", {
            "type": "set"
        });
    }

    function getItem(func) {
        return _.find(this.items, func);
    }

    function getById(id) {
        if (_.has(this.idToPos, id)) {
            return this.items[this.idToPos[id]];
        } else {
            return null;
        }
    }

    function filter(func) {
        return this.items.filter(func);
    }

    function each(func) {
        _.each(this.items, func);
    }

    function on(eventName, handler) {
        if (!_.has(this.handler, eventName) || _.contains(this.handler[eventName], handler)) {
            return;
        }

        this.handler[eventName].push(handler);
    }

    function fire(eventName) {
        if (this.silent) {
            return;
        }

        var args = Array.prototype.slice.call(arguments, 1);

        if (!_.has(this.handler, eventName)) {
            return;
        }

        _.each(this.handler[eventName], function (handler) {
            handler.apply(this, args);
        });
    }

    function savePositions() {
        var that = this;
        that.idToPos = {};
        _.each(that.items, function (item, idx) {
            that.idToPos[item.id] = idx;
        });
    }

    function indexOf(item) {
        if (!this.has(item)) {
            return -1;
        } else {
            return this.idToPos[item.id];
        }
    }

    function has(item) {
        return _.has(this.idToPos, item.id);
    }

    function add(item, index) {
        var i, length;

        if (this.has(item)) {
            return;
        }
        length = this.items.length;

        if (index < 0) {
            index = 0;
        } else if (index > length) {
            index = length;
        }
        this.items.splice(index, 0, item);
        this.length += 1;
        this.idToPos[item.id] = index;
        for (i = index + 1; i < length; i++) {
            this.idToPos[this.items[i].id] += 1;
        }
        //this.changeLog.add(item.name, item.id);
        //this.saveCustomPositions();
        //this.updateTranslations(true);
        this.updateAllCheck();
        this.fire("change", {
            "type": "add",
            "item": item,
            "index": index
        });
    }

    function moveItems(items, offset) {
        var f0, f1, that = this;

        items = items.filter(function (item) {
            return that.has(item);
        });
        items.sort(function (a, b) {
            return (that.idToPos[a.id] - that.idToPos[b.id]) * (offset < 0 ? 1 : -1);
        });
        f0 = offset < 0 ? 1 : 0;
        f1 = offset < 0 ? 0 : 1;

        _.each(items, function (item, num) {
            var index = that.idToPos[item.id],
                newIndex = index + offset;

            if (newIndex < num * f0) {
                newIndex = num * f0;
            } else if (newIndex >= that.length - f1 * num) {
                newIndex = that.length - 1 - f1 * num;
            }

            if (newIndex === index) {
                return;
            }

            that.items.splice(newIndex, 0, that.items.splice(index, 1)[0]);
        });

        that.savePositions();
        that.fire("change", {
            "type": "move",
            "items": items,
            "offset": offset
        });
    }

    function remove(items) {
        var that = this;
        _.each(items, function (item) {
            var i, index;
            if (!that.has(item)) {
                return;
            }
            index = that.idToPos[item.id];
            that.items = _.without(that.items, item);
            delete that.idToPos[item.id];
            that.length -= 1;
            for (i = index; i < that.length; i++) {
                that.idToPos[that.items[i].id] -= 1;
            }
        });
        this.updateAllCheck();
        this.fire("change", {
            "type": "remove",
            "items": items
        });
    }

    function resetSelection() {
        _.each(this.items, function (item) {
            item.selected = false;
        });
        this.updateAllCheck();
        this.fire("select", {
            "type": "reset"
        });
    }

    function updateAllCheck() {
        var countObj;

        countObj = _.countBy(this.items, function(item) {
            return item.selected ? "selected": "unselected";
        });

        if (!countObj.selected) {
            this.checkAllState = 0;
            this.numberOfSelectedItems = 0;
        } else if (!countObj.unselected) {
            this.checkAllState = 1;
            this.numberOfSelectedItems = countObj.selected;
        } else {
            this.checkAllState = 2;
            this.numberOfSelectedItems = countObj.selected;
        }
    }

    function toggle(item) {
        item.selected = !item.selected;
        this.updateAllCheck();
        this.fire("select", {
            "type": "toggle"
        });
    }

    function select(item) {
        _.each(this.items, function (checkItem) {
            checkItem.selected = checkItem === item;
        });
        this.updateAllCheck();
        this.fire("select", {
            "type": "select"
        });
    }

    function toggleAll() {
        if (this.checkAllState === 0 || this.checkAllState === 2) {
            this.checkAllState = 1;
            _.each(this.items, function (item) {
                item.selected = true;
            });
            this.numberOfSelectedItems = this.items.length;
        } else {
            this.checkAllState = 0;
            _.each(this.items, function (item) {
                item.selected = false;
            });
            this.numberOfSelectedItems = 0;
        }
        this.fire("select", {
            "type": "toggleAll"
        });
    }

    function getUniqueId() {
        var id;
        do {
            id = Math.floor(Math.random() * 10000000);
        } while (_.has(this.idToPos, id));
        return id;
    }
}

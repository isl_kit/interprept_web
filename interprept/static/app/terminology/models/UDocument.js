angular
    .module("app.terminology")
    .factory("UDocumentTE", UDocumentTEModel);

/* @ngInject */
function UDocumentTEModel($timeout, UParagraphTE, UPageTE) {

    function UDocument(text, docType, entities) {
        this.docType = docType;
        this.pages = [];
        this.text = text;
        this.htmlObj = $("<div class=\"udoc\"></div>");
        this.uterms = {};
        this.skippedIds = [];
        this.markText(entities, text);
    }
    UDocument.prototype.markText = markText;
    UDocument.prototype.flushHtml = flushHtml;
    UDocument.prototype.removeEntities = removeEntities;
    UDocument.prototype.addEntities = addEntities;
    UDocument.prototype.setIds = setIds;
    UDocument.prototype.unsetIds = unsetIds;
    UDocument.prototype.update = update;
    UDocument.prototype.addPage = addPage;

    return UDocument;

    //////////

    function markText(entities, text) {
        var that = this,
            start = 0,
            entitiesIndex = 0,
            max_i = entities.length,
            pagesText = text.split("\n\n\n");

        _.each(pagesText, function (pageText, i) {
            var pageObj = new UPageTE(i, pagesText.length, start, that);

            _.each(pageText.split("\n\n"), function (paragraphText) {
                var paraEntities = [], paragraphObj, end = start + paragraphText.length, entity;
                for (; entitiesIndex < max_i; entitiesIndex++) {
                    entity = entities[entitiesIndex];
                    if (entity.start < start) {
                        continue;
                    }
                    if (entity.end > end) {
                        break;
                    }
                    paraEntities.push(entity);
                }
                paragraphObj = new UParagraphTE(paragraphText, paraEntities, start, that);
                pageObj.addParagraph(paragraphObj);
                start = end + 2;
            });
            pageObj.finish();
            that.addPage(pageObj);
            start += 1;
        });
    }

    function flushHtml(size, index) {
        var that = this, max_i, i;
        index = index || 0;
        max_i = Math.min(that.pages.length, (index * size) + size);
        for (i = index * size; i < max_i; i++) {
            that.htmlObj.append(that.pages[i].htmlObj);
        }
        if (max_i < that.pages.length) {
            $timeout(function () {
                that.flushHtml(size, index + 1);
            });
        }
    }

    function removeEntities(ids) {
        var that = this;
        _.each(that.pages, function (upage) {
            _.each(upage.paragraphs, function (paragraph) {
                paragraph.removeEntities(ids);
            });
        });
        _.each(ids, function (id) {
            delete that.uterms[id];
        });
    }

    function addEntities(entities) {
        var entitiesIndex = 0, that = this,
            max_i = entities.length;
        _.each(that.pages, function (upage) {
            _.each(upage.paragraphs, function (paragraph) {
                var entity, start = paragraph.start, end = paragraph.start + paragraph.text.length, paraEntities = [];
                for (; entitiesIndex < max_i; entitiesIndex++) {
                    entity = entities[entitiesIndex];
                    if (entity.start < start) {
                        continue;
                    }
                    if (entity.end > end) {
                        break;
                    }
                    paraEntities.push(entity);
                }
                paragraph.addEntities(paraEntities);
            });
        });
    }

    function setIds(ids, newId) {
        var uterms = [], that = this;

        _.each(ids, function (id) {
            var tmpTerms = that.uterms[id];
            if (tmpTerms) {
                uterms = _.union(uterms, tmpTerms);
                delete that.uterms[id];
            }
        });

        if (!_.has(that.uterms, newId)) {
            that.uterms[newId] = [];
        }

        _.each(uterms, function (uterm) {
            uterm.setId(newId);
            that.uterms[newId].push(uterm);
        });
    }

    function unsetIds(id) {
        var uterms = this.uterms[id],
            that = this,
            changedTerms = uterms.filter(function (uterm) {
                return uterm.tmpId !== null;
            });

        _.each(changedTerms, function (uterm) {
            uterm.unsetId();
            if (!_.has(that.uterms, uterm.entity.id)) {
                that.uterms[uterm.entity.id] = [];
            }
            that.uterms[uterm.entity.id].push(uterm);
        });

        this.uterms[id] = _.difference(uterms, changedTerms);
    }

    function update() {
        var that = this;
        _.each(that.pages, function (upage) {
            _.each(upage.paragraphs, function (paragraph) {
                if (paragraph.needsRedraw) {
                    paragraph.createHtml();
                }
            });
        });
    }

    function addPage(page) {
        this.pages.push(page);
    }
}

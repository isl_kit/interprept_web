angular
    .module("app.terminology")
    .factory("OnlineTranslationSource", OnlineTranslationSourceModel);

/* @ngInject */
function OnlineTranslationSourceModel() {

    var repository = {};

    function OnlineTranslationSource(data) {
        var that = this;
        that.id = data.id;
        that.name = data.name;
        that.label = data.label;
        that.url = data.url;
    }

    OnlineTranslationSource.getLink = getLink;
    OnlineTranslationSource.map = map;
    OnlineTranslationSource.get = get;

    OnlineTranslationSource.prototype.getLink = getLinkFromInstance;

    return OnlineTranslationSource;

    //////////

    function getLink(sourceId, sourceTag, targetTag, phrase) {
        if (sourceId === 1) {
            return "https://glosbe.com/" + sourceTag + "/" + targetTag + "/" + phrase;
        } else if (sourceId === 2) {
            return "http://iate.europa.eu";
        }
    }

    function map(onlineTranslationSourceDb) {
        var ots = new OnlineTranslationSource({
            id: onlineTranslationSourceDb.id,
            name: onlineTranslationSourceDb.name,
            label: onlineTranslationSourceDb.label,
            url: onlineTranslationSourceDb.url
        });
        repository[ots.id] = ots;
        return ots;
    }

    function get(id) {
        return repository[id];
    }

    function getLinkFromInstance(sourceTag, targetTag, phrase) {
        return getLink(this.id, sourceTag, targetTag, phrase);
    }
}

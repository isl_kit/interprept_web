angular
    .module("app.terminology")
    .factory("UTermTE", UTermTEModel);

/* @ngInject */
function UTermTEModel() {

    function UTerm(text, selected, udoc, paragraph, entity) {
        this.text = text;
        this.paragraph = paragraph;
        this.entity = entity;
        this.selected = selected;
        this.uid = "C" + Math.round(Math.random() * 1000000000);
        this.htmlObj = this.createHtml();
        this.udoc = udoc;
        this.tmpId = null;
        if (!_.has(udoc.uterms, entity.id)) {
            udoc.uterms[entity.id] = [];
        }
        udoc.uterms[entity.id].push(this);
    }

    UTerm.prototype.setId = setId;
    UTerm.prototype.unsetId = unsetId;
    UTerm.prototype.select = select;
    UTerm.prototype.createHtml = createHtml;

    return UTerm;

    //////////

    function setId(id) {
        if (this.tmpId === null) {
            this.tmpId = this.entity.id;
        }
        this.entity.id = id;
    }

    function unsetId() {
        if (this.tmpId !== null) {
            this.entity.id = this.tmpId;
            this.tmpId = null;
        }
    }

    function select(selected) {
        if (selected === this.selected) {
            return;
        }
        this.selected = selected;
        if (this.selected) {
            this.htmlObj.addClass("selected");
        } else {
            this.htmlObj.removeClass("selected");
        }
    }

    function createHtml() {
        var htmlText = "";

        htmlText += "<span id=\"" + this.uid + "\" data-termid=\"" + this.entity.id + "\" class=\"entity te";
        if (this.selected) {
            htmlText += " selected";
        }
        htmlText += "\">";
        htmlText += this.text;
        htmlText += "</span>";

        return $(htmlText);
    }
}

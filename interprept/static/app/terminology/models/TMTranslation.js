angular
    .module("app.terminology")
    .factory("TMTranslation", TMTranslationModel);

/* @ngInject */
function TMTranslationModel() {

    function TMTranslation(data) {
        var that = this;
        that.id = data.id;
        that.fdrNo = data.fdrNo;
        that.o_context = data.o_context;
        that.o_index = data.o_index;
        that.o_length = data.o_length;
        that.t_context = data.t_context;
        that.t_index = data.t_index;
        that.t_length = data.t_length;
        that.o_pre = that.o_context.substr(0, that.o_index);
        that.o_mid = that.o_context.substr(that.o_index, that.o_length);
        that.o_post = that.o_context.substr(that.o_index + that.o_length);
        that.t_pre = that.t_context.substr(0, that.t_index);
        that.t_mid = that.t_context.substr(that.t_index, that.t_length);
        that.t_post = that.t_context.substr(that.t_index + that.t_length);
    }
    TMTranslation.map = map;

    TMTranslation.prototype.setLanguages = setLanguages;

    return TMTranslation;

    //////////

    function map(tmtranslationDb) {
        return new TMTranslation({
            id: tmtranslationDb.id,
            fdrNo: tmtranslationDb.fdr_no,
            o_context: tmtranslationDb.o_context,
            o_index: tmtranslationDb.o_index,
            o_length: tmtranslationDb.o_length,
            t_context: tmtranslationDb.t_context,
            t_index: tmtranslationDb.t_index,
            t_length: tmtranslationDb.t_length
        });
    }

    function setLanguages(sourceTag, targetTag) {
        if (sourceTag < targetTag) {
            this.source_context = this.t_context;
            this.source_index = this.t_index;
            this.source_length = this.t_length;
            this.source_pre = this.t_pre;
            this.source_mid = this.t_mid;
            this.source_post = this.t_post;
            this.target_context = this.o_context;
            this.target_index = this.o_index;
            this.target_length = this.o_length;
            this.target_pre = this.o_pre;
            this.target_mid = this.o_mid;
            this.target_post = this.o_post;
        } else {
            this.source_context = this.o_context;
            this.source_index = this.o_index;
            this.source_length = this.o_length;
            this.source_pre = this.o_pre;
            this.source_mid = this.o_mid;
            this.source_post = this.o_post;
            this.target_context = this.t_context;
            this.target_index = this.t_index;
            this.target_length = this.t_length;
            this.target_pre = this.t_pre;
            this.target_mid = this.t_mid;
            this.target_post = this.t_post;
        }
    }
}

angular
    .module("app.terminology")
    .factory("SortableList", SortableListModel);

/* @ngInject */
function SortableListModel(GenericList) {

    function SortableList(items, sortOptions, keepOrder) {
        GenericList.call(this, items);
        this.idToPosCustom = {};
        this.customSorting = false;
        this.sortOptions = sortOptions.options;
        this.sortingType = "";
        this.sortingDir = "";
        this.handler.sort = [];
        if (keepOrder) {
            this.saveCustomPositions();
            this.customSorting = true;
            this.sortingType = sortOptions.def.name;
            this.sortingDir = sortOptions.def.dir;
        } else {
            this.sort(sortOptions.def.name, sortOptions.def.dir);
        }
    }
    SortableList.prototype = Object.create(GenericList.prototype);
    SortableList.prototype.constructor = SortableList;

    SortableList.prototype.saveCustomPositions = saveCustomPositions;
    SortableList.prototype.setCustomOrder = setCustomOrder;
    SortableList.prototype.sort = sort;
    SortableList.prototype.add = add;
    SortableList.prototype.moveItems = moveItems;
    SortableList.prototype.setItems = setItems;

    return SortableList;

    //////////

    function saveCustomPositions() {
        var that = this;
        that.idToPosCustom = {};
        that.customSorting = true;
        _.each(that.idToPos, function (idx, id) {
            that.idToPosCustom[id] = idx;
        });
    }

    function setCustomOrder(idArray) {
        var that = this;
        that.items = idArray.map(function (itemId) {
            var index = that.idToPos[itemId];
            if (index !== undefined) {
                return that.items[index];
            } else {
                return null;
            }
        }).filter(function (item) {
            return item !== null;
        });
        this.savePositions();
        this.saveCustomPositions();
        this.fire("change", {
            "type": "customSort"
        });
    }

    function sort(type, dir) {
        if (type === this.sortingType && dir === this.sortingDir && !this.customSorting) {
            return;
        }
        if (type === "custom") {
            this.items.sort(sortByCustomOrder(this.idToPosCustom));
            this.customSorting = true;
        } else {
            this.items.sort(this.sortOptions[type](dir));
            this.sortingType = type;
            this.sortingDir = dir;
            this.customSorting = false;
        }
        this.savePositions();
        this.fire("sort", type, dir);
    }

    function add(item, index) {
        GenericList.prototype.add.call(this, item, index);
        this.saveCustomPositions();
    }

    function moveItems(items, offset) {
        GenericList.prototype.moveItems.call(this, items, offset);
        this.saveCustomPositions();
    }

    function setItems(items) {
        GenericList.prototype.setItems.call(this, items);
        this.saveCustomPositions();
    }

    function sortByCustomOrder(indexes) {
        return function (a, b) {
            return indexes[a.id] - indexes[b.id];
        };
    }
}

angular
    .module("app.terminology")
    .factory("TranslationList", TranslationListModel);

/* @ngInject */
function TranslationListModel(GenericList) {

    function TranslationList(items, tag) {
        GenericList.call(this, items);
        this.numberOfVisibleItems = 0;
        this.checkAllStateVisible = 0;
        this.updateAllCheckVisible();
        this.tag = tag;
    }

    TranslationList.prototype = Object.create(GenericList.prototype);
    TranslationList.prototype.constructor = TranslationList;
    TranslationList.prototype.moveItems = moveItems;
    TranslationList.prototype.setItems = setItems;
    TranslationList.prototype.add = add;
    TranslationList.prototype.remove = remove;
    TranslationList.prototype.updateAllCheckVisible = updateAllCheckVisible;
    TranslationList.prototype.toggleVisible = toggleVisible;
    TranslationList.prototype.toggleAllVisible = toggleAllVisible;

    return TranslationList;

    //////////

    function moveItems(items, offset) {
        var f0, f1, that = this;
        items = items.filter(function (item) {
            return that.has(item);
        });
        items.sort(function (a, b) {
            return (that.idToPos[a.id] - that.idToPos[b.id]) * (offset < 0 ? 1 : -1);
        });
        f0 = offset < 0 ? 1 : 0;
        f1 = offset < 0 ? 0 : 1;
        _.each(items, function (item, num) {
            var index = that.idToPos[item.id],
                newIndex = index + offset;
            if (newIndex < num * f0) {
                newIndex = num * f0;
            } else if (newIndex >= that.numberOfVisibleItems - f1 * num) {
                newIndex = that.numberOfVisibleItems - 1 - f1 * num;
            }
            if (newIndex === index) {
                return;
            }
            that.items.splice(newIndex, 0, that.items.splice(index, 1)[0]);
        });
        that.savePositions();
        that.fire("change", {
            "type": "move",
            "items": items,
            "offset": offset
        });
    }

    function setItems(items) {
        GenericList.prototype.setItems.call(this, items);
        this.updateAllCheckVisible();
    }

    function add(item, index) {
        GenericList.prototype.add.call(this, item, index);
        this.updateAllCheckVisible();
    }

    function remove(items) {
        GenericList.prototype.remove.call(this, items);
        this.updateAllCheckVisible();
    }

    function updateAllCheckVisible() {
        var countObj;
        countObj = _.countBy(this.items, function(item) {
            return item.visible ? "visible": "invisible";
        });
        if (!countObj.visible) {
            this.checkAllStateVisible = 0;
            this.numberOfVisibleItems = 0;
        } else if (!countObj.invisible) {
            this.checkAllStateVisible = 1;
            this.numberOfVisibleItems = countObj.visible;
        } else {
            this.checkAllStateVisible = 2;
            this.numberOfVisibleItems = countObj.visible;
        }
    }

    function toggleVisible(item, preventSelect) {
        item.visible = !item.visible;
        this.updateAllCheckVisible();
        if (preventSelect) {
            return;
        }
        if (item.visible != item.selected) {
            this.toggle(item);
        }
        if (!item.visible) {
            GenericList.prototype.moveItems.call(this, [item], this.length - this.indexOf(item) - 1);
        } else {
            GenericList.prototype.moveItems.call(this, [item], this.indexOf(item) * -1);
        }
    }

    function toggleAllVisible() {
        if (this.checkAllStateVisible === 0 || this.checkAllStateVisible === 2) {
            this.checkAllStateVisible = 1;
            _.each(this.items, function (item) {
                item.visible = true;
            });
            this.numberOfVisibleItems = this.items.length;
        } else {
            this.checkAllStateVisible = 0;
            _.each(this.items, function (item) {
                item.visible = false;
            });
            this.numberOfVisibleItems = 0;
        }
    }
}

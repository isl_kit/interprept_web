angular
    .module("app.terminology")
    .factory("ChangeLog", ChangeLogModel);

/* @ngInject */
function ChangeLogModel(TerminologyService) {

    function ChangeLog () {
        this.saved = true;
        this.deleted = [];
        this.added = {};
    }

    ChangeLog.prototype.add = add;
    ChangeLog.prototype.remove = remove;
    ChangeLog.prototype.clear = clear;
    ChangeLog.prototype.save = save;
    ChangeLog.prototype.getLog = getLog;

    return ChangeLog;

    //////////

    function add(name, id) {
        if (_.contains(this.deleted, name)) {
            this.deleted = _.without(this.deleted, name);
        } else {
            this.added[id] = name;
        }
        this.saved = false;
    }

    function remove(name, id) {
        if (_.has(this.added, id)) {
            delete this.added[id];
        } else {
            this.deleted.push(name);
        }
        this.saved = false;
    }

    function clear() {
        this.saved = true;
        this.deleted = [];
        this.added = {};
    }

    function save() {
        if (this.saved === false) {
            TerminologyService.saveBlackAndWhiteTerms(this.getLog());
            this.saved = true;
        }
    }

    function getLog() {
        var del, add, that = this;
        add = _.map(that.added, function (name) {
            return name;
        }).filter(function (name) {
            return !_.contains(that.deleted, name);
        });
        del = that.deleted.filter(function (name) {
            return !_.contains(that.added, name);
        });
        return {
            deleted: del,
            added: add
        };
    }
}

angular
    .module("app.terminology")
    .controller("TerminologySaveModalController", TerminologySaveModalController);

/* @ngInject */
function TerminologySaveModalController ($scope, $timeout, termList, baseId, FocusService) {

    var vm = this;

    vm.baseId = baseId;
    vm.termListName = "my_term_list.tl";
    vm.closeModal = closeModal;
    vm.onDirSelect = onDirSelect;
    vm.save = save;

    activate();

    //////////

    function activate() {
        $timeout(function () {
            FocusService.focus("term-save-modal-input");
        });
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function onDirSelect(userDirectory) {
        vm.baseId = userDirectory.id;
    }

    function save() {
        termList.saveAs(vm.termListName, vm.baseId);
        $scope.$$closeModal();
    }

}

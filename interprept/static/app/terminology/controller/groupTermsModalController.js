angular
    .module("app.terminology")
    .controller("GroupTermsModalController", GroupTermsModalController);

/* @ngInject */
function GroupTermsModalController ($scope, terms) {

    var vm = this;

    vm.terms = terms;
    vm.closeModal = closeModal;
    vm.group = group;

    //////////

    function closeModal() {
        $scope.$$closeModal();
    }

    function group(term) {
        $scope.$$closeModal(term);
    }
}

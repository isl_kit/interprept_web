angular
    .module("app.terminology")
    .controller("RenameTermModalController", RenameTermModalController);

/* @ngInject */
function RenameTermModalController ($scope, $timeout, ModalService, FocusService, Term, name, isGroup) {

    var vm = this;

    vm.termName = name;
    vm.destroyGroup = true;
    vm.isGroup = isGroup;
    vm.closeModal = closeModal;
    vm.rename = rename;
    vm.toggleDestroyGroup = toggleDestroyGroup;

    activate();

    //////////

    function activate() {
        $timeout(function () {
            FocusService.focus("term-rename-modal-input");
        });
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function rename() {
        var newName = Term.validateName(vm.termName);
        if (newName) {
            $scope.$$closeModal({ name: newName, destroyGroup: vm.destroyGroup });
        } else {
            $scope.$$closeModal();
        }
    }

    function toggleDestroyGroup() {
        vm.destroyGroup = !vm.destroyGroup;
    }
}

angular
    .module("app.terminology")
    .controller("TranslationDetailsModalController", TranslationDetailsModalController);

/* @ngInject */
function TranslationDetailsModalController(UserService, TranslationService, tmTranslations, sourceLanguage, targetLanguage, translation, term) {

    var vm = this;

    vm.term = term;
    vm.translation = translation;
    vm.tmTranslations = tmTranslations;
    vm.sourceLanguage = sourceLanguage;
    vm.targetLanguage = targetLanguage;
    vm.hasGlosbeTranslation = true;
    vm.usersString = translation.users.map(function (user) {
        return user.firstName + " " + user.lastName;
    }).join(", ");
    vm.sourceLinks = {};
    vm.submit = submit;

    activate();

    //////////

    function activate() {
        _.each(tmTranslations, function (tmTranslation) {
            tmTranslation.setLanguages(sourceLanguage.tag, targetLanguage.tag);
        });
        updateHasGlosbeTranslation();
        updateSourceLinks();
    }

    function updateHasGlosbeTranslation() {
        vm.hasGlosbeTranslation = !!_.find(translation.sources, function (source) {
            return source.name === "glosbe";
        });
    }

    function updateSourceLinks() {
        _.each(translation.sources, function (source) {
            vm.sourceLinks[source.id] = source.getLink(sourceLanguage.tag.toLowerCase(), targetLanguage.tag.toLowerCase(), term.name);
        });
    }

    function submit() {
        TranslationService.submit(translation.id).then(function (sources) {
            _.each(sources, function (source) {
                if (!_.find(translation.sources, function (testSource) {
                    return testSource.id === source.id;
                })) {
                    translation.sources.push(source);
                }
            });
            updateHasGlosbeTranslation();
            updateSourceLinks();
        });
    }
}

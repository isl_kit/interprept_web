angular
    .module("app.terminology")
    .controller("CreateTermModalController", CreateTermModalController);

/* @ngInject */
function CreateTermModalController ($scope, $timeout, FocusService, Term, id) {

    var vm = this;

    vm.termName = "";
    vm.closeModal = closeModal;
    vm.create = create;

    activate();

    //////////

    function activate() {
        $timeout(function () {
            FocusService.focus("term-create-modal-input");
        });
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function create() {
        var newName = Term.validateName(vm.termName);
        if (newName) {
            var term = new Term({
                id: id,
                name: vm.termName,
                edited: true,
                importance: 1,
                occurances: null,
                totalMatches: 0
            });
            $scope.$$closeModal(term);
        } else {
            $scope.$$closeModal();
        }
    }
}

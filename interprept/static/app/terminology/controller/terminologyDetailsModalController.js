angular
    .module("app.terminology")
    .controller("TerminologyDetailsModalController", TerminologyDetailsModalController);

/* @ngInject */
function TerminologyDetailsModalController ($scope, $timeout, TranslationService, FocusService, term, langs, baseLang) {

    var vm = this;

    vm.term = term;
    vm.langs = langs;
    vm.transList = null;
    vm.newForm = {
        name: "",
        visible: false
    };
    vm.selectedLang = langs[0];
    vm.createTranslation = createTranslation;
    vm.openNewTranslationForm = openNewTranslationForm;
    vm.toggle = toggle;
    vm.toggleAll = toggleAll;
    vm.select = select;
    vm.toggleVisible = toggleVisible;
    vm.toggleAllVisible = toggleAllVisible;
    vm.moveSelectedTranslations = moveSelectedTranslations;
    vm.closeModal = closeModal;

    activate();

    //////////

    function activate() {
        _.each(langs, function (language) {
            term.combinedTranslations[language.tag].resetSelection();
        });
        if (langs.length > 0) {
            vm.transList = term.combinedTranslations[langs[0].tag];
        }
        $scope.$watch("vm.selectedLang.tag", selectedLangWatcher);
    }

    function createTranslation() {
        if (vm.newForm.name.trim().length > 0) {
            TranslationService.addTranslation(baseLang.tag, vm.transList.tag, vm.term.name, vm.newForm.name.trim()).then(function (translation) {
                translation.visible = true;
                vm.term.addTranslation(vm.transList.tag, translation);
                vm.newForm.visible = false;
            });
        } else {
            vm.newForm.visible = false;
        }
    }

    function openNewTranslationForm() {
        if (!vm.newForm.visible) {
            vm.newForm.name = "";
            vm.newForm.visible = true;
            $timeout(function () {
                FocusService.focus("translation-create-input");
            });
        } else {
            vm.newForm.visible = false;
        }
    }

    function toggle($event, translation) {
        $event.preventDefault();
        $event.stopPropagation();
        this.transList.toggle(translation);
        $("body").click();
        return false;
    }

    function select(translation) {
        this.transList.select(translation);
    }

    function toggleAll() {
        this.transList.toggleAll();
    }

    function toggleVisible($event, translation) {
        $event.preventDefault();
        $event.stopPropagation();
        this.transList.toggleVisible(translation);
        $("body").click();
        return false;
    }

    function toggleAllVisible() {
        this.transList.toggleAllVisible();
    }

    function moveSelectedTranslations(offset) {
        var selectedTranslations = vm.transList.filter(function (translation) {
            return translation.selected;
        });
        vm.transList.moveItems(selectedTranslations, offset);
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function selectedLangWatcher(newVal, oldVal) {
        if (oldVal === newVal) {
            return;
        }
        vm.transList = vm.term.combinedTranslations[vm.selectedLang.tag];
        vm.newForm.visible = false;
    }
}

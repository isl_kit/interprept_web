angular
    .module("app.terminology")
    .controller("TerminologyListController", TerminologyListController);

/* @ngInject */
function TerminologyListController ($scope, $rootScope, languages, termList, CustomizationService, ModalService, MenuService, SelectMenuService) {

    var vm = this;

    var customOption = { name: "custom", dir: "", label: "Custom Sorting" },
        chooseLangs = [];

    vm.termList = termList;
    vm.focusedIdMain = "";
    vm.hideTargetLanguages = false;
    vm.baseLang = termList.language;
    vm.sortOptions = [
        { name: "name", dir: "ascending", label: "Name: ascending" },
        { name: "name", dir: "descending", label: "Name: descending" },
        { name: "importance", dir: "ascending", label: "Importance: ascending" },
        { name: "importance", dir: "descending", label: "Importance: descending" }
    ];
    vm.selectedSortOption = vm.sortOptions[3];

    vm.showSingleMenu = showSingleMenu;
    vm.showMultiMenu = showMultiMenu;
    vm.toggle = toggle;
    vm.toggleAll = toggleAll;
    vm.moveSelectedTerms = moveSelectedTerms;
    vm.deleteSelectedTerms = deleteSelectedTerms;
    vm.createTerm = createTerm;
    vm.showLanguageSelectMenu = showLanguageSelectMenu;
    vm.swapColumn = swapColumn;
    vm.openEditView = openEditView;

    activate();

    //////////

    function activate () {
        initLangs();
        if (termList.customSorting) {
            addCustomSortingOption();
        }
        $scope.$onRootScope("te.selectTeFromText", selectTeFromTextHandler);
        $scope.$onRootScope("terminology.reset", resetHandler);
        $scope.$watch("vm.selectedSortOption.label", selectedSortOptionWatcher);
        termList.on("change", termListChangeHandler);
    }

    function initLangs() {
        chooseLangs = languages.filter(function (language) {
            return language.id !== termList.language.id;
        }).map(function (language) {
            return {
                name: language.name + " (" + language.tag + ")",
                orig: language,
                selected: false
            };
        });
        _.each(termList.targetTags, function (tag) {
            var chooseLang = _.find(chooseLangs, function (lang) {
                return lang.orig.tag === tag;
            });
            if (chooseLang) {
                chooseLang.selected = true;
            }
        });
    }

    function selectTeFromTextHandler(ev, term) {
        vm.focusedIdMain = "TE" + term.id;
    }

    function showSingleMenu($event, term) {

        MenuService.show("singleTermMenu", {
            term: term,
            termList: termList,
            index: termList.indexOf(term)
        }, $event.clientX, $event.clientY);

        $event.preventDefault();
        $event.stopPropagation();
        return false;
    }

    function showMultiMenu($event) {

        if (termList.numberOfSelectedItems === 1) {
            showSingleMenu($event, termList.filter(function (term) {
                return term.selected;
            })[0]);
            return;
        }

        MenuService.show("multiTermMenu", {
            terms: termList.filter(function (term) {
                return term.selected;
            }),
            termList: termList
        }, $event.clientX, $event.clientY);

        $event.preventDefault();
        $event.stopPropagation();
        return false;
    }

    function toggle($event, term) {
        $event.preventDefault();
        $event.stopPropagation();
        this.termList.toggle(term);
        $("body").click();
        return false;
    }

    function toggleAll() {
        this.termList.toggleAll();
    }

    function getSelectedTerms() {
        return termList.filter(function (term) {
            return term.selected;
        });
    }

    function getTranslationIds() {
        var translationIds = [];

        _.each(termList.targetTags, function (tag) {
            termList.each(function (term) {
                var selectedTranslations;
                if (term.translations[tag]) {
                    selectedTranslations = term.translations[tag].filter(function (translation) {
                        return translation.visible;
                    });
                    _.each(selectedTranslations, function (translation) {
                        translationIds.push(translation.id);
                    });
                }
            });
        });
        return translationIds;
    }

    function lockList() {
        if (!locked) {
            $rootScope.$emit("terminology.settingsLocked", true);
            locked = true;
        }
    }

    function resetHandler(ev) {
        updateRawList();
    }

    function moveSelectedTerms(offset) {
        var selectedTerms = getSelectedTerms();
        termList.moveItems(selectedTerms, offset);
    }

    function deleteSelectedTerms() {
        var selectedTerms = getSelectedTerms();
        termList.remove(selectedTerms);
    }

    function createTerm() {
        ModalService.open("normal@createTerm", {
            id: termList.getUniqueId()
        }).then(function (term) {
            termList.add(term, 0);
        });
    }

    function showLanguageSelectMenu($event) {
        SelectMenuService.show(chooseLangs, 10, onLangSelect, $event.clientX, $event.clientY);
        $event.preventDefault();
        $event.stopPropagation();
        return false;
    }

    function saveSelectedLangs() {
        var result = {};

        result[vm.baseLang.tag] = termList.targetTags;
        CustomizationService.set({"terminology.selected_langs": result}, true);
    }

    function onLangSelect() {
        termList.setTargetLanguages(chooseLangs.filter(function (language) {
            return language.selected;
        }).map(function (language) {
            return language.orig;
        }), true);
        saveSelectedLangs();
    }

    function swapColumn(language, offset) {
        termList.moveTargetLanguage(language, offset);
        saveSelectedLangs();
    }

    function selectedSortOptionWatcher(newVal, oldVal) {
        if (oldVal === newVal) {
            return;
        }
        updateSorting();
    }

    function termListChangeHandler(evt) {
        if (evt.type === "move" || evt.type === "add" || evt.type === "ungroup") {
            addCustomSortingOption();
        } else if (evt.type === "set") {
            removeCustomSortingOption();
        }
    }

    function addCustomSortingOption() {
        if (!_.contains(vm.sortOptions, customOption)) {
            vm.sortOptions.push(customOption);
        }
        vm.selectedSortOption = customOption;
    }

    function removeCustomSortingOption(option) {
        vm.sortOptions = _.without(vm.sortOptions, customOption);
        vm.selectedSortOption = option || vm.sortOptions[3];

    }

    function updateSorting() {
        var name = vm.selectedSortOption.name,
            dir = vm.selectedSortOption.dir;
        termList.sort(name, dir);
    }

    function openEditView(term) {
        ModalService.open("normal@terminologyEdit", { term: term, termList: termList });
    }
}

angular
    .module("app.terminology")
    .controller("TerminologyVerifyResetModalController", TerminologyVerifyResetModalController);

/* @ngInject */
function TerminologyVerifyResetModalController ($scope) {

    var vm = this;

    vm.cancel = cancel;
    vm.proceed = proceed;

    //////////

    function cancel() {
        $scope.$$closeModal();
    }

    function proceed() {
        $scope.$$closeModal(true);
    }
}

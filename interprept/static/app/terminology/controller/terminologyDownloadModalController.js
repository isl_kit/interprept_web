angular
    .module("app.terminology")
    .controller("TerminologyDownloadModalController", TerminologyDownloadModalController);

/* @ngInject */
function TerminologyDownloadModalController ($scope, fileName) {

    var vm = this;

    vm.fileName = fileName;
    vm.closeModal = closeModal;
    vm.openTab = openTab;
    vm.download = download;

    //////////

    function closeModal() {
        $scope.$$closeModal();
    }

    function openTab() {
        var win = window.open("/files/downloadinline/" + vm.fileName, '_blank');
        win.focus();
        closeModal();
    }

    function download() {
        var hiddenIFrameID = 'hiddenDownloader',
            iframe = document.getElementById(hiddenIFrameID);
        if (iframe === null) {
            iframe = document.createElement('iframe');
            iframe.id = hiddenIFrameID;
            iframe.style.display = 'none';
            document.body.appendChild(iframe);
        }
        iframe.src = "files/download/" + fileName;
        closeModal();
    }
}

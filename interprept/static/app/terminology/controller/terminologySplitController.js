angular
    .module("app.terminology")
    .controller("TerminologySplitController", TerminologySplitController);

/* @ngInject */
function TerminologySplitController (termList) {

    var vm = this;

    vm.isSplitted = false;

    activate();

    //////////

    function activate() {
        termList.on("selectDoc", selectDocHandler);
    }

    function selectDocHandler(doc) {
        if (doc) {
            vm.isSplitted = true;
        } else {
            vm.isSplitted = false;
        }
    }
}

angular
    .module("app.terminology")
    .controller("TerminologyEditModalController", TerminologyEditModalController);

/* @ngInject */
function TerminologyEditModalController($scope, $timeout, FocusService, ModalService, Term, Translation, TranslationService, termList, term) {

    var vm = this;

    vm.termList = termList;
    vm.selectedTerm = term;
    vm.tempTerm = {};
    vm.expanded = {};
    vm.tempTranslation = {};
    vm.selectedRelTerm = undefined;
    vm.selectRelTerm = selectRelTerm;
    vm.selectTerm = selectTerm;
    vm.editTerm = editTerm;
    vm.modifyTerm = modifyTerm;
    vm.updateTranslations = updateTranslations;
    vm.createTranslation = createTranslation;
    vm.addTranslation = addTranslation;
    vm.moveSelectedTranslations = moveSelectedTranslations;
    vm.toggleOtherTranslations = toggleOtherTranslations;
    vm.toggleTranslationSelection = toggleTranslationSelection;
    vm.toggleTranslationVisibility = toggleTranslationVisibility;
    vm.openTranslationDetails = openTranslationDetails;
    vm.closeModal = closeModal;

    activate();

    //////////

    function activate() {
        selectTerm(term);
    }

    function resetExpanded() {
        _.each(termList.targetLanguages, function (language) {
            vm.expanded[language.tag] = false;
        });
    }

    function resetTemptranslations() {
        _.each(termList.targetLanguages, function (language) {
            vm.tempTranslation[language.tag] = {
                name: "",
                active: false,
                error: false
            };
        });
    }

    function resetTempTerm() {
        vm.tempTerm = {
            name: "",
            active: false,
            error: false
        };
    }

    function unmark(term) {
        _.each(term.translations, function (transArr) {
            _.each(transArr, function (translation) {
                translation.marked = false;
            });
        });
    }

    function selectRelTerm(term) {
        if (term !== vm.selectedRelTerm) {
            _.each(vm.selectedTerm.related, unmark);
            unmark(vm.selectedTerm);
            vm.selectedRelTerm = term;
            if (term) {
                _.each(term.translations, function (transArr) {
                    _.each(transArr, function (translation) {
                        translation.marked = true;
                    });
                });
            }
        } else if (term) {
            _.each(vm.selectedTerm.related, unmark);
            unmark(vm.selectedTerm);
            vm.selectedRelTerm = null;
        }
    }

    function selectTerm(term) {
        vm.selectedTerm = term;
        resetExpanded();
        resetTemptranslations();
        resetTempTerm();
        selectRelTerm(null);
    }

    function editTerm() {
        if (vm.tempTerm.active) {
            resetTempTerm();
        } else {
            resetTempTerm();
            vm.tempTerm.active = true;
            vm.tempTerm.name = vm.selectedTerm.name;
            $timeout(function () {
                FocusService.focus("terminology-edit-input");
            });
        }
    }

    function modifyTerm() {
        var name = Term.validateName(vm.tempTerm.name),
            newTerm;
        if (name) {
            newTerm = termList.rename(vm.selectedTerm, name, false);
            if (newTerm) {
                selectTerm(newTerm);
            }
            resetTempTerm();
        } else {
            vm.tempTerm.error = true;
        }
    }

    function updateTranslations() {
        termList.getTranslations(vm.selectedTerm);
    }

    function createTranslation(tag) {
        var name = Translation.validateName(vm.tempTranslation[tag].name),
            transList;

        if (name) {
            transList = vm.selectedTerm.combinedTranslations[tag];
            if (transList) {
                TranslationService.addTranslation(termList.language.tag, tag, vm.selectedTerm.name, name).then(function (translation) {
                    var presentTranslation = transList.getItem(function (item) {
                        return item.id === translation.id;
                    });
                    if (presentTranslation) {
                        if (!presentTranslation.visible) {
                            transList.toggleVisible(presentTranslation);
                        }
                        if (!presentTranslation.selected) {
                            transList.toggle(presentTranslation);
                        }
                        _.each(translation.users, function (user) {
                            if (!_.find(presentTranslation.users, function(user2) {
                                return user.id === user2.id;
                            })) {
                                presentTranslation.users.push(user);
                            }
                        });
                        transList.moveItems([presentTranslation], transList.indexOf(presentTranslation) * -1);
                    } else {
                        translation.visible = true;
                        translation.selected = true;
                        vm.selectedTerm.addTranslation(tag, translation);
                    }
                    resetTemptranslations();
                });
            } else {
                resetTemptranslations();
            }
        } else {
            vm.tempTranslation[tag].error = true;
        }
    }

    function addTranslation(tag) {
        if (vm.tempTranslation[tag].active) {
            resetTemptranslations();
        } else {
            resetTemptranslations();
            vm.tempTranslation[tag].active = true;
            $timeout(function () {
                FocusService.focus("terminology-create-input_" + tag);
            });
        }
    }

    function moveSelectedTranslations(tag, offset) {
        var transList = vm.selectedTerm.combinedTranslations[tag],
            selectedTranslations;

        if (transList) {
            selectedTranslations = transList.filter(function (translation) {
                return translation.selected;
            });
            transList.moveItems(selectedTranslations, offset);
        }
    }

    function toggleOtherTranslations(tag) {
        vm.expanded[tag] = !vm.expanded[tag];
    }

    function toggleTranslationVisibility(tag, translation) {
        var transList = vm.selectedTerm.combinedTranslations[tag];

        if (transList) {
            transList.toggleVisible(translation);
        }
    }

    function toggleTranslationSelection(tag, translation) {
        var transList = vm.selectedTerm.combinedTranslations[tag];

        if (transList) {
            transList.toggle(translation);
        }
    }

    function openTranslationDetails($event, translation, targetLanguage) {
        ModalService.open("normal@translationDetails", { term: vm.selectedTerm, translation: translation, targetLanguage: targetLanguage, sourceLanguage: termList.language });
        $event.preventDefault();
        $event.stopPropagation();
        return false;
    }

    function closeModal() {
        $scope.$$closeModal();
    }
}

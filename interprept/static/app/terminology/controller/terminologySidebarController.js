angular
    .module("app.terminology")
    .controller("TerminologySidebarController", TerminologySidebarController);

/* @ngInject */
function TerminologySidebarController ($scope, $state, termList, FileType, $stateParams, terminologySettings, ModalService, CustomizationService) {

    var vm = this;

    vm.termList = termList;
    vm.docItems = termList.docs.map(createDocItem);
    vm.minOccTemp = termList.minOcc;
    vm.minOcc = termList.minOcc;
    vm.numTerms = terminologySettings.numTermsOptions;
    vm.selectedNumTerms = _.find(vm.numTerms, function (item) {
        return item.number === termList.maxLength;
    });
    vm.backToFileSpace = backToFileSpace;
    vm.resetList = resetList;
    vm.download = download;
    vm.saveList = saveList;
    vm.saveListAs = saveListAs;
    vm.showFullView = showFullView;

    activate();

    //////////

    function activate() {
        vm.docItems.sort(docItemSortingFunction);
        $scope.$watch("vm.minOcc", minOccWatcher);
        $scope.$watch("vm.selectedNumTerms.number", selectedNumTermsWatcher);
        termList.on("selectDoc", selectDocHandler);
        $(window).on("beforeunload", leaveHandler);
        $scope.$on("$destroy", deactivate);
    }

    function deactivate() {
        leaveHandler();
        $(window).off("beforeunload", leaveHandler);
    }

    function createDocItem(userDocument) {
        return {
            id: userDocument.id,
            selected: false,
            orig: userDocument,
            resource: userDocument.resource,
            name: userDocument.name,
            converted: _.contains(["ok", "deprecated"], userDocument.resource.taskStates.text.state),
            imgPrefix: userDocument.fileType === FileType.DOC ? "file" : "report"
        };
    }

    function docItemSortingFunction(a, b) {
        var lowerA, lowerB;
        if (a.converted && !b.converted) {
            return -1;
        } else if (b.converted && !a.converted) {
            return 1;
        }
        lowerA = a.name.toLowerCase();
        lowerB = b.name.toLowerCase();
        if (lowerA < lowerB) {
            return -1;
        } else if (lowerB < lowerA) {
            return 1;
        }
        return 0;
    }

    function minOccWatcher(newVal, oldVal) {
        if (newVal === oldVal) {
            return;
        }
        CustomizationService.set({"terminology.specificity": newVal});
        termList.minOcc = newVal;
        termList.update();
    }

    function selectedNumTermsWatcher(newVal, oldVal) {
        if (newVal === oldVal) {
            return;
        }
        CustomizationService.set({"terminology.list_length": newVal});
        termList.maxLength = newVal;
        termList.update();
    }

    function backToFileSpace() {
        $state.go("documents.browser", { dir: $stateParams.base });
    }

    function resetList() {
        ModalService.open("normal@terminologyVerifyReset", {}).then(function (proceed) {
            if (proceed) {
                termList.update();
            }
        });
    }

    function download() {
        termList.download();
    }

    function saveList() {
        termList.save();
    }

    function saveListAs() {
        ModalService.open("normal@terminologySave", { baseId: Number($stateParams.base), termList: termList });
    }

    function selectDocHandler(doc) {
        _.each(vm.docItems, function (docItem) {
            if (doc && doc.id === docItem.id) {
                docItem.selected = true;
            } else {
                docItem.selected = false;
            }
        });
    }

    function showFullView(docItem) {
        if (!docItem.converted) {
            return;
        }
        termList.selectDoc(docItem.orig);
    }

    function leaveHandler() {
        termList.autoSave();
        termList.destroy();
    }
}

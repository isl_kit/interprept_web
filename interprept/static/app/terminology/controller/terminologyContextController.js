angular
    .module("app.terminology")
    .controller("TerminologyContextController", TerminologyContextController);

/* @ngInject */
function TerminologyContextController ($scope, $rootScope, termList, Term) {

    var vm = this;

    vm.termList = termList;
    vm.selectedCount = 0;
    vm.selectedFocus = 0;
    vm.selectedUid = 0;
    vm.selectedText = "";
    vm.addTerm = addTerm;
    vm.prev = prev;
    vm.next = next;
    vm.close = close;
    vm.clickHandler = clickHandler;

    activate();

    //////////

    function activate() {
        $scope.$watch("vm.selectedCount", selectedCountWatcher);
    }

    function selectedCountWatcher(newVal, oldVal) {
        if (newVal === oldVal) {
            return;
        }
        if (vm.selectedFocus > vm.selectedCount - 1) {
            vm.selectedFocus = vm.selectedCount - 1;
        }
    }

    function addTerm() {
        if (vm.selectedText.length === 0) {
            return;
        }
        var normName = vm.selectedText.replace("\r\n", "\n").replace("\n", " ");
        var term = new Term({
            id: termList.getUniqueId(),
            name: vm.selectedText,
            edited: true,
            importance: 1,
            occurances: null,
            totalMatches: 0
        });
        termList.add(term, 0);
    }

    function prev() {
        if (vm.selectedCount === 0) {
            return;
        }
        if (vm.selectedFocus <= 0) {
            vm.selectedFocus = vm.selectedCount - 1;
        } else {
            vm.selectedFocus -= 1;
        }
    }

    function next() {
        if (vm.selectedCount === 0) {
            return;
        }
        if (vm.selectedFocus >= vm.selectedCount - 1) {
            vm.selectedFocus = 0;
        } else {
            vm.selectedFocus += 1;
        }
    }

    function close() {
        termList.selectDoc(null);
    }

    function clickHandler(term) {
        $rootScope.$emit("te.selectTeFromText", term);
    }
}

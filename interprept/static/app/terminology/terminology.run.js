angular
    .module("app.terminology")
    .run(terminologyRun);

/* @ngInject */
function terminologyRun(MenuService, ModalService) {

    MenuService.addMenu("singleTermMenu", [
        [
            {
                text: "Details / Edit",
                img: null,
                defaultItem: true,
                callback: function (data) {
                    ModalService.open("normal@terminologyEdit", {
                        term: data.term,
                        termList: data.termList
                    });
                }
            }
        ],
        [
            {
                text: "Update Translations",
                desc: "check online dictionaries for new translations",
                img: null,
                callback: function (data) {
                    data.termList.getTranslations(data.term);
                }
            },
            {
                text: "Insert Before",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@createTerm", {
                        id: data.termList.getUniqueId()
                    }).then(function (term) {
                        data.termList.add(term, data.index);
                    });
                }
            },
            {
                text: "Insert After",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@createTerm", {
                        id: data.termList.getUniqueId()
                    }).then(function (term) {
                        data.termList.add(term, data.index + 1);
                    });
                }
            }
        ],
        [
            {
                text: "Create Group",
                img: null,
                isDisabled: function (data) {
                    if (data.termList.numberOfSelectedItems > 1) {
                        return false;
                    }
                    if (data.termList.numberOfSelectedItems === 1) {
                        return data.termList.filter(function (term) {
                            return term.selected;
                        })[0] === data.term;
                    }
                    return true;
                },
                callback: function (data) {
                    var selectedTerms;

                    selectedTerms = data.termList.filter(function (term) {
                        return term.selected && term !== data.term;
                    });
                    data.termList.groupTerms(data.term, selectedTerms, true);
                }
            },
            {
                text: "Ungroup Terms",
                img: null,
                isDisabled: function (data) {
                    return data.term.related.length === 0;
                },
                callback: function (data) {
                    data.termList.destroyTermGroup(data.term);
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                callback: function (data) {
                    data.termList.remove([data.term]);
                }
            }
        ]
    ]);


    MenuService.addMenu("multiTermMenu", [
        [
            {
                text: "Group Terms",
                img: null,
                isDisabled: function (data) {
                    if (data.terms.length > 1) {
                        return false;
                    }
                    return true;
                },
                callback: function (data) {
                    ModalService.open("normal@groupTerms", {
                        terms: data.terms
                    }).then(function (rootTerm) {
                        data.termList.groupTerms(rootTerm, _.without(data.terms, rootTerm), true);
                    });
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                defaultItem: true,
                callback: function (data) {
                    _.each(data.terms, function (term) {
                        data.termList.remove([term]);
                    });
                }
            }
        ]
    ]);

    ModalService.register({
        name: "terminologyEdit",
        showCloseIcon: true,
        dark: true,
        states: {
            normal: {
                templateUrl: "terminology/templates/terminology-edit-modal.tpl.html",
                controller: "TerminologyEditModalController",
                controllerAs: "vm",
                params: ["termList", "term"]
            }
        }
    });

    ModalService.register({
        name: "translationDetails",
        showCloseIcon: true,
        states: {
            normal: {
                templateUrl: "terminology/templates/translation-details-modal.tpl.html",
                controller: "TranslationDetailsModalController",
                controllerAs: "vm",
                params: ["term", "targetLanguage", "sourceLanguage", "translation"],
                resolve: {
                    "tmTranslations": ["TranslationService", "translation", function (TranslationService, translation) {
                        return TranslationService.getContext(translation.id);
                    }]
                }
            }
        }
    });

    ModalService.register({
        name: "renameTerm",
        showCloseIcon: true,
        states: {
            normal: {
                templateUrl: "terminology/templates/rename-term-modal.tpl.html",
                controller: "RenameTermModalController",
                controllerAs: "vm",
                params: ["name", "isGroup"]
            }
        }
    });

    ModalService.register({
        name: "createTerm",
        showCloseIcon: true,
        states: {
            normal: {
                templateUrl: "terminology/templates/create-term-modal.tpl.html",
                controller: "CreateTermModalController",
                controllerAs: "vm",
                params: ["id"]
            }
        }
    });

    ModalService.register({
        name: "groupTerms",
        showCloseIcon: true,
        states: {
            normal: {
                templateUrl: "terminology/templates/group-terms-modal.tpl.html",
                controller: "GroupTermsModalController",
                controllerAs: "vm",
                params: ["terms"]
            }
        }
    });

    ModalService.register({
        name: "terminologyDetails",
        showCloseIcon: true,
        states: {
            normal: {
                templateUrl: "terminology/templates/terminology-details-modal.tpl.html",
                controller: "TerminologyDetailsModalController",
                controllerAs: "vm",
                params: ["term", "langs", "baseLang"]
            }
        }
    });

    ModalService.register({
        name: "terminologyVerifyReset",
        showCloseIcon: false,
        states: {
            normal: {
                templateUrl: "terminology/templates/terminology-verify-reset-modal.tpl.html",
                controller: "TerminologyVerifyResetModalController",
                controllerAs: "vm",
                params: []
            }
        }
    });

    ModalService.register({
        name: "terminologySave",
        showCloseIcon: false,
        states: {
            normal: {
                templateUrl: "terminology/templates/terminology-save-modal.tpl.html",
                controller: "TerminologySaveModalController",
                controllerAs: "vm",
                params: ["baseId", "termList"]
            }
        }
    });

    ModalService.register({
        name: "terminologyDownload",
        showCloseIcon: false,
        states: {
            normal: {
                templateUrl: "terminology/templates/terminology-download-modal.tpl.html",
                controller: "TerminologyDownloadModalController",
                controllerAs: "vm",
                params: ["fileName"]
            }
        }
    });
}

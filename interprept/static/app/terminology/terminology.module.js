angular.module("app.terminology", [
    "app.core",
    "app.auth",
    "app.resources",
    "app.sessions",
    "app.documents"
]);

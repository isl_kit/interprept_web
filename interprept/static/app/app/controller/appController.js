angular
    .module("app")
    .controller("AppController", AppController);

/* @ngInject */
function AppController($scope, $rootScope, $state, AuthenticationService, UserService, MenuService) {

    var nextTitle = "";

    $rootScope.selfRegistration = window.GLOBAL_CONFIG && window.GLOBAL_CONFIG.SELF_REGISTRATION;

    $scope.$on('$locationChangeSuccess', function (event, newUrl, oldUrl) {
        console.log("success to " + newUrl);
        console.log(arguments);

        $scope.pageTitle = nextTitle;
    });

    $scope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
        var index, mainStateName;
        console.log("statechangesuccess");
        if (angular.isDefined(toState.data.topBarContent)) {
            $scope.topBarContent = toState.data.topBarContent;
        } else {
            $scope.topBarContent = "Logout";
        }

        if (angular.isDefined(toState.data.sidebarDisabled)) {
            $scope.sidebarDisabled = toState.data.sidebarDisabled;
        } else {
            $scope.sidebarDisabled = false;
        }

        if (angular.isDefined(toState.data.pageTitle)) {
            nextTitle = toState.data.pageTitle + " | Interprept";
            if ($scope.pageTitle === "") {
                $scope.pageTitle = nextTitle;
            }
        }

        mainStateName = toState.name;
        index = mainStateName.indexOf(".");
        if (index > -1) {
            mainStateName = mainStateName.substring(0, index);
        }
        $scope.mainStateName = mainStateName;
        $scope.state = toState;
    });

    $scope.user = UserService.user;

    $scope.credentials = {
        email: "",
        password: ""
    };

    $scope.logout = function () {
        AuthenticationService.logout().success(function (data, status) {
            $scope.credentials.email = "";
            $scope.credentials.password = "";
            $state.go('login');
        });
    };

    $scope.login = function () {
        AuthenticationService.login($scope.credentials).then(function (data) {
            var passwordReset, loggingDecisionNeeded, newToWebdav;

            if (data.authenticated) {
                passwordReset = !!data.user.password_was_resetted;
                loggingDecisionNeeded = data.user.logging_decision_needed;
                newToWebdav = !!data.user.new_to_webdav;
                $rootScope.newToWebdav = newToWebdav;
                $rootScope.loggingDecisionNeeded = loggingDecisionNeeded;
                $rootScope.passwordReset = passwordReset;
                if (passwordReset) {
                    $state.go("settings");
                } else {
                    $state.go("documents.browser");
                }
            } else {
                $state.go("login", { trial: 1 });
            }
        }).finally(function () {
            $scope.credentials.email = "";
            $scope.credentials.password = "";
        });
    };

    $scope.headEnabled = true;

    $scope.topbarEnabled = true;

    $scope.pageHeadControlsEnabled = true;

    $scope.sidebarEnabled = true;

    $scope.toggleSidebar = function () {
        $scope.sidebarEnabled = !$scope.sidebarEnabled;
    };

    $scope.sidebarSize = "large";

    $scope.toggleSidebarSize = function () {
        $scope.sidebarSize = $scope.sidebarSize === "large" ? "small" : "large";
        $scope.updateExtend.callback();
    };

    $scope.extendable = false;
    $rootScope.extendEnabled = false;

    $rootScope.toggleExtend = function () {
        $rootScope.extendEnabled = !$rootScope.extendEnabled;
    };

    $scope.updateExtend = {
        callback: function () {

        }
    };


    $rootScope.scrollToExtendElement = "";


    $scope.fixedMenuEnabled = false;

    $scope.openUserMenu = function ($event) {
        console.log($event);
        MenuService.show("userMenu", {}, $event.clientX, $event.clientY);
        $event.preventDefault();
        $event.stopPropagation();
        return false;
    };

    MenuService.addMenu("userMenu", [
        [
            {
                text: "Settings",
                img: null,
                callback: function () {
                    $state.go("settings");
                }
            },
            {
                text: "Feedback",
                img: null,
                callback: function () {
                    $state.go("feedback");
                }
            }
        ],
        [
            {
                text: "Logout",
                img: null,
                callback: function () {
                    AuthenticationService.logout().success(function (data, status) {
                        $scope.credentials.email = "";
                        $scope.credentials.password = "";
                        $state.go('login');
                    });
                }
            }
        ]
    ]);
}

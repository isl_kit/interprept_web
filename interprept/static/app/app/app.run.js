angular
    .module("app")
    .run(appRun);

/* @ngInject */
function appRun($rootScope, $state, AuthenticationService, LoggingService) {
    var routesThatDontRequireAuth = ["login", "register", "welcome", "reset", "about","disclaimer","privacy"],
        routesThatRequireNonAuth = ["login", "register", "welcome", "reset"],
        authFlag = {};

    LoggingService.start();
    $rootScope.authorized = false;
    $rootScope.stopPropagation = stopPropagation;
    $rootScope.$on("$stateChangeStart", onStateChangeStart);
    $rootScope.$on("$stateChangeError", onStateChangeError);

    //////////

    function stopPropagation($event) {
        $event.stopPropagation();
        return false;
    }

    function redirectFinally(stateName, params) {
        authFlag[stateName] = true;
        $state.transitionTo(stateName, params);
    }

    function onStateChangeStart(event, toState, toParams, fromState, fromParams) {
        var needsAuth, authorized;

        if (!authFlag[toState.name]) {

            needsAuth = !_(routesThatDontRequireAuth).contains(toState.name);
            authorized = AuthenticationService.isLoggedIn();

            if (typeof authorized !== "boolean") {
                authorized.then(function (authorized) {
                    if (needsAuth) {
                        if (authorized !== true) {
                            if (toState.name === "documents.browser") {
                                redirectFinally("welcome");
                            } else {
                                redirectFinally("login");
                            }
                        } else {
                            redirectFinally(toState.name, toParams);
                        }
                    } else {
                        if (authorized === true && _(routesThatRequireNonAuth).contains(toState.name)) {
                            redirectFinally("documents.browser");
                        } else {
                            redirectFinally(toState.name, toParams);
                        }
                    }
                    $rootScope.authorized = authorized === true;
                });
                event.preventDefault();
                return;
            }

            if (needsAuth) {
                if (authorized !== true) {
                    event.preventDefault();
                    if (toState.name === "documents.browser") {
                        redirectFinally("welcome");
                    } else {
                        redirectFinally("login");
                    }
                }
            } else {
                if (authorized === true && _(routesThatRequireNonAuth).contains(toState.name)) {
                    event.preventDefault();
                    redirectFinally("documents.browser");
                }
            }
            $rootScope.authorized = authorized === true;

        } else {
            authFlag[toState.name] = false;
        }
    }

    function onStateChangeError(event, toState, toParams, fromState, fromParams, error) {
        if (error.status === 404) {
            $state.go("documents.browser");
        }
    }
}

angular.module("app", [
    "app.core",

    "app.templates",

    "app.terminology",
    "app.ne",
    "app.resources",
    "app.auth",
    "app.legal",
    "app.feedback",
    "app.documents",
    "app.sessions",
    "app.logging"
]);

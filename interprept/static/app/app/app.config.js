angular
    .module("app")
    .config(appConfig);

/* @ngInject */
function appConfig($urlRouterProvider, $httpProvider, $locationProvider, $provide, $compileProvider, DEBUG) {

    $urlRouterProvider.otherwise(function ($injector, $location) {
        var $state = $injector.get("$state");
        $state.go("documents.browser");
    });
    $httpProvider.defaults.xsrfCookieName = "csrftoken";
    $httpProvider.defaults.xsrfHeaderName = "X-CSRFToken";

    if (!DEBUG) {
        $compileProvider.debugInfoEnabled(false);
        $httpProvider.useApplyAsync(true);
    }

    //$locationProvider.html5Mode(true);
    //$locationProvider.hashPrefix("!");

    $provide.decorator("$rootScope", onRootScope);
    $provide.factory("logsOutUserOn401", logsOutUserOn401);
    $provide.factory("pageReloadOnHtmlContent", pageReloadOnHtmlContent);
    $httpProvider.interceptors.push("logsOutUserOn401");
    $httpProvider.interceptors.push("pageReloadOnHtmlContent");

    //////////

    /* @ngInject */
    function onRootScope($delegate){
        Object.defineProperty($delegate.constructor.prototype, "$onRootScope", {
            value: function(name, listener){
                var unsubscribe = $delegate.$on(name, listener);
                this.$on("$destroy", unsubscribe);
            },
            enumerable: false
        });
        return $delegate;
    }

    /* @ngInject */
    function logsOutUserOn401($q, UserService, SessionService) {
        var reloadOnHtml = function (response) {
            var contentType = response.headers("Content-Type");
            if (contentType && contentType.indexOf("text/html") === 0) {
                window.location.reload(true);
            }
        };

        return {
            response: function (response) {
                return response;
            },
            responseError: function (response) {
                if(response.status === 401) {
                    SessionService.unset("authenticated");
                    UserService.clear();
                    window.location.reload(true);
                }
                return $q.reject(response);
            }
        };
    }

    /* @ngInject */
    function pageReloadOnHtmlContent($q) {
        var reloadOnHtml = function (response) {
            var contentType = response.headers("Content-Type");
            if (contentType && contentType.indexOf("text/html") === 0) {
                window.location.reload(true);
            }
        };

        return {
            response: function(response) {
                reloadOnHtml(response);
                return response;
            },
            responseError: function(response) {
                reloadOnHtml(response);
                return $q.reject(response);
            }
        };
    }
}

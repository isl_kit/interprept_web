angular
    .module("app.documents")
    .config(documentsRoutes);

/* @ngInject */
function documentsRoutes($stateProvider) {
    $stateProvider.state("documents", {
        abstract: true,
        url: "/docs",
        views: {
            "main-content": {
                template: "<div ui-view='docs-browser'/>"
            },
            "sidebar": {
                controller: "DocumentsSidebarController",
                controllerAs: "vm",
                templateUrl: "documents/templates/documents.sidebar.tpl.html"
            }
        }
    });
    $stateProvider.state("documents.browser", {
        url: "?dir",
        views: {
            "docs-browser": {
                controller: "BrowserController",
                controllerAs: "vm",
                templateUrl: "documents/templates/browser.tpl.html"
            }
        },
        data: {
            pageTitle: "Documents"
        },
        resolve: {
            "dirContent": /* @ngInject */ function (DocumentsDataService, $stateParams) {
                return DocumentsDataService.getDirContent($stateParams.dir);
            }
        }
    });
}

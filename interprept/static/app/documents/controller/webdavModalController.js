angular
    .module("app.documents")
    .controller("WebdavModalController", WebdavModalController);

/* @ngInject */
function WebdavModalController($scope, $filter, directoryId, webdavContent, FileFormatService, FileOperationsService, ModalService) {

    var vm = this;

    var sortingFunctions = [
            sortByName,
            sortBySize,
            sortByModified
        ];

    vm.initialized = webdavContent.initialized;
    vm.items = webdavContent.files.map(fileToItem);
    vm.webdavLink = window.location.protocol + "//" + window.location.host + "/webdav";

    vm.checkAllState = 0;
    vm.toggle = toggle;
    vm.toggleAll = toggleAll;
    vm.select = select;

    vm.sortIndex = 2;
    vm.sortDirection = "descending";
    vm.sort = sort;

    vm.deleteSelectedItems = deleteSelectedItems;
    vm.startTransfer = startTransfer;
    vm.closeModal = closeModal;

    activate();

    //////////

    function activate() {
        redraw();
    }

    function makePrintDate(date) {
        var now = new Date(),
            month = now.getMonth(),
            day = now.getDate(),
            year = now.getFullYear();

        if (date.getFullYear() === now.getFullYear() &&
            date.getMonth() === now.getMonth() &&
            date.getDate() === now.getDate()) {

            return $filter("date")(date, "HH:mm");
        } else {
            return $filter("date")(date, "dd. MMM. yyyy");
        }
    }

    function fileToItem(file) {
        return _.extend({
            selected: false,
            printSize: FileFormatService.formatFileSize(file.size),
            printModified: makePrintDate(file.modified)
        }, file);
    }

    /**
     * SELECT
     */

    function updateAllCheck() {
        var countObj;

        countObj = _.countBy(vm.items, function(item) {
            return item.selected ? "selected": "unselected";
        });

        if (!countObj.selected) {
            vm.checkAllState = 0;
        } else if (!countObj.unselected) {
            vm.checkAllState = 1;
        } else {
            vm.checkAllState = 2;
        }
    }

    function toggle($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.selected = !item.selected;
        updateAllCheck();
        $("body").click();
        return false;
    }

    function select(item) {
        _.each(vm.items, function (checkItem) {
            checkItem.selected = checkItem === item;
        });

        updateAllCheck();
    }

    function toggleAll() {
        if (vm.checkAllState === 0 || vm.checkAllState === 2) {
            vm.checkAllState = 1;
            _.each(vm.items, function (item) {
                item.selected = true;
            });
        } else {
            vm.checkAllState = 0;
            _.each(vm.items, function (item) {
                item.selected = false;
            });
        }
    }

    /**
     * SORTING
     */

    function sort(index) {
        if (vm.sortIndex === index) {
            vm.sortDirection = vm.sortDirection === "ascending" ? "descending" : "ascending";
        } else {
            vm.sortIndex = index;
            vm.sortDirection = "ascending";
        }
        redraw();
    }

    function redraw() {
        vm.items.sort(sortingFunctions[vm.sortIndex](vm.sortDirection));
    }

    function sortByName(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;

        return function (a, b) {
            var lowerA, lowerB;

            lowerA = a.name.toLowerCase();
            lowerB = b.name.toLowerCase();

            if (lowerA < lowerB) {
                return -1 * dirCorrector;
            } else if (lowerB < lowerA) {
                return 1 * dirCorrector;
            }
            return 0;
        };
    }

    function sortByModified(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;

        return function (a, b) {
            var result;

            result = (a.modified - b.modified) * dirCorrector;
            return result || sortByName("ascending")(a, b);
        };
    }

    function sortBySize(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;

        return function (a, b) {
            var result;

            result = (a.size - b.size) * dirCorrector;
            return result || sortByName("ascending")(a, b);
        };
    }

    function deleteSelectedItems() {
        var names = vm.items.filter(function (item) {
            return item.selected;
        }).map(function (item) {
            return item.name;
        });
        FileOperationsService.deleteFromWebdav(names);
        closeModal();
    }

    function startTransfer() {
        var names = vm.items.filter(function (item) {
            return item.selected;
        }).map(function (item) {
            return item.name;
        });
        FileOperationsService.transferFromWebdav(directoryId, names);
        closeModal();
    }

    function closeModal() {
        $scope.$$closeModal();
    }
}

angular
    .module("app.documents")
    .controller("BrowserController", BrowserController);

/* @ngInject */
function BrowserController($scope, $rootScope, $state, $stateParams, $filter, dirContent, UserDocument, DocumentsDataService, FileFormatService, CustomizationService, MenuService, ModalService, ViewMode, FileType, SelectionMode, TerminologyDocumentService) {

    var vm = this;

    var documents = dirContent.documents,
        directories = dirContent.directories,
        sortingFunctions = [
            sortByName,
            sortBySize,
            sortByType,
            sortByModified
        ];

    vm.base = dirContent.base;
    vm.FileType = FileType;

    vm.items = _.union(documents.map(function (userDocument) {
        return makeDocItem(userDocument);
    }), directories.map(function (userDirectory) {
        return makeDirItem(userDirectory);
    }));

    vm.ViewMode = ViewMode;
    vm.viewMode = CustomizationService.get("file_browser_view_mode", ViewMode.TABLE);
    vm.changeViewMode = changeViewMode;

    vm.showSingleMenu = showSingleMenu;
    vm.showMultiMenu = showMultiMenu;

    vm.SelectionMode = SelectionMode;
    vm.selectionMode = SelectionMode.NONE;
    vm.checkAllState = 0;
    vm.numberOfSelectedItems = 0;
    vm.toggle = toggle;
    vm.toggleAll = toggleAll;
    vm.select = select;

    vm.sortOptions = [
        { index: 0, dir: "ascending", label: "Name: ascending" },
        { index: 0, dir: "descending", label: "Name: descending" },
        { index: 1, dir: "ascending", label: "Size: ascending" },
        { index: 1, dir: "descending", label: "Size: descending" },
        { index: 2, dir: "ascending", label: "Type: ascending" },
        { index: 2, dir: "descending", label: "Type: descending" },
        { index: 3, dir: "ascending", label: "Modified: ascending" },
        { index: 3, dir: "descending", label: "Modified: descending" }
    ];
    vm.sortOption = {
        selected: vm.sortOptions[0]
    };
    vm.sortIndex = 0;
    vm.sortDirection = "ascending";
    vm.sort = sort;

    vm.deleteSelected = deleteSelected;

    vm.startTerminologyList = startTerminologyList;
    vm.startNamedEntities = startNamedEntities;

    activate();

    //////////

    function activate() {
        $scope.$watch("vm.sortOption.selected.label", sortWatcher);
        $scope.$onRootScope("directories.removeDirectories", removeDirectoriesHandler);
        $scope.$onRootScope("directories.removeDocuments", removeDocumentsHandler);
        $scope.$onRootScope("resources.compStateChange", compStateChangeHandler);
        $scope.$onRootScope("directories.selectDirectory", selectDirectoryHandler);
        $scope.$onRootScope("directories.createDirectory", createDirectoryHandler);
        $scope.$onRootScope("directories.addDocument", addDocumentHandler);
        $scope.$onRootScope("directories.renameDirectory", renameDirectoryHandler);
        $scope.$onRootScope("directories.renameDocument", renameDocumentHandler);
        $rootScope.$emit("directories.selectDirectory", dirContent.base.id);
        redraw();
        if ($stateParams.dir) {
            ModalService.startChain([
                {name: "newToWebdav", params: {}, condition: $rootScope.newToWebdav},
                {name: "enableLogging", params: {}, condition: $rootScope.loggingDecisionNeeded}
            ]);
            $rootScope.newToWebdav = false;
            $rootScope.loggingDecisionNeeded = false;
        }
    }

    function makePrintDate(date) {
        var now = new Date(),
            month = now.getMonth(),
            day = now.getDate(),
            year = now.getFullYear();

        if (date.getFullYear() === now.getFullYear() &&
            date.getMonth() === now.getMonth() &&
            date.getDate() === now.getDate()) {

            return $filter("date")(date, "HH:mm");
        } else {
            return $filter("date")(date, "dd. MMM. yyyy");
        }
    }

    function makeDirItem(userDirectory) {
        var size = userDirectory.docCount + userDirectory.dirCount;
        return {
            name: userDirectory.name,
            size: size,
            printSize: size + " Items",
            type: FileType.DIR,
            printType: "Folder",
            modified: userDirectory.modified,
            printModified: makePrintDate(userDirectory.modified),
            link: $state.href("documents.browser", { dir: userDirectory.id }),
            target: "_self",
            imgPrefix: userDirectory.isPrivate ? "folder" : "folderS",
            selected: 0,
            orig: userDirectory
        };
    }

    function getStateImg(state, running) {
        if (running && state !== "deprecated") {
            return "state_active.gif";
        }
        switch(state) {
            case "ok":
                return "state_success.png";
            case "error":
                return "state_fail.png";
            case "init":
                return "state_inactive.png";
            case "deprecated":
                return "state_success.png";
            default:
                return "state_inactive.png";
        }
    }

    function makeTlistItem(userDocument) {
        return {
            name: userDocument.name,
            size: 0,
            printSize: userDocument.terminologyData.terms.length + " Terms",
            type: userDocument.fileType,
            printType: "Terminology List",
            modified: userDocument.modified,
            printModified: makePrintDate(userDocument.modified),
            link: DocumentsDataService.createLink(userDocument, vm.base),
            target: "_self",
            imgPrefix: userDocument.getImgPrefix(),
            selected: 0,
            orig: userDocument
        };
    }

    function makeUserDocItem(userDocument) {
        var resource;

        resource = userDocument.resource;

        return {
            name: userDocument.name,
            size: userDocument.resource.size,
            printSize: FileFormatService.formatFileSize(userDocument.resource.size),
            type: userDocument.fileType,
            printType: "File (" + userDocument.resource.getReadableMimetype() + ")",
            modified: userDocument.modified,
            printModified: makePrintDate(userDocument.modified),
            link: DocumentsDataService.createLink(userDocument, vm.base),
            target: "_blank",
            imgPrefix: userDocument.getImgPrefix(),
            selected: 0,
            resource: userDocument.resource,
            conversionImg: getStateImg(userDocument.resource.taskStates.text.state, userDocument.resource.taskStates.text.running),
            terminologyImg: getStateImg(userDocument.resource.taskStates.terminology.state, userDocument.resource.taskStates.terminology.running),
            neImg: getStateImg(userDocument.resource.taskStates.ne.state, userDocument.resource.taskStates.ne.running),
            translateImg: getStateImg(userDocument.resource.taskStates.translation.state, userDocument.resource.taskStates.translation.running),
            isBusy: false,
            orig: userDocument,
            language: userDocument.resource.language
        };
    }

    function makeDocItem(userDocument) {
        if (userDocument.fileType === FileType.DOC || userDocument.fileType === FileType.REPORT) {
            return makeUserDocItem(userDocument);
        } else {
            return makeTlistItem(userDocument);
        }

    }

    /**
     * VIEWMODE
     */

    function changeViewMode(viewMode) {
        vm.viewMode = viewMode;
        CustomizationService.set({"file_browser_view_mode": viewMode});
    }

    /**
     * MENU
     */

    function showSingleMenu($event, item) {

        if (item.type === FileType.DIR) {
            MenuService.show("singleFolderMenu", {dir: item.orig, base: vm.base}, $event.clientX, $event.clientY);
        } else if (item.type === FileType.DOC || item.type === FileType.REPORT) {
            MenuService.show("singleFileMenu", {doc: item.orig, base: vm.base}, $event.clientX, $event.clientY);
        } else if (item.type === FileType.TLIST) {
            MenuService.show("singleTermFileMenu", {doc: item.orig, base: vm.base}, $event.clientX, $event.clientY);
        }
        $event.preventDefault();
        $event.stopPropagation();
        return false;
    }

    function showMultiMenu($event, item) {
        var selectedItems = getSelectedItems();

        if (selectedItems.docs.length > 0) {
            if (selectedItems.dirs.length > 0) {
                MenuService.show("mixedMenu", _.extend({base: vm.base}, selectedItems), $event.clientX, $event.clientY);
            } else if (selectedItems.docs.length === 1) {
                if (selectedItems.docs[0].fileType === FileType.TLIST) {
                    MenuService.show("singleTermFileMenu", {doc: selectedItems.docs[0], base: vm.base}, $event.clientX, $event.clientY);
                } else {
                    MenuService.show("singleFileMenu", {doc: selectedItems.docs[0], base: vm.base}, $event.clientX, $event.clientY);
                }
             } else {
                if (_.every(selectedItems.docs, function (doc) {
                    return doc.isComputableFile();
                })) {
                    MenuService.show("multiFileMenu", {docs: selectedItems.docs, base: vm.base}, $event.clientX, $event.clientY);
                } else {
                    MenuService.show("multiGenericMenu", {docs: selectedItems.docs, base: vm.base}, $event.clientX, $event.clientY);
                }

            }

        } else if (selectedItems.dirs.length === 1) {
            MenuService.show("singleFolderMenu", {dir: selectedItems.dirs[0], base: vm.base}, $event.clientX, $event.clientY);
        } else if (selectedItems.dirs.length > 1) {
            MenuService.show("multiFolderMenu", {dirs: selectedItems.dirs, base: vm.base}, $event.clientX, $event.clientY);
        }
        $event.preventDefault();
        $event.stopPropagation();
        return false;
    }

    /**
     * SELECT
     */

    function updateAllCheck() {
        var countObj;

        countObj = _.countBy(vm.items, function(item) {
            return item.selected ? "selected": "unselected";
        });

        if (!countObj.selected) {
            vm.checkAllState = 0;
            vm.numberOfSelectedItems = 0;
        } else if (!countObj.unselected) {
            vm.checkAllState = 1;
            vm.numberOfSelectedItems = countObj.selected;
        } else {
            vm.checkAllState = 2;
            vm.numberOfSelectedItems = countObj.selected;
        }
    }

    function updateSelectionMode() {
        var selectedItems = getSelectedItems();

        if (selectedItems.docs.length > 0) {
            if (selectedItems.dirs.length > 0) {
                vm.selectionMode = SelectionMode.MIXED;
            } else if (selectedItems.docs.length === 1) {
                vm.selectionMode = SelectionMode.DOC;
            } else {
                vm.selectionMode = SelectionMode.DOCS;
            }

        } else if (selectedItems.dirs.length === 1) {
            vm.selectionMode = SelectionMode.DIR;
        } else if (selectedItems.dirs.length > 1) {
            vm.selectionMode = SelectionMode.DIRS;
        } else {
            vm.selectionMode = SelectionMode.NONE;
        }
    }

    function toggle($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.selected = !item.selected;
        updateAllCheck();
        updateSelectionMode();
        $("body").click();
        return false;
    }

    function select(item) {
        _.each(vm.items, function (checkItem) {
            checkItem.selected = checkItem === item;
        });
        updateAllCheck();
        updateSelectionMode();
    }

    function toggleAll() {
        if (vm.checkAllState === 0 || vm.checkAllState === 2) {
            vm.checkAllState = 1;
            _.each(vm.items, function (item) {
                item.selected = true;
            });
            vm.numberOfSelectedItems = vm.items.length;
        } else {
            vm.checkAllState = 0;
            _.each(vm.items, function (item) {
                item.selected = false;
            });
            vm.numberOfSelectedItems = 0;
        }
        updateSelectionMode();
    }

    /**
     * SORTING
     */

     function sortWatcher() {
         vm.sortIndex = vm.sortOption.selected.index;
         vm.sortDirection = vm.sortOption.selected.dir;
         redraw();
     }

    function sort(index) {
        if (vm.sortIndex === index) {
            vm.sortDirection = vm.sortDirection === "ascending" ? "descending" : "ascending";
        } else {
            vm.sortIndex = index;
            vm.sortDirection = "ascending";
        }
        vm.sortOption.selected = _.find(vm.sortOptions, function (sortOption) {
            return sortOption.index === vm.sortIndex && sortOption.dir === vm.sortDirection;
        });
        redraw();
    }

    function redraw() {
        vm.items.sort(sortingFunctions[vm.sortIndex](vm.sortDirection));
    }

    function sortFoldersFirst(a, b) {
        if (a.orig.isFile() && b.orig.isDir()) {
            return 1;
        } else if (b.orig.isFile() && a.orig.isDir()) {
            return -1;
        } else {
            return 0;
        }
    }

    function sortByName(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;

        return function (a, b) {
            var lowerA, lowerB, typeResult;

            typeResult = sortFoldersFirst(a, b);

            if (typeResult !== 0) {
                return typeResult;
            }

            lowerA = a.name.toLowerCase();
            lowerB = b.name.toLowerCase();

            if (lowerA < lowerB) {
                return -1 * dirCorrector;
            } else if (lowerB < lowerA) {
                return 1 * dirCorrector;
            }
            return 0;
        };

    }

    function sortByType(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;

        return function (a, b) {
            var lowerA, lowerB, typeResult;

            typeResult = sortFoldersFirst(a, b);

            if (typeResult !== 0) {
                return typeResult;
            }

            lowerA = a.printType.toLowerCase();
            lowerB = b.printType.toLowerCase();

            if (lowerA < lowerB) {
                return -1 * dirCorrector;
            } else if (lowerB < lowerA) {
                return 1 * dirCorrector;
            }
            return sortByName("ascending")(a, b);
        };
    }

    function sortByModified(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;

        return function (a, b) {
            var typeResult, result;

            typeResult = sortFoldersFirst(a, b);

            if (typeResult !== 0) {
                return typeResult;
            }

            result = (a.modified - b.modified) * dirCorrector;
            return result || sortByName("ascending")(a, b);
        };
    }

    function sortBySize(direction) {
        var dirCorrector = direction === "ascending" ? 1 : -1;

        return function (a, b) {
            var typeResult, result;

            typeResult = sortFoldersFirst(a, b);

            if (typeResult !== 0) {
                return typeResult;
            }

            result = (a.size - b.size) * dirCorrector;
            return result || sortByName("ascending")(a, b);
        };
    }

    /**
     * DELETE
     */

     function removeDirectoriesHandler(ev, directoryIds) {
         directories = directories.filter(function (userDirectory) {
             return !_.contains(directoryIds, userDirectory.id);
         });
         vm.items = vm.items.filter(function (item) {
             return !item.orig.isDir() || !_.contains(directoryIds, item.orig.id);
         });
         updateAllCheck();
     }

     function removeDocumentsHandler(ev, documentIds) {
         documents = documents.filter(function (userDocument) {
             return !_.contains(documentIds, userDocument.id);
         });
         vm.items = vm.items.filter(function (item) {
             return item.orig.isDir() || !_.contains(documentIds, item.orig.id);
         });
         updateAllCheck();
     }

    function getSelectedItems() {
        var result = _.groupBy(vm.items.filter(function (item) {
            return item.selected === true;
        }).map(function (item) {
            return item.orig;
        }), function (item) {
            return item instanceof UserDocument ? "docs" : "dirs";
        });
        if (!result.docs) {
            result.docs = [];
        }
        if (!result.dirs) {
            result.dirs = [];
        }
        return result;
    }

    function deleteSelected() {
        var selectedItems = getSelectedItems();

        ModalService.open("normal@remove", {
            directories: selectedItems.dirs,
            documents: selectedItems.docs
        });
    }

    /**
     * TERMINOLOGY LIST
     */

    function startTerminologyList() {
        var selectedItems = getSelectedItems(),
            dirIds, docIds;

        dirIds = selectedItems.dirs.map(function (userDirectory) {
            return userDirectory.id;
        });

        docIds = selectedItems.docs.map(function (userDocument) {
            return userDocument.id;
        });
        TerminologyDocumentService.initTerminology(docIds, dirIds).then(function (doc) {
            $state.go("terminology.list", { draft: doc.id, base: vm.base.id });
        });
    }

    /**
     * NAMED ENTITIES
     */

    function startNamedEntities() {
        var selectedItems = getSelectedItems(),
            docIds;

        docIds = selectedItems.docs.map(function (userDocument) {
            return userDocument.id;
        });

        if (selectedItems.dirs.length === 0 && docIds.length === 1) {
            console.log("GO");
            $state.go("ne.list", { doc_id: docIds[0], base_id: vm.base.id });
        }
    }


    /**
     * COMP STATE
     */

    function compStateChangeHandler(ev, resourceComp) {
        var items;
        items = vm.items.filter(function (item) {
            return item.type === FileType.DOC && item.resource.id === resourceComp.id;
        });
        _.each(items, function (item) {
            var img = getStateImg(resourceComp.taskData.state, resourceComp.taskData.running);
            if (resourceComp.taskName === "text") {
                item.conversionImg = img;
                item.language = resourceComp.language;
            } else if (resourceComp.taskName === "terminology") {
                item.terminologyImg = img;
            } else if (resourceComp.taskName === "ne") {
                item.neImg = img;
            } else if (resourceComp.taskName === "translation") {
                item.translateImg = img;
            }
            item.isBusy = false;
        });
    }

    /**
     * EVENTS
     */

    function selectDirectoryHandler(ev, directoryId) {
        if (vm.base.id !== directoryId) {
        }
    }

    function createDirectoryHandler(ev, args) {
        if (vm.base.id === args.baseId) {
            directories.push(args.directory);
            vm.items.push(makeDirItem(args.directory));
            redraw();
        }
    }

    function addDocumentHandler(ev, args) {
        if (vm.base.id === args.baseId) {
            documents.push(args.userDocument);
            vm.items.push(makeDocItem(args.userDocument));
            redraw();
        }
    }

    function renameDirectoryHandler(ev, args) {
        var dir = _.find(directories, function (userDirectory) {
            return userDirectory.id === args.dirId;
        });
        if (dir) {
            dir.name = args.newName;
            dir.modified = new Date(args.modified);
            vm.items = vm.items.filter(function (item) {
                return item.orig !== dir;
            });
            vm.items.push(makeDirItem(dir));
            redraw();
        }
    }

    function renameDocumentHandler(ev, args) {
        var doc = _.find(documents, function (userDocument) {
            return userDocument.id === args.docId;
        });
        if (doc) {
            doc.name = args.newName;
            doc.modified = new Date(args.modified);
            vm.items = vm.items.filter(function (item) {
                return item.orig !== doc;
            });
            vm.items.push(makeDocItem(doc));
            redraw();
        }
    }
}

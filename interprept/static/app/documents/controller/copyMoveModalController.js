angular
    .module('app.documents')
    .controller("CopyMoveModalController", CopyMoveModalController);

/* @ngInject */
function CopyMoveModalController($scope, FileOperationsService, SelectionMode, directories, documents, remove) {

    var vm = this;

    var base = null;

    vm.baseId = null;
    vm.directories = directories;
    vm.documents = documents;
    vm.SelectionMode = SelectionMode;
    vm.selectionMode = 0;
    vm.remove = remove;

    vm.copy = copy;
    vm.closeModal = closeModal;
    vm.onDirSelect = onDirSelect;

    activate();

    //////////

    function activate() {
        var dirLength = directories.length,
            docLength = documents.length;
        if (dirLength > 0) {
            if (docLength > 0) {
                vm.selectionMode = SelectionMode.MIXED;
            } else if (dirLength > 1) {
                vm.selectionMode = SelectionMode.DIRS;
            } else {
                vm.selectionMode = SelectionMode.DIR;
            }
        } else if (docLength > 0) {
            if (docLength > 1) {
                vm.selectionMode = SelectionMode.DOCS;
            } else {
                vm.selectionMode = SelectionMode.DOC;
            }
        }
    }

    function copy() {
        var docIds, dirIds;
        docIds = documents.map(function (userDocument) {
            return userDocument.id;
        });
        dirIds = directories.map(function (userDirectory) {
            return userDirectory.id;
        });
        if (remove) {
            FileOperationsService.move(base.id, docIds, dirIds);
        } else {
            FileOperationsService.copy(base.id, docIds, dirIds);
        }
        closeModal();
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function onDirSelect(userDirectory) {
        base = userDirectory;
    }
}

angular
    .module("app.documents")
    .controller("CreateDirModalController", CreateDirModalController);

/* @ngInject */
function CreateDirModalController($scope, FileOperationsService, directoryId) {

    var vm = this;

    vm.dirName = "";
    vm.create = create;
    vm.closeModal = closeModal;

    //////////

    function create() {
        if (directoryId === null) {
            return;
        }
        FileOperationsService.createDir(directoryId, vm.dirName);
        closeModal();
    }

    function closeModal() {
        $scope.$$closeModal();
    }
}

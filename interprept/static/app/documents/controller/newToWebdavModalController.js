angular
    .module("app.documents")
    .controller("NewToWebdavModalController", NewToWebdavModalController);

/* @ngInject */
function NewToWebdavModalController($scope) {

    var vm = this;

    vm.webdavLink = window.location.protocol + "//" + window.location.host + "/webdav";
    vm.close = close;

    //////////

    function close() {
        $scope.$$closeModal();
    }
}

angular
    .module("app.documents")
    .controller("RemoveModalController", RemoveModalController);

/* @ngInject */
function RemoveModalController($scope, FileOperationsService, SelectionMode, directories, documents) {

    var vm = this;

    vm.directories = directories;
    vm.documents = documents;
    vm.SelectionMode = SelectionMode;
    vm.selectionMode = 0;

    vm.remove = remove;
    vm.closeModal = closeModal;

    activate();

    //////////

    function activate() {
        var dirLength = directories.length,
            docLength = documents.length;
        if (dirLength > 0) {
            if (docLength > 0) {
                vm.selectionMode = SelectionMode.MIXED;
            } else if (dirLength > 1) {
                vm.selectionMode = SelectionMode.DIRS;
            } else {
                vm.selectionMode = SelectionMode.DIR;
            }
        } else if (docLength > 0) {
            if (docLength > 1) {
                vm.selectionMode = SelectionMode.DOCS;
            } else {
                vm.selectionMode = SelectionMode.DOC;
            }
        }
    }

    function remove() {
        var docIds, dirIds;
        docIds = documents.map(function (userDocument) {
            return userDocument.id;
        });
        dirIds = directories.map(function (userDirectory) {
            return userDirectory.id;
        });
        FileOperationsService.removeDirs(dirIds);
        FileOperationsService.removeDocs(docIds);
        closeModal();
    }

    function closeModal() {
        $scope.$$closeModal();
    }
}

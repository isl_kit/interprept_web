angular
    .module("app.documents")
    .controller("UploadModalController", UploadModalController);

/* @ngInject */
function UploadModalController($scope, $rootScope, $cookies, UserDocument, directoryId) {

    var vm = this;

    var counter = 0;

    vm.uploadFiles = {};
    vm.progressEnabled = !!window.FormData;

    vm.remove = remove;
    vm.add = add;
    vm.done = done;
    vm.progress = progress;
    vm.startUpload = startUpload;
    vm.closeModal = closeModal;

    //////////

    function remove(key) {
        delete vm.uploadFiles[key];
    }

    function add(e, data) {
        var uploadFileObj, key;

        uploadFileObj = {
            data: data,
            isUploading: false,
            name: data.files[0].name,
            progress: 0,
            style: {
                width: "0%"
            }
        };
        key = "c" + (counter++);
        data.formData = {
            dir_id: directoryId,
            "X-CSRFToken": $cookies.csrftoken
        };
        vm.uploadFiles[key] = uploadFileObj;
        data.fileObjKey = key;
    }

    function done(e, data) {
        $rootScope.$emit("directories.addDocument", {
            baseId: directoryId,
            userDocument: UserDocument.map(data.result)
        });
        delete vm.uploadFiles[data.fileObjKey];
    }

    function progress(e, data) {
        var uploadFileObj = vm.uploadFiles[data.fileObjKey];
        if (vm.progressEnabled) {
            uploadFileObj.progress = parseInt(100.0 * data.loaded / data.total, 10);
            uploadFileObj.style.width = uploadFileObj.progress + "%";
        }
    }

    function startUpload() {
        if (directoryId === null) {
            return;
        }
        _.each(vm.uploadFiles, function (uploadFileObj) {
            uploadFileObj.isUploading = true;
            uploadFileObj.data.submit();
        });
    }

    function closeModal() {
        $scope.$$closeModal();
    }
}

angular
    .module("app.documents")
    .controller("DocumentsSidebarController", DocumentsSidebarController);

/* @ngInject */
function DocumentsSidebarController($scope, $rootScope, $state, ModalService, FileOperationsService) {

    var vm = this;

    vm.directoryId = null;

    vm.startReportSelect = startReportSelect;
    vm.startCommitteeSelect = startCommitteeSelect;
    vm.showCreateDirModal = showCreateDirModal;
    vm.showUploadModal = showUploadModal;
    vm.showWebDavModal = showWebDavModal;
    vm.onDirSelect = onDirSelect;

    activate();

    //////////

    function activate() {
        $scope.$onRootScope("directories.selectDirectory", selectDirectoryHandler);
    }

    function startReportSelect() {
        ModalService.open("days@reportSelect", { year: (new Date()).getFullYear() }).then(function (parlDocIds) {
            if (parlDocIds.length > 0) {
                FileOperationsService.addParlDocs(vm.directoryId, parlDocIds);
            }
        });
    }

    function startCommitteeSelect() {
        ModalService.open("organs@committeeSelect", { }).then(function (parlDocIds) {
            if (parlDocIds.length > 0) {
                FileOperationsService.addParlDocs(vm.directoryId, parlDocIds);
            }
        });
    }

    function showCreateDirModal() {
        ModalService.open("normal@createDir", {
            directoryId: vm.directoryId
        });
    }

    function showUploadModal() {
        ModalService.open("normal@upload", { directoryId: vm.directoryId });
    }

    function showWebDavModal() {
        ModalService.open("normal@webdav", { directoryId: vm.directoryId });
    }

    function onDirSelect(directory) {
        $rootScope.$emit("directories.selectDirectory", directory.id);
        $state.go("documents.browser", { dir: directory.id });
    }

    function selectDirectoryHandler(ev, directoryId) {
        if (vm.directoryId !== directoryId) {
            vm.directoryId = directoryId;
        }
    }
}

angular
    .module("app.documents")
    .controller("EnableLoggingModalController", EnableLoggingModalController);

/* @ngInject */
function EnableLoggingModalController($scope, $http) {

    var vm = this;

    vm.close = close;

    //////////

    function close(enabled) {
        $http.post("/logging/changeLoggingSetting/", { enabled: enabled });
        $scope.$$closeModal();
    }
}

angular
    .module("app.documents")
    .controller("RenameModalController", RenameModalController);

/* @ngInject */
function RenameModalController($scope, FileOperationsService, FileType, fileSpaceItem) {

    var vm = this;

    vm.FileType = FileType;
    vm.item = fileSpaceItem;
    vm.itemName = fileSpaceItem.name;

    vm.rename = rename;
    vm.closeModal = closeModal;

    //////////

    function rename() {
        if (fileSpaceItem.isFile()) {
            FileOperationsService.renameDoc(fileSpaceItem.id, vm.itemName);
        } else if (fileSpaceItem.fileType === FileType.DIR) {
            FileOperationsService.renameDir(fileSpaceItem.id, vm.itemName);
        }
        closeModal();
    }

    function closeModal() {
        $scope.$$closeModal();
    }
}

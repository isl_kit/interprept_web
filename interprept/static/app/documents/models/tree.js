angular
    .module("app.documents")
    .factory("Tree", TreeModel);

/* @ngInject */
function TreeModel($q, DocumentsDataService, TreeItem) {

    function Tree(element, onSelect, scope) {
        this.element = element;
        this.selectedItem = null;
        this.onSelect = onSelect;
        this.reloadPromise = null;
        this.scope = scope;
        this.children = [];
    }
    Tree.prototype.add = add;
    Tree.prototype.addDirectory = addDirectory;
    Tree.prototype.renameDirectory = renameDirectory;
    Tree.prototype.removeDirectory = removeDirectory;
    Tree.prototype.selectById = selectById;
    Tree.prototype.selectByItem = selectByItem;
    Tree.prototype.getItemById = getItemById;
    Tree.prototype.getItemByIdRecurse = getItemByIdRecurse;
    Tree.prototype.reload = reload;
    Tree.prototype.sortChildren = sortChildren;

    return Tree;

    //////////

    function add(treeItem) {
        var index;
        treeItem.connect(0, this);
        treeItem.parent = this;
        this.children.push(treeItem);
        this.sortChildren();
        index = _.indexOf(this.children, treeItem);
        if (index === 0) {
            this.element.prepend(treeItem.element);
        } else if (index < this.children.length - 1) {
            this.children[index + 1].element.before(treeItem.element);
        } else {
            this.element.append(treeItem.element);
        }
    }

    function addDirectory(baseId, directory) {
        var item, baseItem = this.getItemByIdRecurse(baseId);
        if (baseItem) {
            if (!baseItem.hasChildren || baseItem.children.length > 0) {
                item = new TreeItem(directory, false, false, directory.dirCount > 0);
                baseItem.hasChildren = true;
                baseItem.add(item);
            }
        }
    }

    function renameDirectory(dirId, name, modified) {
        var item = this.getItemByIdRecurse(dirId);
        if (item) {
            item.rename(name, modified);
        }
    }

    function removeDirectory(dirId) {
        var item = this.getItemByIdRecurse(dirId);
        if (item) {
            item.remove();
        }
    }

    function selectById(id, noEvent) {
        var that = this;
        that.getItemById(id).then(function (item) {
            that.selectByItem(item, noEvent);
        });

    }

    function selectByItem(treeItem, noEvent) {
        if (this.selectedItem) {
            this.selectedItem.deselect();
        }
        treeItem.element.addClass("selected");
        treeItem.selected = true;
        treeItem.open();
        this.selectedItem = treeItem;
        if (!noEvent) {
            this.onSelect(treeItem.directory);
        }
    }

    function getItemById(id) {
        var that = this, item, deferred = $q.defer();
        if (that.reloadPromise) {
            that.reloadPromise.then(function () {
                var item = that.getItemByIdRecurse(id);

                if (!item) {
                    that.reload(id).then(function (item) {
                        deferred.resolve(item);
                    });
                } else {
                    deferred.resolve(item);
                }
            });
        } else {
            item = that.getItemByIdRecurse(id);

            if (!item) {
                that.reload(id).then(function (item) {
                    deferred.resolve(item);
                });
            } else {
                deferred.resolve(item);
            }
        }
        return deferred.promise;
    }

    function getItemByIdRecurse(id, items) {
        var result = null, i, max_i, item;
        items = items || this.children;
        for (i = 0, max_i = items.length; i < max_i; i++) {
            item = items[i];
            if (item.id === id) {
                result = item;
                break;
            }
            result = this.getItemByIdRecurse(id, item.children);
            if (result !== null) {
                break;
            }
        }
        return result;
    }

    function reload(directoryId) {
        var that = this;
        that.reloadPromise = DocumentsDataService.getDirTree(directoryId).then(function (result) {
            var dirs = [result.home, result.shared],
                requestedItem;
            _.each(dirs, function (dir) {
                var tempDir,
                    tempItem,
                    newItem,
                    item = that;

                while (dir) {
                    tempItem = that.getItemByIdRecurse(dir.id);
                    if (!tempItem) {
                        break;
                    }
                    item = tempItem;
                    tempDir = getDirWithChildren(dir.children);
                    if (!tempDir) {
                        createItems(item, dir.children);
                    }
                    dir = tempDir;
                }
                if (dir) {
                    newItem = new TreeItem(dir, false, false, dir.dirCount > 0);
                    item.add(newItem);
                    item = newItem;
                }
                while (dir) {
                    createItems(item, dir.children);
                    item = getItemWithChildren(item.children);
                    dir = getDirWithChildren(dir.children);
                }
            });
            requestedItem = directoryId ? that.getItemByIdRecurse(directoryId) : that.getItemByIdRecurse(result.home.id);
            that.reloadPromise = null;
            return requestedItem;
        });
        return that.reloadPromise;
    }

    function sortChildren() {
        this.children.sort(sortTreeItems);
    }

    function sortTreeItems(a, b) {
        var lowerA = a.name.toLowerCase(),
            lowerB = b.name.toLowerCase();

        if (lowerA < lowerB) {
            return -1;
        } else if (lowerB < lowerA) {
            return 1;
        }
        return 0;
    }

    function getDirWithChildren(dirs) {
        return _.find(dirs, function (dir) {
            return dir.children.length > 0;
        });
    }

    function getItemWithChildren(items) {
        return _.find(items, function (item) {
            return item.hasChildren;
        });
    }

    function createItems(parent, dirs) {
        _.each(dirs, function (dir) {
            var newItem = new TreeItem(dir, false, false, dir.dirCount > 0);
            parent.add(newItem);
        });
    }
}

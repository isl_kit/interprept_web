angular
    .module("app.documents")
    .factory("UserDocument", UserDocumentModel);

/* @ngInject */
function UserDocumentModel(User, Resource, FileType) {

    function UserDocument(data) {
        this.id = data.id;
        this.name = data.name;
        this.created = new Date(data.created);
        this.modified = new Date(data.modified);
        this.isPrivate = data.isPrivate;
        this.user = data.user;
        this.resource = data.resource;
        this.fileType = data.fileType;
        this.parlDocumentId = data.parlDocumentId;
        this.terminologyData = data.terminologyData;
        this.draft = data.draft;
    }
    UserDocument.map = map;
    UserDocument.prototype.getImgPrefix = getImgPrefix;
    UserDocument.prototype.isComputableFile = isComputableFile;
    UserDocument.prototype.isFile = isFile;
    UserDocument.prototype.isDir = isDir;

    return UserDocument;

    //////////

    function map(documentDb, speech) {
        var userDocument = new UserDocument({
            id: documentDb.id,
            fileType: documentDb.type,
            name: documentDb.name,
            created: documentDb.created,
            modified: documentDb.modified,
            isPrivate: documentDb.is_private,
            user: documentDb.user ? User.map(documentDb.user) : null,
            resource: documentDb.resource ? Resource.map(documentDb.resource) : null,
            parlDocumentId: documentDb.parldocument,
            terminologyData: documentDb.terminology_data,
            draft: documentDb.draft
        });
        return userDocument;
    }

    function getImgPrefix() {
        if (this.fileType === FileType.DOC) {
            return "file";
        } else if (this.fileType === FileType.REPORT) {
            return "report";
        } else if (this.fileType === FileType.TLIST) {
            return "tlist";
        }
    }

    function isComputableFile() {
        return _.contains([FileType.DOC, FileType.REPORT], this.fileType);
    }

    function isFile() {
        return _.contains([FileType.DOC, FileType.REPORT, FileType.TLIST], this.fileType);
    }

    function isDir() {
        return this.fileType === FileType.DIR;
    }
}

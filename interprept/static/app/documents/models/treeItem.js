angular
    .module("app.documents")
    .factory("TreeItem", TreeItemModel);

/* @ngInject */
function TreeItemModel() {

    function TreeItem(directory, isOpen, selected, hasChildren) {
        this.id = directory.id;
        this.tree = null;
        this.level = null;
        this.directory = directory;
        this.name = directory.name;
        this.isOpen = isOpen;
        this.selected = selected;
        this.hasChildren = hasChildren;
        this.children = [];
        this.parent = null;
        this.element = null;
        this.container = null;
        this.caretIcon = null;
        this.nameElement = null;
    }
    TreeItem.prototype.createElementOnLevel = createElementOnLevel;
    TreeItem.prototype.connect = connect;
    TreeItem.prototype.rename = rename;
    TreeItem.prototype.remove = remove;
    TreeItem.prototype.add = add;
    TreeItem.prototype.select = select;
    TreeItem.prototype.deselect = deselect;
    TreeItem.prototype.open = open;
    TreeItem.prototype.close = close;
    TreeItem.prototype.sortChildren = sortChildren;

    return TreeItem;

    //////////

    function createElementOnLevel(level) {
        var itemHtml, that = this,
            padding = (level * 10) + "px";

        that.level = level;
        itemHtml = "<div class=\"tree-item";
        if (that.isOpen === true) {
            itemHtml += " open";
        }
        if (that.selected === true) {
            itemHtml += " selected";
        }
        if (that.hasChildren === true) {
            itemHtml += " populated";
        }
        itemHtml += "\"><div title=\"" + that.name + "\" class=\"tree-item-name\" style=\"padding-left:" + padding + "\">";
        if (that.hasChildren === true) {
            itemHtml += "<i class=\"caret fa fa-caret-";
            if (that.isOpen === true) {
                itemHtml += "down";
            } else {
                itemHtml += "right";
            }
            itemHtml += "\"></i>";
        } else {
            itemHtml += "<i class=\"caret\"></i>";
        }
        itemHtml += "<img src=\"/static/assets/images/folder";
        if (that.directory.isPrivate === false) {
            itemHtml += "S";
        }
        itemHtml += "16.png\"></img><span>" + that.name + "</span></div>";
        itemHtml += "<div class=\"tree-item-children\"></div></div>";
        that.element = $(itemHtml);
        that.element.click(function (ev) {
            if (!that.selected) {
                that.tree.scope.$apply(function () {
                    that.select();
                });
            } else {
                if (that.isOpen) {
                    that.close();
                } else {
                    that.open();
                }
            }
            ev.stopPropagation();
        });
        that.container = that.element.find(".tree-item-children");
        that.caretIcon = that.element.find(".caret");
        that.nameElement = that.element.find(".tree-item-name");
    }

    function connect(level, tree) {
        var that = this;
        that.tree = tree;
        that.createElementOnLevel(level);
        _.each(that.children, function (subItem) {
            subItem.connect(level + 1, tree);
            that.container.append(subItem.element);
        });
    }

    function rename(name, modified) {
        var that = this, oldIndex, index;

        that.name = name;
        that.directory.name = name;
        that.directory.modified = new Date(modified);
        that.nameElement.attr("title", name);
        that.nameElement.find("span").text(name);
        that.parent.sortChildren();
        index =  _.indexOf(that.parent.children, that);
        if (index === 0) {
            that.parent.container.prepend(that.element);
        } else if (index < that.parent.children.length - 1) {
            that.parent.children[index + 1].element.before(that.element);
        } else {
            that.parent.container.append(that.element);
        }
    }

    function remove() {
        if (this.parent) {
            this.parent.children = _.without(this.parent.children, this);
            if (this.parent.children.length === 0) {
                this.parent.caretIcon.removeClass("fa fa-caret-right fa-caret-down");
                this.parent.hasChildren = false;
            }
            this.element.remove();
        }
    }

    function add(treeItem) {
        var index;

        this.children.push(treeItem);
        this.sortChildren();
        index = _.indexOf(this.children, treeItem);
        treeItem.parent = this;
        if (!this.caretIcon.hasClass("fa")) {
            this.caretIcon.addClass("fa fa-caret-right");
        }
        if (this.level !== null) {
            treeItem.tree = this.tree;
            treeItem.createElementOnLevel(this.level + 1);
            if (index === 0) {
                this.container.prepend(treeItem.element);
            } else if (index < this.children.length - 1) {
                this.children[index + 1].element.before(treeItem.element);
            } else {
                this.container.append(treeItem.element);
            }
        }
    }

    function select() {
        this.tree.selectByItem(this);
    }

    function deselect() {
        this.selected = false;
        this.element.removeClass("selected");
    }

    function open() {
        var parent = this.parent;
        while (parent instanceof TreeItem) {
            parent.open();
            parent = parent.parent;
        }
        if (this.hasChildren) {
            this.isOpen = true;
            this.element.addClass("open");
            this.caretIcon.removeClass("fa-caret-right");
            this.caretIcon.addClass("fa-caret-down");
            if (this.children.length === 0) {
                this.tree.reload(this.id);
            }
        }
    }

    function close() {
        this.isOpen = false;
        this.element.removeClass("open");
        this.caretIcon.removeClass("fa-caret-down");
        this.caretIcon.addClass("fa-caret-right");
    }

    function sortChildren() {
        this.children.sort(sortTreeItems);
    }

    function sortTreeItems(a, b) {
        var lowerA = a.name.toLowerCase(),
            lowerB = b.name.toLowerCase();

        if (lowerA < lowerB) {
            return -1;
        } else if (lowerB < lowerA) {
            return 1;
        }
        return 0;
    }
}

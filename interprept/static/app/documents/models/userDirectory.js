angular.module("app.documents")
    .factory("UserDirectory", UserDirectoryModel);

/* @ngInject */
function UserDirectoryModel(User, FileType) {

    function UserDirectory(data) {
        this.id = data.id;
        this.name = data.name || "-- no name --";
        this.created = new Date(data.created);
        this.modified = new Date(data.modified);
        this.isPrivate = data.isPrivate;
        this.path = data.path || "";
        this.user = data.user || null;
        this.dirCount = Number(data.dirCount);
        this.docCount = Number(data.docCount);
        this.parent = data.parent;
        this.children = data.children;
        this.fileType = FileType.DIR;
    }
    UserDirectory.map = map;
    UserDirectory.prototype.isFile = isFile;
    UserDirectory.prototype.isDir = isDir;

    return UserDirectory;

    //////////

    function map(directoryDb) {
        var userDirectory = new UserDirectory({
            id: directoryDb.id,
            name: directoryDb.name,
            created: directoryDb.created,
            modified: directoryDb.modified,
            isPrivate: directoryDb.is_private,
            path: directoryDb.path,
            user: directoryDb.user ? User.map(directoryDb.user) : null,
            dirCount: directoryDb.dir_count,
            docCount: directoryDb.doc_count,
            parent: directoryDb.parent ? UserDirectory.map(directoryDb.parent) : null,
            children: directoryDb.children ? directoryDb.children.map(UserDirectory.map) : []
        });
        return userDirectory;
    }

    function isFile() {
        return _.contains([FileType.DOC, FileType.REPORT, FileType.TLIST], this.fileType);
    }

    function isDir() {
        return this.fileType === FileType.DIR;
    }
}

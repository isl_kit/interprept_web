angular
    .module("app.documents")
    .constant("ViewMode", {
        TABLE: 0,
        ICONS: 1
    })
    .constant("FileType", {
        DIR: 0,
        DOC: 1,
        REPORT: 2,
        TLIST: 3
    })
    .constant("SelectionMode", {
        NONE: 0,
        DIR: 1,
        DOC: 2,
        DIRS: 3,
        DOCS: 4,
        TLIST: 5,
        TLISTS: 6,
        MIXED: 7
    });

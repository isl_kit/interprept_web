angular
    .module("app.documents")
    .run(documentsRun);

/* @ngInject */
function documentsRun($state, ModalService, MenuService, DocumentsDataService, TerminologyDocumentService) {

    var okStates = ["ok", "deprecated"];

    MenuService.addMenu("singleTermFileMenu", [
        [
            {
                text: "Open",
                img: null,
                defaultItem: true,
                callback: function (data) {
                    $state.go("terminology.list", { base: data.base.id, file: data.doc.id });
                }
            }
        ],
        [
            {
                text: "Copy",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [],
                        documents: [data.doc],
                        remove: false
                    });
                }
            },
            {
                text: "Move",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [],
                        documents: [data.doc],
                        remove: true
                    });
                }
            },
            {
                text: "Rename",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@rename", {
                        fileSpaceItem: data.doc
                    });
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@remove", {
                        directories: [],
                        documents: [data.doc]
                    });
                }
            }
        ]
    ]);

    MenuService.addMenu("singleFolderMenu", [
        [
            {
                text: "Open",
                img: "folder16.png",
                defaultItem: true,
                callback: function (data) {
                    $state.go("documents.browser", { dir: data.dir.id });
                }
            }
        ],
        [
            {
                text: "Create Terminology List",
                img: null,
                callback: function (data) {
                    TerminologyDocumentService.initTerminology([], [data.dir.id]).then(function (doc) {
                        $state.go("terminology.list", { draft: doc.id, base: data.base.id });
                    });
                }
            }
        ],
        [
            {
                text: "Copy",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [data.dir],
                        documents: [],
                        remove: false
                    });
                }
            },
            {
                text: "Move",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [data.dir],
                        documents: [],
                        remove: true
                    });
                }
            },
            {
                text: "Rename",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@rename", {
                        fileSpaceItem: data.dir
                    });
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@remove", {
                        directories: [data.dir],
                        documents: []
                    });
                }
            }
        ]
    ]);

    MenuService.addMenu("singleFileMenu", [
        [
            {
                text: "Preview",
                img: null,
                defaultItem: true,
                callback: function (data) {
                    var link = DocumentsDataService.createLink(data.doc, data.base);
                    window.open(link);
                }
            }
        ],
        [
            {
                text: "Create Terminology List",
                img: null,
                isDisabled: function (data) {
                    return !_.contains(okStates, data.doc.resource.taskStates.terminology.state);
                },
                callback: function (data) {
                    TerminologyDocumentService.initTerminology([data.doc.id], []).then(function (doc) {
                        $state.go("terminology.list", { draft: doc.id, base: data.base.id });
                    });
                }
            },
            {
                text: "Extract Named Entities",
                img: null,
                isDisabled: function (data) {
                    return !_.contains(okStates, data.doc.resource.taskStates.ne.state);
                },
                callback: function (data) {
                    $state.go("ne.list", { doc_id: data.doc.id, base_id: data.base.id });
                }
            }
        ],
        [
            {
                text: "Copy",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [],
                        documents: [data.doc],
                        remove: false
                    });
                }
            },
            {
                text: "Move",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [],
                        documents: [data.doc],
                        remove: true
                    });
                }
            },
            {
                text: "Rename",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@rename", {
                        fileSpaceItem: data.doc
                    });
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@remove", {
                        directories: [],
                        documents: [data.doc]
                    });
                }
            }
        ]
    ]);

    MenuService.addMenu("multiGenericMenu", [
        [
            {
                text: "Copy",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [],
                        documents: data.docs,
                        remove: false
                    });
                }
            },
            {
                text: "Move",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [],
                        documents: data.docs,
                        remove: true
                    });
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@remove", {
                        directories: [],
                        documents: data.docs
                    });
                }
            }
        ]
    ]);

    MenuService.addMenu("multiFileMenu", [
        [
            {
                text: "Create Terminology List",
                img: null,
                isDisabled: function (data) {
                    var i, max_i;

                    for (i = 0, max_i = data.docs.length; i < max_i; i++) {
                        if (_.contains(okStates, data.docs[i].resource.taskStates.terminology.state)) {
                            return false;
                        }
                    }
                    return true;
                },
                callback: function (data) {
                    var docIds = data.docs.map(function (userDocument) {
                        return userDocument.id;
                    });
                    TerminologyDocumentService.initTerminology(docIds, []).then(function (doc) {
                        $state.go("terminology.list", { draft: doc.id, base: data.base.id });
                    });
                }
            }
        ],
        [
            {
                text: "Copy",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [],
                        documents: data.docs,
                        remove: false
                    });
                }
            },
            {
                text: "Move",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: [],
                        documents: data.docs,
                        remove: true
                    });
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@remove", {
                        directories: [],
                        documents: data.docs
                    });
                }
            }
        ]
    ]);

    MenuService.addMenu("multiFolderMenu", [
        [
            {
                text: "Create Terminology List",
                img: null,
                callback: function (data) {
                    var dirIds = data.dirs.map(function (userDirectory) {
                        return userDirectory.id;
                    });
                    TerminologyDocumentService.initTerminology([], dirIds).then(function (doc) {
                        $state.go("terminology.list", { draft: doc.id, base: data.base.id });
                    });
                }
            }
        ],
        [
            {
                text: "Copy",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: data.dirs,
                        documents: [],
                        remove: false
                    });
                }
            },
            {
                text: "Move",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: data.dirs,
                        documents: [],
                        remove: true
                    });
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@remove", {
                        directories: data.dirs,
                        documents: []
                    });
                }
            }
        ]
    ]);

    MenuService.addMenu("mixedMenu", [
        [
            {
                text: "Create Terminology List",
                img: null,
                callback: function (data) {
                    var dirIds, docIds;
                    dirIds = data.dirs.map(function (userDirectory) {
                        return userDirectory.id;
                    });
                    docIds = data.docs.map(function (userDocument) {
                        return userDocument.id;
                    });
                    TerminologyDocumentService.initTerminology(docIds, dirIds).then(function (doc) {
                        $state.go("terminology.list", { draft: doc.id, base: data.base.id });
                    });
                }
            }
        ],
        [
            {
                text: "Copy",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: data.dirs,
                        documents: data.docs,
                        remove: false
                    });
                }
            },
            {
                text: "Move",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@copy", {
                        directories: data.dirs,
                        documents: data.docs,
                        remove: true
                    });
                }
            }
        ],
        [
            {
                text: "Remove",
                img: null,
                callback: function (data) {
                    ModalService.open("normal@remove", {
                        directories: data.dirs,
                        documents: data.docs
                    });
                }
            }
        ]
    ]);

    ModalService.register({
        name: "webdav",
        states: {
            normal: {
                templateUrl: "documents/templates/webdav-modal.tpl.html",
                controller: "WebdavModalController",
                controllerAs: "vm",
                params: ["directoryId"],
                resolve: {
                    "webdavContent": ["ResourcesDataService", function (ResourcesDataService) {
                        return ResourcesDataService.getWebdavContent();
                    }]
                }
            }
        }
    });

    ModalService.register({
        name: "newToWebdav",
        states: {
            normal: {
                templateUrl: "documents/templates/new-to-webdav-modal.tpl.html",
                controller: "NewToWebdavModalController",
                controllerAs: "vm",
                params: []
            }
        }
    });

    ModalService.register({
        name: "enableLogging",
        showCloseIcon: false,
        states: {
            normal: {
                templateUrl: "documents/templates/enable-logging-modal.tpl.html",
                controller: "EnableLoggingModalController",
                controllerAs: "vm",
                params: []
            }
        }
    });

    ModalService.register({
        name: "upload",
        states: {
            normal: {
                templateUrl: "documents/templates/upload-modal.tpl.html",
                controller: "UploadModalController",
                controllerAs: "vm",
                params: ["directoryId"]
            }
        }
    });

    ModalService.register({
        name: "copy",
        states: {
            normal: {
                templateUrl: "documents/templates/copy-move-modal.tpl.html",
                controller: "CopyMoveModalController",
                controllerAs: "vm",
                params: ["directories", "documents", "remove"]
            }
        }
    });

    ModalService.register({
        name: "rename",
        states: {
            normal: {
                templateUrl: "documents/templates/rename-modal.tpl.html",
                controller: "RenameModalController",
                controllerAs: "vm",
                focus: "renameModalInput",
                params: ["fileSpaceItem"]
            }
        }
    });

    ModalService.register({
        name: "remove",
        states: {
            normal: {
                templateUrl: "documents/templates/remove-modal.tpl.html",
                controller: "RemoveModalController",
                controllerAs: "vm",
                params: ["directories", "documents"]
            }
        }
    });

    ModalService.register({
        name: "createDir",
        states: {
            normal: {
                templateUrl: "documents/templates/create-dir-modal.tpl.html",
                controller: "CreateDirModalController",
                controllerAs: "vm",
                focus: "createDirInput",
                params: ["directoryId"]
            }
        }
    });
}

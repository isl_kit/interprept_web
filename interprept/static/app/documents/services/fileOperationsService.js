angular
    .module("app.documents")
    .factory("FileOperationsService", FileOperationsService);

/* @ngInject */
function FileOperationsService($http, $rootScope, UserDocument, UserDirectory) {

    var service = {
        createDir: createDir,
        transferFromWebdav: transferFromWebdav,
        deleteFromWebdav: deleteFromWebdav,
        addReports: addReports,
        addCommittees: addCommittees,
        addParlDocs: addParlDocs,
        addTerminologyList: addTerminologyList,
        initTerminologyList: initTerminologyList,
        updateTerminologyList: updateTerminologyList,
        removeDocs: removeDocs,
        removeDirs: removeDirs,
        move: move,
        copy: copy,
        renameDoc: renameDoc,
        renameDir: renameDir
    };

    return service;

    //////////

    function createDir(dirId, dirName) {
        return $http.post("/documents/createDir/", {dir_id: dirId, dir_name: dirName}).then(function (response) {
            var directory = UserDirectory.map(response.data);
            $rootScope.$emit("directories.createDirectory", {
                baseId: dirId,
                directory: directory
            });
            return directory;
        });
    }

    function transferFromWebdav(dirId, fileNames) {
        return $http.post("/documents/transferFromWebdav/", {base_id: dirId, file_names: fileNames}).then(function (response) {
            var documents = response.data.docs.map(UserDocument.map);
            _.each(documents, function (userDocument) {
                $rootScope.$emit("directories.addDocument", {
                    baseId: dirId,
                    userDocument: userDocument
                });
            });
        });
    }

    function deleteFromWebdav(fileNames) {
        return $http.post("/documents/deleteFromWebdav/", {file_names: fileNames});
    }

    function addReports(dirId, reportKeys) {
        return $http.post("/documents/addReports/", {dir_id: dirId, report_keys: reportKeys}).then(function (response) {
            var documents = response.data.docs.map(UserDocument.map);
            _.each(documents, function (userDocument) {
                $rootScope.$emit("directories.addDocument", {
                    baseId: dirId,
                    userDocument: userDocument
                });
            });
        });
    }

    function addCommittees(dirId, commdocIds) {
        return $http.post("/documents/addCommittees/", {dir_id: dirId, commdoc_ids: commdocIds}).then(function (response) {
            var documents = response.data.docs.map(UserDocument.map);
            _.each(documents, function (userDocument) {
                $rootScope.$emit("directories.addDocument", {
                    baseId: dirId,
                    userDocument: userDocument
                });
            });
        });
    }

    function addParlDocs(dirId, parlDocIds) {
        return $http.post("/documents/addParlDocs/", {dir_id: dirId, parldoc_ids: parlDocIds}).then(function (response) {
            var documents = response.data.docs.map(UserDocument.map);
            _.each(documents, function (userDocument) {
                $rootScope.$emit("directories.addDocument", {
                    baseId: dirId,
                    userDocument: userDocument
                });
            });
        });
    }

    function addTerminologyList(dirId, name, terminologyData) {
        return $http.post("/documents/addTerminologyList/", {dir_id: dirId, name: name, terminology_data: terminologyData}).then(function (response) {
            var documents = response.data.docs.map(UserDocument.map);
            _.each(documents, function (userDocument) {
                $rootScope.$emit("directories.addDocument", {
                    baseId: dirId,
                    userDocument: userDocument
                });
            });
            return documents[0];
        });
    }

    function initTerminologyList() {
        return $http.post("/documents/saveDraft/", {terminology_data: null}).then(function (response) {
            return response.data.docs.map(UserDocument.map)[0];
        });
    }

    function updateTerminologyList(docId, terminologyData) {
        return $http.post("/documents/updateTerminologyList/", {doc_id: docId, terminology_data: terminologyData}).then(function (response) {
            return response.data.docs.map(UserDocument.map)[0];
        });
    }

    function removeDocs(idArray) {
        return $http.post("/documents/deleteDocsByIdArray/", { "doc_ids": idArray }).then(function (response) {
            $rootScope.$emit("directories.removeDocuments", response.data.doc_ids);
        });
    }

    function removeDirs(idArray) {
        return $http.post("/documents/deleteDirsByIdArray/", { "dir_ids": idArray }).then(function (response) {
            $rootScope.$emit("directories.removeDirectories", response.data.dir_ids);
        });
    }

    function move(baseId, docIds, dirIds) {
        return copyOrMove(baseId, docIds, dirIds, true);
    }

    function copy(baseId, docIds, dirIds) {
        return copyOrMove(baseId, docIds, dirIds, false);
    }

    function renameDoc(docId, name) {
        return $http.post("/documents/renameDoc/", { doc_id: docId, name: name }).then(function (response) {
            $rootScope.$emit("directories.renameDocument", {
                docId: response.data.doc_id,
                newName: response.data.new_name,
                oldName: response.data.old_name,
                modified: response.data.modified
            });
        });
    }

    function renameDir(dirId, name) {
        return $http.post("/documents/renameDir/", { dir_id: dirId, name: name }).then(function (response) {
            console.log("EMIT");
            $rootScope.$emit("directories.renameDirectory", {
                dirId: response.data.dir_id,
                newName: response.data.new_name,
                oldName: response.data.old_name,
                modified: response.data.modified
            });
        });
    }

    function copyOrMove (baseId, docIds, dirIds, removeOriginal) {
        if (!docIds) {
            docIds = [];
        }
        if (!dirIds) {
            dirIds = [];
        }
        return $http.post("/documents/copy/", { doc_ids: docIds, dir_ids: dirIds, base_id: baseId, remove_original: removeOriginal }).then(function (response) {
            var documents = response.data.docs.map(UserDocument.map),
                directories = response.data.dirs.map(UserDirectory.map),
                docIds = response.data.rm_doc_ids,
                dirIds = response.data.rm_dir_ids;

            if (docIds.length > 0) {
                $rootScope.$emit("directories.removeDocuments", docIds);
            }
            if (dirIds.length > 0) {
                $rootScope.$emit("directories.removeDirectories", dirIds);
            }
            _.each(documents, function (userDocument) {
                $rootScope.$emit("directories.addDocument", {
                    baseId: baseId,
                    userDocument: userDocument
                });
            });
            _.each(directories, function (userDirectory) {
                $rootScope.$emit("directories.createDirectory", {
                    baseId: baseId,
                    directory: userDirectory
                });
            });
        });
    }
}

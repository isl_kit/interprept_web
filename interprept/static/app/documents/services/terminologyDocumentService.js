angular
    .module("app.documents")
    .factory("TerminologyDocumentService", TerminologyDocumentService);

/* @ngInject */
function TerminologyDocumentService($q, DocumentsDataService, TermList) {

    var service = {
        initTerminology: initTerminology
    };

    return service;

    //////////

    function initTerminology(docIds, dirIds) {
        var deferred = $q.defer();
        $q.all({
            fromDocs: DocumentsDataService.getDocs(docIds),
            fromDirs: DocumentsDataService.getDocsFromDirs(dirIds)
        }).then(function (result) {
            var documents = _.uniq(_.union(result.fromDocs, result.fromDirs), false, function (userDocument) {
                return userDocument.id;
            }).filter(function (userDocument) {
                return userDocument.resource;
            });
            terminologyData = TermList.serializeInit(documents, documents[0].resource.language);
            DocumentsDataService.saveDraft(terminologyData).then(function (doc) {
                deferred.resolve(doc);
             });
        });
        return deferred.promise;
    }
}

angular
    .module("app.documents")
    .factory("DocumentsDataService", DocumentsDataService);

/* @ngInject */
function DocumentsDataService($http, $state, UserDocument, UserDirectory, FileType) {

    var service = {
        getDirContent: getDirContent,
        getDirTree: getDirTree,
        getDocs: getDocs,
        saveDraft: saveDraft,
        getDocsFromDirs: getDocsFromDirs,
        createLink: createLink
    };

    return service;

    //////////

    function getDirContent(dirId) {
       var params = "";
       if (dirId) {
           params = "?dir_id=" + dirId;
       }
       return $http.get("/documents/getDirContent/" + params).then(function (response) {
           return {
               directories: response.data.dirs.map(UserDirectory.map),
               documents: response.data.docs.map(UserDocument.map),
               base: UserDirectory.map(response.data.base)
           };
       });
    }

    function getDirTree(dirId) {
        var params = "";
        if (dirId) {
            params = "?dir_id=" + dirId;
        }
        return $http.get("/documents/getDirTree/" + params).then(function (response) {
            return {
                home: UserDirectory.map(response.data.home),
                shared: UserDirectory.map(response.data.shared)
            };
        });
    }

    function getDocs(docIds) {
        docIds = docIds.map(function (id) {
            return Number(id);
        });
        return $http.get("/documents/getDocs/?doc_ids=" + JSON.stringify(docIds)).then(function (response) {
            return response.data.docs.map(UserDocument.map);
        });
    }

    function saveDraft(terminologyData) {
        return $http.post("/documents/saveDraft/", {terminology_data: terminologyData}).then(function (response) {
            return UserDocument.map(response.data.doc);
        });
    }

    function getDocsFromDirs(dirIds) {
        return $http.get("/documents/getDocsFromDirs/?dir_ids=" + JSON.stringify(dirIds)).then(function (response) {
            return response.data.docs.map(UserDocument.map);
        });
    }

    function createLink(userDocument, userDirectory) {
        var pathArr = [],
            path = "",
            prefix = "",
            dir = userDirectory;

        if (userDocument.fileType === FileType.TLIST) {
            return $state.href("terminology.list", { base: userDirectory.id, file: userDocument.id });
        } else {
            while (true) {
                if (!dir.parent) {
                    if (dir.name === "SHARED") {
                        prefix = "shared";
                    } else {
                        prefix = "user/" + userDocument.user.id;
                    }
                    break;
                }
                pathArr.unshift(dir.name);
                dir = dir.parent;
            }
            if (pathArr.length > 0) {
                path = pathArr.join("/") + "/";
            }
            return "files/" + prefix + "/PDF/" + path + userDocument.name;
        }
    }
}

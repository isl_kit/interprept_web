angular.module("app.documents", [
    "app.core",
    "app.auth",
    "app.resources",
    "app.terminology"
]);
angular
    .module("app.documents")
    .directive("dirTree", dirTreeDirective);

/* @ngInject */
function dirTreeDirective(Tree) {

    var directive = {
        restrict: "E",
        scope: {
            selectedId: "=",
            onSelect: "="
        },
        link: linkFunction,
        template: "<div class=\"tree-view\"></div>",
        replace: true
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        var tree = new Tree(element, scope.onSelect, scope);

        scope.$watch("selectedId", function (id, oldId) {
            tree.selectById(id, oldId !== id);
        });
        scope.$onRootScope("directories.createDirectory", function (ev, args) {
            tree.addDirectory(args.baseId, args.directory);
        });
        scope.$onRootScope("directories.removeDirectories", function (ev, idArray) {
            _.each(idArray, function (dirId) {
                tree.removeDirectory(dirId);
            });
        });
        scope.$onRootScope("directories.renameDirectory", function (ev, args) {
            tree.renameDirectory(args.dirId, args.newName, args.modified);
        });
    }
}

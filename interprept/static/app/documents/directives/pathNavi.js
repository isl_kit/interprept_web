angular
    .module("app.documents")
    .directive("pathNavi", pathNaviDirective);

/* @ngInject */
function pathNaviDirective($parse, $state, $timeout) {

    var directive = {
        restrict: "C",
        link: linkFunction
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        var dirGetter = $parse(attributes.dir),
            localRedrawFunc = redrawPath.bind(null, scope, element, dirGetter);

        $timeout(localRedrawFunc);
        $(window).on("resize", localRedrawFunc);
        scope.$on('$destroy', function() {
            $(window).off("resize", localRedrawFunc);
        });
    }

    function redrawPath(scope, element, dirGetter) {
        var dir = dirGetter(scope),
            pathArr,
            pathArrMiddle,
            width = element.parent().width(),
            testObj,
            htmlText = "",
            longest;

        pathArr = [];
        while (dir) {
            pathArr.unshift(dir);
            dir = dir.parent;
        }
        pathArr = pathArr.map(function (item) {
            return {
                id: item.id,
                name: item.name,
                display: item.name
            };
        });
        pathArrMiddle = pathArr.slice(1, -1);
        htmlText = getHtml(pathArr);
        testObj = $("<span class=\"path-navi\" style=\"visibility:hidden;padding:0px\"></span>").html(htmlText);
        $("body").append(testObj);
        if (pathArrMiddle.length > 0) {
            while (testObj.width() > width) {
                longest = getLongest(pathArrMiddle);
                if (longest.display.length <= 3) {
                    pathArr[pathArr.length - 1].display = "...";
                    htmlText = getHtml(pathArr);
                    break;
                }
                longest.display = "...";
                htmlText = getHtml(pathArr);
                testObj.html(htmlText);
            }
        }
        testObj.remove();
        element.html(htmlText);
    }

    function getLongest(pathArr) {
        if (pathArr.length === 1) {
            return pathArr[0];
        } else {
            return pathArr.reduce(function (a, b) {
                return a.display.length > b.display.length ? a : b;
            });
        }
    }

    function createSpan(item, noLink) {
         var link = $state.href("documents.browser", {dir: item.id});
         if (noLink) {
             return "<span title=\"" + item.name + "\">" + item.display + "</span>";
         } else {
             return "<a href=\"" + link + "\" title=\"" + item.name + "\">" + item.display + "</a>";
         }
     }

     function getHtml(pathArr) {
         var htmlArr = [];
         _.each(pathArr, function (item, index) {
             htmlArr.push(createSpan(item, index === pathArr.length - 1));
         });
         return htmlArr.join("<i class=\"fa fa-angle-right\"></i>");
     }
}

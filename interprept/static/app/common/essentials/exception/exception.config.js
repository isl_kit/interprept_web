// exception.config.js
angular
    .module("co.exception")
    .config(exceptionConfig);

/* @ngInject */
function exceptionConfig($provide) {

    $provide.decorator("$exceptionHandler", /* @ngInject */ function($delegate, $window) {
        return function (exception, cause) {
            var errorData = { exception: exception, cause: cause };
            $delegate(exception, cause);
            //console.log("!!!", exception.message, errorData);
        };
    });
}
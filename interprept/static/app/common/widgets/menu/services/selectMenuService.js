angular
    .module("co.menu")
    .factory("SelectMenuService", SelectMenuService);

/* @ngInject */
function SelectMenuService($rootScope) {

    var service = {
        show: show,
        hide: hide
    };

    activate();

    return service;

    //////////

    function activate() {
        $rootScope.$on("$locationChangeStart", function () {
            hide();
        });
        $("body").click(function () {
            hide();
        });
    }

    function handleClick(ev, src, list, callback) {
        var index;
        while (src.length > 0 && !src.hasClass("menu-item") && src.attr("id") !== "select-menu-container") {
            src = src.parent();
        }

        if (src.hasClass("menu-item")) {
            src.toggleClass("selected");
            index = src.data("menu-index");
            list[index].selected = !list[index].selected;
            $rootScope.$apply(callback);
            ev.stopPropagation();
            return;
        }
        hide();
    }

    function touchHandler(ev, list, callback) {
        ev = ev || window.event;
        var src = ev.target || ev.srcElement;
        handleClick(ev, $(src), list, callback);
        ev.preventDefault();
        return false;
    }

    function clickHandler(ev, list, callback) {
        var src = $(ev.target);
        handleClick(ev, src, list, callback);
    }

    function hide () {
        $("#select-menu-container").remove();
        document.body.removeEventListener("touchend", hide, false);
    }

    function show(list, max, callback, x, y) {
        var menuWidth, menuHeight,
            container,
            containerWidth, containerHeight,
            table = $("<table data-menu-name=\"" + name + "\" class=\"co-menu-select\"></table>"),
            tbody = $("<tbody class=\"menu-group\"></tbody>"),
            index = 0,
            cols = Math.ceil(list.length / max);

        hide();

        var html = "<tr class=\"menu-row\">";
        _.each(list, function (item) {
            html += "<td class=\"menu-item";
            if (item.selected) {
                html += " selected";
            }
            html += "\" data-menu-index=\"" + index + "\"><div class=\"menu-item-img\">";
            html += "<i class=\"fa fa-check\"></i>";
            html += "</div><div class=\"menu-item-text\">" + item.name + "</div>";
            html += "</td>";
            index++;
            if (index > 0 && index < list.length && index % cols === 0) {
                html += "</tr><tr class=\"menu-row\">";
            }
        });
        html += "</tr>";
        tbody.append($(html));
        table.append(tbody);
        container = $("<div id=\"select-menu-container\"></div>").click(function (ev) {
            clickHandler(ev, list, callback);
        });
        $("body").append(container);
        container[0].addEventListener("touchend", function (ev) {
            touchHandler(ev, list, callback);
        }, false);

        container.append(table);
        container.css("visibility", "hidden");
        container.show();

        menuWidth = table.width();
        menuHeight = table.height();

        containerWidth = $(window).width();
        containerHeight = $(window).height();

        distRight = containerWidth - (x + menuWidth);
        distLeft = x - menuWidth;

        distBottom = containerHeight - (y + menuHeight);
        distTop = y - menuHeight;

        if (distRight > 20) {
            table.css("left", (x + 10) + "px");
        } else if (distLeft > 20) {
            table.css("left", (x - menuWidth - 10) + "px");
        } else if (distLeft > distRight) {
            table.css("left", "10px");
        } else {
            table.css("left", (containerWidth - menuWidth - 10) + "px");
        }

        if (distBottom > 20) {
            table.css("top", (y + 10) + "px");
        } else if (distTop > 20) {
            table.css("top", (y - menuHeight - 10) + "px");
        } else if (distTop > distBottom) {
            table.css("top", "10px");
        } else {
            table.css("top", (containerHeight - menuHeight - 10) + "px");
        }
        document.body.addEventListener("touchend", hide, false);
        container.css("visibility", "visible");
    }
}

angular
    .module("co.menu")
    .factory("MenuService", menuService);

/* @ngInject */
function menuService($rootScope) {

    var menus = {},
        container = null;

    var service = {
        removeMenu: removeMenu,
        addMenu: addMenu,
        show: show,
        hide: hide
    };

    activate();

    return service;

    //////////

    function activate() {
        $rootScope.$on("$locationChangeStart", function () {
            hide();
        });
        $("body").click(function () {
            hide();
        });
    }

    function handleClick(src) {
        var menuObj, name, menu, index;

        while (src.length > 0 && !src.hasClass("menu-item") && src.attr("id") !== "menu-container") {
            src = src.parent();
        }

        if (src.hasClass("menu-item") && !src.hasClass("disabled")) {
            menuObj = src.parents(".co-menu");
            name = menuObj.data("menu-name");
            menu = menus[name];
            index = src.data("menu-index");
            $rootScope.$apply(function () {
                menu.callbacks[index](menu.context);
            });
        }
        hide();
    }

    function touchHandler(ev) {
        console.log("touch");
        ev = ev || window.event;
        var src = ev.target || ev.srcElement;
        handleClick($(src));
        ev.preventDefault();
        return false;
    }

    function clickHandler(ev) {
        console.log("click");
        var src = $(ev.target);

        handleClick(src);
    }

    function isEnabled() {
        return false;
    }

    function removeMenu(name) {
        delete menus[name];
    }

    function addMenu(name, menuDef) {
        var table = $("<table data-menu-name=\"" + name + "\" class=\"co-menu\"></table>"),
            index = 0,
            menu;

        menu = {
            context: null,
            obj: table,
            callbacks: [],
            disableFuncs: []
        };
        menus[name] = menu;

        _.each(menuDef, function (group) {
            var tbody = $("<tbody class=\"menu-group\"></tbody>");
            _.each(group, function (item) {
                var html = "<tr class=\"menu-item";

                if (item.defaultItem) {
                    html += " default";
                }
                html += "\" data-menu-index=\"" + index + "\"><td class=\"menu-item-img\">";
                if (item.img) {
                    html += "<img src=\"/static/assets/images/" + item.img + "\">";
                }
                html += "</td><td class=\"menu-item-text\">" + item.text;
                if (item.desc) {
                    html += "<p class=\"menu-item-desc\">(" + item.desc + ")</p>";
                }
                html += "</td>";
                html += "</tr>";

                tbody.append($(html));
                menu.callbacks.push(item.callback);
                menu.disableFuncs.push(item.isDisabled || isEnabled);
                index++;
            });
            table.append(tbody);
        });
    }

    function show(name, context, x, y) {
        var menu = menus[name],
            menuWidth, menuHeight,
            containerWidth, containerHeight,
            container;

        container = $("#menu-container");
        if (container.length === 0) {
            container = $("<div id=\"menu-container\"></div>").click(clickHandler);
            $("body").append(container);
            container[0].addEventListener("touchend", touchHandler, false);
        }

        menu.context = context;
        menu.obj.find(".menu-item").each(function () {
            var menuItem = $(this);
            var index = menuItem.data("menu-index");
            var isDisabled = menu.disableFuncs[index](menu.context);
            if (isDisabled) {
                menuItem.addClass("disabled");
            } else {
                menuItem.removeClass("disabled");
            }
        });

        container.empty().append(menu.obj);
        container.css("visibility", "hidden");
        container.show();

        menuWidth = menu.obj.width();
        menuHeight = menu.obj.height();

        containerWidth = $(window).width();
        containerHeight = $(window).height();

        distRight = containerWidth - (x + menuWidth);
        distLeft = x - menuWidth;

        distBottom = containerHeight - (y + menuHeight);
        distTop = y - menuHeight;

        if (distRight > 20) {
            menu.obj.css("left", (x + 10) + "px");
        } else if (distLeft > 20) {
            menu.obj.css("left", (x - menuWidth - 10) + "px");
        } else if (distLeft > distRight) {
            menu.obj.css("left", "10px");
        } else {
            menu.obj.css("left", (containerWidth - menuWidth - 10) + "px");
        }

        if (distBottom > 20) {
            menu.obj.css("top", (y + 10) + "px");
        } else if (distTop > 20) {
            menu.obj.css("top", (y - menuHeight - 10) + "px");
        } else if (distTop > distBottom) {
            menu.obj.css("top", "10px");
        } else {
            menu.obj.css("top", (containerHeight - menuHeight - 10) + "px");
        }
        document.body.addEventListener("touchend", hide, false);
        container.css("visibility", "visible");
    }

    function hide () {
        $("#menu-container").remove();
        document.body.removeEventListener("touchend", hide, false);
    }
}

angular
    .module("co.menu")
    .directive("coMenu", menuDirective);

/* @ngInject */
function menuDirective(MenuService) {
    var directive = {
        restrict: "E",
        scope: {
            onClose: "="
        },
        link: linkFunction,
        replace: true,
        transclude: true,
        templateUrl: "menu/templates/menu.tpl.html"
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        MenuService.push(element, attributes.menuName);
        scope.close = function () {
            MenuService.hide(attributes.menuName);
        };
        element.bind("$destroy", function () {
            MenuService.remove(attributes.menuName);
        });
    }
}

angular
    .module("co.wait-icon")
    .directive("coWaitIcon", waitIconDirective);

/* @ngInject */
function waitIconDirective($timeout) {

    var oriColor = {
            r: 220,
            g: 220,
            b: 220
        },
        blendColor = {
            r: 0,
            g: 166,
            b: 255
        };

    var directive = {
        restrict: "E",
        scope: {
            active: "=",
            size: "=",
            center: "=",
            fill: "="
        },

        link: linkFunction,
        templateUrl: "common/widgets/wait-icon/templates/wait-icon.tpl.html",
        replace: true
    };

    return directive;

    //////////

    function linkFunction (scope, element, attributes) {
        function toggleAnimation() {
            var i = 0, k, max_k, squares, control = [];

            if (scope.active === true) {
                squares = element.find(".wait-square");
                for (k = 0, max_k = squares.length; k < max_k; k++) {
                    control.push({
                        pos: 0,
                        dir: "up"
                    });
                }
                (function interval() {
                    var color, ctrl = control[i], square = squares.eq(i);

                    ctrl.pos += 0.05;
                    if (ctrl.pos > 1) {
                        ctrl.pos = 0;
                        i = i < squares.length - 1 ? i + 1 : 0;
                    }
                    color = getColor(ctrl.pos);
                    square.css("background-color", "rgb(" + color.r + ", " + color.g + ", " + color.b + ")");
                    if (scope.active === true) {
                        $timeout(interval, 10);
                    }
                }());
            }
        }
        scope.$watch("active", toggleAnimation);
    }

    function blend(min, max, pos) {
        return Math.round(min + (max - min) * pos);
    }

    function getColor(pos) {
        return {
            r: blend(oriColor.r, blendColor.r, pos),
            g: blend(oriColor.g, blendColor.g, pos),
            b: blend(oriColor.b, blendColor.b, pos)
        };
    }
}

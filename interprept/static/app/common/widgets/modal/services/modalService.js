angular
    .module("co.modal")
    .factory("ModalService", ModalService);

/* @ngInject */
function ModalService($controller, $rootScope, $q, $templateCache, $compile, $injector) {

    var modals = {},
        modalDefs = {},
        deferreds = {},
        openModals = [],
        mainScope = null,
        scope = null;

    var service = {
        open: open,
        register: register,
        startChain: startChain,
        close: close,
        remove: remove,
        push: push,
        show: show,
        hide: hide
    };

    return service;

    //////////

    function register(modalDefinition) {
        modalDefs[modalDefinition.name] = {
            params: modalDefinition,
            deferred: null,
            scope: null,
            mainScope: null,
            obj: null
        };
    }

    function getTemplate(templateUrl) {
        return $templateCache.get(templateUrl);
    }

    function getController(controllerName, controllerAsName, scope, stateParams) {
        var controllerParams = _.extend({ $scope: scope, $$close: closeModal }, stateParams);
        if (controllerAsName) {
            controllerName += " as " + controllerAsName;
        }
        return $controller(controllerName, controllerParams);
    }

    function getContainer(modal, showCloseIcon) {
        var container = modal.obj;

        if (!container) {
            container = $(getTemplate("common/widgets/modal/templates/modal.tpl.html")).appendTo($("body"));
            if (openModals.length > 1) {
                container.addClass("neutral");
            }
            modal.obj = container;
        } else {
            container.find(".co-modal-body").empty();
        }

        if (!modal.mainScope) {
            modal.mainScope = $rootScope.$new();
            modal.mainScope.$$closeModal = closeModal.bind(null, modal, true);
            modal.mainScope.$$showCloseIcon = showCloseIcon;
            $compile(container.contents())(modal.mainScope);
        }

        return container;
    }

    function resolve(resolveObject, stateParams) {
        var promises = {};

        if (!resolveObject) {
            return $q.when({});
        }
        _.each(resolveObject, function (func, key) {
            promises[key] = $injector.invoke(func, null, stateParams);
        });
        return $q.all(promises);
    }

    function open(name, params) {
        var deferred,
            splitName = name.split("@"),
            modalName = splitName.length > 1 ? splitName[1] : splitName[0],
            stateName = splitName.length > 1 ? splitName[0] : "normal",
            modal, modalParams, showCloseIcon, state, focusId, resolveObject, template, stateParams = {}, controller, container, controllerParams, element;



        params = params || {};
        modal = modalDefs[modalName];
        modalParams = modal.params;

        showCloseIcon = _.has(modalParams, "showCloseIcon") ? modalParams.showCloseIcon : true;

        state = modalParams.states[stateName];
        focusId = state.focus;

        if (!_.contains(openModals, modal)) {
            //closeModal(true);
            openModals.push(modal);
            modal.deferred = $q.defer();
        } else {
            closeModal(modal, false);
        }

        _.each(state.params, function (paramName) {
            stateParams[paramName] = params[paramName];
        });

        resolveObject = state.resolve;
        resolve(resolveObject, stateParams).then(function (resolveParams) {
            container = getContainer(modal, showCloseIcon);
            if (modalParams.dark) {
                container.addClass("dark");
            } else {
                container.removeClass("dark");
            }
            if (modalParams.fullScreen) {
                container.addClass("fullScreen");
            } else {
                container.removeClass("fullScreen");
            }

            modal.scope = modal.mainScope.$new();

            template = getTemplate(state.templateUrl);


            element = container.find(".co-modal-body");
            element.html(template);
            if (state.controller) {
                controller = getController(state.controller, state.controllerAs, modal.scope, _.extend(stateParams, resolveParams));
                element.children().data('$ngControllerController', controller);
            }

            $compile(element.contents())(modal.scope);
            container.css("display", "table");

            if (focusId) {
                $("#" + focusId).focus(function () {
                    $(this).select();
                });
                $("#" + focusId).focus();
            }
        });

        return modal.deferred.promise;
    }

    function closeModal(modal, closeMain, result) {
        if (modal.scope) {
            modal.scope.$destroy();
            modal.scope = null;
            if (closeMain) {
                modal.mainScope.$destroy();
                modal.mainScope = null;
                if (result) {
                    modal.deferred.resolve(result);
                } else {
                    modal.deferred.reject("no action taken");
                }
                modal.deferred = null;
                openModals = _.without(openModals, modal);
                if (modal.obj) {
                    modal.obj.remove();
                    modal.obj = null;
                }
            }

        }
    }

    function startChain(modals) {
        var promises = [],
            index = 0;

        function next() {
            var modal = modals[index];

            while (modal && !modal.condition) {
                modal = modals[++index];
            }

            if (modal) {
                index += 1;
                open(modal.name, modal.params).then(next, next);
            }
        }

        next();
    }

    function remove(name) {
        delete modals[name];
    }

    function push(element, name, onClose, focusId, scope) {
        var background = element,
            area = element.find(".co-modal-table"),
            active = modals[name] ? modals[name].active : false;

        modals[name] = {
            name: name,
            onClose: onClose,
            focusId: focusId,
            background: background,
            area: area,
            active: active,
            initialized: true
        };

        if (focusId) {
            $("#" + focusId).focus(function () {
                $(this).select();
            });
        }

        if (active) {
            this.show(name);
        }
    }

    function show(name) {
        var key, modal = modals[name];

        if (modal) {
            if (modal.initialized) {
                modal.background.css("display", "table");
                //modal.area.animate({ "padding-left": "0%" }, 200);
                if (modal.focusId) {
                    $("#" + modal.focusId).focus();
                }
            }
            modal.active = true;
        } else {
            modals[name] = {
                active: true,
                initialized: false
            };
        }
    }

    function hide(name) {
        var modal = modals[name];

        if (modal) {
            if (modal.initialized) {
                modal.background.css("display", "none");
                //modal.area.css("padding-left", "50%");
            }
            console.log(modal.onClose);
            if (modal.onClose && typeof modal.onClose === "function") {
                modal.onClose();
            }
            modal.active = false;
        } else {
            modals[name] = {
                active: false,
                initialized: false
            };
        }
    }

    function close(result) {
        closeModal(true, result);
    }
}

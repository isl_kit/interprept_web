angular
    .module("co.modal")
    .directive("coModal", modalDirective);

/* @ngInject */
function modalDirective(ModalService) {
    var directive = {
        restrict: "E",
        scope: {
            onClose: "="
        },
        link: linkFunction,
        replace: true,
        transclude: true,
        templateUrl: "common/widgets/modal/templates/modal.tpl.html"
    };

    return directive;

    //////////

    function linkFunction (scope, element, attributes) {
        ModalService.push(element, attributes.modalName, scope.onClose, attributes.focusId, scope);
        scope.close = function () {
            ModalService.hide(attributes.modalName);
        };
        element.bind("$destroy", function () {
            ModalService.remove(attributes.modalName);
        });
    }
}

angular
    .module("co.checkbox")
    .directive("coCheckbox", checkboxDirective);

/* @ngInject */
function checkboxDirective() {

    var directive = {
        restrict: "E",
        scope: {
            checked: "="
        },
        link: linkFunction,
        templateUrl: "common/widgets/checkbox/templates/checkbox.tpl.html",
        replace: true
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        scope.check = function () {
            scope.checked = !scope.checked;
        };
    }
}

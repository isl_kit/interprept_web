angular
    .module("co.slider")
    .directive("coSlider", sliderDirective);

/* @ngInject */
function sliderDirective() {

    var directive = {
        restrict: "E",
        scope: {
            min: "=",
            max: "=",
            units: "=",
            releaseValue: "=",
            slideValue: "="
        },
        link: linkFunction,
        templateUrl: "common/widgets/slider/templates/slider.tpl.html",
        replace: true
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        var el = element[0],
            touchId = null,
            steps,
            mouseDown = false,
            downX = 0,
            force = false,
            newSteps, tempSteps,
            button = angular.element(el.querySelector(".slider-button")),
            slot = angular.element(el.querySelector(".slider-slot")),
            filler = angular.element(el.querySelector(".slider-slot-fill")),
            buttonWidth = parseInt(window.getComputedStyle(button[0], null).getPropertyValue("width"), 10),
            slotLength = parseInt(window.getComputedStyle(slot[0], null).getPropertyValue("width"), 10) - buttonWidth,
            stepWidth = slotLength / scope.units;

        button[0].addEventListener("mousedown", down, false);
        button[0].addEventListener("touchstart", downTouch, false);
        slot[0].addEventListener("mousedown", slotDown, false);
        slot[0].addEventListener("touchstart", slotDownTouch, false);
        document.addEventListener("mouseup", up, false);
        document.addEventListener("touchend", upTouch, false);
        document.addEventListener("mousemove", move, false);
        document.addEventListener("touchmove", moveTouch, false);

        steps = valueToSteps(scope.releaseValue);

        scope.$watch("releaseValue", function () {
            setValue(scope.releaseValue);
        });

        function setValue(value) {
            var steps = valueToSteps(value);
            setSteps(steps);
        }

        function setSteps(normalizedSteps) {
            var value;

            steps = normalizedSteps;
            value = stepsToValue(steps);
            button.css("left", (steps / scope.units * 100) + "%");
            filler.css("width", (steps / scope.units * 100) + "%");
            scope.releaseValue = value;
            scope.slideValue = value;
        }

        function normalizeSteps(steps) {
            if (steps < 0) {
                return 0;
            } else if (steps > scope.units) {
                return scope.units;
            }
            return steps;
        }

        function valueToSteps(value) {
            var steps = Math.round((value - scope.min) / (scope.max - scope.min) * scope.units);
            return normalizeSteps(steps);
        }

        function stepsToValue(steps) {
            return steps / scope.units * (scope.max - scope.min) + scope.min;
        }

        function down (ev) {
            mouseDown = true;
            downX = ev.clientX;
            newSteps = steps;
            tempSteps = steps;
            return false;
        }

        function up (ev) {
            var src;

            ev = ev || window.event;
            src = ev.target || ev.srcElement;

            if (mouseDown) {
                mouseDown = false;
                if (steps !== newSteps || force) {
                    steps = newSteps;
                    scope.$apply(function () {
                        scope.releaseValue = scope.slideValue;
                    });
                }
                force = false;
            }
        }

        function downTouch(ev) {
            var touches = event.changedTouches,
                touch = touches[0];

            touchId = touch.identifier;
            console.log(touchId);
            mouseDown = true;
            downX = touch.clientX;
            newSteps = steps;
            tempSteps = steps;
            return false;
        }

        function slotDownTouch(ev) {
            var touches = event.changedTouches,
                touch = touches[0];

            touchId = touch.identifier;

            perc = (touch.clientX - slot.offset().left) / slotLength;
            newSteps = normalizeSteps(Math.round(scope.units * perc));
            if (newSteps !== steps) {
                scope.$apply(function () {
                    scope.slideValue = stepsToValue(newSteps);
                });
                button.css("left", (newSteps / scope.units * 100) + "%");
                filler.css("width", (newSteps / scope.units * 100) + "%");
                force = true;
            }
            downX = touch.clientX;
            mouseDown = true;
            steps = newSteps;
            tempSteps = newSteps;
            return false;
        }

        function moveTouch(ev) {
            var touches = event.changedTouches,
                touch, diff;

            if (touchId !== null) {
                touch = _.find(touches, function (touch) {
                    return touch.identifier === touchId;
                });
                if (touch) {

                    diff = touch.clientX - downX;
                    console.log("diff " +  touch.clientX);
                    newSteps = normalizeSteps(steps + Math.round(diff / stepWidth));

                    if (newSteps !== tempSteps) {
                        tempSteps = newSteps;
                        scope.$apply(function () {
                            scope.slideValue = stepsToValue(tempSteps);
                        });
                        button.css("left", (tempSteps / scope.units * 100) + "%");
                        filler.css("width", (tempSteps / scope.units * 100) + "%");
                    }

                }
            }
        }

        function upTouch(ev) {
            var touches = event.changedTouches,
                touch, diff;

            if (touchId !== null) {
                touch = _.find(touches, function (touch) {
                    return touch.identifier === touchId;
                });
                if (touch) {
                    mouseDown = false;
                    if (steps !== newSteps || force) {
                        steps = newSteps;
                        scope.$apply(function () {
                            scope.releaseValue = scope.slideValue;
                        });
                    }
                    force = false;
                    touchId = null;
                }
            }
        }

        function slotDown(ev) {
            var src, perc;

            ev = ev || window.event;
            src = ev.target || ev.srcElement;

            perc = (ev.clientX - slot.offset().left) / slotLength;
            newSteps = normalizeSteps(Math.round(scope.units * perc));
            if (newSteps !== steps) {
                scope.$apply(function () {
                    scope.slideValue = stepsToValue(newSteps);
                });
                button.css("left", (newSteps / scope.units * 100) + "%");
                filler.css("width", (newSteps / scope.units * 100) + "%");
                force = true;
            }
            downX = ev.clientX;
            mouseDown = true;
            steps = newSteps;
            tempSteps = newSteps;
            return false;
        }

        function move(ev) {
            var src, diff;

            ev = ev || window.event;
            src = ev.target || ev.srcElement;
            diff = ev.clientX - downX;

            if (mouseDown === true) {
               newSteps = normalizeSteps(steps + Math.round(diff / stepWidth));

                if (newSteps !== tempSteps) {
                    tempSteps = newSteps;
                    scope.$apply(function () {
                        scope.slideValue = stepsToValue(tempSteps);
                    });
                    button.css("left", (tempSteps / scope.units * 100) + "%");
                    filler.css("width", (tempSteps / scope.units * 100) + "%");
                }
            }
        }
    }
}

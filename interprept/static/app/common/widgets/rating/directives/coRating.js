angular
    .module("co.rating")
    .directive("coRating", ratingDirective);

/* @ngInject */
function ratingDirective() {

    var directive = {
        restrict: "E",
        scope: {
            value: "="
        },
        link: linkFunction,
        templateUrl: "common/widgets/rating/templates/rating.tpl.html",
        replace: true
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {

        function redraw() {
            var val = scope.value <= 1 ? scope.value : 1;

            if (scope.value === -1) {
                scope.imgNum = "user";
                scope.percValue = 100;
                scope.absValue = 1;
            } else {
                scope.imgNum = Math.round(val * 5);
                scope.percValue = val * 100;
                scope.absValue = val;
            }
        }

        if (!attributes.showNumber) {
            scope.showNumber = true;
        } else {
            scope.showNumber = attributes.showNumber === "true" ? true : false;
        }
        scope.$watch("value", redraw);
    }
}

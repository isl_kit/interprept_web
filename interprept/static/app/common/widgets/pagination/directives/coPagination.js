angular
    .module("co.pagination")
    .directive("coPagination", paginationDirective);

/* @ngInject */
function paginationDirective() {

    var directive = {
        restrict: "E",
        scope: {
          numberOfPages: '=',
          currentPage: '=',
          maxNumberOfPages: '='
        },
        link: linkFunction,
        templateUrl: "common/widgets/pagination/templates/pagination.tpl.html",
        replace: true
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {

        scope.$watch('numberOfPages + currentPage + maxNumberOfPages', function() {
            var i, max_i, gaps = [], gap, gapPage,
                numberOfPages = Number(scope.numberOfPages),
                currentPage = Number(scope.currentPage),
                maxNumberOfPages = Number(scope.maxNumberOfPages);

            scope.pages = [];

            if (maxNumberOfPages < numberOfPages) {
                //show numbers until end (hole at the start)
                if (numberOfPages - currentPage - 1 < maxNumberOfPages / 2) {
                    gaps.push(makeGap(2, numberOfPages - maxNumberOfPages + 2));
                //show number from beginning (hole at the end)
                } else if (currentPage + 1 < maxNumberOfPages / 2) {
                    gaps.push(makeGap(maxNumberOfPages - 1, numberOfPages - 1));
                //hole on both sides
                } else {
                    gaps.push(makeGap(2, currentPage + 1 - Math.floor(maxNumberOfPages / 2) + 2));
                    gaps.push(makeGap(currentPage + 1 - Math.floor(maxNumberOfPages / 2) + maxNumberOfPages - 1, numberOfPages - 1));

                }
            }

            for (i = 1; i <= numberOfPages; i++) {
                gap = isStartOfGaps(i, gaps);
                if (gap && gap.end > i) {
                    gapPage = i - 1 < scope.currentPage ? gap.end : i;
                    scope.pages.push(makePage(gapPage, "...", false, false));
                    i = gap.end;
                } else {
                    scope.pages.push(makePage(i, i, isActive(i - 1), false));
                }

            }

        });

        function isActive(number) {
            return number === scope.currentPage;
        }

        scope.changePage = function (pageNumber) {
            if (pageNumber >= 0 && pageNumber < scope.numberOfPages) {
                scope.currentPage = Number(pageNumber);
            }
        };
    }

    function Page(type, content) {
        this.type = type;
        this.content = type === "placeholder" ? "..." : content;
    }

    function makePage(number, text, isActive, isDisabled) {
        return {
            number: number,
            text: text,
            active: isActive,
            disabled: isDisabled
        };
    }

    function makeGap(start, end) {
        return {
            start: start,
            end: end
        };
    }

    function isStartOfGaps(number, gapArray) {
        var i, max_i;

        for (i = 0, max_i = gapArray.length; i < max_i; i++) {
            if (gapArray[i].start === number) {
                return gapArray[i];
            }
        }
        return false;
    }
}

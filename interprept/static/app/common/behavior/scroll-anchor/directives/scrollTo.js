angular
    .module("co.scroll-anchor")
    .directive("scrollTo", scrollToDirective);

/* @ngInject */
function scrollToDirective(ScrollService) {

    var directive = {
        restrict: "A",
        scope: {
            scrollTo: "="
        },
        link: linkFunction,
        transclude: true,
        template: "<div ng-transclude></div>"
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        var id = attributes.id,
            markType = attributes.markType,
            el = element[0],
            scrollCont = $(el);

        if (!id) {
            el = document.body;
            scrollCont = $("html, body");
        }
        scope.$watch("scrollTo", function (id) {
            if (!id) {
                return;
            }
            ScrollService.scrollTo(el, scrollCont, id, markType);
        });
    }
}

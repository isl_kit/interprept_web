angular
    .module("co.scroll-anchor")
    .factory("ScrollService", ScrollService);

/* @ngInject */
function ScrollService($q) {

    var service = {
        scrollTo: scrollTo
    };

    return service;

    //////////

    function scrollTo(container, scrollCont, childId, markType) {
        getElementWithId(childId).then(function (child) {
            scroll(child, container, scrollCont, markType);
        }, function (message) {
            console.log(message);
        });
    }

    function scroll(child, container, scrollCont, markType) {
        var top = 0,
            left = 0,
            duration,
            ch = child;

        while (ch !== null && ch !== container) {
            top = top + ch.offsetTop;
            left = left + ch.offsetLeft;
            ch = ch.offsetParent;
        }

        duration = Math.abs($(container).scrollTop() - top + 100);
        if (duration > 600) {
            duration = 600;
        }

        scrollCont.animate({scrollTop: top - 100}, duration , function () {
            if (markType === "flash") {
                flash(child);
            } else {
                echolot(container, child, top);
            }
        });
    }

    function echolot(container, child, top) {
        var left = 0,
            ch = child,
            childWidth = parseInt($(child).css("width"), 10),
            containerWidth = parseInt($(container).css("width"), 10),
            startLeft;

        while (ch !== null && ch !== container) {
            left = left + ch.offsetLeft;
            ch = ch.offsetParent;
        }

        $(container).append("<div id='scroll-selector'></div>");

        startLeft = Math.min(left + childWidth / 2, (left + containerWidth) / 2);

        $(container).find("#scroll-selector").css("left", startLeft + "px");
        $(container).find("#scroll-selector").css("top", (top + parseInt($(child).css("height"), 10) / 2) + "px");

        $(container).find("#scroll-selector").animate({
            height: "200px",
            width: "200px",
            left: startLeft - 100,
            top: top + parseInt($(child).css("height"), 10) / 2 - 100,
            borderRadius: "100px"
        }, 600, function () {
            $(container).find("#scroll-selector").remove();
        });
    }

    function flash(element) {
        var opacity = 1, intervalKey;

        intervalKey = setInterval(function () {
            $(element).css("background-color", "rgba(0,166,255," + opacity + ")");
            opacity -= 0.01;
            if (opacity <= 0) {
                $(element).css("background-color", "");
                clearInterval(intervalKey);
            }
        }, 10);
    }

    function getElementWithId(elementId) {
        var deferred = $q.defer(),
            intervalKey,
            counter = 0, maxIterations = 20;


        intervalKey = setInterval(function () {
            var element = document.getElementById(elementId);
            if (element) {
                deferred.resolve(element);
                clearInterval(intervalKey);
            } else if (counter >= maxIterations) {
                deferred.reject("no element found (" + elementId + ")");
                clearInterval(intervalKey);
            }
            counter++;
        }, 100);

        return deferred.promise;
    }
}

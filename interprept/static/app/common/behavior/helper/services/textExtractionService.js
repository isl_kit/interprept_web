angular
    .module("co.helper")
    .factory("TextExtractionService", TextExtractionService);

/* @ngInject */
function TextExtractionService() {

    var service = {
        getNeighborhood: getNeighborhood
    };

    return service;

    //////////

    function getNeighborhood(text, index, length, span) {
        var whitespaceRegex = /\s/,
            postFix = "", preFix = "",
            fullLength = text.length,
            start = index - span,
            end = index + length + span;

        if (start < 0) {
            end -= start;
            start = 0;
        }
        if (end > fullLength - 1) {
            start -= (end - fullLength + 1);
            end = fullLength - 1;
        }

        while (start > 0 && !whitespaceRegex.test(text.charAt(start - 1))) {
            start--;
        }

        while (end < fullLength - 1 && !whitespaceRegex.test(text.charAt(end + 1))) {
            end++;
        }

        if (end < fullLength - 1) {
            postFix = "...";
        }
        if (start > 0) {
            preFix = "...";
        }

        return {
            pre: preFix + text.substring(start, index),
            actual: text.substring(index, index + length),
            post: text.substring(index + length, end + 1) + postFix
        };
    }
}

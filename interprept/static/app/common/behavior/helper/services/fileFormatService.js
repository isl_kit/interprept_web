angular
    .module("co.helper")
    .factory("FileFormatService", FileFormatService);

/* @ngInject */
function FileFormatService() {

    var tags = ["Bytes", "kB", "MB", "GB", "TB", "PB"];

    var service = {
        formatFileSize: formatFileSize
    };

    return service;

    //////////

    function formatFileSize(bytes) {
        var counter = 0;

        while (bytes > 1000) {
            if (counter === tags.length - 1) {
                break;
            }
            bytes = bytes / 1024;
            counter++;
        }
        return (Math.round(bytes * 100) / 100).toString().replace(/\.[0]+$/,"") + " " + tags[counter];
    }
}

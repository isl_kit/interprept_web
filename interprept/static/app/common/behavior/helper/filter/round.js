angular
    .module("co.helper")
    .filter("round", roundFilter);

/* @ngInject */
function roundFilter() {

    return round;

    //////////

    function round(number, digets) {
        var mult = Math.pow(10, digets),
            roundNumber = (Math.round(number * mult) / mult).toString(),
            realDigets = 0,
            index = roundNumber.lastIndexOf(".");

        if (index > -1) {
            realDigets = roundNumber.substring(index + 1).length;
        } else {
            roundNumber += ".";
        }
        return roundNumber + new Array(digets - realDigets + 1).join("0");
    }
}

angular
    .module("co.helper")
    .filter("formatFileSize", formatFileSizeFilter);

/* @ngInject */
function formatFileSizeFilter(FileFormatService) {
    return FileFormatService.formatFileSize;
}

angular
    .module("co.responsive")
    .factory("ResponsiveService", ResponsiveService);

/* @ngInject */
function ResponsiveService($rootScope, breakPoints) {
    var elements = [],
        currentArea = null;

    var service = {
        addElement: addElement
    };

    activate();

    return service;

    //////////

    function activate() {
        window.onresize = resize;
        window.addEventListener("load", resize, false);
    }

    function addElement(element) {
        elements.push(element);
        updateElement(element);
    }

    function resize() {
        var i, k, max_k, element, size = getWindowSize(), area = 0, breakPoint, width = size.width, eventObj;

        for (k = 0, max_k = breakPoints.length; k < max_k; k++) {
            breakPoint = breakPoints[k];
            if (width < breakPoint) {
                area++;
            } else {
                break;
            }
        }

        if (area !== currentArea) {
            eventObj = { from: currentArea, to: area };
            $rootScope.$broadcast("beforeAreaChanged", eventObj);
            currentArea = area;
            for (i = elements.length - 1; i >= 0; i--) {
                element = elements[i];
                if (!element) {
                    elements.splice(i, 1);
                } else {
                    updateElement(element);
                }
            }
            $rootScope.$broadcast("afterAreaChanged", eventObj);
        }
    }

    function updateElement(element) {
        var k, max_k;

        for (k = 0; k <= currentArea; k++) {
            element.addClass("r" + k);
        }
        for (k = currentArea + 1, max_k = breakPoints.length; k <= max_k; k++) {
            element.removeClass("r" + k);
        }
    }

    function getWindowSize() {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight|| e.clientHeight|| g.clientHeight;

        return {
            width: w.innerWidth || e.clientWidth || g.clientWidth,
            height: w.innerHeight|| e.clientHeight|| g.clientHeight
        };
    }
}

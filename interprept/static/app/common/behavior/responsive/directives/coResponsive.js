angular
    .module("co.responsive")
    .directive("coResponsive", responsiveDirective);

/* @ngInject */
function responsiveDirective(ResponsiveService) {

    var directive = {
        restrict: "C",
        link: linkFunction
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        ResponsiveService.addElement(element);
    }
}

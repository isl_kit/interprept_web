angular
    .module("co.focus")
    .factory("FocusService", FocusService);

/* @ngInject */
function FocusService() {

    var service = {
        focus: focus
    };

    return service;

    //////////

    function focus(id) {
        $("#" + id).focus();
        $("#" + id).select();
    }
}

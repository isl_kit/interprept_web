angular
    .module("co.upload")
    .directive("coUpload", uploadDirective);

/* @ngInject */
function uploadDirective($parse, $cookies) {
    var directive = {
        restrict: "C",
        link: linkFunction
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        var fn = $parse(attributes.test);
        var t = fn(scope);
        var add_func = $parse(attributes.add),
            progress_func = $parse(attributes.progress),
            done_func = $parse(attributes.done);

        element.fileupload({
            url: "/documents/upload/",
            dataType: "json",
            add: function (e, data) {
                scope.$apply(function () {
                    add_func(scope, { e: e, data: data });
                });
            },
            done: function (e, data) {
                scope.$apply(function () {
                    done_func(scope, { e: e, data: data });
                });
            },
            progress: function (e, data) {
                scope.$apply(function () {
                    progress_func(scope, { e: e, data: data });
                });
            }
        });
    }
}

angular
    .module("co.active-on-navigation")
    .directive("activeOnNavigation", activeOnNavigation);

/* @ngInject */
function activeOnNavigation($state) {

    var directive = {
        restrict: "A",
        scope: {
            equalStateLimit: "="
        },
        link: linkFunction
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        var sref = attributes.uiSref;

        if (sref) {
            updateElement(element, sref, $state.current.name, scope.equalStateLimit);
            scope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
                updateElement(element, sref, toState.name, scope.equalStateLimit);
            });
        }
    }

    function checkStateEquality(parentState, childState, limit) {
        var childArray = childState.split("."),
            parentArray = parentState.split("."),
            maxLength = parentArray.length;

        if (limit && limit < maxLength) {
            maxLength = limit;
        }

        childArray.length = maxLength;
        parentArray.length = maxLength;

        childState = childArray.join(".");
        parentState = parentArray.join(".");

        if (parentState === childState) {
            return true;
        }
        return false;
    }

    function updateElement(element, parentState, childState, limit) {
        if (checkStateEquality(parentState, childState, limit)) {
            element.addClass("active");
        } else {
            element.removeClass("active");
        }
    }
}

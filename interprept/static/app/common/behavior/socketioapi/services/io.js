angular
    .module("co.socketioapi")
    .factory("io", ioService);

/* @ngInject */
function ioService($http) {

    var socket = null,
        connectHandler = [],
        joined = [];

    var service = {
        join: join,
        leave: leave,
        onConnect: onConnect,
        on: on,
        off: off
    };

    activate();

    return service;

    //////////

    function activate() {
        if (typeof io === "function" && typeof SOCKETIO_CONF === "object") {
            console.log("try connect to", SOCKETIO_CONF.url);
            socket = io.connect(SOCKETIO_CONF.url, {});
        }
        if (socket) {
            socket.on("connect", function () {
                console.log("CONNECTED TO: " + SOCKETIO_CONF.url);
                _.each(connectHandler, function (handler) {
                    handler();
                });
                _.each(joined, function (room) {
                    post_to_server("join", {room: room, sessionid: socket.id});
                });
            });
        }
    }

    function post_to_server(path, data) {
        var url = "/" + SOCKETIO_CONF.path_prefix + path + "/";
        $http.post(url, data);
    }


    function join(room) {
        if (!socket || _.contains(joined, room)) {
            return;
        }
        joined.push(room);
        if (socket.connected) {
            post_to_server("join", {room: room, sessionid: socket.id});
        }
    }

    function leave(room) {
        if (!socket || !_.contains(joined, room)) {
            return;
        }
        post_to_server("leave", {room: room, sessionid: socket.id});
        joined = _.without(joined, room);
    }

    function onConnect (handler) {
        connectHandler.push(handler);
    }

    function on () {
        if (!socket) {
            return;
        }
        socket.on.apply(socket, arguments);
    }

    function off () {
        if (!socket) {
            return;
        }
        socket.off.apply(socket, arguments);
    }
}

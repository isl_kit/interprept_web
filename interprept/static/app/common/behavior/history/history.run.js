angular
    .module("co.history")
    .run(historyRun);

/* @ngInject */
function historyRun($rootScope, HistoryService) {
    $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
        HistoryService.add(toState.name, toParams);
    });
}

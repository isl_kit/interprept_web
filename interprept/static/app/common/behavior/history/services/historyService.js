angular
    .module("co.history")
    .factory("HistoryService", HistoryService);

/* @ngInject */
function HistoryService($state) {

    var history = [{
            name: "documents.browser",
            params: {}
        }],
        index = 0;

    var service = {
        add: add,
        back: back,
        forward: forward
    };

    return service;

    //////////

    function add(name, params) {
        history = history.slice(0, index + 1);
        history.push({
            name: name,
            params: params
        });
        index = history.length - 1;
    }

    function back() {
        if (index > 0) {
            index -= 1;
            $state.go(history[index].name, history[index].params);
        }
    }

    function forward() {
        if (index < history.length - 1) {
            index += 1;
            $state.go(history[index].name, history[index].params);
        }
    }
}

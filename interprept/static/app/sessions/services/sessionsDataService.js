angular
    .module("app.sessions")
    .factory("SessionsDataService", SessionsDataService);

/* @ngInject */
function SessionsDataService($http, Session, Organ, CommitteeMeeting) {

    var service = {
        getDays: getDays,
        getOrgans: getOrgans,
        getCommitteeDays: getCommitteeDays,
        getCommitteesOfDay: getCommitteesOfDay,
        getSessionsOfDay: getSessionsOfDay
    };

    return service;

    //////////

    function getDays(year) {
        return $http.get("/speeches/getDays/?year=" + year).then(function (response) {
            return response.data;
        });
    }

    function getOrgans() {
        return $http.get("/speeches/getOrgans/").then(function (response) {
            return response.data.organs.map(Organ.map);
        });
    }

    function getCommitteeDays(organ, year) {
        return $http.get("/speeches/getCommitteeDays/?year=" + year + "&organ_id=" + organ.id).then(function (response) {
            return response.data;
        });
    }

    function getCommitteesOfDay(organ, date) {
        return $http.get("/speeches/getCommitteesOfDay/?date=" + date + "&organ_id=" + organ.id).then(function (response) {
            return response.data.committee_meetings.map(CommitteeMeeting.map);
        });
    }

    function getSessionsOfDay(date) {
        return $http.get("/speeches/getSessionsOfDay/?date=" + date).then(function (response) {
            return response.data.sessions.map(Session.map);
        });
    }
}

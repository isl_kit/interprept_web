angular
    .module("app.sessions")
    .controller("CommitteeSelectDaysModalController", CommitteeSelectDaysModalController);

/* @ngInject */
function CommitteeSelectDaysModalController($scope, $filter, organ, daysData, parlDocIds, ModalService) {

    var vm = this;

    var dates = daysData.dates.map(function (date) {
        date = new Date(Date.parse(date));
        return {
            "date": date,
            "display": $filter("date")(date, "dd"),
            "link": $filter("date")(date, "yyyy-MM-dd")
        };
    });

    vm.numberOfParlDocs = 0;
    vm.year = daysData.year;
    vm.months = [
        {"name": "JAN", "dates": filterDatesForMonth(dates, 0)},
        {"name": "FEB", "dates": filterDatesForMonth(dates, 1)},
        {"name": "MAR", "dates": filterDatesForMonth(dates, 2)},
        {"name": "APR", "dates": filterDatesForMonth(dates, 3)},
        {"name": "MAY", "dates": filterDatesForMonth(dates, 4)},
        {"name": "JUN", "dates": filterDatesForMonth(dates, 5)},
        {"name": "JUL", "dates": filterDatesForMonth(dates, 6)},
        {"name": "AUG", "dates": filterDatesForMonth(dates, 7)},
        {"name": "SEP", "dates": filterDatesForMonth(dates, 8)},
        {"name": "OCT", "dates": filterDatesForMonth(dates, 9)},
        {"name": "NOV", "dates": filterDatesForMonth(dates, 10)},
        {"name": "DEC", "dates": filterDatesForMonth(dates, 11)}
    ];
    vm.closeModal = closeModal;
    vm.selectDay = selectDay;
    vm.addCommdocs = addCommdocs;
    vm.goToYear = goToYear;
    vm.goBackToOrgans = goBackToOrgans;

    activate();

    //////////

    function activate() {
        parlDocIds = parlDocIds || [];
        vm.numberOfParlDocs = parlDocIds.length;
    }

    function filterDatesForMonth(dates, month) {
        return dates.filter(function (date) {
            return date.date.getMonth() === month;
        });
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function selectDay(dateString) {
        ModalService.open("commdocs@committeeSelect", { date: dateString, organ: organ, parlDocIds: parlDocIds });
    }

    function addCommdocs() {
        $scope.$$closeModal(parlDocIds);
    }

    function goToYear(year) {
        ModalService.open("days@committeeSelect", { year: year, organ: organ, parlDocIds: parlDocIds });
    }

    function goBackToOrgans() {
        ModalService.open("organs@committeeSelect", { parlDocIds: parlDocIds });
    }
}

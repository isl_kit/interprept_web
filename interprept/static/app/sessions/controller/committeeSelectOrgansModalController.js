angular
    .module("app.sessions")
    .controller("CommitteeSelectOrgansModalController", CommitteeSelectOrgansModalController);

/* @ngInject */
function CommitteeSelectOrgansModalController($scope, parlDocIds, organs, ModalService) {

    var vm = this;

    vm.numberOfParlDocs = 0;
    vm.organs = organs;
    vm.closeModal = closeModal;
    vm.addCommdocs = addCommdocs;
    vm.selectOrgan = selectOrgan;

    activate();

    //////////

    function activate() {
        parlDocIds = parlDocIds || [];
        vm.numberOfParlDocs = parlDocIds.length;
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function addCommdocs() {
        $scope.$$closeModal(parlDocIds);
    }

    function selectOrgan(organ) {
        ModalService.open("days@committeeSelect", { organ: organ, year: (new Date()).getFullYear(), parlDocIds: parlDocIds });
    }
}

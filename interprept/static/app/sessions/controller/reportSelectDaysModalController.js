angular
    .module("app.sessions")
    .controller("ReportSelectDaysModalController", ReportSelectDaysModalController);

/* @ngInject */
function ReportSelectDaysModalController($scope, $filter, daysData, parlDocIds, ModalService) {

    var vm = this;

    var dates = daysData.dates.map(function (date) {
        date = new Date(Date.parse(date));
        return {
            "date": date,
            "display": $filter("date")(date, "dd"),
            "link": $filter("date")(date, "yyyy-MM-dd")
        };
    });

    vm.numberOfParlDocs = 0;
    vm.year = daysData.year;
    vm.months = [
        {"name": "JAN", "dates": filterDatesForMonth(dates, 0)},
        {"name": "FEB", "dates": filterDatesForMonth(dates, 1)},
        {"name": "MAR", "dates": filterDatesForMonth(dates, 2)},
        {"name": "APR", "dates": filterDatesForMonth(dates, 3)},
        {"name": "MAY", "dates": filterDatesForMonth(dates, 4)},
        {"name": "JUN", "dates": filterDatesForMonth(dates, 5)},
        {"name": "JUL", "dates": filterDatesForMonth(dates, 6)},
        {"name": "AUG", "dates": filterDatesForMonth(dates, 7)},
        {"name": "SEP", "dates": filterDatesForMonth(dates, 8)},
        {"name": "OCT", "dates": filterDatesForMonth(dates, 9)},
        {"name": "NOV", "dates": filterDatesForMonth(dates, 10)},
        {"name": "DEC", "dates": filterDatesForMonth(dates, 11)}
    ];
    vm.closeModal = closeModal;
    vm.selectDay = selectDay;
    vm.addReports = addReports;
    vm.goToYear = goToYear;

    activate();

    //////////

    function activate() {
        parlDocIds = parlDocIds || [];
        vm.numberOfParlDocs = parlDocIds.length;
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function selectDay(dateString) {
        ModalService.open("procedures@reportSelect", { date: dateString, parlDocIds: parlDocIds });
    }

    function addReports() {
        $scope.$$closeModal(parlDocIds);
    }

    function goToYear(year) {
        ModalService.open("days@reportSelect", { year: year, parlDocIds: parlDocIds });
    }

    function filterDatesForMonth(dates, month) {
        return dates.filter(function (date) {
            return date.date.getMonth() === month;
        });
    }
}

angular
    .module("app.sessions")
    .controller("CommitteeSelectDocsModalController", CommitteeSelectDocsModalController);

/* @ngInject */
function CommitteeSelectDocsModalController($scope, organ, commMeetings, date, parlDocIds, ModalService) {

    var vm = this;

    vm.commMeetings = commMeetings;
    vm.date = new Date(date);
    vm.numberOfParlDocs = parlDocIds.length;
    vm.toggle = toggle;
    vm.closeModal = closeModal;
    vm.addCommdocs = addCommdocs;
    vm.goBackToYear = goBackToYear;

    activate();

    //////////

    function activate() {
        _.each(vm.commMeetings, function (commMeeting) {
            commMeeting.parlDocuments = commMeeting.parlDocuments.map(function (parlDocument) {
                return _.extend({
                    selected: !!_.find(parlDocIds, function (parlDocId) {
                        return parlDocId === parlDocument.id;
                    })
                }, parlDocument);
            });
        });
    }

    function toggle(parlDocument) {
        parlDocument.selected = !parlDocument.selected;
        if (parlDocument.selected) {
            parlDocIds.push(parlDocument.id);
        } else {
            parlDocIds = _.without(parlDocIds, parlDocument.id);
        }
        vm.numberOfParlDocs = parlDocIds.length;
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function addCommdocs() {
        $scope.$$closeModal(parlDocIds);
    }

    function goBackToYear() {
        ModalService.open("days@committeeSelect", { year: vm.date.getFullYear(), organ: organ, parlDocIds: parlDocIds });
    }
}

angular
    .module("app.sessions")
    .controller("ReportSelectProceduresModalController", ReportSelectProceduresModalController);

/* @ngInject */
function ReportSelectProceduresModalController($scope, sessions, date, parlDocIds, ModalService) {

    var vm = this;

    vm.date = new Date(date);
    vm.numberOfParlDocs = parlDocIds.length;
    vm.ItemType = {
        GROUP: 0,
        PROCEDURE: 1
    };
    vm.sessions = sessions.map(makeSessionItem);
    vm.toggle = toggle;
    vm.stopPropagation = stopPropagation;
    vm.toggleReportDetails = toggleReportDetails;
    vm.closeModal = closeModal;
    vm.addReports = addReports;
    vm.goBackToYear = goBackToYear;

    //////////

    function makeSessionItem(session) {
        var items = [],
            groupsHash = {};
        _.each(session.procedures, function (procedure) {
            var groupItem;

            if (procedure.group) {
                if (!groupsHash[procedure.group.id]) {
                    groupsHash[procedure.group.id] = makeGroupItem(procedure.group);
                    items.push(groupsHash[procedure.group.id]);
                }
                groupItem = groupsHash[procedure.group.id];
            }
            items.push(makeProcedureItem(procedure, groupItem));
        });
        _.each(items, function (item) {
            if (item.type === vm.ItemType.GROUP) {
                item.selectable = item.procedures.filter(function (procedureItem) {
                    return procedureItem.selectable;
                }).length > 0;
                if (item.selectable) {
                    updateGroupCheck(item);
                }
            }
        });
        return {
            id: session.id,
            date: session.date,
            startTime: session.startTime,
            endTime: session.endTime,
            title: session.title,
            items: items
        };
    }

    function makeGroupItem(group) {
        return {
            id: group.id,
            title: group.title,
            type: vm.ItemType.GROUP,
            procedures: [],
            resources: []
        };
    }

    function makeProcedureItem(procedure, groupItem) {
        var procedureItem, parlDocs;
        parlDocs = procedure.parlDocuments.map(function (parlDocument) {
            return _.extend({
                selected: _.contains(parlDocIds, parlDocument.id)
            }, parlDocument);
        });
        procedureItem = {
            id: procedure.id,
            title: procedure.title,
            group: groupItem,
            isChild: !!groupItem,
            authors: procedure.getAuthorNames(),
            organs: procedure.getOrganLabels(),
            type: vm.ItemType.PROCEDURE,
            showReportDetails: false,
            parlDocuments: parlDocs,
            report: _.find(parlDocs, function (parlDocument) {
                return parlDocument.language.tag.toLowerCase() === "en";
            })
        };
        if (groupItem) {
            groupItem.procedures.push(procedureItem);
        }
        return procedureItem;
    }

    function toggle(parlDocument) {
        parlDocument.selected = !parlDocument.selected;

        if (parlDocument.selected) {
            parlDocIds.push(parlDocument.id);
        } else {
            parlDocIds = _.without(parlDocIds, parlDocument.id);
        }
        vm.numberOfParlDocs = parlDocIds.length;
    }

    function stopPropagation($event) {
        $event.stopPropagation();
    }

    function toggleReportDetails(item) {
        item.showReportDetails = !item.showReportDetails;
    }

    function closeModal() {
        $scope.$$closeModal();
    }

    function addReports() {
        $scope.$$closeModal(parlDocIds);
    }

    function goBackToYear() {
        ModalService.open("days@reportSelect", { year: vm.date.getFullYear(), parlDocIds: parlDocIds });
    }
}

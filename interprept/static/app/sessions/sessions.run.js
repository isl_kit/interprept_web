angular
    .module("app.sessions")
    .run(sessionsRun);

/* @ngInject */
function sessionsRun(ModalService) {

    ModalService.register({
        name: "reportSelect",
        states: {
            days: {
                templateUrl: "sessions/templates/report-select-days-modal.tpl.html",
                controller: "ReportSelectDaysModalController",
                controllerAs: "vm",
                params: ["year", "parlDocIds", "promise"],
                resolve: {
                    "daysData": ["SessionsDataService", "year", function (SessionsDataService, year) {
                        return SessionsDataService.getDays(year);
                    }]
                }
            },
            procedures: {
                templateUrl: "sessions/templates/report-select-procedures-modal.tpl.html",
                controller: "ReportSelectProceduresModalController",
                controllerAs: "vm",
                params: ["parlDocIds", "date", "promise"],
                resolve: {
                    "sessions": ["SessionsDataService", "date", function (SessionsDataService, date) {
                        return SessionsDataService.getSessionsOfDay(date);
                    }]
                }
            }
        }
    });

    ModalService.register({
        name: "committeeSelect",
        states: {
            organs: {
                templateUrl: "sessions/templates/committee-select-organs-modal.tpl.html",
                controller: "CommitteeSelectOrgansModalController",
                controllerAs: "vm",
                params: ["parlDocIds"],
                resolve: {
                    "organs": ["SessionsDataService", function (SessionsDataService) {
                        return SessionsDataService.getOrgans();
                    }]
                }
            },
            days: {
                templateUrl: "sessions/templates/committee-select-days-modal.tpl.html",
                controller: "CommitteeSelectDaysModalController",
                controllerAs: "vm",
                params: ["organ", "year", "parlDocIds"],
                resolve: {
                    "daysData": ["SessionsDataService", "year", "organ", function (SessionsDataService, year, organ) {
                        return SessionsDataService.getCommitteeDays(organ, year);
                    }]
                }
            },
            commdocs: {
                templateUrl: "sessions/templates/committee-select-docs-modal.tpl.html",
                controller: "CommitteeSelectDocsModalController",
                controllerAs: "vm",
                params: ["organ", "parlDocIds", "date"],
                resolve: {
                    "commMeetings": ["SessionsDataService", "date", "organ", function (SessionsDataService, date, organ) {
                        return SessionsDataService.getCommitteesOfDay(organ, date);
                    }]
                }
            }
        }
    });
}

angular
    .module("app.sessions")
    .factory("ProcedureGroup", ProcedureGroupModel);

/* @ngInject */
function ProcedureGroupModel() {

    function ProcedureGroup(data) {
        this.id = data.id;
        this.title = data.title;
    }
    ProcedureGroup.map = map;

    return ProcedureGroup;

    //////////

    function map(groupDb) {
        return new ProcedureGroup({
            id: groupDb.id,
            title: groupDb.title
        });
    }
}

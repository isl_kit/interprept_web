angular
    .module("app.sessions")
    .factory("Session", SessionModel);

/* @ngInject */
function SessionModel($q, Procedure) {

    function Session(data) {
        this.id = data.id;
        this.date = new Date(data.date);
        this.startTime = new Date(data.startTime);
        this.endTime = new Date(data.endTime);
        this.title = data.title;
        this.procedures = data.procedures;

        activate(this);
    }
    Session.map = map;

    return Session;

    //////////

    function activate(instance) {
        instance.procedures.sort(function (a, b) {
            return a.position - b.position;
        });
    }

    function map(sessionDb) {
        return new Session({
            id: sessionDb.id,
            title: sessionDb.title,
            date: sessionDb.date,
            startTime: sessionDb.start_time,
            endTime: sessionDb.end_time,
            procedures: sessionDb.procedures.map(function (procedureDb) {
                return Procedure.map(procedureDb);
            })
        });
    }
}

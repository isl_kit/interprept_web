angular
    .module("app.sessions")
    .factory("Organ", OrganModel);

/* @ngInject */
function OrganModel() {

    function Organ(data) {
        this.id = data.id;
        this.name = data.name;
        this.label = data.label;
    }
    Organ.map = map;

    return Organ;

    //////////

    function map(organDb) {
        return new Organ({
            id: organDb.id,
            name: organDb.name,
            label: organDb.label
        });
    }
}

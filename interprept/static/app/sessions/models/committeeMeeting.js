angular
    .module("app.sessions")
    .factory("CommitteeMeeting", CommitteeMeetingModel);

/* @ngInject */
function CommitteeMeetingModel(Organ, ParlDocument) {

    function CommitteeMeeting(data) {
        this.id = data.id;
        this.title = data.title;
        this.date = new Date(data.date);
        this.organ = data.organ;
        this.agendaNo = data.agendaNo;
        this.ref = data.ref;
        this.parlDocuments = data.parlDocuments;
        this.docType = data.docType;
    }
    CommitteeMeeting.map = map;

    return CommitteeMeeting;

    //////////

    function map(committeeMeetingDb) {
        return new CommitteeMeeting({
            id: committeeMeetingDb.id,
            title: committeeMeetingDb.title,
            organ: Organ.map(committeeMeetingDb.organ),
            date: committeeMeetingDb.date,
            agendaNo: committeeMeetingDb.agenda_no,
            ref: committeeMeetingDb.ref,
            parlDocuments: committeeMeetingDb.parl_documents.map(ParlDocument.map),
            docType: committeeMeetingDb.doc_type
        });
    }
}

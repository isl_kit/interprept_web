angular
    .module("app.sessions")
    .factory("Procedure", ProcedureModel);

/* @ngInject */
function ProcedureModel($q, ParlDocument, ProcedureGroup, Person, Organ) {

    function Procedure(data) {
        this.id = data.id;
        this.created = new Date(Date.parse(data.created));
        this.modified = new Date(Date.parse(data.modified));
        this.title = data.title;
        this.position = data.position;
        this.parlDocuments = data.parlDocuments;
        this.group = data.group;
        this.authors = data.authors;
        this.organs = data.organs;
        this.documents = data.documents || {};
        this.documentDeferred = {};
        this.session = data.session;
    }
    Procedure.map = map;
    Procedure.prototype.getAuthorNames = getAuthorNames;
    Procedure.prototype.getOrganLabels = getOrganLabels;
    Procedure.prototype.getFullOrganNames = getFullOrganNames;
    Procedure.prototype.addDocument = addDocument;
    Procedure.prototype.getDocument = getDocument;

    return Procedure;

    //////////

    function map(procedureDb, session) {
        return new Procedure({
            id: procedureDb.id,
            created: procedureDb.created,
            modified: procedureDb.modified,
            title: procedureDb.title,
            position: procedureDb.position,
            parlDocuments: procedureDb.parl_documents.map(ParlDocument.map),
            group: procedureDb.group ? ProcedureGroup.map(procedureDb.group) : null,
            authors: procedureDb.authors.map(function (personDb) {
                return Person.map(personDb);
            }),
            organs: procedureDb.organs.map(function (organDb) {
                return Organ.map(organDb);
            }),
            documents: {},
            session: session || null
        });
    }

    function getAuthorNames() {
        return this.authors.map(function (author) {
            return author.name;
        }).join(", ");
    }

    function getOrganLabels() {
        return this.organs.map(function (organ) {
            return organ.label;
        }).join(", ");
    }

    function getFullOrganNames() {
        return this.organs.map(function (organ) {
            return organ.name + " (" + organ.label + ")";
        }).join(", ");
    }

    function addDocument(userDocument) {
        var deferred;
        if (this.documents[userDocument.id]) {
            return this.documents[userDocument.id];
        } else {
            this.documents[userDocument.id] = userDocument;
            deferred = this.documentDeferred[userDocument.id];
            if (deferred) {
                _.each(deferred, function (def) {
                    def.resolve(userDocument);
                });
                delete this.documentDeferred[userDocument.id];
            }
            return userDocument;
        }
    }

    function getDocument(documentId) {
        var deferred = $q.defer(),
            resource = this.documents[documentId];
        if (documentId < 0) {
            deferred.resolve(null);
        } if (resource) {
            deferred.resolve(resource);
        } else {
            if (!this.documentDeferred[documentId]) {
                this.documentDeferred[documentId] = [];
            }
            this.documentDeferred[documentId].push(deferred);
        }
        return deferred.promise;
    }
}

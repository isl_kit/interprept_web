angular
    .module("app.sessions")
    .factory("ParlDocument", ParlDocumentModel);

/* @ngInject */
function ParlDocumentModel(Organ, Person, Resource, Language) {

    function ParlDocument(data) {
        this.id = data.id;
        this.title = data.title;
        this.organs = data.organs;
        this.authors = data.authors;
        this.docType = data.docType;
        this.published = new Date(data.published);
        this.a7No = data.a7No;
        this.peNo = data.peNo;
        this.fdrNo = data.fdrNo;
        this.resource = data.resource;
        this.language = data.language;
    }
    ParlDocument.map = map;

    return ParlDocument;

    //////////

    function map(parlDocDb) {
        return new ParlDocument({
            id: parlDocDb.id,
            title: parlDocDb.title,
            organs: parlDocDb.organs.map(Organ.map),
            authors: parlDocDb.authors.map(Person.map),
            docType: parlDocDb.doc_type,
            published: parlDocDb.published,
            a7No: parlDocDb.a7_no,
            peNo: parlDocDb.pe_no,
            fdrNo: parlDocDb.fdr_no,
            resource: Resource.map(parlDocDb.resource),
            language: Language.map(parlDocDb.language)
        });
    }
}

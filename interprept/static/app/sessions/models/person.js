angular
    .module("app.sessions")
    .factory("Person", PersonModel);

/* @ngInject */
function PersonModel() {

    function Person(data) {
        this.id = data.id;
        this.name = data.name;
    }
    Person.map = map;

    return Person;

    //////////

    function map(personDb) {
        return new Person({
            id: personDb.id,
            name: personDb.name
        });
    }
}

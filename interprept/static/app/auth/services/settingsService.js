angular
    .module("app.auth")
    .factory("SettingsService", SettingsService);

/* @ngInject */
function SettingsService($http) {

    var service = {
        getSettings: getSettings,
        setSettings: setSettings
    };
    return service;

    //////////

    function getSettings() {
        return $http.get("auth/getSettings/").then(function (response) {
            return {
                isSubscriber: response.data.is_subscriber,
                hasEnabledLogging: response.data.has_enabled_logging
            };
        });
    }

    function setSettings(isSubscriber, hasEnabledLogging) {
        var data = {
            is_subscriber: isSubscriber,
            has_enabled_logging: hasEnabledLogging
        };
        return $http.post("auth/setSettings/", data);
    }
}

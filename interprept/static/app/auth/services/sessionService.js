angular
    .module("app.auth")
    .factory("SessionService", SessionService);

/* @ngInject */ 
function SessionService($cookieStore) {

    var isActive = {
        localStorage: canUseLocalStorage(),
        sessionStorage: canUseSessionStorage()
    };

    var service = {
        get: getGet(),
        set: getSet(),
        unset: getUnset()
    };
    return service;

    //////////

    function canUseSessionStorage() {
        var storageTestKey = 'sTest',
            storage = window.sessionStorage;

        try {
          storage.setItem(storageTestKey, 'test');
          if (storage.getItem(storageTestKey) !== 'test') {
            return false;
          }
          storage.removeItem(storageTestKey);
        } catch (e) {
            return false;
        }
        return true;
    }

    function canUseLocalStorage() {
        var storageTestKey = 'sTest',
            storage = window.localStorage;

        try {
          storage.setItem(storageTestKey, 'test');
          if (storage.getItem(storageTestKey) !== 'test') {
            return false;
          }
          storage.removeItem(storageTestKey);
        } catch (e) {
            return false;
        }
        return true;
    }

    function getGet() {
        if (isActive.sessionStorage) {
            return function (key) {
                return sessionStorage.getItem(key);
            };
        } else if (isActive.localStorage) {
            return function (key) {
                return localStorage.getItem(key);
            };
        } else {
            return function (key) {
                return $cookieStore.get(key);
            };
        }
    }

    function getSet() {
        if (isActive.sessionStorage) {
            return function (key, value) {
                return sessionStorage.setItem(key, value);
            };
        } else if (isActive.localStorage) {
            return function (key, value) {
                return localStorage.setItem(key, value);
            };
        } else {
            return function (key, value) {
                return $cookieStore.put(key, value);
            };
        }
    }

    function getUnset() {
        if (isActive.sessionStorage) {
            return function (key) {
                return sessionStorage.removeItem(key);
            };
        } else if (isActive.localStorage) {
            return function (key) {
                return localStorage.removeItem(key);
            };
        } else {
            return function (key) {
                return $cookieStore.remove(key);
            };
        }
    }
}

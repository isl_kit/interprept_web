angular
    .module("app.auth")
    .factory("UserService", UserService);

/* @ngInject */
function UserService(User) {

    var service = {
        user: new User({ id: 0 }),
        setData: setData,
        clear: clear
    };
    return service;

    //////////

    function setData(userDb) {
        _.each(User.map(userDb), function (value, key) {
            service.user[key] = value;
        });
    }

    function clear() {
        service.user.id = 0;
        service.user.email = "";
        service.user.lastName = "";
        service.user.firstName = "";
        service.user.customization = {};
    }
}

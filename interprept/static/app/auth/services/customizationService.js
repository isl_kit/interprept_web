angular
    .module("app.auth")
    .factory("CustomizationService", CustomizationService);

/* @ngInject */
function CustomizationService($http, UserService) {

    var callbacks = {};

    var service = {
        set: set,
        get: get,
        addListener: addListener,
        removeListener: removeListener
    };
    return service;

    //////////

 
    function set(customization, extend) {
        _.each(customization, function (value, key) {
            if (value !== UserService.user.customization[key] && callbacks[key]) {
                _.each(callbacks[key], function (callback) {
                    callback(value, UserService.user.customization[key]);
                });
            }
            if (extend) {
                if (!UserService.user.customization[key]) {
                    UserService.user.customization[key] = {};
                }
                _.extend(UserService.user.customization[key], value);
            } else {
                UserService.user.customization[key] = value;
            }

        });
        return $http.post("/auth/setCustomization/", {customization: customization}).then(function (response) {
            return response.data;
        });
    }

    function get(key, defaultValue) {
        if (Object.prototype.hasOwnProperty.call(UserService.user.customization, key)) {
            return UserService.user.customization[key];
        } else {
            UserService.user.customization[key] = defaultValue;
            return defaultValue;
        }
    }

    function addListener(key, callback) {
        if (!callbacks[key]) {
            callbacks[key] = [];
        } else if (_.indexOf(callbacks[key], callback) > -1) {
            return;
        }
        callbacks[key].push(callback);
    }

    function removeListener(key, callback) {
        if (!callbacks[key]) {
            return;
        } else {
            callbacks[key] = _.without(callbacks[key], callback);
        }
    }
}

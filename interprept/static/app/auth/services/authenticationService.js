angular
    .module("app.auth")
    .factory("AuthenticationService", AuthenticationService);

/* @ngInject */
function AuthenticationService($http, $q, SessionService, UserService) {

    var logoutHandler = [];

    var service = {
        register: register,
        resetPassword: resetPassword,
        changePassword: changePassword,
        login: login,
        logout: logout,
        onLogout: onLogout,
        isLoggedIn: isLoggedIn
    };
    return service;

    //////////

    function cacheSession() {
        SessionService.set('authenticated', true);
    }

    function uncacheSession() {
        SessionService.unset('authenticated');
    }

    function sanitizeCredentials(credentials) {
        return {
            email: credentials.email,
            password: credentials.password
        };
    }

    function sanitizeUserinfo(userinfo) {
        return {
            email: userinfo.email,
            first_name: userinfo.firstName,
            last_name: userinfo.lastName,
            password1: userinfo.password,
            password2: userinfo.passwordConfirm,
            subscribe: userinfo.subscribe
        };
    }

    function sanitizePcData(pcData) {
        return {
            password_old: pcData.passwordOld,
            password_new1: pcData.passwordNew,
            password_new2: pcData.passwordNewRepeat
        };
    }

    function register(userinfo) {
        return $http.post("auth/register/", sanitizeUserinfo(userinfo));
    }

    function resetPassword(email) {
        return $http.post("auth/reset/", { email: email });
    }

    function changePassword(pcData) {
        return $http.post("auth/change/", sanitizePcData(pcData));
    }

    function login(credentials) {
        var loginPromise = $http.post("auth/login/", sanitizeCredentials(credentials));
        return loginPromise.then(function (response) {
            if (response.data.authenticated) {
                cacheSession();
                UserService.setData(response.data.user);
            }
            return response.data;
        });
    }

    function logout() {
        var logoutPromise = $http.get("auth/logout/");
        return logoutPromise.success(function (data, status) {
            uncacheSession();
            _.each(logoutHandler, function (handler) {
                handler(UserService.user.email);
            });
            UserService.clear();
        });
    }

    function onLogout(handler) {
        logoutHandler.push(handler);
    }

    function isLoggedIn() {
        var authenticated = SessionService.get('authenticated') === "true" ? true : false;

        if (!authenticated) {
            return $http.get("auth/loginState/").then(function (response) {
                authenticated = response.data.authenticated;
                if (authenticated) {
                    cacheSession();
                    UserService.setData(response.data.user);
                }
                return authenticated;
            });
        } else {
            if (!UserService.user.email) {
                $http.get('/auth/user/').success(function (userDb, status) {
                    UserService.setData(userDb);
                });
            }
        }
        return authenticated;
    }
}

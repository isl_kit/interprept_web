angular
    .module("app.auth")
    .config(authRoutes);

/* @ngInject */
function authRoutes($stateProvider) {
    $stateProvider.state("welcome", {
        url: "/welcome",
        views: {
            "main-content": {
                controller: "WelcomeController",
                controllerAs: "vm",
                templateUrl: "auth/templates/welcome.tpl.html"
            }
        },
        data: {
            pageTitle: "Welcome",
            topBarContent: "Login",
            sidebarDisabled: true
        }
    });

    $stateProvider.state("login", {
        url: "/login?verify&trial",
        views: {
            "main-content": {
                controller: "LoginController",
                controllerAs: "vm",
                templateUrl: "auth/templates/login.tpl.html"
            }
        },
        data: {
            pageTitle: "Login",
            topBarContent: "Register",
            sidebarDisabled: true
        }
    });

    if (window.GLOBAL_CONFIG && window.GLOBAL_CONFIG.SELF_REGISTRATION) {
        $stateProvider.state("register", {
            url: "/register",
            views: {
                "main-content": {
                    controller: "RegisterController",
                    controllerAs: "vm",
                    templateUrl: "auth/templates/register.tpl.html"
                }
            },
            data: {
                pageTitle: "Register",
                topBarContent: "Login",
                sidebarDisabled: true
            }
        });
    }

    $stateProvider.state("reset", {
        url: "/reset",
        views: {
            "main-content": {
                controller: "ResetController",
                controllerAs: "vm",
                templateUrl: "auth/templates/reset.tpl.html"
            }
        },
        data: {
            pageTitle: "Reset",
            topBarContent: "Login",
            sidebarDisabled: true
        }
    });

    $stateProvider.state("settings", {
        url: "/settings?firstLoginRedirect?passwordReset",
        views: {
            "main-content": {
                controller: "SettingsController",
                controllerAs: "vm",
                templateUrl: "auth/templates/settings.tpl.html"
            }
        },
        data: {
            pageTitle: "Settings",
            topBarContent: "Login",
            sidebarDisabled: true
        },
        resolve: {
            "settings": /* @ngInject */ function (SettingsService) {
                return SettingsService.getSettings();
            }
        }
    });
}

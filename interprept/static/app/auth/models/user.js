angular
    .module("app.auth")
    .factory("User", UserModel);

/* @ngInject */
function UserModel() {

    function User(data) {
        this.id = data.id;
        this.email = data.email || "";
        this.firstName = data.firstName || "";
        this.lastName = data.lastName || "";
        this.customization = data.customization || {};
    }
    User.map = map;

    return User;

    //////////

    function map(userDb) {
        return new User({
            id: userDb.id,
            email: userDb.email,
            firstName: userDb.firstname,
            lastName: userDb.lastname,
            customization: userDb.customization
        });
    }
}

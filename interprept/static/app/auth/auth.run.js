angular
    .module("app.auth")
    .run(authRun);

/* @ngInject */
function authRun(ModalService) {

    ModalService.register({
        name: "registerSuccess",
        states: {
            normal: {
                templateUrl: "auth/templates/register-success-modal.tpl.html",
                controller: "RegisterSuccessModalController",
                controllerAs: "vm",
                params: []
            }
        }
    });

    ModalService.register({
        name: "changePassword",
        states: {
            normal: {
                templateUrl: "auth/templates/change-password-modal.tpl.html",
                controller: "ChangePasswordModalController",
                controllerAs: "vm",
                params: ["afterReset"]
            }
        }
    });

    ModalService.register({
        name: "changePasswordSuccess",
        states: {
            normal: {
                templateUrl: "auth/templates/change-password-success-modal.tpl.html",
                controller: "ChangePasswordSuccessModalController",
                controllerAs: "vm",
                params: ["afterReset"]
            }
        }
    });

    ModalService.register({
        name: "firstLogin",
        states: {
            normal: {
                templateUrl: "auth/templates/first-login-modal.tpl.html",
                controller: "FirstLoginModalController",
                controllerAs: "vm",
                params: []
            }
        }
    });


    ModalService.register({
        name: "verificationSuccess",
        states: {
            normal: {
                templateUrl: "auth/templates/verification-success-modal.tpl.html",
                controller: "VerificationSuccessModalController",
                controllerAs: "vm",
                params: []
            }
        }
    });

    ModalService.register({
        name: "resetSuccess",
        states: {
            normal: {
                templateUrl: "auth/templates/reset-success-modal.tpl.html",
                controller: "ResetSuccessModalController",
                controllerAs: "vm",
                params: []
            }
        }
    });
}

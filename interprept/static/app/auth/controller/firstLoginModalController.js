angular
    .module("app.auth")
    .controller("FirstLoginModalController", FirstLoginModalController);

/* @ngInject */
function FirstLoginModalController($scope) {

    var vm = this;

    vm.close = close;

    //////////

    function close() {
        $scope.$$closeModal("ok");
    }
}
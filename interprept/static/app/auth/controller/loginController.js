angular
    .module("app.auth")
    .controller("LoginController", LoginController);

/* @ngInject */
function LoginController($rootScope, $state, $stateParams, ModalService, AuthenticationService) {

    var vm = this;

    vm.credentials = {
        email: "",
        password: ""
    };
    vm.loginFailed = !!$stateParams.trial;
    vm.login = login;

    activate();

    //////////

    function activate() {
        if ($stateParams.verify === "1") {
            ModalService.open("verificationSuccess");
        }
    }

    function login() {
        AuthenticationService.login(vm.credentials).then(function (data) {
            var passwordReset, loggingDecisionNeeded, newToWebdav;

            if (data.authenticated) {
                passwordReset = !!data.user.password_was_resetted;
                loggingDecisionNeeded = data.user.logging_decision_needed;
                newToWebdav = !!data.user.new_to_webdav;
                $rootScope.newToWebdav = newToWebdav;
                $rootScope.loggingDecisionNeeded = loggingDecisionNeeded;
                $rootScope.passwordReset = passwordReset;
                if (passwordReset) {
                    $state.go("settings");
                } else {
                    $state.go("documents.browser");
                }
            } else {
                vm.loginFailed = true;
            }
        }).finally(function () {
            vm.credentials.email = "";
            vm.credentials.password = "";
        });
    }
}

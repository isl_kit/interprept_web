angular
    .module("app.auth")
    .controller("WelcomeController", WelcomeController);

/* @ngInject */
function WelcomeController($state, AuthenticationService, ModalService) {

    var vm = this, origData;

    vm.registerInfo = {
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        passwordConfirm: "",
        subscribe: false
    };
    vm.registerError = 0;
    vm.showRegisterError = false;
    vm.register = register;

    activate();

    //////////

    function activate() {
        origData = angular.copy(vm.registerInfo);
    }

    function resetRegisterInfo() {
        vm.registerInfo = angular.copy(origData);
    }

    function register() {
        AuthenticationService.register(vm.registerInfo).then(function (data, status) {
            vm.showRegisterError = false;
            resetRegisterInfo();
            ModalService.open("registerSuccess");
        }, function (response) {
            vm.registerError = response.data.verr;
            vm.showRegisterError = true;
        });
    }
}

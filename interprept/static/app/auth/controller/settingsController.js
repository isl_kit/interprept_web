angular
    .module("app.auth")
    .controller("SettingsController", SettingsController);

/* @ngInject */
function SettingsController($state, $rootScope, settings, ModalService, SettingsService, HistoryService) {

    var vm = this;

    vm.settings = settings;
    vm.inProgress = false;
    vm.submit = submit;
    vm.discard = discard;
    vm.openChangePasswordModal = openChangePasswordModal;

    activate();

    //////////

    function activate() {
        if ($rootScope.passwordReset) {
            ModalService.open("changePassword", {"afterReset": true});
        }
    }

    function submit() {
        var isSubscriber, hasEnabledLogging;

        if (vm.inProgress) {
            return;
        }

        vm.inProgress = true;

        isSubscriber = vm.settings.isSubscriber;
        hasEnabledLogging = vm.settings.hasEnabledLogging;

        SettingsService.setSettings(isSubscriber, hasEnabledLogging).then(function () {
            vm.inProgress = false;
            redirect();
        });
    }

    function discard() {
        if (vm.inProgress) {
            return;
        }
        redirect();
    }

    function redirect() {
        HistoryService.back();
    }

    function openChangePasswordModal() {
        ModalService.open("changePassword");
    }
}
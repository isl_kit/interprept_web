angular
    .module("app.auth")
    .controller("RegisterSuccessModalController", RegisterSuccessModalController);

/* @ngInject */
function RegisterSuccessModalController($scope) {

    var vm = this;

    vm.close = close;

    //////////

    function close() {
        $scope.$$closeModal("ok");
    }
}
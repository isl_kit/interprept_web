angular
    .module("app.auth")
    .controller("ChangePasswordSuccessModalController", ChangePasswordSuccessModalController);

/* @ngInject */
function ChangePasswordSuccessModalController($scope) {

    var vm = this;

    vm.close = close;

    //////////

    function close() {
        $scope.$$closeModal("ok");
    }
}
angular
    .module("app.auth")
    .controller("VerificationSuccessModalController", VerificationSuccessModalController);

/* @ngInject */
function VerificationSuccessModalController($scope) {

    var vm = this;

    vm.close = close;

    //////////

    function close() {
        $scope.$$closeModal("ok");
    }
}

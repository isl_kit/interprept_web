angular
    .module("app.auth")
    .controller("ChangePasswordModalController", ChangePasswordModalController);

/* @ngInject */
function ChangePasswordModalController($scope, $rootScope, $state, ModalService, AuthenticationService, afterReset) {

    var vm = this;

    vm.pcData = {
        passwordOld: "",
        passwordNew: "",
        passwordNewRepeat: ""
    };
    vm.passwordChangeFailed = false;
    vm.passwordChangeError = 0;
    vm.afterReset = !!afterReset;
    vm.changePassword = changePassword;
    vm.close = close;

    //////////

    function changePassword() {
        AuthenticationService.changePassword(vm.pcData).then(function (response) {
            $scope.$$closeModal();
            $rootScope.passwordReset = false;
            ModalService.open("changePasswordSuccess", {"afterReset": vm.afterReset}).then(function () {
                if (vm.afterReset) {
                    $state.go("documents.browser");
                }

            });
        }, function (response) {
            vm.pcData.passwordOld = "";
            vm.pcData.passwordNew = "";
            vm.pcData.passwordNewRepeat = "";
            vm.passwordChangeError = response.data.verr;
            vm.passwordChangeFailed = true;
        });
    }

    function close() {
        $scope.$$closeModal();
    }
}

angular
    .module("app.auth")
    .controller("ResetController", ResetController);

/* @ngInject */
function ResetController($state, AuthenticationService, ModalService) {

    var vm = this;

    vm.email = "";
    vm.reset = reset;

    //////////

    function reset() {
        AuthenticationService.resetPassword(vm.email).then(function (response) {
            ModalService.open("resetSuccess").then(function () {
                $state.go("login");
            });
        });
    }
}

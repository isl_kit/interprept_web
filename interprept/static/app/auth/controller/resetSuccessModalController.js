angular
    .module("app.auth")
    .controller("ResetSuccessModalController", ResetSuccessModalController);

/* @ngInject */
function ResetSuccessModalController($scope) {

    var vm = this;

    vm.close = close;

    //////////

    function close() {
        $scope.$$closeModal("ok");
    }
}

angular.module("app.core", [
    /* Angular modules */
    "ngAnimate",
    "ngTouch",
    "ngCookies",
    "ngSanitize",

    /* Application configuration */
    "app.config",

    /* common: essentials */
    "co.exception",

    /* common: behaviour */
    "co.active-on-navigation",
    "co.focus",
    "co.helper",
    "co.history",
    "co.responsive",
    "co.scroll-anchor",
    "co.socketioapi",
    "co.upload",

    /* common: widgets */
    "co.checkbox",
    "co.menu",
    "co.modal",
    "co.pagination",
    "co.rating",
    "co.slider",
    "co.wait-icon",

    /* 3rd party modules */
    "ui.router",
    "chieffancypants.loadingBar"
]);

angular
    .module("app.ne")
    .directive("neDocumentViewer", neDocumentViewer);

/* @ngInject */
function neDocumentViewer(CustomizationService, UDocumentNE) {

    var directive = {
        restrict: "C",
        scope: {
            onNeClick: "&",
            selectedFocused: "=",
            selectedUid: "=",
            neList: "=",
            doc: "="
        },
        link: linkFunction
    };

    return directive;

    //////////

    function linkFunction(scope, element, attributes) {

        var fullText = "",
            userDocument = null,
            entities = [],
            searchText = "",
            selectedTypes = {},
            udoc = null;

        activate();

        //////////

        function activate() {
            selectedTypes = CustomizationService.get("ne.selected_types", {});
            CustomizationService.addListener("ne.selected_types", selectedTypesHandler);
            scope.$onRootScope("ne.search", searchHandler);
            scope.$watch("selectedTypes", selectedTypesWatcher);
            scope.$watch("searchText", searchTextWatcher);
            scope.$watch("selectedFocused", selectedFocusedWatcher);
            element.click(clickHandler);
            scope.doc.resource.getFullText().then(function (text) {
                fullText = text;
                createEntities();
                redrawFromScratch();
            });
        }

        function updateVisibility() {
            _.each(udoc.pages, function (upage) {
                _.each(upage.paragraphs, function (paragraph) {
                    _.each(paragraph.terms, function (uterm) {
                        uterm.setVisibility(getEntityVisibility(uterm.entity.type, uterm.entity.name));
                    });
                });
            });
        }

        function getEntityVisibility(typeId, name) {
            return selectedTypes[typeId] && (searchText.length === 0 || name.toLowerCase().indexOf(searchText.toLowerCase()) > -1);
        }

        function selectedTypesHandler(types) {
            selectedTypes = types;
            updateVisibility();
        }

        function searchHandler(ev, text) {
            searchText = text;
            updateVisibility();
        }

        function selectedTypesWatcher(newVal, oldVal) {
            if (newVal === oldVal || !userDocument) {
                return;
            }
            console.log("UPDATE");
            updateVisibility();
        }

        function searchTextWatcher(newVal, oldVal) {
            if (newVal === oldVal || !userDocument) {
                return;
            }
            console.log("UPDATE1");
            updateVisibility();
        }

        function selectedFocusedWatcher(newVal, oldVal) {
            if (newVal === oldVal || !userDocument) {
                return;
            }
            var uterms = [], index = 0;
            _.each(udoc.pages, function (upage) {
                _.each(upage.paragraphs, function (paragraph) {
                    _.each(paragraph.terms, function (uterm) {
                        if (uterm.selected) {
                            uterms.push(uterm);
                        }
                    });
                });
            });
            if (newVal < 0) {
                newVal = 0;
            } else if (newVal > uterms.length - 1) {
                newVal = uterms.length - 1;
            }
            scope.selectedUid = uterms[newVal].uid;
        }

        function clickHandler(ev) {
            var target = $(ev.target);
            if (target.hasClass("entity")) {
                var neId = target.data("neid");

                if (scope.onNeClick) {
                    scope.$apply(function () {
                        scope.onNeClick({ id: neId });
                    });
                }
            }
        }

        function neToEntities(nes) {
            var res = [];
            _.each(nes, function (ne) {
                res.push({
                    id: ne.id,
                    name: ne.name,
                    visible: getEntityVisibility(ne.neType.id, ne.name),
                    start: ne.index,
                    end: ne.index + ne.length,
                    color: ne.neType.color,
                    type: ne.neType.id
                });
            });
            res.sort(function (a, b) {
                return a.start - b.start;
            });
            return res;
        }

        function createEntities() {
            entities = neToEntities(scope.neList);
        }

        function redrawFromScratch() {
            udoc = new UDocumentNE(fullText, "pdf", entities);
            element.empty();
            element.append(udoc.htmlObj);
            udoc.flushHtml(10);
        }

        function clear() {
            udoc = null;
            entities = [];
            fullText = "";
            element.empty();
        }
    }
}

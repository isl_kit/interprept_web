angular
    .module("app.ne")
    .constant("NeViewMode", {
        SMALL: 0,
        LARGE: 1
    });

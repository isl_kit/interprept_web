angular.module("app.ne", [
    "app.core",
    "app.documents",
    "app.resources",
    "app.sessions"
]);
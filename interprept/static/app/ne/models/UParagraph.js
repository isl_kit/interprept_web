angular
    .module("app.ne")
    .factory("UParagraphNE", UParagraphNEModel);

/* @ngInject */
function UParagraphNEModel(UTermNE) {

    function UParagraph(text, entities, start, udoc) {
        this.entities = entities;
        this.terms = [];
        this.text = text;
        this.udoc = udoc;
        this.start = start;
        this.htmlObj = $("<p></p>");
        this.createHtml();
        this.needsRedraw = false;
    }

    UParagraph.prototype.addEntities = addEntities;
    UParagraph.prototype.removeEntities = removeEntities;
    UParagraph.prototype.createHtml = createHtml;

    return UParagraph;

    //////////

    function addEntities(entities) {
        this.entities = _.union(this.entities, entities);
        this.entities.sort(function (a, b) {
            return a.start - b.start;
        });
        this.needsRedraw = true;
    }

    function removeEntities(ids) {
        var remEntities = this.terms.filter(function (uterm) {
            return _.contains(ids, uterm.entity.id);
        }).map(function (uterm) {
            return uterm.entity;
        });
        if (remEntities.length === 0) {
            return;
        }
        this.entities = _.difference(this.entities, remEntities);
        this.needsRedraw = true;
    }

    function createHtml() {
        var lastIndex = 0,
            that = this;
         that.htmlObj.empty();
         that.term = [];
        _.each(this.entities, function (entity) {
            var text = that.text.substring(lastIndex), foundIdx, preString, term;
            foundIdx = findIt(text, entity.name, entity.start - that.start - lastIndex);
            //console.log(entity.name + "|||" + (entity.start - that.start - lastIndex) + " / " + foundIdx[0] + "/" + that.start + "/" + entity.start);
            if (foundIdx[0] === -1) {
                return;
            }
            lastIndex += foundIdx[1];
            preString = text.substring(0, foundIdx[0]);
            if (preString.length > 0) {
                that.htmlObj.append(document.createTextNode(preString));
            }
            //console.log(foundIdx[0] + "$$$" + foundIdx[1] + "$$$" + text);
            term = new UTermNE(text.substring(foundIdx[0], foundIdx[1]), entity.visible, that.udoc, that, entity);
            that.terms.push(term);
            that.htmlObj.append(term.htmlObj);
        });
        if (lastIndex < that.text.length) {
            that.htmlObj.append(document.createTextNode(that.text.substring(lastIndex)));
        }
        that.needsRedraw = false;
    }

    function getClosestIndex(text, sub, idx) {
        var res = -1, i = -1, dist = Infinity, d;
        while((i = text.indexOf(sub, i + 1)) >= 0) {
            d = Math.abs(i - idx);
            if (d < dist) {
                dist = d;
                res = i;
            } else {
                break;
            }
        }
        return res;
    }

    function findIt(text, sub, start) {
        var skipped = 0,
            normalizedSub = sub.replace(/\s+/, " "),
            normalizedStart,
            normalizedText = "",
            inSkip = false,
            mapRev = [],
            map = [],
            idx, idxEnd,
            i, max_i, ch;
        for (i = 0, max_i = text.length; i < max_i; i++) {
            ch = text[i];
            if (/\s/.test(ch)) {
                if (!inSkip) {
                    inSkip = true;
                } else {
                    skipped += 1;
                    mapRev.push(i - skipped);
                    continue;
                }
            } else {
                inSkip = false;
            }
            normalizedText += ch;
            mapRev.push(i - skipped);
            map.push(skipped);
        }
        if (start < 0) {
            start = 0;
        }
        normalizedStart = mapRev[start];
        idx = getClosestIndex(normalizedText, normalizedSub, normalizedStart);
        idxEnd = idx + normalizedSub.length;
        if (idx > -1) {
            idx += map[idx];
        }
        idxEnd += map[idxEnd > map.length - 1 ? map.length - 1 : idxEnd];
        return [idx, idxEnd];
    }
}

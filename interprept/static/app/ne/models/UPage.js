angular
    .module("app.ne")
    .factory("UPageNE", UPageNEModel);

/* @ngInject */
function UPageNEModel() {

    function UPage(num, max, start, udoc) {
        this.num = num + 1;
        this.max = max;
        this.start = start;
        this.paragraphs = [];
        this.udoc = udoc;
        this.htmlObj = $("<div class=\"upage\"></div>");
    }

    UPage.prototype.finish = finish;
    UPage.prototype.addParagraph = addParagraph;

    return UPage;

    //////////

    function finish(paragraph) {
        this.htmlObj.append("<div class=\"page-num\"><i class=\"fa fa-caret-up\"></i> " + this.num + " / " + this.max + " <i class=\"fa fa-caret-up\"></i></div>");
    }

    function addParagraph(paragraph) {
        this.paragraphs.push(paragraph);
        this.htmlObj.append(paragraph.htmlObj);
    }
}

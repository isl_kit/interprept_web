angular
    .module("app.ne")
    .factory("NE", NEModel);

/* @ngInject */
function NEModel(NEType) {

    function NE(data) {
        this.id = data.id;
        this.index = data.index;
        this.length = data.length;
        this.name = data.name;
        this.lowerName = data.name.toLowerCase();
        this.neType = data.neType;
        this.preText = data.preText || "";
        this.postText = data.postText || "";
    }
    NE.map = map;

    return NE;

    //////////

    function map(neDb) {
        return new NE({
            id: neDb.id,
            index: Number(neDb.start_index),
            length: Number(neDb.end_index) - Number(neDb.start_index),
            name: neDb.name,
            neType: NEType.map(neDb.ne_type)
        });
    }
}

angular
    .module("app.ne")
    .factory("NEType", NETypeModel);

/* @ngInject */
function NETypeModel($q) {

    var neTypeCache = {},
        neTypeDeferred = {},
        idToPos = {
            "1": 1,
            "2": 2,
            "3": 0,
            "4": 6,
            "5": 4,
            "6": 7,
            "7": 5,
            "8": 3,
            "9": 8,
            "10": 9,
            "11": 10,
            "12": 11,
            "13": 12
        };

    function NEType(data) {
        this.id = data.id;
        this.pos = idToPos[data.id];
        this.name = data.name || "";
        this.label = data.label || "";
        this.color = data.color || "#000000";
        this.style = {
            "background-color": this.color
        };
    }
    NEType.get = get;
    NEType.map = map;

    return NEType;

    //////////

    function get(id) {
        var deferred = $q.defer();
        if (neTypeCache[id]) {
            deferred.resolve(neTypeCache[id]);
        } else {
            if (!neTypeDeferred[id]) {
                neTypeDeferred[id] = [];
            }
            neTypeDeferred[id].push(deferred);
        }
        return deferred.promise;
    }

    function map(neTypeDb) {
        return new NEType({
            id: neTypeDb.id,
            name: neTypeDb.name,
            label: neTypeDb.label,
            color: neTypeDb.color
        });
    }
}

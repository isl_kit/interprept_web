angular
    .module("app.ne")
    .factory("UTermNE", UTermNEModel);

/* @ngInject */
function UTermNEModel() {

    function UTerm(text, visible, udoc, paragraph, entity) {
        this.text = text;
        this.paragraph = paragraph;
        this.entity = entity;
        this.visible = visible;
        this.uid = "neSide" + entity.id;
        this.htmlObj = this.createHtml();
        this.udoc = udoc;
        this.tmpId = null;

        if (!_.has(udoc.uterms, entity.id)) {
            udoc.uterms[entity.id] = [];
        }
        udoc.uterms[entity.id].push(this);
    }

    UTerm.prototype.setId = setId;
    UTerm.prototype.unsetId = unsetId;
    UTerm.prototype.setVisibility = setVisibility;
    UTerm.prototype.createHtml = createHtml;

    return UTerm;

    //////////

    function setId(id) {
        if (this.tmpId === null) {
            this.tmpId = this.entity.id;
        }
        this.entity.id = id;
    }

    function unsetId() {
        if (this.tmpId !== null) {
            this.entity.id = this.tmpId;
            this.tmpId = null;
        }
    }

    function setVisibility(visible) {
        if (visible === this.visible) {
            return;
        }
        this.visible = visible;
        if (this.visible) {
            this.htmlObj.addClass("visible");
        } else {
            this.htmlObj.removeClass("visible");
        }
    }

    function createHtml() {
        var htmlText = "";
        htmlText += "<span id=\"" + this.uid + "\" data-neid=\"" + this.entity.id + "\" class=\"entity ne";
        if (this.visible) {
            htmlText += " visible";
        }
        htmlText += "\"><span class=\"ne-color\" style=\"background-color:" + this.entity.color + "\"></span>";
        htmlText += this.text;
        htmlText += "</span>";
        return $(htmlText);
    }
}

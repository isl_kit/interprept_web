angular
    .module("app.ne")
    .config(neRoutes);

/* @ngInject */
function neRoutes($stateProvider) {
    $stateProvider.state("ne", {
        url: "/ne?doc_id?base_id",
        abstract: true,
        views: {
            "main-content": {
                controller: "NeSplitController",
                controllerAs: "vm",
                templateUrl: "ne/templates/ne-split.tpl.html"
            },
            "sidebar": {
                controller: "NeSidebarController",
                controllerAs: "vm",
                templateUrl: "ne/templates/ne-sidebar.tpl.html"
            }
        },
        resolve: {
            userDocument: /* @ngInject */ function (DocumentsDataService, $stateParams) {
                return DocumentsDataService.getDocs([$stateParams.doc_id]).then(function (documents) {
                    var userDocument = documents[0];
                    return userDocument;
                });
            },
            neDefinitions: /* @ngInject */ function (NeService) {
                return NeService.getNeDefinitions();
            }
        }
    });

    $stateProvider.state("ne.list", {
        url: "",
        views: {
            "ne-list": {
                controller: "NeListController",
                controllerAs: "vm",
                templateUrl: "ne/templates/ne-list.tpl.html"
            },
            "ne-context": {
                controller: "NeContextController",
                controllerAs: "vm",
                templateUrl: "ne/templates/ne-context.tpl.html"
            }
        },
        data: {
            pageTitle: "Named Entities"
        },
        resolve: {
            fullText: /* @ngInject */ function (ResourcesDataService, userDocument) {
                return ResourcesDataService.getFullText(userDocument.resource.id);
            },
            nes: /* @ngInject */ function (NeService, userDocument) {
                return NeService.getNes(userDocument.resource.id);
            }
        }
    });
}

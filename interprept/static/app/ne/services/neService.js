angular
    .module("app.ne")
    .factory("NeService", NeService);

/* @ngInject */
function NeService($http, NEType, NE) {

    var service = {
        getNes: getNes,
        getNeDefinitions: getNeDefinitions
    };

    return service;

    //////////

    function getNes(resourceId) {
        return $http.get("/resources/getNe/?resource_id=" + resourceId).then(function (response) {
            return response.data.nes.map(NE.map);
        });
    }

    function getNeDefinitions() {
        return $http.get("/resources/getNeDefinitions/").then(function (response) {
            return response.data.nes_def.map(NEType.map);
        });
    }
}

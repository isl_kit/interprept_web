angular
    .module("app.ne")
    .controller("NeSidebarController", NeSidebarController);

/* @ngInject */
function NeSidebarController($state, $stateParams, neDefinitions, userDocument, CustomizationService, FileType) {

    var vm = this;

    var selectedTypes = CustomizationService.get("ne.selected_types", {});

    vm.neDefinitions = neDefinitions.sort(neDefinitionSortingFunction).map(extendNeDefinition);
    vm.docItem = {
        name: userDocument.name,
        imgPrefix: userDocument.fileType === FileType.DOC ? "file" : "report"
    };
    vm.checkAllState = 0;
    vm.backToFileSpace = backToFileSpace;
    vm.toggleAll = toggleAll;
    vm.toggleNeDefinition = toggleNeDefinition;

    activate();

    //////////

    function activate() {
        updateCheckAllState();
    }

    function neDefinitionSortingFunction(a, b) {
        return a.pos - b.pos;
    }

    function extendNeDefinition(neDefinition) {
        return _.extend({ selected: selectedTypes[neDefinition.id] !== false }, neDefinition);
    }

    function backToFileSpace() {
        $state.go("documents.browser", { dir: $stateParams.base_id });
    }

    function updateCheckAllState() {
        var selectedDefs = vm.neDefinitions.filter(function (neDefinition) {
            return neDefinition.selected === true;
        });
        if (selectedDefs.length === vm.neDefinitions.length) {
            vm.checkAllState = 1;
        } else if (selectedDefs.length === 0) {
            vm.checkAllState = 0;
        } else {
            vm.checkAllState = 2;
        }
    }

    function saveSelectedNeTypes() {
        var result = {};
        _.each(vm.neDefinitions, function (neDefinition) {
            result[neDefinition.id] = neDefinition.selected;
        });
        CustomizationService.set({"ne.selected_types": result});
    }

    function toggleNeDefinition(neDefinition) {
        neDefinition.selected = !neDefinition.selected;
        updateCheckAllState();
        saveSelectedNeTypes();
    }

    function toggleAll() {
        if (vm.checkAllState === 0 || vm.checkAllState === 2) {
            vm.checkAllState = 1;
            _.each(vm.neDefinitions, function (neDefinition) {
                neDefinition.selected = true;
            });
        } else {
            vm.checkAllState = 0;
            _.each(vm.neDefinitions, function (neDefinition) {
                neDefinition.selected = false;
            });
        }
        saveSelectedNeTypes();
    }
}

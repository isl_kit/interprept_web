angular
    .module("app.ne")
    .controller("NeSplitController", NeSplitController);

/* @ngInject */
function NeSplitController($scope) {

    var vm = this;

    vm.isSplitted = false;
    vm.split = split;
    vm.close = close;

    //////////

    function split() {
        vm.isSplitted = true;
    }

    function close() {
        vm.isSplitted = false;
    }
}

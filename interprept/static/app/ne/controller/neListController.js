angular
    .module("app.ne")
    .controller("NeListController", NeListController);

/* @ngInject */
function NeListController($scope, $rootScope, NeViewMode, nes, fullText, TextExtractionService, CustomizationService) {

    var vm = this;

    var selectedTypes = CustomizationService.get("ne.selected_types", {}),
        extendedNes = nes.map(extendNe),
        filteredNes = extendedNes,
        pageSize = 50,
        sortString = "sortPos",
        sortRevert = false;

    vm.numberOfAllNes = nes.length;
    vm.numberOfFilteredNes = nes.length;
    vm.pageNes = extendedNes;

    vm.NeViewMode = NeViewMode;
    vm.viewMode = NeViewMode.SMALL;
    vm.changeViewMode = changeViewMode;

    vm.pageNumber = 0;
    vm.maxNumPages = 10;
    vm.numPages = 1;

    vm.sortOptions = [
        { name: "sortName", revert: false, label: "Name: ascending" },
        { name: "sortName", revert: true, label: "Name: descending" },
        { name: "sortType", revert: false, label: "Type: ascending" },
        { name: "sortType", revert: true, label: "Type: descending" },
        { name: "sortPos", revert: false, label: "Position: ascending" },
        { name: "sortPos", revert: true, label: "Position: descending" }
    ];
    vm.sortOption = {
        selected: vm.sortOptions[4]
    };

    vm.searchString = "";
    vm.focusedIdMain = "";
    vm.selectNeFromList = selectNeFromList;

    activate();

    //////////

    function activate() {
        $scope.$watch("vm.pageNumber", pageNumberWatcher);
        $scope.$watch("vm.sortOption.selected.label", sortOptionWatcher);
        $scope.$watch("vm.searchString", searchStringWatcher);
        CustomizationService.addListener("ne.selected_types", selectedTypesHandler);
        $scope.$onRootScope("ne.selectNeFromText", selectNeHandler);
        filter();
    }

    function extendNe(ne) {
        var extNe = _.extend({
            sortType: (10 + ne.neType.pos) + ne.lowerName,
            sortName: ne.lowerName,
            sortPos: ne.index
        }, ne), extRes;
        extRes = TextExtractionService.getNeighborhood(fullText, extNe.index, extNe.length, 80);
        extNe.preText = extRes.pre;
        extNe.postText = extRes.post;
        return extNe;
    }

    function getNeighborhood(ne, fullText) {
        var span = 80,
            whitespaceRegex = /\s/,
            postFix = "", preFix = "",
            fullLength = fullText.length,
            index = ne.index,
            length = ne.length,
            start = index - span,
            end = index + length + span;

        if (start < 0) {
            end -= start;
            start = 0;
        }
        if (end > fullLength - 1) {
            start -= (end - fullLength + 1);
            end = fullLength - 1;
        }

        while (start > 0 && !whitespaceRegex.test(fullText.charAt(start - 1))) {
            start--;
        }

        while (end < fullLength - 1 && !whitespaceRegex.test(fullText.charAt(end + 1))) {
            end++;
        }

        if (end < fullLength - 1) {
            postFix = "...";
        }
        if (start > 0) {
            preFix = "...";
        }
        ne.preText = preFix + fullText.substring(start, index);
        ne.postText = fullText.substring(index + length, end + 1) + postFix;
    }

    function changeViewMode(viewMode) {
        vm.viewMode = viewMode;
    }

    function updatePagination() {
        vm.numPages = Math.ceil(1.0 * filteredNes.length / pageSize) || 1;
        if (vm.pageNumber > vm.numPages - 1) {
            vm.pageNumber = vm.numPages - 1;
        }
    }

    function filterByPage() {
        vm.pageNes = filteredNes.slice(vm.pageNumber * pageSize, (vm.pageNumber + 1) * pageSize);
    }

    function pageNumberWatcher(newVal, oldVal) {
        if (oldVal === newVal) {
            return;
        }
        filterByPage();
    }

    function sortOptionWatcher(oldVal, newVal) {
        if (oldVal === newVal) {
            return;
        }
        sortString = vm.sortOption.selected.name;
        sortRevert = vm.sortOption.selected.revert;
        sort();
    }

    function sort() {
        filteredNes.sort(function (a, b) {
            var valA = a[sortString],
                valB = b[sortString];

            if (valA < valB) {
                return - 1;
            } else if (valB < valA) {
                return 1;
            }
            return 0;
        });
        if (sortRevert) {
            filteredNes.reverse();
        }
        filterByPage();
    }

    function searchStringWatcher(newVal, oldVal) {
        if (newVal === oldVal) {
            return;
        }
        $rootScope.$emit("ne.search", newVal);
        filter();
    }

    function selectedTypesHandler(types) {
        selectedTypes = types;
        filter();
    }

    function filter() {
        var lowerVal = vm.searchString.toLowerCase();
        filteredNes = extendedNes.filter(function (ne) {
            return selectedTypes[ne.neType.id] !== false && ne.lowerName.indexOf(lowerVal) > -1;
        });
        vm.numberOfFilteredNes = filteredNes.length;
        sort();
        filterByPage();
        updatePagination();
    }

    function getPageFromNeId(neId) {
        var i, max_i, index = -1;
        for (i = 0, max_i = filteredNes.length; i < max_i; i++) {
            if (filteredNes[i].id === neId) {
                index = i;
                break;
            }
        }
        if (index === -1) {
            return -1;
        }
        return Math.floor(index / pageSize);
    }

    function selectNeHandler(ev, neId) {
        var pageNum = getPageFromNeId(neId);
        if (pageNum > -1) {
            vm.pageNumber = pageNum;
            vm.focusedIdMain = "neMain" + neId;
        }
    }

    function selectNeFromList(ne) {
        $scope.$parent.vm.split();
        $rootScope.$emit("ne.selectNe", ne);
    }
}

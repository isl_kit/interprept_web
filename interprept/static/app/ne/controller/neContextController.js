angular
    .module("app.ne")
    .controller("NeContextController", NeContextController);

/* @ngInject */
function NeContextController($scope, $rootScope, nes, userDocument) {

    var vm = this;

    vm.ne = null;
    vm.nes = nes;
    vm.userDocument = userDocument;
    vm.selectedFocus = 0;
    vm.selectedUid = 0;
    vm.selectNeFromText = selectNeFromText;

    activate();

    //////////

    function activate() {
        $scope.$onRootScope("ne.selectNe", selectNeHandler);
    }

    function selectNeFromText(neId) {
        $rootScope.$emit("ne.selectNeFromText", neId);
    }

    function selectNeHandler(ev, ne) {
        vm.ne = ne;
        if (ne) {
            vm.selectedUid = "neSide" + ne.id;
        }
    }
}

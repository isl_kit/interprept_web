angular
    .module("app.feedback")
    .controller("FeedbackController", FeedbackController);

/* @ngInject */
function FeedbackController(FeedbackService, ModalService, ABOUT) {

    var vm = this;

    vm.feedback = {
        title: "",
        message: "",
        sendCopy: false
    };
    vm.inProgress = false;
    vm.sendFeedback = sendFeedback;
    vm.email = ABOUT.CONTACT.EMAIL;
    vm.people = ABOUT.CONTACT.PEOPLE.map(function (person) {
        return {
            name: person.NAME,
            phone: person.PHONE
        };
    });

    //////////

    function clearFeedbackForm() {
        vm.feedback.title = "";
        vm.feedback.message = "";
        vm.feedback.sendCopy = false;
    }

    function sendFeedback() {
        if (vm.inProgress === true) {
            return;
        }
        vm.inProgress = true;
        FeedbackService.sendFeedback(vm.feedback).then(function () {
            vm.inProgress = false;
            clearFeedbackForm();
            ModalService.open("feedbackSuccess");
        }, function () {
            vm.inProgress = false;
        });
    }
}

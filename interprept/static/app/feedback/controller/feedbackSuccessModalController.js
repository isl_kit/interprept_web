angular
    .module("app.feedback")
    .controller("FeedbackSuccessModalController", FeedbackSuccessModalController);

/* @ngInject */
function FeedbackSuccessModalController($scope, HistoryService) {

    var vm = this;

    vm.close = close;
    vm.back = back;

    //////////

    function close () {
        $scope.$$closeModal();
    }

    function back() {
        $scope.$$closeModal();
        HistoryService.back();
    }
}

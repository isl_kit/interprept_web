angular
    .module("app.feedback")
    .run(feedbackRun);

/* @ngInject */
function feedbackRun(ModalService) {
    ModalService.register({
        name: "feedbackSuccess",
        states: {
            normal: {
                templateUrl: "feedback/templates/feedback-success-modal.tpl.html",
                controller: "FeedbackSuccessModalController",
                controllerAs: "vm",
                params: []
            }
        }
    });
}

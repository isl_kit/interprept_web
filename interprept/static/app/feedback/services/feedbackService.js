angular
    .module("app.feedback")
    .factory("FeedbackService", FeedbackService);

/* @ngInject */
function FeedbackService($http, $sanitize) {

    var service = {
        sendFeedback: sendFeedback
    };

    return service;

    //////////

    function sendFeedback(feedback) {
        var feedbackPromise = $http.post("mail/feedback/", sanitizeFeedback(feedback));
        return feedbackPromise;
    }

    function sanitizeFeedback(feedback) {
        return {
            title: $sanitize(feedback.title),
            message: feedback.message,
            send_copy: feedback.sendCopy
        };
    }
}

angular
    .module("app.feedback")
    .config(feedbackRoutes);

/* @ngInject */
function feedbackRoutes($stateProvider) {
    $stateProvider.state("feedback", {
        url: "/feedback",
        views: {
            "main-content": {
                controller: "FeedbackController",
                controllerAs: "vm",
                templateUrl: "feedback/templates/feedback.tpl.html"
            }
        },
        data: {
            pageTitle: "Feedback",
            sidebarDisabled: true
        },
        resolve: {
        }
    });
}

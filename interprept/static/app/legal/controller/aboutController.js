angular
    .module("app.legal")
    .controller("AboutController", AboutController);

/* @ngInject */
function AboutController(ABOUT) {

    var vm = this,
        person = ABOUT.CONTACT.PEOPLE[0];

    vm.address = ABOUT.ADDRESS;
    vm.contact = [];

    activate();

    //////////

    function activate() {
        addContactInfo("Support", person.NAME);
        if (person.PHONE) {
            addContactInfo("Phone", person.PHONE);
        }
        if (person.FAX) {
            addContactInfo("Fax", person.FAX);
        }
        if (person.EMAIL) {
            addContactInfo("Email", person.EMAIL);
        }
    }

    function addContactInfo(title, text) {
        vm.contact.push({
            title: title,
            text: text
        });
    }
}

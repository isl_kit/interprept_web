angular
    .module("app.legal")
    .config(legalRoutes);

/* @ngInject */
function legalRoutes($stateProvider) {
    $stateProvider.state("about", {
        url: "/about",
        views: {
            "main-content": {
                controller: "AboutController",
                controllerAs: "vm",
                templateUrl: "legal/templates/about.tpl.html"
            }
        },
        data: {
            topBarContent: "Login",
            pageTitle: "About",
            sidebarDisabled: true
        }
    });
}

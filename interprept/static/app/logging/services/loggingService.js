angular
    .module("app.logging")
    .factory("LoggingService", LoggingService);

/* @ngInject */
function LoggingService($rootScope, $http, BrowserInfo, AuthenticationService) {

    var dereg = function () {},
        tempBuffer = [],
        buffer = [],
        browser, key;

    var service = {
        start: start,
        stop: stop
    };

    activate();

    return service;

    //////////

    function activate() {
        browser = BrowserInfo.getBrowserName() + BrowserInfo.getBrowserVersion() + BrowserInfo.getOperatingSystem() + BrowserInfo.getOsType() + BrowserInfo.getOsBits() + BrowserInfo.getBrowserLanguage();
        key = guid();
    }

    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    function guid() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    function pad(number) {
        if ( number < 10 ) {
            return '0' + number;
        }
        return number;
    }

    function toISOString(date) {
        return date.getUTCFullYear() +
            '-' + pad( date.getUTCMonth() + 1 ) +
            '-' + pad( date.getUTCDate() ) +
            'T' + pad( date.getUTCHours() ) +
            ':' + pad( date.getUTCMinutes() ) +
            ':' + pad( date.getUTCSeconds() ) +
            '.' + (date.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) +
            'Z';
    }

    function log(type, data) {
        buffer.push({
            type: type,
            data: data,
            time: toISOString(new Date())
        });
    }

    function flush() {
        tempBuffer = buffer.slice(0);
        buffer = [];
        if (tempBuffer.length > 0) {
            $http.post("/logging/write/", { messages: tempBuffer, uuid: key, browser: browser }).then(function (response) {
                if (!response.data.success) {
                    buffer = tempBuffer.concat(buffer);
                }
            });
        }
    }

    function stateChangeHandler(event, toState, toParams, fromState, fromParams) {
        log("stateChange", {
            state: toState.name,
            params: toParams
        });
        flush();
    }

    function getElementProps(el) {
        var attributes = {};
        _.each(el.attributes, function (attr) {
            attributes[attr.name] = attr.value;
        });
        return {
            name: el.tagName,
            attributes: attributes
        };
    }

    function clickHandler(event) {
        var el = getElementProps(event.target);
        log("click", {
            element: el,
            x: event.clientX || "unknown",
            y: event.clientY || "unknown",
            w: $(window).width(),
            h: $(window).height()
        });
    }

    function beforeUnloadHandler() {
        log("end", {});
        flush();
    }

    function LogoutHandler(email) {
        log("logout", {
            email: email
        });
        flush();
    }

    function start() {
        log("init", {
            browserName: BrowserInfo.getBrowserName(),
            browserVersion: BrowserInfo.getBrowserVersion(),
            os: BrowserInfo.getOperatingSystem(),
            osType: BrowserInfo.getOsType(),
            osBits: BrowserInfo.getOsBits(),
            language: BrowserInfo.getBrowserLanguage()
        });
        dereg = $rootScope.$on("$stateChangeSuccess", stateChangeHandler);
        $(document).click(clickHandler);
        $(window).on("beforeunload", beforeUnloadHandler);
        AuthenticationService.onLogout(LogoutHandler);
    }

    function stop() {
        dereg();
        $(document).off(clickHandler);
        $(window).off("beforeunload", beforeUnloadHandler);
    }
}

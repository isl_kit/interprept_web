angular
    .module("app.resources")
    .factory("ResourcesDataService", ResourcesDataService);

/* @ngInject */
function ResourcesDataService($http, Resource) {

    var service = {
        getById: getById,
        getFullText: getFullText,
        getWebdavContent: getWebdavContent
    };

    return service;

    //////////

    function getById(resourceId) {
        return $http.get("/resources/getResource/?resource_id=" + resourceId).then(function (response) {
            return Resource.map(response.data.resource);
        });
    }

    function getFullText(resourceId) {
        return $http.get("/resources/getText/?resource_id=" + resourceId).then(function (response) {
            return response.data.text;
        });
    }

    function getWebdavContent() {
        return $http.get("/resources/getWebdavContent/").then(function (response) {
            return {
                initialized: response.data.initialized,
                files: response.data.files.map(function (file) {
                    return {
                        name: file.name,
                        size: Number(file.size),
                        modified: new Date(Number(file.modified) * 1000)
                    };
                })
            };
        });
    }
}

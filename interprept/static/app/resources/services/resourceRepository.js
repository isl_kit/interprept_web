angular
    .module("app.resources")
    .factory("ResourceRepository", ResourceRepositoryService);

/* @ngInject */
function ResourceRepositoryService($rootScope, io, LanguageService) {

    var repository = {},
        changeLog = {};

    var service = {
        add: add,
        remove: remove
    };

    activate();

    return service;

    //////////

    function activate() {
        io.join("resources");
        io.on("message_resources", messageHandler);
    }

    function add(resource) {
       repository[resource.id] = resource;
    }

    function remove(resourceId) {
        delete repository[resourceId];
    }

    function messageHandler (message) {
        var type, resourceDb, log, timestamp;
        if (!message) {
            return;
        }
        type = message.type;
        $rootScope.$apply(function () {
            var res;
            if (type === "task_data_change") {
                resourceDb = message.data.resource;
                log = changeLog[resourceDb.id];
                timestamp = (new Date(resourceDb.time)).getTime();
                if (!log || log.timestamp < timestamp) {
                    res = {
                        timestamp: timestamp,
                        id: resourceDb.id,
                        taskName: resourceDb.task_name,
                        taskData: resourceDb.task_data
                    };
                    changeLog[resourceDb.id] = res;
                    if (resourceDb.task_name == "text" && resourceDb.extra.language) {
                        LanguageService.getByTag(resourceDb.extra.language).then(function (language) {
                            res.language = language;
                            $rootScope.$emit("resources.compStateChange", res);
                            updateResource(resourceDb.id);
                        });
                    } else {
                        $rootScope.$emit("resources.compStateChange", res);
                        updateResource(resourceDb.id);
                    }
                }
            }
        });
    }

    function updateResource(resourceId) {
        var resource = repository[resourceId],
            log = changeLog[resourceId];
        if (resource && log) {
            resource.taskStates[log.taskName] = log.taskData;
            if (log.taskName === "text") {
                resource.language = log.language;
            }
        }
    }
}

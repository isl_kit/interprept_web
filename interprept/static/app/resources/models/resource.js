angular
    .module("app.resources")
    .factory("Resource", ResourceModel);

/* @ngInject */
function ResourceModel($http, $q, ResourceRepository, Language) {

    function Resource(data) {
        this.id = data.id;
        this.name = data.name;
        this.size = data.size;
        this.fullText = "";
        this.mimetype = data.mimetype;
        this.hash = data.hash;
        this.language = data.language;
        this.taskStates = data.taskStates;
    }
    Resource.map = map;
    Resource.prototype.checkState = checkState;
    Resource.prototype.getFullText = getFullText;
    Resource.prototype.getReadableMimetype = getReadableMimetype;

    return Resource;

    //////////

    function map(resourceDb, speech) {
        var resource = new Resource({
            id: resourceDb.id,
            name: resourceDb.name,
            size: resourceDb.size,
            owner: resourceDb.owner,
            mimetype: resourceDb.mimetype,
            hash: resourceDb.hash,
            language: resourceDb.language ? Language.map(resourceDb.language): null,
            taskStates: resourceDb.task_states
        });
        ResourceRepository.add(resource);
        return resource;
    }

    function checkState(taskName, states) {
        return _.contains(states, this.taskStates[taskName].state);
    }

    function getFullText() {
        var that = this;
        if (that.fullText === "") {
            return $http.get("/resources/getText/?resource_id=" + that.id).then(function (response) {
                that.fullText = response.data.text;
                return response.data.text;
            });
        } else {
            return $q.when(that.fullText);
        }
    }

    function getReadableMimetype() {
        var subType;
        if (this.mimetype.indexOf("text/") === 0) {
            return "Text";
        } else {
            subType = this.mimetype.split("/")[1];
            return subType ? subType.toUpperCase() : "unknown";
        }
    }
}

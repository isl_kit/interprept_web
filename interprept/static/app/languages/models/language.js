angular
    .module("app.languages")
    .factory("Language", LanguageModel);

/* @ngInject */
function LanguageModel() {

    function Language(data) {
        this.id = data.id;
        this.name = data.name || "";
        this.tag = data.tag || "";
    }
    Language.map = map;

    return Language;

    //////////

    function map(languageDb) {
        var language = new Language({
            id: languageDb.id,
            name: languageDb.name,
            tag: languageDb.tag
        });
        return language;
    }
}

angular
    .module("app.languages")
    .factory("LanguageService", LanguageService);

/* @ngInject */
function LanguageService($http, $q, Language) {

    var languagesRepository = {},
        loaded = false;

    var service = {
        getAll: getAll,
        getById: getById,
        getByTag: getByTag
    };

    return service;

    //////////

    function getAll(forceReload) {
        var deferred = $q.defer();
        if (!loaded || forceReload) {
            $http.get("/languages/getAll/").then(function (response) {
                var result = response.data.languages.map(function (languageDb) {
                    var language = Language.map(languageDb);
                    languagesRepository[language.id] = language;
                    languagesRepository[language.tag] = language;
                    return language;
                });
                loaded = true;
                deferred.resolve(result);
            }, function () {
                deferred.reject("");
            });
        } else {
            deferred.resolve(_.map(languagesRepository, function (value) {
                return value;
            }));
        }
        return deferred.promise;
    }

    function getById(id, forceReload) {
        var deferred = $q.defer();
        if (!loaded || forceReload) {
            this.getAll(forceReload).then(function () {
                resolve(id, deferred);
            });
        } else {
            resolve(id, deferred);
        }
        return deferred.promise;
    }

    function getByTag(tag, forceReload) {
        var deferred = $q.defer();
        if (!loaded || forceReload) {
            this.getAll(forceReload).then(function () {
                resolve(tag, deferred);
            });
        } else {
            resolve(tag, deferred);
        }
        return deferred.promise;
    }

    function resolve(id_tag, deferred) {
        if (languagesRepository[id_tag] instanceof Language) {
            deferred.resolve(languagesRepository[id_tag]);
        } else {
            deferred.reject("language not found");
        }
    }
}

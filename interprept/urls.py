#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

from apps.socketioapi.conf import io_auth

from apps.app.views import index, angular_redirect, maintenance

def check_login_io(request, room):
    return request.user.is_authenticated()


urlpatterns = patterns('',
    url(r'^' + settings.SOCKETIO_PATH_PREFIX, include('apps.socketioapi.urls')),
    url(r'^admgmt/', include(admin.site.urls)),
    url(r'^maintenance/', maintenance, name="maintenance"),
    url(r'^auth/', include('apps.accounts.urls')),
    url(r'^speeches/', include('apps.speeches.urls')),
    url(r'^resources/', include('apps.resources.urls')),
    url(r'^documents/', include('apps.documents.urls')),
    url(r'^translation/', include('apps.translation.urls')),
    url(r'^downloads/', include('apps.downloads.urls')),
    url(r'^logging/', include('apps.logging.urls')),
    url(r'^files/', include('apps.files.urls')),
    url(r'^mail/', include('apps.mail.urls')),
    url(r'^languages/', include('apps.languages.urls')),
    url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^$', index, name="index"),
    url(r'^(?P<path>.*)/$', angular_redirect, name="angular_redirect"),

    # url(r'^.*$', RedirectView.as_view(url='/', permanent=False), name='index')
)

io_auth.set_data({
    "^.*$": check_login_io
})

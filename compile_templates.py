#!/usr/bin/env python

import re
from os import listdir, makedirs, environ
from os.path import abspath, dirname, join, isfile, exists
from shutil import rmtree
from functools import partial


PARAMS = ["AUSER", "CCRCY", "CS_HOST", "EXPORT_DIR", "IO_PORT", "JAVAPATH", "PGPW", "RBPW", "REPO_DIR", "REP_CS", "REP_WS", "TNXTPATH", "VHOME_CS", "VHOME_WS", "WEB_HOST", "WEB_PORT"]

prog = re.compile("|".join(["\{" + r + "\}" for r in PARAMS]))


def repl(params, match):
    name = match.group(0)[1:-1]
    return params.get(name, match.group(0))


def clean():
    repo_dir = dirname(abspath(__file__))
    template_dir = join(repo_dir, "templates")
    compile_dir = join(template_dir, "compile")
    if exists(compile_dir):
        rmtree(compile_dir)


def read_params():
    params = {}
    for name in PARAMS:
        value = environ.get(name)
        if value is None:
            raise Exception("Param " + name + " is not set!")
        params[name] = value
    return params


def compile_templates(params):
    repo_dir = dirname(abspath(__file__))
    template_dir = join(repo_dir, "templates")
    compile_dir = join(template_dir, "compile")
    if not exists(compile_dir):
        makedirs(compile_dir)
    for i in listdir(template_dir):
        file_path = join(template_dir, i)
        compiled_file_path = join(compile_dir, i)
        if not isfile(file_path):
            continue
        with open(file_path, "r") as f:
            content = f.read()
        compile_content = re.sub(prog, partial(repl, params), content)
        if i == "interprept.conf":
            if params["WEB_PORT"] == params["IO_PORT"]:
                compile_content = re.sub("#CUTOFF.*", "", compile_content, flags=re.DOTALL)
            else:
                compile_content = re.sub("#CUTOFF.*", "", compile_content)
        with open(compiled_file_path, "w+") as f2:
            f2.write(compile_content)


if __name__ == "__main__":
    clean()
    params = read_params()
    compile_templates(params)

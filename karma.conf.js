var path = require("path");


module.exports = function ( karma ) {
  karma.set({

    basePath: ".",

    files: [
        <!-- compiled CSS --><% _.forEach(karmaFiles, function(file) { %>
        "<%- file %>",<% }); %>
        "conf/**/*.js",
        "tpl/**/*.js",
        "app/**/*.module.js",
        "app/**/*.js",
        "test/**/*.js"
    ],
    exclude: [],
    frameworks: [ "jasmine" ],
    plugins: [ "karma-jasmine", "karma-phantomjs-launcher" ],
    preprocessors: {},

    reporters: "dots",

    port: 9101,
    runnerPort: 9111,
    urlRoot: "/",


    autoWatch: false,

    browsers: [
        "PhantomJS"
    ]
  });
};


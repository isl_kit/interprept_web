#!{VHOME_WS}/interprept/bin/python
# -*- coding: utf-8 -*-

import traceback
import site
import os
import requests
import json
from time import sleep

site.addsitedir('{VHOME_WS}/interprept/local/lib/python2.7/site-packages')
site.addsitedir('{REP_WS}/interprept')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.production")

from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings


open("/tmp/interprept_diag.out", "w+").close()


def write_to_file(text):
    with open("/tmp/interprept_diag.out", "a+") as f:
        f.write(text + "\n")


def log(name, status, details=None):
    if status == 1:
        print "\033[92mOK\033[0m:    %s" % (name,)
        write_to_file("OK:    %s" % (name,))
    else:
        print "\033[91mERROR\033[0m: %s" % (name,)
        write_to_file("ERROR: %s" % (name,))
        if details:
            write_to_file("  -->" + details)


def log_heading(text):
    print "----------\n" + text + "\n----------"
    write_to_file("----------\n" + text + "\n----------")


def test_web_server():
    try:
        r = requests.get(settings.WEB_URL)
        if r.status_code == 200:
            log("communication over %s" % (settings.WEB_URL,), 1)
        else:
            log("communication over %s" % (settings.WEB_URL,), 0)
    except:
        log("communication over %s" % (settings.WEB_URL,), 0, traceback.format_exc())


def test_socketio_internal():
    try:
        r = requests.post(settings.SOCKETIO_INT_URL + "/emit/", data=json.dumps({"appid": settings.SOCKET_APP_ID, "message": "hello"}, cls=DjangoJSONEncoder), headers={"content-type": "application/json"}, verify=False)
        if r.text == "everything is ok":
            log("internal communication over %s" % (settings.SOCKETIO_INT_URL,), 1)
        else:
            log("internal communication over %s" % (settings.SOCKETIO_INT_URL,), 0)
    except:
        log("internal communication over %s" % (settings.SOCKETIO_INT_URL,), 0, traceback.format_exc())



def test_socketio_external():
    try:
        r = requests.get(settings.SOCKETIO_EXT_URL + "/socket.io/?EIO=3&transport=polling&t=1456588834523-0")
        if r.status_code == 200:
            log("external communication over %s/socket.io" % (settings.SOCKETIO_EXT_URL,), 1)
        else:
            log("external communication over %s/socket.io" % (settings.SOCKETIO_EXT_URL,), 0)
    except:
        log("external communication over %s/socket.io" % (settings.SOCKETIO_EXT_URL,), 0, traceback.format_exc())


def test_database_access():
    try:
        from apps.resources.models import Resource
        if Resource.objects.count() > 0:
            log("database access", 1)
        else:
            log("database access", 1)
    except:
        log("database access", 0, traceback.format_exc())


ne_finished = False


def test_ne():
    from celeryrunner import runner
    runner.start("interprept.ne.tasks.create_ne_list", args=["Welcome to the year 2016", "en"], meta={}, callback=done_ne)


def done_ne(tid, result, meta, error):
    global ne_finished
    if error:
        log("Named Entity Tagging", 0, error)
    else:
        log("Named Entity Tagging", 1)
    ne_finished = True


te_finished = False


def test_te():
    from celeryrunner import runner
    runner.start("interprept.terminology.tasks.create_terminology_list", args=["Welcome to the year 2016", 1, "en"], meta={}, callback=done_te)


def done_te(tid, result, meta, error):
    global te_finished
    if error:
        log("Terminology Extraction", 0, error)
    else:
        log("Terminology Extraction", 1)
    te_finished = True


def test_iate():
    try:
        from apps.translation.translators.iate_trans import IateTranslator
        itr = IateTranslator()
        iate_result = itr.request("en", ["de"], "dog")
        if "Hund" in iate_result["de"]:
            log("IATE translations", 1)
        else:
            log("IATE translations", 0)
    except:
        log("IATE translations", 0, traceback.format_exc())

def test_glosbe():
    try:
        from apps.translation.translators.glosbe_trans import GlosbeTranslator
        gtr = GlosbeTranslator()
        glosbe_result = gtr.request("en", ["de"], ["dog"])
        if "Hund" in glosbe_result["dog"]["de"]:
            log("Glosbe translations", 1)
        else:
            log("Glosbe translations", 0)
    except:
        log("Glosbe translations", 0, traceback.format_exc())


def test_parliament():
    try:
        r = requests.get("http://www.europarl.europa.eu")
        if r.status_code == 200:
            log("Parliament webcrawl", 1)
        else:
            log("Parliament webcrawl", 0)
    except:
        log("Parliament webcrawl", 0, traceback.format_exc())


log_heading("TEST WEB SERVER")
test_web_server()

log_heading("TEST SOCKET IO SERVER")
test_socketio_internal()
test_socketio_external()

log_heading("TEST DATABASE CONNECTION")
test_database_access()

log_heading("TEST termNEXt")
test_ne()
counter = 0
ne_error = False
while not ne_finished:
    sleep(1)
    counter += 1
    if counter >= 600:
        ne_error = True
        break
if ne_error:
    log("Named Entity Tagging", 0, "!!TIMEOUT!! 10m")
test_te()
counter = 0
te_error = False
while not te_finished:
    sleep(1)
    counter += 1
    if counter >= 600:
        te_error = True
        break
if te_error:
    log("Terminology Extraction", 0, "!!TIMEOUT!! 10m")

log_heading("TEST ONLINE SERVICES")
test_iate()
test_glosbe()
test_parliament()

#!/usr/bin/env python
import os
import sys
import site

if __name__ == "__main__":

    site.addsitedir('{VHOME_WS}/interprept/local/lib/python2.7/site-packages')
    site.addsitedir('{REP_WS}/interprept')

    activate_env = os.path.expanduser("{VHOME_WS}/interprept/bin/activate_this.py")
    execfile(activate_env, dict(__file__=activate_env))


    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.production")

    from django.core.management import execute_from_command_line

    execute_from_command_line(["manage.py", "sync_webdav"])
